#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__


usage() {
   echo "build.sh <target>"
   echo " <target>    suite.clean, suite.build, suite.build-zip"
   echo ""
   echo ""
   echo "> The build.sh script creates the file:"
   echo "> EventMonitor/nbproject/private/platform-private.properties"
   echo "> from the following template: .platform-rgb.properties"
   echo "> There are the site specific platform settings"
   echo "> to be used instead of the definitions in "
   echo "> $HOME/.netbeans/<nbversion>/build.properties"
 
}

if [ $# -lt 1 ]; then
   usage
   exit 1
fi

ANT_HOME=/usr

#
#  PATH to the java development kit version 1.6
#
if [ "$JAVA_HOME" = "" ]; then
   JAVA_HOME=/usr
fi

if [ -f ./build.site ]; then
   . ./build.site
fi

if [ ! -f EventMonitor/nbproject/private/platform-private.properties ]; then
   mkdir -p EventMonitor/nbproject/private
   cp .platform-rgb.properties EventMonitor/nbproject/private/platform-private.properties
fi

export ANT_HOME JAVA_HOME

if [ $# -eq 1 ]; then
  target=$1
else
  echo "Illegal number of arguments"
  usage
  exit 1
fi

#
# copy needed libs
#
#cp -r /off_home/gridengine/sdm.libs/lib/* EventMonitor/hmacc-sdm-libs/release/modules/ext
#cp /off_home/gridengine/sge62u3.libs/* EventMonitor/Monitor-JGDI-LIBS/release/modules/ext

$ANT_HOME/bin/ant -f EventMonitor/build.xml $target

