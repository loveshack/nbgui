/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.monitor.cluster.ClusterOverview;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.ClusterOpenAction;
import com.sun.grid.sge.ui.sixtwoufive.navigator.actions.AscendingSortAction;
import com.sun.grid.sge.ui.sixtwoufive.navigator.actions.DescendingSortAction;
import javax.swing.Action;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Comparator;
import org.openide.ErrorManager;
import org.openide.actions.PropertiesAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;


public class ClusterPEViewNode extends AbstractNode implements PropertyChangeListener {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/cluster.png"; // NOI18N
    private static final String DISCONNECTED_ICON_PATH = "com/sun/grid/sge/ui/resources/cluster_disconnected.png";

    public ClusterPEViewNode(Cluster cluster, Comparator<PE> comparator) {
        super(Children.create(new ClusterPEChildFactory(cluster, comparator), true),
              Lookups.singleton(cluster));
        cluster.addPropertyChangeListener(WeakListeners.propertyChange(this, cluster));
    }

    private Cluster getCluster() {
        return getLookup().lookup(Cluster.class);
    }

    @Override
    public String getDisplayName() {
        Cluster cluster = getCluster();
        return cluster.getDisplayName();
    }

    @Override
    public Image getIcon(int type) {
        Cluster cluster = getCluster();
        if (cluster.isConnected()) {
            return ImageUtilities.loadImage(IMAGE_PATH);
        } else {
            return ImageUtilities.loadImage(DISCONNECTED_ICON_PATH);
        }
    }

    @Override
    public Image getOpenedIcon(int type) {
        Cluster cluster = getCluster();
        if (cluster.isConnected()) {
            return ImageUtilities.loadImage(IMAGE_PATH);
        } else {
            return ImageUtilities.loadImage(DISCONNECTED_ICON_PATH);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if ("config".equals(evt.getPropertyName())) {
            this.fireDisplayNameChange(null, getDisplayName());
        } else if (Cluster.PROPERTY_CLUSTER_CONNECTED.equals(evt.getPropertyName())) {
            this.fireOpenedIconChange();
            this.fireIconChange();
        }
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{
                  ClusterOpenAction.createInstance(),
                  null,
                  AscendingSortAction.getInstance(),
                  DescendingSortAction.getInstance(),
                  null,
                  SystemAction.get(PropertiesAction.class),
              };
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    public Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Cluster cluster = getCluster();
        ClusterOverview clusterOverview = (ClusterOverview) SGEFactory.getDefault().getModel(cluster);

        try {
            Property clusterName = new PropertySupport.Reflection(clusterOverview, String.class, "getClusterName", null);
            Property version = new PropertySupport.Reflection(clusterOverview, String.class, "getVersion", null);
            Property sgeRoot = new PropertySupport.Reflection(clusterOverview, String.class, "getSgeRoot", null);
            Property sgeCell = new PropertySupport.Reflection(clusterOverview, String.class, "getSgeCell", null);
            Property sgeAdmin = new PropertySupport.Reflection(clusterOverview, String.class, "getSgeAdmin", null);
            Property qmasterPort = new PropertySupport.Reflection(clusterOverview, Integer.class, "getQmasterPort", null);
            Property execdPort = new PropertySupport.Reflection(clusterOverview, Integer.class, "getExecdPort", null);
            Property jmxMasterHost = new PropertySupport.Reflection(clusterOverview, String.class, "getJmxMasterHost", null);
            Property jmxPort = new PropertySupport.Reflection(clusterOverview, Integer.class, "getJmxPort", null);

            clusterName.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_SGE_CLUSTER_NAME"));
            version.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_Version"));
            sgeRoot.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_SGE_ROOT"));
            sgeCell.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_SGE_CELL"));
            sgeAdmin.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_ADMIN_USER"));
            qmasterPort.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_SGE_QMASTER_PORT"));
            execdPort.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_SGE_EXECD_PORT"));
            jmxMasterHost.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_JMX_MASTER_HOST"));
            jmxPort.setName(NbBundle.getMessage(ClusterHostViewNode.class, "PROP_JMX_PORT"));

            set.put(clusterName);
            set.put(version);
            set.put(sgeRoot);
            set.put(sgeCell);
            set.put(sgeAdmin);
            set.put(qmasterPort);
            set.put(execdPort);
            set.put(jmxMasterHost);
            set.put(jmxPort);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;
    }

}
