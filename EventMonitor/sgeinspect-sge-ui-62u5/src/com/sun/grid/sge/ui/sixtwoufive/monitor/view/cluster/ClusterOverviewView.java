/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.cluster;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.ClusterQueueSummaryTableModel;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.HostSummaryTableModel;
import com.sun.grid.sge.ui.tables.PagingTablePanel;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.StatesTableModel;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterOverview;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import java.util.Collections;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

public class ClusterOverviewView extends DataSourceView implements DataRemovedListener<Cluster>, ChangeListener {

    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/overview.png"; // NOI18N
    private ClusterOverview clusterOverview;
    private ClusterQueueSummaryPanel queueSummaryPanel;
    private HostSummaryPanel hostSummaryPanel;
    private StatesSummaryPanel statesSummaryPanel;
    private Cluster cluster;

    public ClusterOverviewView(Cluster cluster) {
        super(cluster, NbBundle.getMessage(ClusterOverviewView.class, "LBL_Overview"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = cluster;
    }

    public void dataRemoved(Cluster cluster) {
        hostSummaryPanel.refresh(null);
        queueSummaryPanel.refresh(null);
        statesSummaryPanel.refresh(null);

        cluster.removeChangeListener(this);
    }

    @Override
    protected void willBeAdded() {
        clusterOverview = (ClusterOverview) SGEFactory.getDefault().getModel(cluster);
        cluster.addChangeListener(this);
    }

    @Override
    protected void removed() {
        hostSummaryPanel.refresh(null);
        queueSummaryPanel.refresh(null);
        statesSummaryPanel.refresh(null);

        cluster.removeChangeListener(this);
    }

    @Override
    protected DataViewComponent createComponent() {

        //Configuration of master view:
        dvc = new DataViewComponent(
                new SummaryPanel(cluster).getMasterView(), new DataViewComponent.MasterViewConfiguration(false));

        //Add detail views to the component:
        queueSummaryPanel = new ClusterQueueSummaryPanel(cluster);
        dvc.addDetailsView(queueSummaryPanel.getDetailsView(), DataViewComponent.TOP_LEFT);
        statesSummaryPanel = new StatesSummaryPanel(cluster);
        dvc.addDetailsView(statesSummaryPanel.getDetailsView(), DataViewComponent.TOP_LEFT);
        hostSummaryPanel = new HostSummaryPanel(cluster);
        dvc.addDetailsView(hostSummaryPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);
        cluster.notifyWhenRemoved(this);

        return dvc;
    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                hostSummaryPanel.refresh(cluster);
                queueSummaryPanel.refresh(cluster);
                statesSummaryPanel.refresh(cluster);
            }
        });
    }

    private static class SummaryPanel extends JPanel {

        public SummaryPanel(Cluster cluster) {
            initComponents(cluster);
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(ClusterOverviewView.class, "LBL_Overview"), null, this);   // NOI18N
        }

        private void initComponents(Cluster cluster) {
            setLayout(new BorderLayout());
            setOpaque(false);

            HTMLTextArea area = new HTMLTextArea("<nobr></nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));
            add(area, BorderLayout.CENTER);
        }
    }

    private static class ClusterQueueSummaryPanel {

        JTable queueSummary;
        ClusterQueueSummaryTableModel model;
        PagingTablePanel panel;

        public ClusterQueueSummaryPanel(Cluster cluster) {
            initComponents(cluster);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterOverviewView.class, "LBL_ClusterQueueSummary"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(Cluster cluster) {
            model = new ClusterQueueSummaryTableModel(cluster.getQueueList());
            panel = new PagingTablePanel(model);
        }

        public void refresh(Cluster cluster) {
            if (cluster != null) {
                model.runUpdate(cluster.getQueueList());
            } else {
                model.runUpdate(Collections.<Queue>emptyList());
            }
        }
    }

    private static class StatesSummaryPanel {

        StatesTableModel model;
        PagingTablePanel panel;

        public StatesSummaryPanel(Cluster cluster) {
            initComponents(cluster);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterOverviewView.class, "LBL_States"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(Cluster cluster) {
            model = new StatesTableModel(cluster.getQueueList());
            panel = new PagingTablePanel(model);
        }

        public void refresh(Cluster cluster) {
            if (cluster != null) {
                model.runUpdate(cluster.getQueueList());
            } else {
                model.runUpdate(Collections.<Queue>emptyList());
            }
        }
    }

    private static class HostSummaryPanel {

        HostSummaryTableModel model;
        PagingTablePanel panel;

        public HostSummaryPanel(Cluster cluster) {
            initComponents(cluster);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterOverviewView.class, "LBL_HostSummary"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(Cluster cluster) {
            model = new HostSummaryTableModel(cluster.getHostList());
            panel = new PagingTablePanel(model);
        }

        public void refresh(Cluster cluster) {
            if (cluster != null) {
                model.runUpdate(cluster.getHostList());
            } else {
                model.runUpdate(Collections.<Host>emptyList());
            }
        }
    }
}
