/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

//This node is used when view in Navigator is set to 'Host View'
public class HostChildFactory extends ChildFactory<Queue> {

    private Host host;
    private final QueueDataListener queueListener = new QueueDataListener();

    public HostChildFactory(Host host, Cluster cluster) {
        this.host = host;
        cluster.getRepository().addDataChangeListener(queueListener, Queue.class);
    }

    @Override
    protected synchronized boolean createKeys(List<Queue> keys) {
        List<Queue> queues = new LinkedList<Queue>(host.getQueueList());
        for (Queue queue : queues) {
            keys.add(queue);
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(Queue queue) {
        return new QueueLeafNode(queue);
    }

    private class QueueDataListener implements DataChangeListener<Queue> {

        public void dataChanged(DataChangeEvent<Queue> evt) {
            refresh(true);
        }
    }
}
