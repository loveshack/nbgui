/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator.actions;

import com.sun.grid.sge.ui.sixtwoufive.navigator.NavigatorSupport;
import com.sun.grid.sge.ui.sixtwoufive.navigator.buttons.ButtonPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public class AscendingSortAction extends AbstractAction implements Presenter.Popup {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/sort_ascending.png";
    private static final String LABEL = NbBundle.getMessage(AscendingSortAction.class, "LBL_Sort_Asc");
    private static AscendingSortAction instance;
    JRadioButtonMenuItem ascending;
//    private NavigatorTopComponent navig;
    public AscendingSortAction(String text, Icon icon) {
        super(text, icon);
        ascending = new JRadioButtonMenuItem(text, icon);
//        this.navig = NavigatorTopComponent.findInstance();
    }

    public void actionPerformed(ActionEvent e) {
        boolean selected = true;
        if (e.getSource() instanceof JToggleButton) {
            selected = ((JToggleButton) e.getSource()).isSelected();
        } else if (e.getSource() instanceof JMenuItem) {
            selected = ((JMenuItem) e.getSource()).isSelected();
        }
//        HostSortAscendingComparator comp = (HostSortAscendingComparator) item.getClientProperty(SORT);
        ButtonPanel.btnSortDesc.setSelected(!selected);
        ButtonPanel.btnSortAsc.setSelected(selected);
//        ButtonPanel.btnSortAsc.setsetEnabled(!selected);
        DescendingSortAction.getInstance().descending.setSelected(!selected);
        ascending.setSelected(selected);

        NavigatorSupport.setSort(NavigatorSupport.SORT_ASC);
    }

    public JMenuItem getPopupPresenter() {
//        JRadioButtonMenuItem ascending = new JRadioButtonMenuItem(NbBundle.getMessage(FilterByHostTypeAction.class, "LBL_Sort_Asc"));
//        ascending.putClientProperty(SORT, new HostSortAscendingComparator());
        ascending.addActionListener(this);
        return ascending;
    }

    public static synchronized AscendingSortAction getInstance() {
        if (instance == null) {
            instance = new AscendingSortAction(LABEL, ImageUtilities.image2Icon(ImageUtilities.loadImage(IMAGE_PATH)));
        }
        return instance;
    }
}
