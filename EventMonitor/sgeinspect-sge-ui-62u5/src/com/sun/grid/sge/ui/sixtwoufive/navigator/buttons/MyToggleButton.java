/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator.buttons;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JToggleButton;

public class MyToggleButton extends JToggleButton {

    MyToggleButton() {
        initButton();
    }

    MyToggleButton(Action action) {
        super(action);
        initButton();
    }

    private void initButton() {
        setContentAreaFilled(false);
        setMaximumSize(new java.awt.Dimension(24, 24));
        setMinimumSize(new java.awt.Dimension(24, 24));
        setPreferredSize(new java.awt.Dimension(24, 24));
        setRolloverEnabled(true);
        addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                myButtonMouseEntered(evt);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                myButtonMouseExited(evt);
            }
        });
    }

    @Override
    public void setIcon(Icon defaultIcon) {
        super.setIcon(defaultIcon);
        setSelectedIcon(defaultIcon);
    }

    private void myButtonMouseEntered(java.awt.event.MouseEvent evt) {
        if (!(isSelected())) {
            setContentAreaFilled(true);
        }
    }

    private void myButtonMouseExited(java.awt.event.MouseEvent evt) {
        if (!(isSelected())) {
            setContentAreaFilled(false);
        }
    }

    public void setSelected(boolean b) {
        super.setSelected(b);
        setContentAreaFilled(b);
    }
}
