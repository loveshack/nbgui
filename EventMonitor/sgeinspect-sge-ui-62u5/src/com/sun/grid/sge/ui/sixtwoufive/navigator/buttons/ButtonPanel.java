/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator.buttons;

import com.sun.grid.sge.api.monitor.host.HostArchFilter;
import com.sun.grid.sge.ui.sixtwoufive.navigator.actions.AscendingSortAction;
import com.sun.grid.sge.ui.sixtwoufive.navigator.actions.DescendingSortAction;
import com.sun.grid.sge.ui.sixtwoufive.navigator.actions.FilterByArchAction;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;

public class ButtonPanel extends JPanel {

    private static final String ARCH_FILTER_IMAGE_PATH = "com/sun/grid/sge/ui/resources/arch_filter.png";
    public static MyToggleButton btnSortAsc,  btnSortDesc,  btnSortProcessors,  btnArchFilter;
    ButtonGroup sortGroup;
    ButtonGroup filterGroup;

    public ButtonPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        initComponents();
    }

    private void initComponents() {
        sortGroup = new ButtonGroup();
        filterGroup = new ButtonGroup();

        btnSortAsc = new MyToggleButton(AscendingSortAction.getInstance());
        btnSortAsc.setToolTipText(btnSortAsc.getText());
        btnSortAsc.setText("");
        btnSortAsc.setFocusPainted(false);
        sortGroup.add(btnSortAsc);

        btnSortDesc = new MyToggleButton(DescendingSortAction.getInstance());
        btnSortDesc.setToolTipText(btnSortDesc.getText());
        btnSortDesc.setText("");
        btnSortDesc.setFocusPainted(false);
        sortGroup.add(btnSortDesc);

//        btnSortProcessors = new MyToggleButton();        
//        btnSortProcessors.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sge/ui/resources/sort_right.png"))); // NOI18N
//        btnSortProcessors.setToolTipText(org.openide.util.NbBundle.getMessage(ButtonPanel.class, "TT_Sort_By_Processors")); // NOI18N
//        sortGroup.add(btnSortProcessors);

        btnArchFilter = new MyToggleButton();
        btnArchFilter.setIcon(ImageUtilities.image2Icon(ImageUtilities.loadImage(ARCH_FILTER_IMAGE_PATH)));
        btnArchFilter.setToolTipText(org.openide.util.NbBundle.getMessage(ButtonPanel.class, "TT_Filter_By_Host_Type"));
        btnArchFilter.setFocusPainted(false);
        btnArchFilter.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JMenu menu = (JMenu) FilterByArchAction.getInstance().getPopupPresenter();
                menu.getPopupMenu().show(btnArchFilter, 1, btnArchFilter.getHeight());
                HostArchFilter.setActive(true);
            }
        });

        createBox();

    }

    private void createBox() {
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(btnArchFilter);
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(btnSortAsc);
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(btnSortDesc);
//        this.add(Box.createRigidArea(new Dimension(5, 0)));
//        this.add(btnSortProcessors);
        this.add(Box.createGlue());
    }
}
