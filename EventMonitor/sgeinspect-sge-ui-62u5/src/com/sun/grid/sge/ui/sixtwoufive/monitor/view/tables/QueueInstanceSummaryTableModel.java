/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueInstanceOverview;
import com.sun.grid.sge.api.monitor.util.NumberFormater;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import org.openide.util.NbBundle;

public class QueueInstanceSummaryTableModel extends PagingTableModel<QueueInstance> {

    private Class columnClass[] = {String.class, String.class, String.class,
        String.class, String.class, String.class
    };
    public final String[] COLUMNS = {
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "Name"),
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "Type"),
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "Slot_Usage"),
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "Load_Average"),
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "System"),
        NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "State")
    };
    private final DecimalFormat form = new DecimalFormat();

    public QueueInstanceSummaryTableModel(List<QueueInstance> queueInstances) {
        super(queueInstances);
        form.setMaximumFractionDigits(2);
        form.setRoundingMode(RoundingMode.HALF_UP);
    }

    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

    public Class getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public synchronized Object getValueAt(int columnIndex, QueueInstance source) {
        QueueInstanceOverview summary = (QueueInstanceOverview) SGEFactory.getDefault().getModel(source);

        switch (columnIndex) {
            case 0:
                return source.getQueueName();
            case 1:
                return summary.getQueueType();
            case 2:
                return NumberFormater.format(form, summary.getSlotUsage());
            case 3:
                return NumberFormater.format(form, summary.getLoadAvg());
            case 4:
                return summary.getArch();
            case 5:
                return summary.getState();
            default:
                return NbBundle.getMessage(QueueInstanceSummaryTableModel.class, "Error");
        }
    }
}
