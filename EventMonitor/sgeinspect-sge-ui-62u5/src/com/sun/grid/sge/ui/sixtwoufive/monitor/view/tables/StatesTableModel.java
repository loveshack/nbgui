/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueOverview;
import java.util.List;
import org.openide.util.NbBundle;

public class StatesTableModel extends PagingTableModel<Queue> {
    private Class columnClass [] = {String.class, Number.class, Number.class, Number.class, Number.class, 
             Number.class, Number.class, Number.class, Number.class, Number.class, Number.class };

    public final String[] COLUMNS = {
        NbBundle.getMessage(StatesTableModel.class, "Cluster_Queue"),
        NbBundle.getMessage(StatesTableModel.class, "Suspend_Manual"),
        NbBundle.getMessage(StatesTableModel.class, "Suspend_Threshold"),
        NbBundle.getMessage(StatesTableModel.class, "Suspend_by_Calendar"),
        NbBundle.getMessage(StatesTableModel.class, "Alarm_Load"),
        NbBundle.getMessage(StatesTableModel.class, "Disabled_Manual"),
        NbBundle.getMessage(StatesTableModel.class, "Disabled_by_Calendar"),
        NbBundle.getMessage(StatesTableModel.class, "Error"),
        NbBundle.getMessage(StatesTableModel.class, "Orphaned"),
        NbBundle.getMessage(StatesTableModel.class, "Configuration_Ambigous"),
        NbBundle.getMessage(StatesTableModel.class, "Unknown_Qinstance_Host")
    };

    public StatesTableModel(List<Queue> queues) {
        super(queues);
    }

    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public synchronized Object getValueAt(int columnIndex, Queue source) {
        QueueOverview summary = (QueueOverview)SGEFactory.getDefault().getModel(source);

        switch (columnIndex) {
            case 0:
                return source.getQueueName();
            case 1:
                return summary.getSuspendManual();
            case 2:
                return summary.getSuspendThreshold();
            case 3:
                return summary.getSuspendByCalendar();
            case 4:
                return summary.getLoadAlarm();
            case 5:
                return summary.getDisabledManual();
            case 6:
                return summary.getDisabledByCalendar();
            case 7:
                return summary.getError();
            case 8:
                return summary.getOrphaned();
            case 9:
                return summary.getAmbiguous();
            case 10:
                return summary.getUnknown();
            default:
                return NbBundle.getMessage(StatesTableModel.class, "Error");
        }
    }
}
