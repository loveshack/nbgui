/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.queue;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.ui.tables.PagingTablePanel;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.QueueInstanceSummaryTableModel;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueOverview;
import com.sun.grid.sge.ui.charts.ChartSupport;
import com.sun.grid.sge.ui.charts.SlotPanelSupport;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.host.HostOverviewView;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import java.util.Collections;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

public class QueueMonitorView extends DataSourceView implements DataRemovedListener<Queue>, ChangeListener {

    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/monitor.png"; // NOI18N
    private QueueOverview queueOverview;
    private SlotsPanel slotsPanel;
    private QueueInstanceSummaryPanel qInstancePanel;
    private SlotChartPanel slotChartPanel;
    private final Cluster cluster;

    public QueueMonitorView(Queue queue) {
        super(queue, NbBundle.getMessage(QueueMonitorView.class, "LBL_Monitor"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = (Cluster) getDataSource().getOwner();
    }

    public void dataRemoved(Queue queue) {
        if (cluster != null) {
            qInstancePanel.refresh(null);
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void removed() {
        if (cluster != null) {
            qInstancePanel.refresh(null);
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void willBeAdded() {
        queueOverview = (QueueOverview) SGEFactory.getDefault().getModel(getDataSource());
        if (cluster != null) {
            cluster.addChangeListener(this);
        }
    }

    @Override
    protected DataViewComponent createComponent() {

        //Configuration of master view:
        dvc = new DataViewComponent(
                new OverviewPanel((Queue) getDataSource()).getMasterView(), new DataViewComponent.MasterViewConfiguration(false));

        //Add detail views to the component:
        slotsPanel = new SlotsPanel(queueOverview);
        dvc.addDetailsView(slotsPanel.getDetailsView(), DataViewComponent.TOP_LEFT);

        slotChartPanel = new SlotChartPanel(queueOverview);
        dvc.addDetailsView(slotChartPanel.getDetailsView(), DataViewComponent.TOP_RIGHT);

        qInstancePanel = new QueueInstanceSummaryPanel((Queue) getDataSource());
        dvc.addDetailsView(qInstancePanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);

        (getDataSource()).notifyWhenRemoved(this);

        return dvc;

    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                Queue queue = (Queue) getDataSource();
                if (queue != null && !queue.isRemoved()) {
                    final long time = System.currentTimeMillis();
                    slotsPanel.refresh(queueOverview);
                    qInstancePanel.refresh(queue);
                    slotChartPanel.refresh(queueOverview, time);
                } else {
                    qInstancePanel.refresh(null);
                }
            }
        });
    }

    private static class OverviewPanel extends JPanel {

        public OverviewPanel(Queue queue) {
            initComponents(queue);
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(HostOverviewView.class, "LBL_Monitor"), null, this);   // NOI18N
        }

        private void initComponents(Queue queue) {
            setLayout(new BorderLayout());
            setOpaque(false);

            HTMLTextArea area = new HTMLTextArea("<nobr></nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }
    }

    private static class SlotsPanel extends JPanel {

        HTMLTextArea area;

        public SlotsPanel(QueueOverview queueOverview) {
            initComponents(queueOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueMonitorView.class, "LBL_Slots"), null, 10, this, null);    // NOI18N
        }

        private void initComponents(QueueOverview queueOverview) {
            setLayout(new BorderLayout());
            setOpaque(false);

            area = new HTMLTextArea("<nobr>" + getSlotsOverviewInfo(queueOverview) + "</nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }

        String getSlotsOverviewInfo(QueueOverview overview) {
            StringBuilder data = new StringBuilder();

            String used = NbBundle.getMessage(QueueMonitorView.class, "LBL_Used_Slots"); // NOI18N
            data.append("<b>" + used + ":</b> " + overview.getUsedSlots() + "<br>"); // NOI18N
            String available = NbBundle.getMessage(QueueMonitorView.class, "LBL_Available_Slots"); // NOI18N
            data.append("<b>" + available + ":</b> " + overview.getAvailableSlots() + "<br>"); // NOI18N
            String reserved = NbBundle.getMessage(QueueMonitorView.class, "LBL_Reserved_Slots"); // NOI18N
            data.append("<b>" + reserved + ":</b> " + overview.getReservedSlots() + "<br>"); // NOI18N
            String total = NbBundle.getMessage(QueueMonitorView.class, "LBL_Total_Slots"); // NOI18N
            data.append("<b>" + total + ":</b> " + overview.getTotalSlots() + "<br>"); // NOI18N

            return data.toString();

        }

        public void refresh(QueueOverview queueOverview) {
            area.setText("<nobr>" + getSlotsOverviewInfo(queueOverview) + "</nobr>");
        }
    }

    private static class QueueInstanceSummaryPanel {

        QueueInstanceSummaryTableModel model;
        PagingTablePanel panel;

        public QueueInstanceSummaryPanel(Queue queue) {
            initComponents(queue);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueMonitorView.class, "LBL_Queue_Instance_Summary"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(Queue queue) {
            model = new QueueInstanceSummaryTableModel(queue.getQueueInstances());
            panel = new PagingTablePanel(model);
        }

        public void refresh(Queue queue) {
            if (queue != null) {
                model.runUpdate(queue.getQueueInstances());
            } else {
                model.runUpdate(Collections.<QueueInstance>emptyList());
            }
        }
    }

    // --- Slot Usage ------------------------------------------------------------
    private static class SlotChartPanel {

        ChartSupport.SlotChart slotChart;
        SlotPanelSupport panel;

        public SlotChartPanel(QueueOverview overview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueMonitorView.class, "LBL_Slots"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(QueueOverview overview, long time) {
            double used = overview.getUsedSlots();
            double total = overview.getTotalSlots();
            addNewSlotValue(used, total, time);
        }

        private void initComponents() {
            slotChart = new ChartSupport.SlotChart();
            panel = slotChart.getChartPanel();
        }

        private void addNewSlotValue(double used, double total, long time) {
            slotChart.addNewValue(new double[]{used, total}, time);
            panel.setSlots(used, total);
        }
    }
}
