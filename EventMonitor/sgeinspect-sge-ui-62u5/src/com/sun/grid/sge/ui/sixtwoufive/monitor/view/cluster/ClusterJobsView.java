/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.cluster;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterOverview;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.ClusterJobsTableModel;
import com.sun.grid.sge.ui.tables.PagingTablePanel;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ClusterJobsView extends DataSourceView implements DataRemovedListener<Cluster>, ChangeListener {

    private static final Logger logger = Logger.getLogger(ClusterJobsView.class.getName());
    
    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/jobs.png"; // NOI18N
    private ClusterOverview clusterOverview;
    private JobsPanel jobsPanel;
    private RunningJobsPanel runningJobsPanel;
    private PendingJobsPanel pendingJobsPanel;
    private FinishedJobsPanel finishedJobsPanel;
    private Cluster cluster;

    public ClusterJobsView(Cluster cluster) {
        super(cluster, NbBundle.getMessage(ClusterJobsView.class, "LBL_Jobs"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = cluster;
    }

    public void dataRemoved(Cluster cluster) {
        cluster.removeChangeListener(this);
    }

    @Override
    protected void willBeAdded() {
        clusterOverview = (ClusterOverview) SGEFactory.getDefault().getModel(cluster);
        cluster.addChangeListener(this);
    }

    @Override
    protected void removed() {
        cluster.removeChangeListener(this);
    }

    @Override
    protected DataViewComponent createComponent() {

        jobsPanel = new JobsPanel(cluster);
        //Configuration of master view:
        dvc = new DataViewComponent(
                jobsPanel.getMasterView(), new DataViewComponent.MasterViewConfiguration(false));

        //Add detail views to the component:
        runningJobsPanel = new RunningJobsPanel(clusterOverview);
        dvc.addDetailsView(runningJobsPanel.getDetailsView(), DataViewComponent.TOP_LEFT);
        pendingJobsPanel = new PendingJobsPanel(clusterOverview);
        dvc.addDetailsView(pendingJobsPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);
        finishedJobsPanel = new FinishedJobsPanel(clusterOverview);
        dvc.addDetailsView(finishedJobsPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);
        cluster.notifyWhenRemoved(this);

        return dvc;
    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                runningJobsPanel.refresh(clusterOverview);
                pendingJobsPanel.refresh(clusterOverview);
                finishedJobsPanel.refresh(clusterOverview);
            }
        });
    }

    private static class JobsPanel extends JPanel {

        public JobsPanel(Cluster cluster) {
            initComponents(cluster);
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(ClusterJobsView.class, "LBL_Jobs"), null, this);   // NOI18N
        }

        private void initComponents(Cluster cluster) {
            setLayout(new BorderLayout());
            setOpaque(false);

            HTMLTextArea area = new HTMLTextArea("<nobr></nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }
    }

    private static class RunningJobsPanel {

        ClusterJobsTableModel model;
        PagingTablePanel panel;

        public RunningJobsPanel(ClusterOverview clusterOverview) {
            initComponents(clusterOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterJobsView.class, "LBL_Running_Jobs"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(ClusterOverview clusterOverview) {
            model = new ClusterJobsTableModel(clusterOverview.getRunningJobs());
            panel = new PagingTablePanel(model);
        }

        public void refresh(ClusterOverview clusterOverview) {
            logger.log(Level.FINER, "ClusterJobsView - RunningJobsPanel: " + clusterOverview.getRunningJobs().size());
            model.runUpdate(clusterOverview.getRunningJobs());
        }
    }

    private static class PendingJobsPanel {

        ClusterJobsTableModel model;
        PagingTablePanel panel;

        public PendingJobsPanel(ClusterOverview clusterOverview) {
            initComponents(clusterOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterJobsView.class, "LBL_Pending_Jobs"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(ClusterOverview clusterOverview) {
            model = new ClusterJobsTableModel(clusterOverview.getPendingJobs());
            panel = new PagingTablePanel(model);
        }

        public void refresh(ClusterOverview clusterOverview) {
            logger.log(Level.FINER, "ClusterJobsView - PendingJobsPanel: " + clusterOverview.getPendingJobs().size());
            model.runUpdate(clusterOverview.getPendingJobs());
        }
    }

    private static class FinishedJobsPanel {

        ClusterJobsTableModel model;
        PagingTablePanel panel;

        public FinishedJobsPanel(ClusterOverview clusterOverview) {
            initComponents(clusterOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(ClusterJobsView.class, "LBL_Finished_Jobs"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(ClusterOverview clusterOverview) {
            model = new ClusterJobsTableModel(clusterOverview.getZombieJobs());
            panel = new PagingTablePanel(model);
        }

        public void refresh(ClusterOverview clusterOverview) {
            logger.log(Level.FINER, "ClusterJobsView - FinishedJobsPanel: " + clusterOverview.getZombieJobs().size());
            model.runUpdate(clusterOverview.getZombieJobs());
        }
    }
}
