/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.host;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostOverview;
import com.sun.grid.sge.ui.charts.LoadPanelSupport;
import com.sun.grid.sge.ui.charts.ChartSupport;
import com.sun.grid.sge.ui.charts.MemoryPanelSupport;
import com.sun.grid.sge.ui.charts.SlotPanelSupport;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

public class HostMonitorView extends DataSourceView implements DataRemovedListener<Host>, ChangeListener {

    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/monitor.png"; // NOI18N
    private HostOverview hostOverview;
    private CpuLoadChartPanel cpuChartPanel;
    private MemoryChartPanel memoryChartPanel;
    private SwapChartPanel swapChartPanel;
    private SlotChartPanel slotChartPanel;
    private Cluster cluster;

    public HostMonitorView(Host host) {
        super(host, NbBundle.getMessage(HostMonitorView.class, "LBL_Monitor"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = (Cluster) getDataSource().getOwner();
    }

    public void dataRemoved(Host host) {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void removed() {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void willBeAdded() {
        hostOverview = (HostOverview) SGEFactory.getDefault().getModel(getDataSource());
        if (cluster != null) {
            cluster.addChangeListener(this);
        }
    }

    @Override
    protected DataViewComponent createComponent() {

        //Add the master view and configuration view to the component:
        dvc = new DataViewComponent(new MasterViewSupport().getMasterView(),
                new DataViewComponent.MasterViewConfiguration(false));

        cpuChartPanel = new CpuLoadChartPanel(hostOverview);
        dvc.addDetailsView(cpuChartPanel.getDetailsView(), DataViewComponent.TOP_LEFT);

        memoryChartPanel = new MemoryChartPanel(hostOverview);
        dvc.addDetailsView(memoryChartPanel.getDetailsView(), DataViewComponent.TOP_RIGHT);

        swapChartPanel = new SwapChartPanel(hostOverview);
        dvc.addDetailsView(swapChartPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);

        slotChartPanel = new SlotChartPanel(hostOverview);
        dvc.addDetailsView(slotChartPanel.getDetailsView(), DataViewComponent.BOTTOM_RIGHT);

        (getDataSource()).notifyWhenRemoved(this);

        return dvc;
    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                final long time = System.currentTimeMillis();
                cpuChartPanel.refresh(hostOverview, time);
                memoryChartPanel.refresh(hostOverview, time);
                swapChartPanel.refresh(hostOverview, time);
                slotChartPanel.refresh(hostOverview, time);
            }
        });
    }
    // --- General data --------------------------------------------------------

    private static class MasterViewSupport extends JPanel {

        public MasterViewSupport() {
            initComponents();
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(HostMonitorView.class, "LBL_Monitor"), null, this);
        }

        private void initComponents() {
            setLayout(new BorderLayout());
            setOpaque(false);
            HTMLTextArea area = new HTMLTextArea();
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));
            add(area, BorderLayout.CENTER);
        }
    }
    // --- CPU load ------------------------------------------------------------

    private static class CpuLoadChartPanel {

        ChartSupport.LoadChart loadChart;
        LoadPanelSupport panel;

        public CpuLoadChartPanel(HostOverview hostOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostMonitorView.class, "LBL_Load"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(HostOverview hostOverview, long time) {
            double load = hostOverview.getLoadAvg();
            addNewLoadValue(load, time);
        }

        private void initComponents() {
            loadChart = new ChartSupport.LoadChart();
            panel = loadChart.getChartPanel();
        }

        private void addNewLoadValue(double load, long time) {
            loadChart.addNewValue(new double[]{load}, time);
            panel.setLoad(load);
        }
    }

// --- Memory Usage ------------------------------------------------------------
    private static class MemoryChartPanel {

        ChartSupport.MemoryChart memoryChart;
        MemoryPanelSupport panel;

        public MemoryChartPanel(HostOverview hostOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostMonitorView.class, "LBL_Memory"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(HostOverview hostOverview, long time) {
            double used = hostOverview.getMemUsed();
            double total = hostOverview.getMemTotal();
            addNewMemoryValue(used, total, time);
        }

        private void initComponents() {
            memoryChart = new ChartSupport.MemoryChart();
            panel = memoryChart.getChartPanel();
        }

        private void addNewMemoryValue(double used, double total, long time) {
            memoryChart.addNewValue(new double[]{used, total}, time);
            panel.setMemory(used, total);
        }
    }

// --- Swap Usage ------------------------------------------------------------
    private static class SwapChartPanel {

        ChartSupport.SwapChart swapChart;
        MemoryPanelSupport panel;

        public SwapChartPanel(HostOverview hostOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostMonitorView.class, "LBL_Swap"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(HostOverview hostOverview, long time) {
            double used = hostOverview.getSwapUsed();
            double total = hostOverview.getSwapTotal();
            addNewSwapValue(used, total, time);
        }

        private void initComponents() {
            swapChart = new ChartSupport.SwapChart();
            panel = swapChart.getChartPanel();
        }

        private void addNewSwapValue(double used, double total, long time) {
            swapChart.addNewValue(new double[]{used, total}, time);
            panel.setMemory(used, total);
        }
    }

// --- Slot Usage ------------------------------------------------------------
    private static class SlotChartPanel {

        ChartSupport.SlotChart slotChart;
        SlotPanelSupport panel;

        public SlotChartPanel(HostOverview hostOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostMonitorView.class, "LBL_Slots"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(HostOverview hostOverview, long time) {
            double used = hostOverview.getUsedSlots();
            double total = hostOverview.getTotalSlots();
            addNewSlotValue(used, total, time);
        }

        private void initComponents() {
            slotChart = new ChartSupport.SlotChart();
            panel = slotChart.getChartPanel();
        }

        private void addNewSlotValue(double used, double total, long time) {
            slotChart.addNewValue(new double[]{used, total}, time);
            panel.setSlots(used, total);
        }
    }
}
