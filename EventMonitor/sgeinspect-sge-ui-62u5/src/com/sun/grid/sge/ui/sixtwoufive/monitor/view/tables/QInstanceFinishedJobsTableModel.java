/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.api.monitor.job.JobOverview;
import java.util.Calendar;
import java.util.List;
import org.openide.util.NbBundle;

public class QInstanceFinishedJobsTableModel extends PagingTableModel<Job> {

    private Class columnClass[] = {Number.class, String.class, String.class,
        Number.class, Calendar.class, String.class
    };
    
    public final String[] COLUMNS = {
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "ID"),
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Job_Name"),
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Owner"),
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Slots"),
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Start_Time"),
        NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Task")
    };

    public QInstanceFinishedJobsTableModel(List<Job> jobs) {
        super(jobs);
    }

    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public Object getValueAt(int columnIndex, Job source) {
        JobOverview jobOverview = (JobOverview) SGEFactory.getDefault().getModel(source);

        switch (columnIndex) {
            case 0:
                return jobOverview.getId();
            case 1:
                return jobOverview.getName();
            case 2:
                return jobOverview.getUser();
            case 3:
                return jobOverview.getSlots();
            case 4:
                return super.getDateFormat().format(jobOverview.getStartTime());
            case 5:
                return jobOverview.getTaskId();
            default:
                return NbBundle.getMessage(QInstanceFinishedJobsTableModel.class, "Error");
        }
    }
}
