/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.shared.api.layer.OpenSingleDataSourceAction;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;


public class ClusterOpenAction extends OpenSingleDataSourceAction<ClusterImpl> {
    private static final String ICON_PATH = "com/sun/grid/sge/ui/resources/cluster.png";  // NOI18N
    private static final Image ICON = ImageUtilities.loadImage(ICON_PATH);
    private final PropertyChangeListener propertyListener = new ClusterPropertyListener();
    private static ClusterOpenAction INSTANCE;

    @Override
    protected void actionPerformed(final ClusterImpl cluster, ActionEvent actionEvent) {
          DataSourceWindowManager manager = DataSourceWindowManager.sharedInstance();
          manager.openDataSource(cluster);
    }

    @Override
    protected boolean isEnabled(ClusterImpl cluster) {
        cluster.addPropertyChangeListener(propertyListener);
        return cluster.isConnected();
    }

    private class ClusterPropertyListener implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            INSTANCE.setEnabled(((Cluster) evt.getSource()).isConnected());
        }
    }

    public static synchronized ClusterOpenAction createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClusterOpenAction();
        }
        return INSTANCE;
    }

    private ClusterOpenAction() {
        super(ClusterImpl.class);
        putValue(NAME, NbBundle.getMessage(ClusterOpenAction.class, "LBL_Monitor_Cluster"));  // NOI18N
        putValue(SHORT_DESCRIPTION, NbBundle.getMessage(ClusterOpenAction.class, "ToolTip_Monitor_Cluster")); // NOI18N
        putValue(SMALL_ICON, new ImageIcon(ICON));
        putValue("iconBase", ICON_PATH);
        putValue("noIconInMenu", Boolean.TRUE);
    }

}
