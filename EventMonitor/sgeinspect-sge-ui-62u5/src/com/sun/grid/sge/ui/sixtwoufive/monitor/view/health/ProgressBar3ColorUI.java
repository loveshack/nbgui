/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.health;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.plaf.basic.BasicProgressBarUI;

public class ProgressBar3ColorUI extends BasicProgressBarUI {
    //The value (percent of the bar width) where the second color should cStart 
    private int start;
    //The value (percent of the bar width) where the second color value should cEnd
    private int end;
    private boolean redToGreen;

    /** Creates a new instance of ProgressBar3ColorUI */
    public ProgressBar3ColorUI() {
        redToGreen = true;
    }

    public static ComponentUI createUI(JComponent c) {
        return new ProgressBar3ColorUI();
    }

    /**
     * All purpose paint method that should do the right thing for almost
     * all linear, determinate progress bars. By setting a few values in
     * the defaults
     * table, things should work just fine to paint your progress bar.
     * Naturally, override this if you are making a circular or
     * semi-circular progress bar.
     * 
     * 
     * @see #paintIndeterminate
     * @since 1.4
     */
    protected void paintDeterminate(Graphics g, JComponent c) {
        if (!(g instanceof Graphics2D)) {
            return;
        }

        Insets b = progressBar.getInsets(); // area for border
        int barRectWidth = progressBar.getWidth() - (b.right + b.left);
        int barRectHeight = progressBar.getHeight() - (b.top + b.bottom);

        if (barRectWidth <= 0 || barRectHeight <= 0) {
            return;
        }

        int cellLength = getCellLength();
        int cellSpacing = getCellSpacing();
        // amount of progress to draw
        int amountFull = getAmountFull(b, barRectWidth, barRectHeight);

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(getForeground());

        if (progressBar.getOrientation() == JProgressBar.HORIZONTAL) {
            // draw the cells
            if (cellSpacing == 0 && amountFull > 0) {
                // draw one big Rect because there is no space between cells
                g2.setStroke(new BasicStroke((float) barRectHeight,
                      BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
            } else {
                // draw each individual cell
                g2.setStroke(new BasicStroke((float) barRectHeight,
                      BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
                      0.f, new float[]{cellLength, cellSpacing}, 0.f));
            }

            g2.drawLine(b.left, (barRectHeight / 2) + b.top,
                  amountFull + b.left, (barRectHeight / 2) + b.top);

        } else { // VERTICAL
            // draw the cells
            if (cellSpacing == 0 && amountFull > 0) {
                // draw one big Rect because there is no space between cells
                g2.setStroke(new BasicStroke((float) barRectWidth,
                      BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
            } else {
                // draw each individual cell
                g2.setStroke(new BasicStroke((float) barRectWidth,
                      BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
                      0f, new float[]{cellLength, cellSpacing}, 0f));
            }

            g2.drawLine(barRectWidth / 2 + b.left,
                  b.top + barRectHeight,
                  barRectWidth / 2 + b.left,
                  b.top + barRectHeight - amountFull);
        }

        // Deal with possible text painting
        if (progressBar.isStringPainted()) {
            paintString(g, b.left, b.top,
                  barRectWidth, barRectHeight,
                  amountFull, b);
        }
    }

    protected Color getForeground() {
        int complete = (int) Math.round(progressBar.getPercentComplete() * 100);
        if (isRedToGreen()) {
            if (complete < start) {
                return Color.RED;
            } else if (complete > end) {
                return Color.GREEN;
            }
        } else if (!isRedToGreen()) {
            if (complete < start) {
                return Color.GREEN;
            } else if (complete > end) {
                return Color.RED;
            }
        }

        return Color.ORANGE;
    }

    /**
     * sets the order of gradient colors
     * True - bar will show colors from left to right as red, orange, green
     * False - bar will show colors from left to right as green, orange, red
     */
    public void setRedToGreen(boolean b) {
        redToGreen = b;
    }

    public boolean isRedToGreen() {
        return redToGreen;
    }

    public void setMiddleColorBounds(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStartColorValue() {
        return start;
    }

    public int getEndColorValue() {
        return end;
    }

    @Override
    protected Color getSelectionForeground() {
        Color retValue;

        retValue = Color.BLACK;
        return retValue;

    }

    @Override
    protected Color getSelectionBackground() {
        Color retValue;

        retValue = Color.BLACK;
        return retValue;
    }
}
