/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueOverview;
import com.sun.grid.sge.api.monitor.util.NumberFormater;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import org.openide.util.NbBundle;

public class ClusterQueueSummaryTableModel extends PagingTableModel<Queue> {
   private Class columnClass [] = {String.class, String.class, 
             Number.class, Number.class, Number.class, Number.class, Number.class, Number.class };
    

    private final DecimalFormat form = new DecimalFormat();
    public final String[] COLUMNS = {
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Cluster_Queue"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Load"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Used_Slots"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Reserved_Slots"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Available_Slots"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Total_Slots"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Temporary_Disabled"),
        NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Manual_Intervention_Needed")
    };

    public ClusterQueueSummaryTableModel(List<Queue> queues) {
        super(queues);
        form.setMaximumFractionDigits(2);
        form.setRoundingMode(RoundingMode.HALF_UP);
    }

    public int getColumnCount() {
        return COLUMNS.length;
    }
    
    public Class getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

    public synchronized Object getValueAt(int columnIndex, Queue source) {
        QueueOverview summary = (QueueOverview) SGEFactory.getDefault().getModel(source);
              
        switch (columnIndex) {
            case 0:
                return source.getQueueName();
            case 1:
                return NumberFormater.format(form, summary.getLoad());
            case 2:
                return summary.getUsedSlots();
            case 3:
                return summary.getReservedSlots();
            case 4:
                return summary.getAvailableSlots();
            case 5:
                return summary.getTotalSlots();
            case 6:
                return summary.getTempDisabled();
            case 7:
                return summary.getManualIntervention();
            default:
                return NbBundle.getMessage(ClusterQueueSummaryTableModel.class, "Error");
        }
    }
}
