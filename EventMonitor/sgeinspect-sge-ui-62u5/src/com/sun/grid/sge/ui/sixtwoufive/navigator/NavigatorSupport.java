/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.util.DataSourceComparator;
import com.sun.grid.sge.ui.navigator.FilterListener;
import com.sun.grid.sge.ui.navigator.SortListener;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.ClusterHostViewNode;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.ClusterPEViewNode;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.ClusterQueueViewNode;
import com.sun.grid.sge.ui.sixtwoufive.navigator.buttons.ButtonPanel;
import com.sun.grid.shared.ui.navigator.NavigatorActionHandler;
import com.sun.grid.shared.ui.navigator.NavigatorTopComponent;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class NavigatorSupport {

    public static final int SORT_ASC = 0;
    public static final int SORT_DESC = 1;
    public static final String HOST_VIEW = NbBundle.getMessage(NavigatorSupport.class, "Host_View");
    public static final String QUEUE_VIEW = NbBundle.getMessage(NavigatorSupport.class, "Queue_View");
    public static final String PE_VIEW = NbBundle.getMessage(NavigatorSupport.class, "PE_View");
    private static Object lastSelectedItem;
    private final MyNavigatorActionHandler nah = new MyNavigatorActionHandler();
    private final ClusterRemovedListener clRemoveListener = new ClusterRemovedListener();
    private Cluster selected_cluster;
    private int sort = SORT_ASC;
    private final ButtonPanel buttonPane;
    private final Map<Cluster, AbstractNode> hViewRoots = new WeakHashMap<Cluster, AbstractNode>();
    private final Map<Cluster, AbstractNode> qViewRoots = new WeakHashMap<Cluster, AbstractNode>();
    private final Map<Cluster, AbstractNode> peViewRoots = new WeakHashMap<Cluster, AbstractNode>();
    private final ComboBoxModel cbm = new DefaultComboBoxModel(new String[]{HOST_VIEW, QUEUE_VIEW, PE_VIEW});
    private List<FilterListener> filterListeners = Collections.synchronizedList(new LinkedList<FilterListener>());
    private List<SortListener> hSortListeners = Collections.synchronizedList(new LinkedList<SortListener>());

    private static final NavigatorSupport INSTANCE = new NavigatorSupport();

    @SuppressWarnings(value = "unchecked")
    private NavigatorSupport() {
        buttonPane = new ButtonPanel();
//        hostComparator = new Comparator[]{new HostSortAscendingComparator(), new HostSortDescendingComparator()};
//        queueComparator = new Comparator[]{new QueueInstanceAscendingComparator(), new QueueInstanceDescendingComparator()};
    }

    public static void initialize(final Class<? extends DataSource> m) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                NavigatorTopComponent.findInstance().addNavigatorActionHandler(m, INSTANCE.nah);
            }
        });
    }

    public static void shutdown(final Class<? extends DataSource> m) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                NavigatorTopComponent.findInstance().removeNavigatorActionHandler(m, INSTANCE.nah);
            }
        });
    }
    
    public static NavigatorSupport getInstance() {
        return INSTANCE;
    }

    public NavigatorActionHandler getNavigatorActionHandler() {
        return nah;
    }

    private class MyNavigatorActionHandler implements NavigatorActionHandler {

        AbstractNode root;

        public void handleSelectionChanged(Set<DataSource> arg0, Object selectedItem) {
            if (!arg0.isEmpty()) {
                Object obj = arg0.iterator().next();
                if (obj instanceof Cluster) {
                    lastSelectedItem = selectedItem;
                    selected_cluster = (Cluster) obj;
                    selected_cluster.notifyWhenRemoved(clRemoveListener);
                    changeContent(false, lastSelectedItem);
                } else {
                    selected_cluster = null;
                }
            }
        }

        public void handleViewChanged(ActionEvent evt, Object selectedItem) {
            if (selected_cluster != null) {
                lastSelectedItem = selectedItem;
                changeContent(false, selectedItem);
            }
        }

        public void handleComponentOpened() {
        }

        public void handleComponentClosed() {
        }

        public Node getRootContext() {
            return root;
        }

        public JPanel getToolbarPanel() {
            return buttonPane;
        }

        public ComboBoxModel getViewModel() {
            return cbm;
        }

        private synchronized void changeContent(final boolean newRoot, Object selectedItem) {
            if (selectedItem != null &&
                  selectedItem.equals(HOST_VIEW)) {
                ButtonPanel.btnArchFilter.setVisible(true);
                root = hViewRoots.get(selected_cluster);
                if (root == null || newRoot) {
                    root = new ClusterHostViewNode(selected_cluster, DataSourceComparator.getComparator(sort));
                    hViewRoots.put(selected_cluster, root);
                }
            } else if (selectedItem != null && selectedItem.equals(QUEUE_VIEW)){
                ButtonPanel.btnArchFilter.setVisible(false);
                root = qViewRoots.get(selected_cluster);
                if (root == null || newRoot) {
                    root = new ClusterQueueViewNode(selected_cluster, DataSourceComparator.getComparator(sort));
                    qViewRoots.put(selected_cluster, root);
                }
            } else {
                ButtonPanel.btnArchFilter.setVisible(false);
                root = peViewRoots.get(selected_cluster);
                if (root == null || newRoot) {
                    root = new ClusterPEViewNode(selected_cluster, DataSourceComparator.getComparator(sort));
                    peViewRoots.put(selected_cluster, root);
                }
            }
        }
    }

    public static void setSort(int sort) {
        INSTANCE.sort = sort;
        INSTANCE.fireSortEvent();
    }

    public static void filterApplied() {
        INSTANCE.fireFilterEvent();
    }

    protected void fireFilterEvent() {
        for (FilterListener fl : filterListeners) {
            fl.filterApplied();
        }
    }

    public static void addFilterListener(FilterListener fl) {
        if (!INSTANCE.filterListeners.contains(fl)) {
            INSTANCE.filterListeners.add(fl);
        }
    }

    public static void removeFilterListener(FilterListener fl) {
        INSTANCE.filterListeners.remove(fl);
    }

    public static void removeSortListener(SortListener hsl) {
        INSTANCE.hSortListeners.remove(hsl);
    }

    public static void addSortListener(SortListener hsl) {
        if (!INSTANCE.hSortListeners.contains(hsl)) {
            INSTANCE.hSortListeners.add(hsl);
        }
    }

    protected void fireSortEvent() {
        for (SortListener hsl : hSortListeners) {
            hsl.sortApplied(DataSourceComparator.getComparator(sort));
        }
    }

    private class ClusterRemovedListener implements DataRemovedListener<Cluster> {

        public void dataRemoved(Cluster c) {
            if (selected_cluster != null) {
                qViewRoots.remove(c);
                hViewRoots.remove(c);
                peViewRoots.remove(c);

                selected_cluster = null;
            }
        }
    }
}
