/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueOverview;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.QueueDetailOpenAction;
import java.awt.Image;
import java.util.Comparator;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

//Node used when the View in Navigator is set to 'Queue View'
public class QueueNode extends AbstractNode {
    
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/queue.png"; // NOI18N
    private List<Action> actions = null;

    public QueueNode(Queue queue, Comparator<QueueInstance> comparator) {
        super(Children.create(new QueueChildFactory(queue, comparator), true),
                Lookups.singleton(queue));
    }

    public Queue getQueue() {
        return getLookup().lookup(Queue.class);
    }
    
    @Override
    public Action getPreferredAction() {
        return QueueDetailOpenAction.createInstance();
    }
    
    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{
                    QueueDetailOpenAction.createInstance(),};
//        // get all actions from super class
//        if (actions == null) {
//            actions = new ArrayList<Action>(Arrays.asList(super.getActions(popup)));
//            actions.add(null); // add separator
//        }
//        // no actions from super class e.g. Properties
//        // but may be added via layer.xml entry
//        if (actions == null) {
//            actions = new ArrayList<Action>();
//        }
//        Lookup lkp = Lookups.forPath("ContextActions/QueueNode");
//        actions.addAll(lkp.lookupAll(Action.class));
//
//        return actions.toArray(new Action[actions.size()]);
    }
    
    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public String getDisplayName() {
        Queue queue = getQueue();
        return queue.getQueueName();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Queue queue = getQueue();
        QueueOverview queueOverview = (QueueOverview) SGEFactory.getDefault().getModel(queue);

        try {
            Property queueName = new PropertySupport.Reflection(queueOverview, String.class, "getName", null);

            queueName.setName(NbBundle.getMessage(HostNode.class, "Queue_Name"));

            set.put(queueName);
        } catch (NoSuchMethodException ex) {

        }

        sheet.put(set);
        return sheet;
    }
}
