/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueInstanceOverview;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.QueueInstanceDetailOpenAction;
import java.awt.Image;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class QueueInstanceNode extends AbstractNode {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/queue_instance.png"; // NOI18N

    public QueueInstanceNode(QueueInstance qInstance) {
        super(Children.LEAF, Lookups.singleton(qInstance));
    }

    public QueueInstance getQueueInstance() {
        return getLookup().lookup(QueueInstance.class);
    }

    @Override
    public String getDisplayName() {
        QueueInstance qInstance = getQueueInstance();
        return qInstance.getNameWithType();
    }

    @Override
    public String getHtmlDisplayName() {
        QueueInstance qInstance = getQueueInstance();
        if (qInstance != null) {
            return "<font color='!textText'> " + qInstance.getQueueName() + "</font>" +
                    "<font color='!OptionPane.questionDialog.titlePane.shadow'> (" + qInstance.getType() + ")</font>";
        } else {
            return null;
        }
    }

    @Override
    public String getName() {
        QueueInstance qInstance = getQueueInstance();
        return qInstance.getQueueName();
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Action getPreferredAction() {
        return QueueInstanceDetailOpenAction.createInstance();
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{
                  QueueInstanceDetailOpenAction.createInstance(),
              };
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        QueueInstance queueInstance = getQueueInstance();
        QueueInstanceOverview queueOverview = (QueueInstanceOverview) SGEFactory.getDefault().getModel(queueInstance);

        try {
            Property queueName = new PropertySupport.Reflection(queueOverview, String.class, "getName", null);

            queueName.setName(NbBundle.getMessage(HostNode.class, "QueueInstance_Name"));

            set.put(queueName);
        } catch (NoSuchMethodException ex) {
        }

        sheet.put(set);
        return sheet;
    }
}
