/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.health;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;

public final class HealthStatusSupport {
    
    private static Map<Cluster, HealthStatusTopComponent> instanceMap;
    private final ClusterRemovedListener clRemoveListener = new ClusterRemovedListener();
    private static HealthStatusSupport instance;
    
     private HealthStatusSupport() {

     }
     
    public static synchronized HealthStatusSupport getInstance() {
        if (instance == null) {
            instance = new HealthStatusSupport();
        }
        return instance;
    }
     
     public HealthStatusTopComponent getHealthStatusWindow(Cluster cluster) {
        
        if (instanceMap == null) {
            instanceMap = new HashMap<Cluster, HealthStatusTopComponent>();
        }
       
        if (!instanceMap.containsKey(cluster)) {
            instanceMap.put(cluster, new HealthStatusTopComponent(cluster));
            cluster.notifyWhenRemoved(clRemoveListener);
        }

        return instanceMap.get(cluster);
     }
    
    private class ClusterRemovedListener implements DataRemovedListener<Cluster> {

        public void dataRemoved(Cluster c) {
                final HealthStatusTopComponent win = instanceMap.get(c);
                instanceMap.remove(c);
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {
                        win.close();
                    }
                });
            }
        }
}
