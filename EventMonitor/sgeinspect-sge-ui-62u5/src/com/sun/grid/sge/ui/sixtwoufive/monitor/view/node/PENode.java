/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor.AllocationPropertySupport;
import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.DeletePEAction;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor.CommandPropertySupport;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor.UrgencyPropertySupport;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor.UserListPropertySupport;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.awt.Image;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.PropertiesAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

public class PENode extends AbstractNode {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/parallel_environment_16.png"; // NOI18N
    private Cluster cluster;

    public PENode(PE pe, Cluster cluster) {
        super(Children.LEAF, Lookups.singleton(pe));
        this.cluster = cluster;
    }

    public PE getPE() {
        return getLookup().lookup(PE.class);
    }

    @Override
    public Action getPreferredAction() {
        return SystemAction.get(PropertiesAction.class);
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{
                  DeletePEAction.createInstance(),
                  null,
                  SystemAction.get(PropertiesAction.class),};
    }

    @Override
    public String getDisplayName() {
        PE pe = getPE();
        return pe.getName();
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        PE pe = getPE();

        try {
            Property name = new PropertySupport.Reflection(pe, String.class, "getName", null);
            Property slots = new PropertySupport.Reflection(pe, Integer.class, "slots");
            PropertySupport.Reflection allocation = new PropertySupport.Reflection(pe, String.class, "allocationRule");
            allocation.setPropertyEditorClass(AllocationPropertySupport.class);
            Property slaves = new PropertySupport.Reflection(pe, Boolean.class, "controlSlaves");
            Property jobTask = new PropertySupport.Reflection(pe, Boolean.class, "jobIsFirstTask");
            Property accounting = new PropertySupport.Reflection(pe, Boolean.class, "accountingSummary");
            PropertySupport.Reflection urgency = new PropertySupport.Reflection(pe, String.class, "urgencySlots");
            urgency.setPropertyEditorClass(UrgencyPropertySupport.class);

            name.setName(NbBundle.getMessage(PENode.class, "NAME"));
            slots.setName(NbBundle.getMessage(PENode.class, "Slots"));
            slots.setShortDescription(NbBundle.getMessage(PENode.class, "Slots_Desc"));
            allocation.setName(NbBundle.getMessage(PENode.class, "ALLOCATION_RULE"));
            allocation.setShortDescription(NbBundle.getMessage(PENode.class, "Allocation_Desc"));
            slaves.setName(NbBundle.getMessage(PENode.class, "SLAVES"));
            slaves.setShortDescription(NbBundle.getMessage(PENode.class, "Slaves_Desc"));
            jobTask.setName(NbBundle.getMessage(PENode.class, "IS_JOB_FIRST_TASK"));
            jobTask.setShortDescription(NbBundle.getMessage(PENode.class, "First_Task_Desc"));
            accounting.setName(NbBundle.getMessage(PENode.class, "ACCOUNTING_SUMMARY"));
            accounting.setShortDescription(NbBundle.getMessage(PENode.class, "Accounting_Desc"));
            urgency.setName(NbBundle.getMessage(PENode.class, "URGENCY_SLOTS"));
            urgency.setShortDescription(NbBundle.getMessage(PENode.class, "Urgency_Desc"));
//            users.setName("Allowed Users");

            set.put(name);
            set.put(slots);
            set.put(new CommandsProperty(pe, CommandPropertySupport.TYPE_START_COMMAND,
                  NbBundle.getMessage(PENode.class, "START_COMMAND"), NbBundle.getMessage(PENode.class, "START_COMMAND_DESC")));
            set.put(new CommandsProperty(pe, CommandPropertySupport.TYPE_STOP_COMMAND,
                  NbBundle.getMessage(PENode.class, "STOP_COMMAND"), NbBundle.getMessage(PENode.class, "STOP_COMMAND_DESC")));
            set.put(allocation);
            set.put(slaves);
            set.put(jobTask);
            set.put(accounting);
            set.put(urgency);
            set.put(new UsersProperty(pe, cluster));
            set.put(new XUsersProperty(pe, cluster));

        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }

        sheet.put(set);
        return sheet;
    }

    private static final class UsersProperty extends PropertySupport.ReadWrite<List> {

        private final PE pe;
        private final Cluster cluster;

        UsersProperty(PE pe, Cluster cluster) {
            super("userList", List.class, NbBundle.getMessage(PENode.class, "ALLOWED_USERS"),
                  NbBundle.getMessage(PENode.class, "User_Node_Desc"));
            this.pe = pe;
            this.cluster = cluster;
        }

        @Override
        public List<UserSet> getValue() throws IllegalAccessException, InvocationTargetException {
            return pe.getUserList();
        }

        @Override
        public void setValue(List list) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            pe.setUserList((List<UserSet>) list);
        }

        @Override
        public boolean supportsDefaultValue() {
            return true;
        }

        @Override
        public void restoreDefaultValue() {
            List<UserSet> list = new LinkedList<UserSet>();
            pe.setXuserList(list);
            pe.setUserList(list);
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return new UserListPropertySupport(pe, cluster);
        }
    }

    private static final class XUsersProperty extends PropertySupport.ReadWrite<List> {

        private final PE pe;
        private final Cluster cluster;

        XUsersProperty(PE pe, Cluster cluster) {
            super("xuserList", List.class, NbBundle.getMessage(PENode.class, "DISALLOWED_USERS"),
                  NbBundle.getMessage(PENode.class, "User_Node_Desc"));
            this.pe = pe;
            this.cluster = cluster;

        }

        @Override
        public List<UserSet> getValue() throws IllegalAccessException, InvocationTargetException {
            return pe.getXuserList();
        }

        @Override
        public void setValue(List list) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            pe.setXuserList((List<UserSet>) list);
        }

        @Override
        public boolean supportsDefaultValue() {
            return true;
        }

        @Override
        public void restoreDefaultValue() {
            List<UserSet> list = new LinkedList<UserSet>();
            pe.setXuserList(list);
            pe.setUserList(list);
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return new UserListPropertySupport(pe, cluster);
        }
    }

    /**
     * Property support for PE command properties
     */
    private static final class CommandsProperty extends PropertySupport.ReadWrite<String> {

        private PE pe;
        private int type;

        /**
         * Constructor
         * @param pe The pe which has the command property
         * @param type The type of the command
         * @param displayName The dsiplay name for the property
         * @param shortDescription Short description of the property
         */
        public CommandsProperty(PE pe, int type, String displayName, String shortDescription) {
            super(displayName, String.class, displayName, shortDescription);

            this.pe = pe;
            this.type = type;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return CommandPropertySupport.getCommand(pe, type);
        }

        @Override
        public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            try {
                CommandPropertySupport.setCommand(pe, type, val);
            } catch (PEException pex) {
                ErrorDisplayer.submitWarning(pex.getMessage());
            }
        }

        @Override
        public boolean supportsDefaultValue() {
            return true;
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return new CommandPropertySupport(pe, type);
        }

        @Override
        public void restoreDefaultValue() {
            CommandPropertySupport.setCommandToDefault(pe, type);
        }
    }
}
