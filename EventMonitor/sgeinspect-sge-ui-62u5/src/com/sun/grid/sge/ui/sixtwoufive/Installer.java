/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive;

import com.sun.grid.sge.api.monitor.cluster.ClusterContainer;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.cluster.ClusterOverviewViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.cluster.ClusterJobsViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.host.HostOverviewViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.host.HostMonitorViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.queue.QueueInstanceMonitorViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.queue.QueueMonitorViewProvider;
import com.sun.grid.sge.ui.sixtwoufive.navigator.NavigatorSupport;
import org.openide.modules.ModuleInstall;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.shared.main.ApplicationFilter;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    @Override
    public void restored() {
        ClusterOverviewViewProvider.initialize();
        ClusterJobsViewProvider.initialize();
        HostOverviewViewProvider.initialize();
        HostMonitorViewProvider.initialize();
        QueueMonitorViewProvider.initialize();
        QueueInstanceMonitorViewProvider.initialize();
        NavigatorSupport.initialize(ClusterImpl.class);
        ApplicationFilter.getInstance().registerApplication(ClusterContainer.class);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void uninstalled() {
        super.uninstalled();
        ClusterOverviewViewProvider.unregister();
        ClusterJobsViewProvider.unregister();
        HostOverviewViewProvider.unregister();
        HostMonitorViewProvider.unregister();
        QueueMonitorViewProvider.unregister();
        QueueInstanceMonitorViewProvider.unregister();
        NavigatorSupport.shutdown(ClusterImpl.class);
        ApplicationFilter.getInstance().unregisterApplication(ClusterContainer.class);
    }
}
