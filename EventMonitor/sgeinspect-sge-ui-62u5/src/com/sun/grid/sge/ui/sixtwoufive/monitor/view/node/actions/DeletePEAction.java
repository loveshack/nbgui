/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions;

import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.sixtwoufive.config.pe.impl.PEImpl;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.shared.api.layer.OpenCookieAction;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CookieAction;

public class DeletePEAction extends OpenCookieAction {

    private static DeletePEAction instance;

    protected void performAction(Node[] activatedNodes) {
        final DataSourceWindowManager manager = DataSourceWindowManager.sharedInstance();
        for (Node n : activatedNodes) {
            final PE pe = n.getLookup().lookup(PEImpl.class);
            final ClusterImpl cluster = (ClusterImpl) pe.getOwner();
            RequestProcessor.getDefault().post(new Runnable() {

                public void run() {
                    if (pe != null) {
                        try {
                            cluster.deletePE(pe.getName());
                            StatusDisplayer.getDefault().setStatusText(
                                  NbBundle.getMessage(DeletePEAction.class, "Deleting_PE", pe.getName()));
                        } catch (PEException pex) {
                            ErrorDisplayer.submitWarning(pex.getMessage());
                        }

                    }
                }
            });
        }
    }

    protected int mode() {
        return CookieAction.MODE_ALL;
    }

    public String getName() {
        return NbBundle.getMessage(DeletePEAction.class, "CTL_Delete_PE");
    }

    protected Class[] cookieClasses() {
        return new Class[]{PEImpl.class};
    }

    @Override
    protected void initialize() {
        super.initialize();
        // see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
        putValue("noIconInMenu", Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    private DeletePEAction() {
        super();
    }

    public static synchronized DeletePEAction createInstance() {
        if (instance == null) {
            instance = new DeletePEAction();
        }
        return instance;
    }
}
