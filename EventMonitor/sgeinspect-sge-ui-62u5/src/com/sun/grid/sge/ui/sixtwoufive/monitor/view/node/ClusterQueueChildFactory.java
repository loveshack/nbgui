/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

//This Factory used when view in Navigator set to 'Queue View'
import org.openide.util.WeakListeners;

public class ClusterQueueChildFactory extends ChildFactory<Queue> implements PropertyChangeListener {

    private Cluster cluster;
    private Comparator<QueueInstance> comparator;
    private final QueueDataListener queueListener = new QueueDataListener();

    public ClusterQueueChildFactory(Cluster cluster, Comparator<QueueInstance> comparator) {
        this.cluster = cluster;
        this.comparator = comparator;
        cluster.getRepository().addDataChangeListener(queueListener, Queue.class);
        cluster.addPropertyChangeListener(WeakListeners.propertyChange(this, cluster));
    }

    @Override
    protected boolean createKeys(List<Queue> keys) {
        List<Queue> queues;
        if (!cluster.isConnected()) {
            queues = new LinkedList<Queue>();
        } else {
            queues = new LinkedList<Queue>(cluster.getQueueList());
        }
        for (Queue queue : queues) {
            keys.add(queue);
        }
        Collections.sort(keys);
        return true;
    }

    @Override
    protected Node createNodeForKey(Queue queue) {
        return new QueueNode(queue, comparator);
    }

    private class QueueDataListener implements DataChangeListener<Queue> {

        public void dataChanged(DataChangeEvent<Queue> evt) {
            refresh(true);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        refresh(true);
    }
}
