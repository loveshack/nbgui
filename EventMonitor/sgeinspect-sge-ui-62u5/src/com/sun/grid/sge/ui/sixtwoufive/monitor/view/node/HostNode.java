/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostOverview;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.HostDetailOpenAction;
import java.awt.Image;
import javax.swing.Action;
import org.openide.ErrorManager;
import org.openide.actions.PropertiesAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

public class HostNode extends AbstractNode {

    private Cluster cluster;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/host.png"; // NOI18N

    public HostNode(Cluster cluster, Host host) {
        super(Children.create(new HostChildFactory(host, cluster), true),
              Lookups.singleton(host));
        this.cluster = cluster;
    }

    public Host getHost() {
        return getLookup().lookup(Host.class);
    }

    public String getType() {
        Host host = getHost();
        return host.getType();
    }

    @Override
    public String getDisplayName() {
        Host host = getHost();
        return host.getHostName() + " (" + host.getArch() + ")";
    }

    public String getHtmlDisplayName() {
    Host host = getHost();
    if (host != null) {
        return "<font color='!textText'> " + host.getHostName() + "</font>" +
                "<font color='!TabbedPane.focus'> (" + host.getArch() + ")</font>";
    } else {
        return null;
    }
    }

    @Override
    public String getName() {
        Host host = getHost();
        return host.getHostName();
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return ImageUtilities.loadImage(IMAGE_PATH);
    }

    @Override
    public Action getPreferredAction() {
        return HostDetailOpenAction.createInstance();
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{
                  HostDetailOpenAction.createInstance(),
                  null,
                  SystemAction.get(PropertiesAction.class),
              };
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Host host = getHost();
        HostOverview hostOverview = (HostOverview) SGEFactory.getDefault().getModel(host);

        try {
            Property hostName = new PropertySupport.Reflection(hostOverview, String.class, "getHostname", null);
//            Property ip = new PropertySupport.Reflection(hostOverview, String.class, "getIp", null);
//            Property os = new PropertySupport.Reflection(hostOverview, String.class, "getOs", null);
            Property arch = new PropertySupport.Reflection(hostOverview, String.class, "getArch", null);
            Property numberOfProcessors = new PropertySupport.Reflection(hostOverview, String.class, "getNumberOfProcessors", null);

            hostName.setName(NbBundle.getMessage(HostNode.class, "Host_Name"));
//            ip.setName("Host IP");
//            os.setName("Operating System");
            arch.setName(NbBundle.getMessage(HostNode.class, "Architecture"));
            numberOfProcessors.setName(NbBundle.getMessage(HostNode.class, "Number_of_Processors"));

            set.put(hostName);
//            set.put(ip);
//            set.put(os);
            set.put(arch);
            set.put(numberOfProcessors);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;
    }
}
