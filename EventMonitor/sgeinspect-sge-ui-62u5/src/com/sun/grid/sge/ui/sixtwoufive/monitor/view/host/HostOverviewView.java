/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.host;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.HostRunningJobsTableModel;
import com.sun.grid.sge.ui.tables.PagingTablePanel;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostOverview;
import com.sun.grid.sge.sixtwoufive.monitor.host.impl.HostImpl;
import com.sun.grid.sge.ui.charts.ChartSupport;
import com.sun.grid.sge.ui.charts.JobPanelSupport;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

public class HostOverviewView extends DataSourceView implements DataRemovedListener<Host>, ChangeListener {

    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/overview.png"; // NOI18N
    private HostOverview hostOverview;
    private JobsChartPanel jobsChartPanel;
    private SlotsPanel slotsPanel;
    private RunningJobsPanel runningJobsPanel;
    private Cluster cluster;

    public HostOverviewView(Host host) {
        super(host, NbBundle.getMessage(HostOverviewView.class, "LBL_Overview"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = (Cluster) getDataSource().getOwner();
    }

    @Override
    protected void willBeAdded() {
        hostOverview = (HostOverview)SGEFactory.getDefault().getModel(getDataSource());
        if (cluster != null) {
            cluster.addChangeListener(this);
        }
    }

    public void dataRemoved(Host arg0) {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void removed() {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected DataViewComponent createComponent() {

        //Configuration of master view:
        dvc = new DataViewComponent(
                new OverviewPanel((Host) getDataSource()).getMasterView(), new DataViewComponent.MasterViewConfiguration(false));

        //Add detail views to the component:
        jobsChartPanel = new JobsChartPanel(hostOverview);
        dvc.addDetailsView(jobsChartPanel.getDetailsView(), DataViewComponent.TOP_RIGHT);
        slotsPanel = new SlotsPanel(hostOverview);
        dvc.addDetailsView(slotsPanel.getDetailsView(), DataViewComponent.TOP_LEFT);
        runningJobsPanel = new RunningJobsPanel(hostOverview);
        dvc.addDetailsView(runningJobsPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);
        (getDataSource()).notifyWhenRemoved(this);

        return dvc;

    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                final long time = System.currentTimeMillis();
                if (hostOverview != null) {
                    runningJobsPanel.refresh(hostOverview);
                    slotsPanel.refresh(hostOverview);
                    jobsChartPanel.refresh(hostOverview, time);
            }
            }
        });
    }

    private static class OverviewPanel extends JPanel {

        public OverviewPanel(Host host) {
            initComponents(host);
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(HostOverviewView.class, "LBL_Overview"), null, this);   // NOI18N
        }

        private void initComponents(Host host) {
            setLayout(new BorderLayout());
            setOpaque(false);

            HTMLTextArea area = new HTMLTextArea("<nobr></nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }
    }

    private static class RunningJobsPanel {

        HostRunningJobsTableModel model;
        PagingTablePanel panel;

        public RunningJobsPanel(HostOverview hostOverview) {
            initComponents(hostOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostOverviewView.class, "LBL_Running_Jobs"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(HostOverview hostOverview) {
            model = new HostRunningJobsTableModel(hostOverview.getRunningJobs());
            panel = new PagingTablePanel(model);
        }

        public void refresh(HostOverview hostOverview) {
            model.runUpdate(hostOverview.getRunningJobs());
        }
    }

    private static class SlotsPanel extends JPanel {

        HTMLTextArea area;

        public SlotsPanel(HostOverview hostOverview) {
            initComponents(hostOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostOverviewView.class, "LBL_Slots"), null, 10, this, null);    // NOI18N
        }

        private void initComponents(HostOverview hostOverview) {
            setLayout(new BorderLayout());
            setOpaque(false);

            area = new HTMLTextArea("<nobr>" + getSlotsOverviewInfo(hostOverview) + "</nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }

        String getSlotsOverviewInfo(HostOverview overview) {
            StringBuilder data = new StringBuilder();

            String used = NbBundle.getMessage(HostOverviewView.class, "LBL_Used_Slots"); // NOI18N
            data.append("<b>" + used + ":</b> " + overview.getUsedSlots() + "<br>"); // NOI18N
            String available = NbBundle.getMessage(HostOverviewView.class, "LBL_Available_Slots"); // NOI18N
            data.append("<b>" + available + ":</b> " + overview.getAvailableSlots() + "<br>"); // NOI18N
            String reserved = NbBundle.getMessage(HostOverviewView.class, "LBL_Reserved_Slots"); // NOI18N
            data.append("<b>" + reserved + ":</b> " + overview.getReservedSlots() + "<br>"); // NOI18N
            String total = NbBundle.getMessage(HostOverviewView.class, "LBL_Total_Slots"); // NOI18N
            data.append("<b>" + total + ":</b> " + overview.getTotalSlots() + "<br>"); // NOI18N

            return data.toString();

        }

        public void refresh(HostOverview hostOverview) {
            area.setText("<nobr>" + getSlotsOverviewInfo(hostOverview) + "</nobr>");
        }
    }

    private static class JobsChartPanel extends JPanel {

        ChartSupport.RunningJobsChart jobsChart;
        JobPanelSupport panel;

        public JobsChartPanel(HostOverview hostOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(HostOverviewView.class, "LBL_Jobs_Chart"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(HostOverview hostOverview, long time) {
            int runningCount = hostOverview.getRunningJobsCount();
            addNewRunningJobsValue(runningCount, time);
        }

        private void initComponents() {
            jobsChart = new ChartSupport.RunningJobsChart();
            panel = jobsChart.getChartPanel();
        }

        private void addNewRunningJobsValue(int jobs, long time) {
            jobsChart.addNewValue(new int[]{jobs}, time);
            panel.setJobs(jobs);
        }
    }
}
