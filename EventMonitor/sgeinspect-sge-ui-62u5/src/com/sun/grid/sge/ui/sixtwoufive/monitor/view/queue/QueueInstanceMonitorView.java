/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.queue;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTablePanel;
import com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables.QInstanceRunningJobsTableModel;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueInstanceOverview;
import com.sun.grid.sge.ui.charts.ChartSupport;
import com.sun.grid.sge.ui.charts.SlotPanelSupport;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.lib.profiler.ui.components.HTMLTextArea;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

public class QueueInstanceMonitorView extends DataSourceView implements DataRemovedListener<QueueInstance>, ChangeListener {

    private DataViewComponent dvc;
    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/monitor.png"; // NOI18N
    private SlotsPanel slotsPanel;
    private QueueInstanceOverview qInstanceOverview;
    private RunningJobsPanel runningJobsPanel;
    private SlotChartPanel slotChartPanel;
    private Cluster cluster;

    public QueueInstanceMonitorView(QueueInstance qInstance) {
        super(qInstance, NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Monitor"),
                new ImageIcon(ImageUtilities.loadImage(IMAGE_PATH, true)).getImage(), 60, false);
        this.cluster = (Cluster) getDataSource().getOwner().getOwner();
    }

    public void dataRemoved(QueueInstance queueInstance) {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void removed() {
        if (cluster != null) {
            cluster.removeChangeListener(this);
        }
    }

    @Override
    protected void willBeAdded() {
        qInstanceOverview = (QueueInstanceOverview) SGEFactory.getDefault().getModel(getDataSource());
        if (cluster != null) {
            cluster.addChangeListener(this);
        }
    }

    @Override
    protected DataViewComponent createComponent() {

        //Configuration of master view:
        dvc = new DataViewComponent(
                new OverviewPanel().getMasterView(), new DataViewComponent.MasterViewConfiguration(false));

        //Add detail views to the component:
        slotsPanel = new SlotsPanel(qInstanceOverview);
        dvc.addDetailsView(slotsPanel.getDetailsView(), DataViewComponent.TOP_LEFT);

        slotChartPanel = new SlotChartPanel(qInstanceOverview);
        dvc.addDetailsView(slotChartPanel.getDetailsView(), DataViewComponent.TOP_RIGHT);

        runningJobsPanel = new RunningJobsPanel(qInstanceOverview);
        dvc.addDetailsView(runningJobsPanel.getDetailsView(), DataViewComponent.BOTTOM_LEFT);

        ((QueueInstance) getDataSource()).notifyWhenRemoved(this);

        return dvc;

    }

    private static class OverviewPanel extends JPanel {

        public OverviewPanel() {
            initComponents();
        }

        public DataViewComponent.MasterView getMasterView() {
            return new DataViewComponent.MasterView(NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Monitor"), null, this);   // NOI18N
        }

        private void initComponents() {
            setLayout(new BorderLayout());
            setOpaque(false);

            HTMLTextArea area = new HTMLTextArea("<nobr></nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }
    }

    public void stateChanged(ChangeEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                final long time = System.currentTimeMillis();
                slotsPanel.refresh(qInstanceOverview);
                runningJobsPanel.refresh(qInstanceOverview);
                slotChartPanel.refresh(qInstanceOverview, time);
            }
        });
    }

    private static class SlotsPanel extends JPanel {

        HTMLTextArea area;

        public SlotsPanel(QueueInstanceOverview qInstanceOverview) {
            initComponents(qInstanceOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueMonitorView.class, "LBL_Slots"), null, 10, this, null);    // NOI18N
        }

        private void initComponents(QueueInstanceOverview qInstanceOverview) {
            setLayout(new BorderLayout());
            setOpaque(false);

            area = new HTMLTextArea("<nobr>" + getSlotsOverviewInfo(qInstanceOverview) + "</nobr>");  // NOI18N
            area.setBorder(BorderFactory.createEmptyBorder(14, 8, 14, 8));

            add(area, BorderLayout.CENTER);
        }

        String getSlotsOverviewInfo(QueueInstanceOverview overview) {
            StringBuilder data = new StringBuilder();

            String used = NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Used_Slots"); // NOI18N
            data.append("<b>" + used + ":</b> " + overview.getUsedSlots() + "<br>"); // NOI18N
            String available = NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Available_Slots"); // NOI18N
            data.append("<b>" + available + ":</b> " + overview.getFreeSlots() + "<br>"); // NOI18N
            String reserved = NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Reserved_Slots"); // NOI18N
            data.append("<b>" + reserved + ":</b> " + overview.getReservedSlots() + "<br>"); // NOI18N
            String total = NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Total_Slots"); // NOI18N
            data.append("<b>" + total + ":</b> " + overview.getTotalSlots() + "<br>"); // NOI18N

            return data.toString();

        }

        public void refresh(QueueInstanceOverview qInstanceOverview) {
            area.setText("<nobr>" + getSlotsOverviewInfo(qInstanceOverview) + "</nobr>");
        }
    }

    private static class RunningJobsPanel {

        QInstanceRunningJobsTableModel model;
        PagingTablePanel panel;

        public RunningJobsPanel(QueueInstanceOverview qInstanceOverview) {
            initComponents(qInstanceOverview);
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_Running_Jobs"), null, 10, panel, null);    // NOI18N
        }

        private void initComponents(QueueInstanceOverview qInstanceOverview) {
            model = new QInstanceRunningJobsTableModel(qInstanceOverview.getRunningJobs());
            panel = new PagingTablePanel(model);
        }

        public void refresh(QueueInstanceOverview qInstanceOverview) {
            model.runUpdate(qInstanceOverview.getRunningJobs());
        }
    }

    // --- Slot Usage ------------------------------------------------------------
    private static class SlotChartPanel {

        ChartSupport.SlotChart slotChart;
        SlotPanelSupport panel;

        public SlotChartPanel(QueueInstanceOverview qInstanceOverview) {
            initComponents();
        }

        public DataViewComponent.DetailsView getDetailsView() {
            return new DataViewComponent.DetailsView(NbBundle.getMessage(QueueInstanceMonitorView.class, "LBL_SlotUsage"), null, 10, panel, null);    // NOI18N
        }

        public void refresh(QueueInstanceOverview qInstanceOverview, long time) {
            double used = qInstanceOverview.getUsedSlots();
            double total = qInstanceOverview.getTotalSlots();
            addNewSlotValue(used, total, time);
        }

        private void initComponents() {
            slotChart = new ChartSupport.SlotChart();
            panel = slotChart.getChartPanel();
        }

        private void addNewSlotValue(double used, double total, long time) {
            slotChart.addNewValue(new double[]{used, total}, time);
            panel.setSlots(used, total);
        }
    }
}
