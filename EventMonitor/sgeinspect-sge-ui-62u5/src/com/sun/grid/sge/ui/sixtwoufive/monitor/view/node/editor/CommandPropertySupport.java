/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor;

import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyEditorSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 * {@link PropertyEditorSupport} class for PE command properties
 */
public class CommandPropertySupport extends PropertyEditorSupport implements ExPropertyEditor, VetoableChangeListener {

    private final PECommandEditorPanel commandEditorPanel;
    private PE pe;
    private int type = 0;
    public static final int TYPE_START_COMMAND = 0;
    public static final int TYPE_STOP_COMMAND = 1;

    /**
     * Constructor
     * @param pe The pe whose command property has to be managed
     * @param type The type.
     *
     * @see CommandPropertySupport#TYPE_START_COMMAND
     * @see CommandPropertySupport#TYPE_STOP_COMMAND
     */
    public CommandPropertySupport(PE pe, int type) {
        this.pe = pe;
        this.type = type;

        commandEditorPanel = new PECommandEditorPanel();

        init();
    }

    @Override
    public void setValue(Object value) {
        try {
            String tmp = (String)value;
                if (tmp.toUpperCase().startsWith(PEWizardSettings.CMD_NONE) || value.equals("")) {
                    value = PEWizardSettings.CMD_NONE;
                } else {
                    value = ((String)value).trim();
                }
            setCommand(pe, type, (String) value);
            super.setValue(value);
        } catch (PEException pex) {
            ErrorDisplayer.submitWarning(pex.getMessage());
        }
    }

    /**
     * Initializes the panel
     */
    protected void init() {
        /*
         * Set the 'command' and the 'arguments' fields. The first part of the
         * pe value contains the command and the rest is the arguments.
         */
        String command = getCommand(pe, type).trim();
        String arguments = "";

        int firstWildcardPos = command.indexOf(" ");

        if (firstWildcardPos > -1) {
            arguments = command.substring(firstWildcardPos).trim();
            command = command.substring(0, firstWildcardPos).trim();
        }

        commandEditorPanel.setCommand(command);
        commandEditorPanel.setArguments(arguments);
    }

    @Override
    public Component getCustomEditor() {
        return commandEditorPanel;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    /**
     * Returns the editied command value
     * @return The command value appened with the set arguments
     */
    private String getCommandFromPanel() {
        String command = commandEditorPanel.getCommand().trim();

        if (command.equalsIgnoreCase(PEWizardSettings.CMD_NONE)  || command.equals("")) {
            return command;
        }
        return commandEditorPanel.getCommand().trim() + " " + commandEditorPanel.getArguments().trim();
    }

    /**
     * Returns the command value of the pe
     * @param pe The pe whose command value we are interested in
     * @param type The type which identifies the command type to return
     * @return The command value
     *
     * @see CommandPropertySupport#TYPE_START_COMMAND
     * @see CommandPropertySupport#TYPE_STOP_COMMAND
     */
    public static String getCommand(PE pe, int type) {
        String command = "";
        switch (type) {
            case TYPE_START_COMMAND: {
                command = pe.getStartProcArgs();
                break;
            }
            default: {
                command = pe.getStopProcArgs();
            }
        }

        return command != null ? command : PEWizardSettings.CMD_NONE;
    }

    /**
     * Sets the command value of the pe
     * @param pe The pe whose command value we want to set
     * @param type The type which identifies the command type to set
     * @param command The command to set
     *
     * @see CommandPropertySupport#TYPE_START_COMMAND
     * @see CommandPropertySupport#TYPE_STOP_COMMAND
     */
    public static void setCommand(PE pe, int type, String command) throws PEException {
        switch (type) {
            case TYPE_START_COMMAND: {
                pe.setStartProcArgs(command);
                break;
            }
            default: {
                pe.setStopProcArgs(command);
            }
        }
    }

    /**
     * Sets the command value of the pe to default
     * @param pe The pe whose command value we want to set
     * @param type The type which identifies the command type to set
     *
     * @see CommandPropertySupport#TYPE_START_COMMAND
     * @see CommandPropertySupport#TYPE_STOP_COMMAND
     * @see PEWizardSettings#START_CMD_DEFAULT
     * @see PEWizardSettings#STOP_CMD_DEFAULT
     */
    public static void setCommandToDefault(PE pe, int type) {
        try {
            switch (type) {
                case TYPE_START_COMMAND: {
                    pe.setStartProcArgs(PEWizardSettings.START_CMD_DEFAULT);
                    break;
                }
                default: {
                    pe.setStopProcArgs(PEWizardSettings.STOP_CMD_DEFAULT);
                }
            }
        } catch (PEException pex) {
            ErrorDisplayer.submitWarning(pex.getMessage());
        }
    }
    PropertyEnv env;

    public void attachEnv(PropertyEnv env) {
        this.env = env;
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addVetoableChangeListener(this);
    }

    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        try {
            setCommand(pe, type, getCommandFromPanel().trim());
        } catch (PEException pex) {
            throw new PropertyVetoException(pex.getMessage(), evt);
        }
    }
}
