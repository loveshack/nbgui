/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.tables;

import com.sun.grid.sge.api.SGEFactory;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostOverview;
import com.sun.grid.sge.api.monitor.util.NumberFormater;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import org.openide.util.NbBundle;

public class HostSummaryTableModel extends PagingTableModel<Host> {

    private Class columnClass[] = {String.class, String.class, String.class,
        String.class, String.class, String.class, String.class, String.class, String.class
    };
    
    public final String[] COLUMNS = {
        NbBundle.getMessage(HostSummaryTableModel.class, "Host"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Arch"),
        NbBundle.getMessage(HostSummaryTableModel.class, "CPU"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Mem_Used"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Mem_Total"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Swap_Used"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Swap_Total"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Virtual_Used"),
        NbBundle.getMessage(HostSummaryTableModel.class, "Virtual_Total")
    };
    private final DecimalFormat form = new DecimalFormat();

    public HostSummaryTableModel(List<Host> hosts) {
        super(hosts);
        form.setMaximumFractionDigits(2);
        form.setRoundingMode(RoundingMode.HALF_UP);
    }

    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

    public Class getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public Object getValueAt(int columnIndex, Host source) {
        HostOverview summary = (HostOverview)SGEFactory.getDefault().getModel(source);

        switch (columnIndex) {
            case 0:
                return summary.getHostname();
            case 1:
                return summary.getArch();
            case 2:
                return summary.getNumberOfProcessors();
            case 3:
                return NumberFormater.format(form, summary.getMemUsed());
            case 4:
                return NumberFormater.format(form, summary.getMemTotal());
            case 5:
                return NumberFormater.format(form, summary.getSwapUsed());
            case 6:
                return NumberFormater.format(form, summary.getSwapTotal());
            case 7:
                return NumberFormater.format(form, summary.getVirtualUsed());
            case 8:
                return NumberFormater.format(form, summary.getVirtualTotal());
            default:
                return NbBundle.getMessage(HostSummaryTableModel.class, "Error");
        }
    }
}
