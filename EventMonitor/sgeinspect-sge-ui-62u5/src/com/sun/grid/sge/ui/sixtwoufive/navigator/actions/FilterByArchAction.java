/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator.actions;

import com.sun.grid.sge.api.monitor.host.HostArchFilter;
import com.sun.grid.sge.ui.sixtwoufive.navigator.NavigatorSupport;
import com.sun.grid.sge.ui.sixtwoufive.navigator.buttons.ButtonPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public final class FilterByArchAction extends AbstractAction implements Presenter.Popup {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/arch_filter.png";
    private static final String LABEL = NbBundle.getMessage(FilterByArchAction.class, "LBL_Filter_By_Arch");
    private static FilterByArchAction instance;
    private static final String PROP_ARCH_TYPE = "hostArch";
    private int numItemsSelected = 0;
    private int itemCount = 0;
    private Icon icon;
    private String text;

    public FilterByArchAction(String text, Icon icon) {
        super(text, icon);
        this.text = text;
    }

    public void actionPerformed(ActionEvent e) {
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        HostArchFilter f = (HostArchFilter) item.getClientProperty(PROP_ARCH_TYPE);
        //set the Filter depending on what the user selected in the Popup Menu
        f.setSelected(item.isSelected());
        if (item.isSelected()) {
            numItemsSelected++;
        } else {
            numItemsSelected--;
        }
        setFilter();
        NavigatorSupport.filterApplied();
    }

    public JMenuItem getPopupPresenter() {
        numItemsSelected = 0;
        itemCount = 0;
        JMenu menu = new JMenu();

        menu.setText(text);
        menu.setIcon(icon);

        for (HostArchFilter f : HostArchFilter.values()) {
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(f.toString());
            item.setSelected(f.isSelected());
            //store the HostType this menu item represents
            //so we can fetch it in the actionPerformed()
            item.putClientProperty(PROP_ARCH_TYPE, f);
            item.addActionListener(this);
            menu.add(item);
            itemCount++;
            if (item.isSelected()) {
                numItemsSelected++;
            }
        }
        setFilter();
        return menu;
    }

    public static synchronized FilterByArchAction getInstance() {
        if (instance == null) {
            instance = new FilterByArchAction(LABEL, ImageUtilities.image2Icon(ImageUtilities.loadImage(IMAGE_PATH)));
        }
        return instance;
    }

    // if all items are selected means that this menu is not active
    private void setFilter() {
        HostArchFilter.setActive(!(itemCount == numItemsSelected));
        ButtonPanel.btnArchFilter.setSelected(!(itemCount == numItemsSelected));
    }
}
