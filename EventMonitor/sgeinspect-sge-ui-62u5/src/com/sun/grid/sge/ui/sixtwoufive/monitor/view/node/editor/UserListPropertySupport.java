/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.editor;

import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyEditorSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public class UserListPropertySupport extends PropertyEditorSupport implements ExPropertyEditor, VetoableChangeListener {

    private final PEUserEditor panel;
    private Cluster cluster;
    private PE pe;

    public UserListPropertySupport(PE pe, Cluster cluster) {
        setSource(this);
        this.cluster = cluster;
        this.pe = pe;
        panel = new PEUserEditor();
        populateList();
        panel.setButtonState();
        panel.setTextDocuments();
        panel.txtAvailable.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                change();
            }

            public void removeUpdate(DocumentEvent e) {
                change();
            }

            public void changedUpdate(DocumentEvent e) {
                change();
            }
        });
        panel.txtAllowed.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                changeAllowed();
            }

            public void removeUpdate(DocumentEvent e) {
                changeAllowed();
            }

            public void changedUpdate(DocumentEvent e) {
                changeAllowed();
            }
        });
        panel.txtDisallowed.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                changeDisallowed();
            }

            public void removeUpdate(DocumentEvent e) {
                changeDisallowed();
            }

            public void changedUpdate(DocumentEvent e) {
                changeDisallowed();
            }
        });
    }

    public void change() {
        panel.getAvailableList().setSelectedValue(panel.txtAvailable.getText(), true);
    }

    public void changeAllowed() {
        panel.getAllowedList().setSelectedValue(panel.txtAllowed.getText(), true);
    }

    public void changeDisallowed() {
        panel.getDisallowedList().setSelectedValue(panel.txtDisallowed.getText(), true);
    }

    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @Override
    public void setAsText(String s) {
        //do nothing, they must use custom editor
    }

    private void populateList() {
        List<String> userNames = new LinkedList<String>();
        List<String> xuserNames = new LinkedList<String>();
        if (pe != null) {
            List<UserSet> userList = pe.getUserList();
            for (UserSet user : userList) {
                userNames.add(user.getName());
                panel.allowedModel.addElement(user.getName());
            }

            List<UserSet> xuserList = pe.getXuserList();
            for (UserSet user : xuserList) {
                xuserNames.add(user.getName());
                panel.disallowedModel.addElement(user.getName());
            }
        }

        if (cluster != null) {
            List<UserSet> availableList = cluster.getUserList();
            for (UserSet user : availableList) {
                if (!(userNames.contains(user.getName())) && !(xuserNames.contains(user.getName()))) {
                    panel.availableModel.addElement(user.getName());
                }
            }
        }
    }

    private List<UserSet> getUserSets(DefaultListModel model) {
        List<UserSet> users = new LinkedList<UserSet>();
        if (model != null) {
            Object[] objects = model.toArray();
            List<UserSet> clusterL = cluster.getUserList();
            for (Object user : objects) {
                for (UserSet set : clusterL) {
                    if (set.getName().equals(user)) {
                        users.add(set);
                    }
                }
            }
        }

        return users;
    }

    @Override
    public Component getCustomEditor() {
        return panel;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }
    PropertyEnv env;

    public void attachEnv(PropertyEnv env) {
        this.env = env;
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addVetoableChangeListener(this);
    }

    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        pe.setUserList(getUserSets(panel.allowedModel));
        pe.setXuserList(getUserSets(panel.disallowedModel));
    }
}
