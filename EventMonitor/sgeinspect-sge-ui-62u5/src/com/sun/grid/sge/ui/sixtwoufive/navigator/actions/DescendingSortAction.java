/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.sixtwoufive.navigator.actions;

import com.sun.grid.sge.ui.sixtwoufive.navigator.NavigatorSupport;
import com.sun.grid.sge.ui.sixtwoufive.navigator.buttons.ButtonPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public class DescendingSortAction extends AbstractAction implements Presenter.Popup {

    private static final String IMAGE_PATH = "com/sun/grid/sge/ui/resources/sort_descending.png";
    private static final String LABEL = NbBundle.getMessage(DescendingSortAction.class, "LBL_Sort_Desc");
    private static DescendingSortAction instance;
    JRadioButtonMenuItem descending;

    public DescendingSortAction(String text, Icon icon) {
        super(text, icon);
        descending = new JRadioButtonMenuItem(text, icon);
    }

    public void actionPerformed(ActionEvent e) {
        boolean selected = true;

        if (e.getSource() instanceof JToggleButton) {
            selected = ((JToggleButton) e.getSource()).isSelected();
        } else if (e.getSource() instanceof JMenuItem) {
            selected = ((JMenuItem) e.getSource()).isSelected();
        }

        ButtonPanel.btnSortAsc.setSelected(!selected);
        ButtonPanel.btnSortDesc.setSelected(selected);
        AscendingSortAction.getInstance().ascending.setSelected(!selected);
        descending.setSelected(selected);

        NavigatorSupport.setSort(NavigatorSupport.SORT_DESC);
    }

    public JMenuItem getPopupPresenter() {
        descending.addActionListener(this);

        return descending;
    }

    public static synchronized DescendingSortAction getInstance() {
        if (instance == null) {
            instance = new DescendingSortAction(LABEL, ImageUtilities.image2Icon(ImageUtilities.loadImage(IMAGE_PATH)));
        }
        return instance;
    }
}
