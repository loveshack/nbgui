/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.sge.libs.sixtwoufive;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ClassLoader anchor
 */
public class SGESixTwoUFive {

    private static final Lock cllock = new ReentrantLock();

    public static void executeInSGEVersionedClassLoader(CommandBlock block) {
        cllock.lock();
        ClassLoader orig = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(SGESixTwoUFive.class.getClassLoader());
        try {
            block.execute();
        } finally {
            Thread.currentThread().setContextClassLoader(orig);
            cllock.unlock();
        }
    }

    public static interface CommandBlock {

        public void execute();
    }
}
