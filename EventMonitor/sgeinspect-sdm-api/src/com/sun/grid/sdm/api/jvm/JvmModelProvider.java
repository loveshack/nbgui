/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.api.jvm;

import com.sun.tools.visualvm.application.Application;
import com.sun.tools.visualvm.application.jvm.Jvm;
import com.sun.tools.visualvm.application.jvm.JvmFactory;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JvmModelProvider extends AbstractModelProvider<JvmModel, Application> {

    private static final String BUNDLE = "com.sun.grid.sdm.api.jvm.Bundle";
    private static final Logger log = Logger.getLogger(JvmModelProvider.class.getName(), BUNDLE);
    private static final Pattern csUrlPattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.csInfo=(.+?)\\s", 40);
    private static final Pattern prefTypePattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.preferencesType=(.+?)\\s", 40);
    private static final Pattern systemNamePattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.systemname=(.+?)\\s", 40);
    private static final Pattern csFlagPattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.isCS=(.+?)\\s", 40);
    private static final Pattern sslFlagPattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.no_ssl=(.+?)\\s", 40);
    private static final Pattern spoolDirPattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.localspool=(.+?)\\s", 40);
    private static final Pattern distDirPattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.dist=(.+?)\\s", 40);
    private static final Pattern jvmNamePattern = Pattern.compile("-Dcom\\.sun\\.grid\\.grm\\.bootstrap\\.jvmname=(.+?)\\s", 40);

    private static final class Singleton {

        private static final JvmModelProvider INSTANCE = new JvmModelProvider();

        private Singleton() {
        }
    }

    public JvmModel createModelFor(Application app) {
        Jvm jvm = JvmFactory.getJVMFor(app);
        if (jvm != null) {
            String args = null;
            try {
                args = jvm.getJvmArgs();
            } catch (Exception ex) {
                log.log(Level.INFO, "MSG_null_args", app);
            }
            System.out.println("args: " + args);
            if (args != null) {
                Matcher urlMatcher = csUrlPattern.matcher(args);
                Matcher jvmMatcher = jvmNamePattern.matcher(args);
                Matcher prefMatcher = prefTypePattern.matcher(args);
                Matcher snameMatcher = systemNamePattern.matcher(args);
                Matcher serviceMatcher = csFlagPattern.matcher(args);
                Matcher sslFlagMatcher = sslFlagPattern.matcher(args);
                Matcher spoolDirMatcher = spoolDirPattern.matcher(args);
                Matcher distDirMatcher = distDirPattern.matcher(args);
                if (jvmMatcher.find() && urlMatcher.find() && prefMatcher.find() && snameMatcher.find() && serviceMatcher.find() && spoolDirMatcher.find() && distDirMatcher.find()) {
                    if (sslFlagMatcher.find()) {
                        return new JvmModel(snameMatcher.group(1), jvmMatcher.group(1), urlMatcher.group(1), prefMatcher.group(1), spoolDirMatcher.group(1), distDirMatcher.group(1), sslFlagMatcher.group(1), serviceMatcher.group(1));
                    } else {
                        return new JvmModel(snameMatcher.group(1), jvmMatcher.group(1), urlMatcher.group(1), prefMatcher.group(1), spoolDirMatcher.group(1), distDirMatcher.group(1), "false", serviceMatcher.group(1));
                    }
                }
            }
        }
        return null;
    }

    @SuppressWarnings(value = "unchecked")
    public static void initialize() {
        JvmModelFactory.getDefault().registerProvider(Singleton.INSTANCE);
    }

    @SuppressWarnings(value = "unchecked")
    public static void shutdown() {
        JvmModelFactory.getDefault().unregisterProvider(Singleton.INSTANCE);
    }
}
