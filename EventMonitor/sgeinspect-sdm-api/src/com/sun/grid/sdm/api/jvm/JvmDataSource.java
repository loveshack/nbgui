/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.api.jvm;

import com.sun.grid.sdm.api.base.VersionableDataSource;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.application.Application;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.Stateful;
import com.sun.tools.visualvm.jmx.JmxApplicationException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class to allow to show the SDM JVMs.
 * 
 */
public abstract class JvmDataSource extends VersionableDataSource implements Stateful {

    private static final String BUNDLE = "com.sun.grid.sdm.api.jvm.Bundle";
    private static final Logger log = Logger.getLogger(JvmDataSource.class.getName(), BUNDLE);
    public static Map<Application, JvmDataSource> appWrapPairs = new HashMap<Application, JvmDataSource>();
    /** a map of all contained services */
    protected final Map<String, ServiceDataSource> services;
    /** a map of all contained components */
    protected final Map<String, ComponentDataSource> components;
    /** a map of all contained non-cs-jvms */
    protected final Map<String, JvmDataSource> jvms;
    /** convenient mapping of jvm@host */
    protected final Map<String, List<String>> jvmHosts;
    private final String jvmName;
    private final String jvmHost;
    private final Application app;
    private final ApplicationStateListener asl = new ApplicationStateListener();
    private final boolean isCS;

    public JvmDataSource(String jvmName, String jvmHost, Application appl, boolean isCS) throws JmxApplicationException {
        this.app = appl;
        this.app.addPropertyChangeListener(asl);
        this.services = new HashMap<String, ServiceDataSource>();
        this.components = new HashMap<String, ComponentDataSource>();
        this.jvmHost = jvmHost;
        this.jvmName = jvmName;
        this.jvms = new HashMap<String, JvmDataSource>();
        this.jvmHosts = new HashMap<String, List<String>>();
        this.isCS = isCS;
        appWrapPairs.put(app, this);
    }

    public Application getApplication() {
        return app;
    }

    public String getName() {
        return jvmName;
    }

    public String getHost() {
        return jvmHost;
    }

    public boolean isCS() {
        return isCS;
    }

    @Override
    public void remove() {
        super.remove();
        appWrapPairs.remove(getApplication());
    }

    @Override
    public String toString() {
        return JvmDataSourceDescriptorProvider.INSTANCE.createModelFor(this).getName();
    }

    /**
     * Helper method to format JMXServiceURL string.
     *
     * @param env execution environment of sdm system
     * @param jvm an active jvm instance
     * @return a formated string usable for jmx application creation
     */
    private String formatJvmId(String jvmName, String hostname) {
        final String pattern = "%s@%s";
        return String.format(pattern, jvmName, hostname);
    }

    public ComponentDataSource getComponentDataSource(String componentName) {
        return components.get(componentName);
    }

    public ServiceDataSource getServiceDataSource(String serviceName) {
        return services.get(serviceName);
    }

    public JvmDataSource getJvmDataSource(String jvmName, String hostname) {
        return jvms.get(formatJvmId(jvmName, hostname));
    }

    private class ApplicationStateListener implements PropertyChangeListener {

        /**
         * temporary fix (=to do nothing)- there is probably a bug in visualvm. needs to be solved
         * in september 09.
         * // TODO fix this with visualvm team
         * @param evt
         */
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt != null && evt.getPropertyName().equals(Stateful.PROPERTY_STATE)) {
                log.log(Level.FINE, "MSG_application_state_changed", new Object[]{JvmDataSource.this, app.getState()});
                if (app.getState() != Stateful.STATE_AVAILABLE) {
                    DataSource.EVENT_QUEUE.post(new Runnable() {

                        public void run() {
//                            if (JvmDataSource.this.getOwner() != null && !JvmDataSource.this.isRemoved()) {
//                                JvmDataSource.this.getOwner().getRepository().removeDataSource(JvmDataSource.this);
//                            }
                        }
                    });
                    if (isCS()) {
//                        submitWarning(NbBundle.getMessage(JvmDataSource.class, "MSG_CS_JVM_shutdown", toString()));
                    }
                }
            }
        }
    }
}


