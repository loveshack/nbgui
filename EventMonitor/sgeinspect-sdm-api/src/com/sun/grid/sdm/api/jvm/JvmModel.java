/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.api.jvm;

import com.sun.tools.visualvm.core.model.Model;

public class JvmModel extends Model {

    private final String jvmName;
    private final String systemName;
    private final String connectionInfo;
    private final String prefType;
    private final String spoolDir;
    private final String distDir;
    private final boolean isCS;
    private final boolean sslFlag;

    public JvmModel(String systemName, String jvmName, String connInfo, String prefType, String spoolDir, String distDir, String isNoSslFlag, String isServiceFlag) {
        this.systemName = systemName;
        this.jvmName = jvmName;
        this.connectionInfo = connInfo;
        this.prefType = prefType;
        this.isCS = Boolean.valueOf(isServiceFlag).booleanValue();
        this.spoolDir = spoolDir;
        this.distDir = distDir;
        this.sslFlag = !Boolean.valueOf(isNoSslFlag).booleanValue();
    }

    public String getSystemName() {
        return systemName;
    }

    public String getJVMName() {
        return jvmName;
    }

    public String getConnectionInfo() {
        return connectionInfo;
    }

    public String getPreferencesType() {
        return prefType;
    }

    public boolean isCS() {
        return isCS;
    }

    public String getDistDir() {
        return distDir;
    }

    public String getSpoolDir() {
        return spoolDir;
    }

    public boolean isSsl() {
        return sslFlag;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.connectionInfo != null ? this.connectionInfo.hashCode() : 0);
        hash = 37 * hash + (this.distDir != null ? this.distDir.hashCode() : 0);
        hash = 37 * hash + (this.jvmName != null ? this.jvmName.hashCode() : 0);
        hash = 37 * hash + (this.prefType != null ? this.prefType.hashCode() : 0);
        hash = 37 * hash + (this.spoolDir != null ? this.spoolDir.hashCode() : 0);
        hash = 37 * hash + (this.systemName != null ? this.systemName.hashCode() : 0);
        hash = 37 * hash + (this.isCS ? 1 : 0);
        hash = 37 * hash + (this.sslFlag? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JvmModel other = (JvmModel) obj;
        if ((this.jvmName == null) ? (other.jvmName != null) : !this.jvmName.equals(other.jvmName)) {
            return false;
        }
        if ((this.systemName == null) ? (other.systemName != null) : !this.systemName.equals(other.systemName)) {
            return false;
        }
        if ((this.connectionInfo == null) ? (other.connectionInfo != null) : !this.connectionInfo.equals(other.connectionInfo)) {
            return false;
        }
        if ((this.prefType == null) ? (other.prefType != null) : !this.prefType.equals(other.prefType)) {
            return false;
        }
        if ((this.spoolDir == null) ? (other.spoolDir != null) : !this.spoolDir.equals(other.spoolDir)) {
            return false;
        }
        if ((this.distDir == null) ? (other.distDir != null) : !this.distDir.equals(other.distDir)) {
            return false;
        }
        if (this.isCS != other.isCS) {
            return false;
        }
        if (this.sslFlag != other.sslFlag) {
            return false;
        }
        return true;
    }
}
