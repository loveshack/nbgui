/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.api.base;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.jvm.JvmModel;
import com.sun.grid.sdm.api.jvm.JvmModelFactory;
import com.sun.tools.visualvm.application.Application;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import com.sun.tools.visualvm.core.model.ModelProvider;
import java.awt.Image;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

/**
 * Toplevel node 'SDM Systems' in the Applications window.
 *
 */
public class SDMContainer extends DataSource {

    private static SDMContainer sharedInstance;
    private final ModelProvider<DataSourceDescriptor, DataSource> dsProvider;
    private final DataChangeListener<JvmDataSource> dsListener;
    private final Set<String> monitoredSystems;
    private final Set<String> removedSystems;
    private final Lock lock;

    /**
     * Helper method that exposes shared instance.
     *
     * @return a shared (singleton) instance of SDMContainer
     */
    public static synchronized SDMContainer sharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new SDMContainer();
        }
        return sharedInstance;
    }

    private SDMContainer() {
        dsProvider = new SDMContainerDescriptorProvider();
        dsListener = new SDMWrapperDSListener();
        monitoredSystems = new HashSet<String>();
        removedSystems = new HashSet<String>();
        lock = new ReentrantLock();
    }

    private static class SDMContainerDescriptorProvider extends AbstractModelProvider<DataSourceDescriptor, DataSource> {

        @Override
        public DataSourceDescriptor createModelFor(DataSource ds) {
            if (SDMContainer.sharedInstance().equals(ds)) {
                return new SDMContainerDescriptor();
            }
            return null;
        }

        private static class SDMContainerDescriptor extends DataSourceDescriptor<SDMContainer> {

            private static final Image CLUSTER_NODE_ICON = ImageUtilities.loadImage(
                    "com/sun/grid/sdm/api/resources/SDMcontainer.png", true); // NOI18N

            SDMContainerDescriptor() {
                super(SDMContainer.sharedInstance(),
                        NbBundle.getMessage(SDMContainer.class, "LBL_SDM"),
                        null,
                        CLUSTER_NODE_ICON,
                        //                        "com/sun/grid/gui/monitor/cluster/resources/cluster.png",
                        10,
                        EXPAND_ON_EACH_NEW_CHILD);
            }
        }
    }

    public void addSDMWrapperDS(JvmDataSource ds) {
        lock.lock();
        try {
            final JvmModel am = JvmModelFactory.getDefault().getModel(ds.getApplication());
            if (am != null) {
                final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                monitoredSystems.add(key);
                getRepository().addDataSource(ds);
            }
        } finally {
            lock.unlock();
        }
    }

    public void removeSDMWrapperDS(JvmDataSource ds) {
        lock.lock();
        try {
            final JvmModel am = JvmModelFactory.getDefault().getModel(ds.getApplication());
            if (am != null) {
                final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                monitoredSystems.remove(key);
                removedSystems.add(key);
                getRepository().removeDataSource(ds);
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean registerSDMSystemForApplication(Application aA, boolean reAdd) {
        lock.lock();
        try {
            final JvmModel am = JvmModelFactory.getDefault().getModel(aA);
            if (am != null) {
                final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                if (removedSystems.contains(key) && !reAdd) {
                    return false;
                } else {
                    removedSystems.remove(key);
                    boolean b = monitoredSystems.add(key);
                    return b;
                }
            } else {
                return false;
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean unregisterSDMSystemForApplication(Application aA) {
        lock.lock();
        try {
            final JvmModel am = JvmModelFactory.getDefault().getModel(aA);
            if (am != null) {
                final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                return monitoredSystems.remove(key);
            } else {
                return false;
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean registerSDMSystemForApplication(String systemName, String connectionInfo, boolean reAdd) {
        lock.lock();
        try {
            final String key = createSDMSystemKey(systemName, connectionInfo);
            if (removedSystems.contains(key) && !reAdd) {
                return false;
            } else {
                removedSystems.remove(key);
                boolean b = monitoredSystems.add(key);
                return b;
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean unregisterSDMSystemForApplication(String systemName, String connectionInfo) {
        lock.lock();
        try {
            final String key = createSDMSystemKey(systemName, connectionInfo);
            return monitoredSystems.remove(key);
        } finally {
            lock.unlock();
        }
    }

    private String createSDMSystemKey(String systemName, String conectionInfo) {
        final String pattern = "%s_%s";
        return String.format(pattern, systemName, conectionInfo);
    }

    public void initialize() {
        DataSourceDescriptorFactory.getDefault().registerProvider(dsProvider);
        DataSourceRepository.sharedInstance().addDataChangeListener(dsListener, JvmDataSource.class);
    }

    public void shutdown() {
        DataSourceDescriptorFactory.getDefault().unregisterProvider(dsProvider);
        DataSourceRepository.sharedInstance().removeDataChangeListener(dsListener);
    }

    private class SDMWrapperDSListener implements DataChangeListener<JvmDataSource> {

        public void dataChanged(DataChangeEvent<JvmDataSource> event) {
            for (final JvmDataSource ds : event.getAdded()) {
                final JvmModel am = JvmModelFactory.getDefault().getModel(ds.getApplication());
                if (am != null && am.isCS()) {
                    final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                    monitoredSystems.add(key);
                }
            }
            for (final JvmDataSource ds : event.getRemoved()) {
                final JvmModel am = JvmModelFactory.getDefault().getModel(ds.getApplication());
                if (am != null && am.isCS()) {
                    final String key = createSDMSystemKey(am.getSystemName(), am.getConnectionInfo());
                    monitoredSystems.remove(key);
                }
            }
        }
    }
}
