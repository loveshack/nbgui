/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.api.jvm;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup.Template;
import org.openide.util.lookup.AbstractLookup;

/**
 * A lookup for JvmDataSourceFactories
 * 
 */
public class JvmDataSourceFactoryLookup extends AbstractLookup {

    private static final long serialVersionUID = -2009100101L;
    private static JvmDataSourceFactoryLookup INSTANCE = null;
    private Factory.Type lookupType;

    private JvmDataSourceFactoryLookup(Factory.Type lookupType) {
        this.lookupType = lookupType;
    }

    public static JvmDataSourceFactoryLookup getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JvmDataSourceFactoryLookup(Factory.Type.SGE);
        }

        return INSTANCE;
    }

    @Override
    protected void beforeLookup(Template<?> tmplt) {
        super.beforeLookup(tmplt);
    }

    public void registerFactory(JvmDataSourceFactory instance) {
        for (String v : instance.getVersions()) {
            Factory s = new Factory(new FactoryVersion(v), lookupType, instance.getClass());
            addPair(new P(s, instance));
        }

    }

    public JvmDataSourceFactory getFactory(String version) {
//        Factory s = new Factory(new FactoryVersion(version), lookupType, HelloFactory.class);
        Result<JvmDataSourceFactory> result = lookup(new Template<JvmDataSourceFactory>(JvmDataSourceFactory.class));
        @SuppressWarnings("unchecked")
        Collection<P> pairs = (Collection<P>) result.allItems();

        for (P pair : pairs) {
            if (version.equals(pair.service.version.getVersion())) {
                return pair.getInstance();
            }
        }

        return null;
    }

    public Collection<String> getAllRegisteredFactoryVersions() {
        Result<JvmDataSourceFactory> result = lookup(new Template<JvmDataSourceFactory>(JvmDataSourceFactory.class));
        @SuppressWarnings("unchecked")
        Collection<P> pairs = (Collection<P>) result.allItems();

        Set<String> versions = new HashSet<String>();
        for (P pair : pairs) {
            versions.add(pair.service.getVersion().getVersion());
        }

        return versions;
    }

    private static final class P extends AbstractLookup.Pair<JvmDataSourceFactory> {

        private static final long serialVersionUID = -2009100101L;
        Factory service;
        JvmDataSourceFactory instance;

        public P(Factory service, JvmDataSourceFactory instance) {
            this.service = service;
            this.instance = instance;
        }

        @Override
        protected boolean instanceOf(Class<?> clazz) {
            return clazz.isAssignableFrom(instance.getClass());
        }

        @Override
        protected boolean creatorOf(Object obj) {
            return obj == instance;
        }

        @Override
        public JvmDataSourceFactory getInstance() {
            return instance;
        }

        @Override
        public Class<? extends JvmDataSourceFactory> getType() {
            return instance.getClass();
        }

        @Override
        public String getId() {
            return service.getId();
        }

        @Override
        public String getDisplayName() {
            return service.getDisplayName();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final P other = (P) obj;
            if (this.service != other.service && (this.service == null || !this.service.equals(other.service))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + (this.service != null ? this.service.hashCode() : 0);
            return hash;
        }
    }

    private static final class FactoryVersion {

        private String version;

        public FactoryVersion(String version) {
            this.version = version;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return version;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final FactoryVersion other = (FactoryVersion) obj;
            if ((this.version == null) ? (other.version != null) : !this.version.equals(other.version)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + (this.version != null ? this.version.hashCode() : 0);
            return hash;
        }
    }

    private static class Factory {

        private FactoryVersion version = null;
        private Type type = null;
        private Class<?> clazz;
        private String id;

        protected enum Type {

            SGE,
            SDM
        }

        public Factory(FactoryVersion version, Type type, Class<?> clazz) {
            this.version = version;
            this.type = type;
            this.clazz = clazz;
            this.id = type + ":" + clazz + "@" + version;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public Type getType() {
            return type;
        }

        public FactoryVersion getVersion() {
            return version;
        }

        public String getId() {
            return id;
        }

        public String getDisplayName() {
            return getId();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Factory other = (Factory) obj;
            if (!this.getId().equals(other.getId())) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
            return hash;
        }
    }
}
