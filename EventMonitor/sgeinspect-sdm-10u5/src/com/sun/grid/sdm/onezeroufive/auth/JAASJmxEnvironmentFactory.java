/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.auth;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.security.ClientSecurityContext;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.tools.visualvm.jmx.JmxApplicationException;
import com.sun.tools.visualvm.jmx.JmxEnvironmentFactory;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.callback.CallbackHandler;

/**
 * A wrapper for DefaultClientSecurityContext
 *
 */
public class JAASJmxEnvironmentFactory implements JmxEnvironmentFactory {

    private static final String BUNDLE = "com.sun.grid.sdm.onezeroufive.auth.Bundle";
    private static final Logger log = Logger.getLogger(JAASJmxEnvironmentFactory.class.getName(), BUNDLE);
    private ClientSecurityContext csc;

    public JAASJmxEnvironmentFactory(final ExecutionEnv env, final String defaultUserName, final CallbackHandler ch) throws JmxApplicationException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, JmxApplicationException>() {

            public Void execute() throws JmxApplicationException {
                try {
                    log.log(Level.INFO, "MSG_create_client_context", new Object[]{env, defaultUserName, ch});
                    csc = Modules.getSecurityModule().createClientContext(env, defaultUserName, ch);
                } catch (GrmSecurityException ex) {
                    throw new JmxApplicationException(ex.getLocalizedMessage(), ex);
                }
                return null;
            }
        });
    }

    public Map<String, Object> createJmxEnvironment(CallbackHandler ch) throws JmxApplicationException {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Map<String, Object>, JmxApplicationException>() {

            public Map<String, Object> execute() throws JmxApplicationException {
                try {
                    Map<String, Object> env = csc.createJMXEnvironment();
                    env.put("jmx.remote.default.class.loader", Thread.currentThread().getContextClassLoader());
                    return env;
                } catch (GrmSecurityException ex) {
                    throw new JmxApplicationException(ex.getLocalizedMessage(), ex);
                }
            }
        });
    }
}
