/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.jvm;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.security.CachingClientSecurityContextFactory;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSourceCreationException;
import com.sun.grid.sdm.api.jvm.JvmDataSourceFactory;
import com.sun.grid.sdm.api.jvm.JvmDataSourceFactoryLookup;
import com.sun.grid.sdm.api.jvm.JvmDataSourceVersionException;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.auth.JAASJmxEnvironmentFactory;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import com.sun.tools.visualvm.jmx.JmxApplicationException;
import com.sun.tools.visualvm.jmx.JmxApplicationsSupport;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.NbBundle;

/**
 * A factory class to create instances of JvmDataSource
 * 
 */
public class JvmDataSourceFactoryImpl implements JvmDataSourceFactory {

    private static final String BUNDLE = "com.sun.grid.sdm.onezeroufive.jvm.Bundle";
    private static final Logger log = Logger.getLogger(JvmDataSourceFactoryImpl.class.getName(), BUNDLE);
    private final static JvmDataSourceFactoryImpl INSTANCE = new JvmDataSourceFactoryImpl();
    private static final CallbackHandler dummyHandler = new DummyCallbackHandler();
    private static final CachingClientSecurityContextFactory clientResourceFactory = ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<CachingClientSecurityContextFactory>() {

        public CachingClientSecurityContextFactory execute() {
            try {
                return new CachingClientSecurityContextFactory();
            } catch (Exception ex) {
                ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
            }
            return null;
        }
    });

    private JvmDataSourceFactoryImpl() {
    }

    public List<String> getVersions() {
        return Collections.<String>singletonList(Version.getVersions());
    }

    public JvmDataSource createJvmDataSource(final String systemname, final String preferencestype, final String name, final String host, final int port) throws JvmDataSourceCreationException, JvmDataSourceVersionException {
        final ProgressHandle p = ProgressHandleFactory.createHandle(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_Adding_system"));
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                p.setInitialDelay(0);
                p.start(1);
            }
        });
        final String urlPattern = "service:jmx:rmi://%s:%d/jndi/rmi://%s:%d/%s"; // NOI18N
        String displayNamePattern = "name= %s, host= %s"; // NOI18N
        final String displayname = String.format(displayNamePattern, name, host);

        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlockWithExceptions<JvmDataSource, JvmDataSourceCreationException, JvmDataSourceVersionException>() {

            public JvmDataSource execute() throws JvmDataSourceCreationException, JvmDataSourceVersionException {
                try {
                    PreferencesType pt = PreferencesType.valueOf(preferencestype);
                    String csinfo = PreferencesUtil.getCSInfo(systemname, pt);
                    HostAndPort hap = HostAndPort.newInstance(csinfo);

                    clientResourceFactory.registerContext(hap, System.getProperty("user.name"), dummyHandler);
                    log.log(Level.FINE, "registering context for: " + hap + ", " + System.getProperty("user.name"));

                    ExecutionEnv env = ExecutionEnvFactory.newClientInstance(systemname, pt, clientResourceFactory);
                    log.log(Level.INFO, "created executionenv: " + env + ", for: " + systemname + ", " + pt + " and client resource factory: " + clientResourceFactory);

                    String userName = System.getProperty("user.name"); // NOI18N
                    JAASJmxEnvironmentFactory jmxEnvFactory = new JAASJmxEnvironmentFactory(env, userName, dummyHandler);

                    checkVersion(env);

                    ActiveJvm jvm = getCSActiveJvm(env);
                    String url = String.format(urlPattern, jvm.getHost(), jvm.getPort(), jvm.getHost(), jvm.getPort(), env.getSystemName());

                    Application csa = JmxApplicationsSupport.getInstance().createJmxApplication(url, displayname, jmxEnvFactory, null, false, false);
                    csa.setVisible(false);
                    JvmDataSourceImpl csd = new JvmDataSourceImpl(jvm.getName(), jvm.getHost(), csa, true, dummyHandler);
                    csd.initialize();
                    return csd;
                } catch (BackingStoreException ex) {
                    throw new JvmDataSourceCreationException("backing store error: ", ex);
                } catch (PreferencesNotAvailableException ex) {
                    throw new JvmDataSourceCreationException("preferences error: ", ex);
                } catch (GrmSecurityException ex) {
                    throw new JvmDataSourceCreationException("security error: ", ex);
                } catch (GrmException ex) {
                    throw new JvmDataSourceCreationException("sdm error: ", ex);
                } catch (JmxApplicationException ex) {
                    throw new JvmDataSourceCreationException("jmx support error: ", ex);
                } finally {
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            p.progress(1);
                            p.finish();
                        }
                    });
                }
            }
        });
    }

    public static void initialize() {
        JvmDataSourceFactoryLookup.getInstance().registerFactory(INSTANCE);
    }

    /**
     * Helper method to pick a CS vm from active JVMs.
     *
     * @param env execution env
     * @return an ActiveJvm of CS or null
     */
    private ActiveJvm getCSActiveJvm(final ExecutionEnv env) throws JvmDataSourceCreationException {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlockWithException<ActiveJvm, JvmDataSourceCreationException>() {

            public ActiveJvm execute() throws JvmDataSourceCreationException {
                try {
                    List<ActiveJvm> jvmsList = Collections.<ActiveJvm>emptyList();
                    jvmsList = env.getCommandService().execute(new GetActiveJVMListCommand()).getReturnValue();
                    for (ActiveJvm jvm : jvmsList) {
                        if (SystemUtil.isCSJvmHostAndPort(env, jvm.getHost(), jvm.getPort())) {
                            return jvm;
                        }
                    }
                } catch (Exception e) {
                    throw new JvmDataSourceCreationException(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_Is_system_{0}_running", env.getSystemName()), e);
                }
                throw new JvmDataSourceCreationException(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_No_active_csvm_in_system_{0}", env.getSystemName()));
            }
        });
    }

    /**
     * Helper method to pick a CS vm from active JVMs.
     *
     * @param env execution env
     * @return an ActiveJvm of CS or null
     */
    private void checkVersion(final ExecutionEnv env) throws JvmDataSourceVersionException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, JvmDataSourceVersionException>() {

            public Void execute() throws JvmDataSourceVersionException {
                String version = "";
                try {
                    version = ComponentServiceWithClassLoader.getCS(env).getVersion().substring(0, 5);
                } catch (GrmRemoteException ex) {
                    if (ex.getCause() instanceof IllegalStateException) {
                        throw new JvmDataSourceVersionException(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_incorrect_version", env.getSystemName()), ex);
                    } else {
                        log.log(Level.INFO, "can not get version, cs is not running", ex);
                        // silently ignore, cs is not running - is handled elsewhere
                        return null;
                    }
                } catch (UndeclaredThrowableException ex) {
                    if (ex.getCause().getCause() instanceof IllegalStateException) {
                        throw new JvmDataSourceVersionException(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_incorrect_version", env.getSystemName()), ex);
                    } else {
                        log.log(Level.INFO, "can not get version, cs is not running", ex);
                        // silently ignore, cs is not running - is handled elsewhere
                        return null;
                    }
                } catch (GrmException ex) {
                    log.log(Level.INFO, "version is ok, but other error has occured", ex);
                    // silently ignore  - is handled elsewhere
                    return null;
                } catch (Exception ex) {
                    log.log(Level.INFO, "can not get version, cs is not running", ex);
                    // silently ignore, cs is not running - is handled elsewhere
                    return null;
                }
                if (!Version.getVersions().contains(version)) {
                    throw new JvmDataSourceVersionException(NbBundle.getMessage(JvmDataSourceFactoryImpl.class, "LBL_unsupported_version", new Object[]{env.getSystemName(), version}));
                } else {
                    return null;
                }
            }
        });
    }

    private static class DummyCallbackHandler implements CallbackHandler {

        protected void handle(Callback cb) throws IOException, UnsupportedCallbackException {
            String user = System.getProperty("user.name");
            String pass = "";
            if (cb instanceof PasswordCallback) {
                PasswordCallback pwCallback = (PasswordCallback) cb;
                pwCallback.setPassword(pass.toCharArray());
            } else if (cb instanceof NameCallback) {
                NameCallback ncb = (NameCallback) cb;
                ncb.setName(user);
            } else {
                throw new UnsupportedCallbackException(cb);
            }
        }

        public void handle(Callback callbacks[]) throws IOException, UnsupportedCallbackException {
            Callback arr$[] = callbacks;
            int len$ = arr$.length;
            for (int i$ = 0; i$ <
                    len$; i$++) {
                Callback cb = arr$[i$];
                handle(cb);
            }
        }
    }
}


