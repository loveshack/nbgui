/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.component.rp;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.ResourceProviderNotActiveException;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.component.ComponentDataSourceSupport;
import com.sun.grid.sdm.onezeroufive.resource.ResourceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.util.HistorySupport;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResourceProviderDataSourceImpl extends ResourceProviderDataSource implements HistoryObservable<Object[], Filter<Report>> {

    private final static Logger LOGGER = Logger.getLogger(ResourceProviderDataSourceImpl.class.getName());
    private final Map<ResourceId, ResourceDataSourceImpl> resources = new HashMap<ResourceId, ResourceDataSourceImpl>();
    private final RPL rpl = new RPL();
    private final RPCL rpcl = new RPCL();
    private final ComponentDataSourceSupport cdss;
    private final ExecutionEnv e;
    private final ResourceProvider rp;
    private final HistorySupport hs;
    private final AtomicLong nextExpectedEventSequenceNumber = new AtomicLong(-1);

    public ResourceProviderDataSourceImpl(ExecutionEnv env, String sName, String jvmName, String host, ResourceProvider c) {
        super(sName, jvmName, host);
        this.e = env;
        this.rp = c;
        this.hs = new HistorySupport(env);
        this.cdss = new ComponentDataSourceSupport(c, getChangeSupport());
        EVENT_QUEUE.post(new RefreshAction());
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                try {
                    rp.addResourceProviderEventListener(rpl);
                    rp.addComponentEventListener(rpcl);
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                }
                return null;
            }
        });
    }

    @Override
    public void getHistory(Filter<Report> f) {
        hs.getHistory(f);
    }

    @Override
    public void cancelHistory() {
        hs.cancelHistory();
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public String getVersionName(String version) {
        return Version.getVersionName(version);
    }

    public void addHistoryListener(HistoryListener<Object[]> listener) {
        hs.addHistoryListener(listener);
    }

    public void removeHistoryListener(HistoryListener<Object[]> listener) {
        hs.removeHistoryListener(listener);
    }

    @Override
    public String getComponentState() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return rp.getState().name();
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    return ComponentState.UNKNOWN.name();
                }
            }
        });
    }

    private class RPL implements ResourceProviderEventListener {

        public void addResource(final AddResourceEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceAdded(final ResourceAddedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceRejected(final ResourceRejectedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RemoveResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void removeResource(final RemoveResourceEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceRemoved(final ResourceRemovedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RemoveResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceError(final ResourceErrorEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceReset(final ResourceResetEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }

        public void resourceChanged(final ResourceChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    return null;
                }
            });
        }
    }

    private class RPCL implements ComponentEventListener {

        public void connectionClosed() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void connectionFailed() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void eventsLost() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentUnknown(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentStarting(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentStarted(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentStopping(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentStopped(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }

        public void componentReloading(ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    EVENT_QUEUE.post(new RefreshAction());
                    return null;
                }
            });
        }
    }

    private abstract class Action implements Runnable {

        /**
         * All 'unexpected' exceptions are consumed and logged by {@link Action#run()}  method.
         *
         * @throws java.lang.Exception if unexpected error occurs.
         */
        public abstract void execute() throws Exception;

        public final void run() {
            try {
//                if (LOGGER.isLoggable(Level.FINE)) {
//                    LOGGER.log(Level.FINE, "scp.action.exe", new Object[]{name, this.toString()});
//                }
                execute();
            } catch (Exception ex) {
//                if (LOGGER.isLoggable(Level.WARNING)) {
//                    LogRecord lr = new LogRecord(Level.WARNING, "scp.a.unexpected");
//                    lr.setParameters(new Object[]{name, this, ex});
//                    lr.setThrown(ex);
//                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
//                    LOGGER.log(lr);
//                }
            } finally {
            }
//            if (LOGGER.isLoggable(Level.FINE)) {
//                LOGGER.log(Level.FINE, "scp.action.fini", new Object[]{name, this.toString()});
//            }
        }
    }

    /**
     * Abstract base class of all actions which has been triggered by a ServiceEvent
     * or a ComponentEvent.
     */
    private abstract class AbstractEventTriggeredAction extends Action {

        /**
         * Get the sequence number of the event
         * @return the sequence number
         */
        protected abstract long getEventSequenceNumber();

        @Override
        public final void execute() throws Exception {
            long seqNo = getEventSequenceNumber();
            long expSeqNo = nextExpectedEventSequenceNumber.get();
            if (expSeqNo == -1) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ignore.initial", new Object[]{"resource_provider", this.toString()});
                }
            } else if (seqNo == 0 && expSeqNo > 0) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.restart",
                            new Object[]{"resource_provider", this.toString(), seqNo, expSeqNo});
                }
                // A new life cylce of the service has started
                DataSource.EVENT_QUEUE.post(new RefreshAction());
            } else if (seqNo < expSeqNo) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.outdated",
                            new Object[]{"resource_provider", this.toString(), seqNo, expSeqNo});
                }
            } else if (seqNo > expSeqNo) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.unexpected",
                            new Object[]{"resource_provider", this.toString(), seqNo, expSeqNo});
                }
                DataSource.EVENT_QUEUE.post(new RefreshAction());
            } else {
                nextExpectedEventSequenceNumber.incrementAndGet();
                executeAction();
            }
        }

        protected abstract void executeAction() throws Exception;
    }

    /**
     * Abstract base class of all actions triggered by a service event
     * @param <T> the type of the event
     */
    private abstract class AbstractServiceEventAction<T extends AbstractServiceEvent> extends AbstractEventTriggeredAction {

        protected final T event;

        protected AbstractServiceEventAction(T event) {
            if (event == null) {
                throw new IllegalArgumentException("Event must not be null!");
            }
            this.event = event;
        }

        protected long getEventSequenceNumber() {
            return event.getSequenceNumber();
        }

        @Override
        public final String toString() {
            return String.format("[%s:ev=%s]", getClass().getSimpleName(), event);
        }
    }

    private class AddResourceAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final Resource r;

        public AddResourceAction(AbstractServiceEvent event, Resource r) {
            super(event);
            this.r = r;
        }

        @Override
        protected void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    ResourceDataSourceImpl rdr = new ResourceDataSourceImpl(e, r);
                    if (!resources.containsKey(r.getId())) {
                        resources.put(r.getId(), rdr);
                        getRepository().addDataSource(rdr);
                    } else {
                        rdr = resources.get(r.getId());
                        rdr.setResource(r);
                        getChangeSupport().firePropertyChange(ResourceProviderDataSource.RP_RESOURCE_PROPERTY, null, rdr);
                    }
                    return null;
                }
            });
        }
    }

    private class RefreshAction extends Action {

        public RefreshAction() {
            super();
        }

        public void execute() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    final List<Resource> snap = ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<List<Resource>>() {

                        public List<Resource> execute() {
                            try {
                                return rp.getUnassignedResources(null);
                            } catch (ResourceProviderNotActiveException ex) {
                                LOGGER.log(Level.INFO, getComponentName() + " is not active, message: " + ex.getLocalizedMessage());
                            } catch (GrmRemoteException ex) {
                                ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                            }
                            return Collections.<Resource>emptyList();
                        }
                    });

                    Set<ResourceId> snapid = new HashSet<ResourceId>(snap.size());
                    for (Resource r : snap) {
                        snapid.add(r.getId());
                        ResourceDataSourceImpl rdr = new ResourceDataSourceImpl(e, r);
                        if (!resources.containsKey(r.getId())) {
                            resources.put(r.getId(), rdr);
                            getRepository().addDataSource(rdr);
                        } else {
                            rdr = resources.get(r.getId());
                            rdr.setResource(r);
                            getChangeSupport().firePropertyChange(ResourceProviderDataSource.RP_RESOURCE_PROPERTY, null, rdr);
                        }
                    }
                    List<ResourceId> toRemove = new LinkedList<ResourceId>();
                    for (ResourceId key : resources.keySet()) {
                        if (!snapid.contains(key)) {
                            toRemove.add(key);
                        }
                    }
                    for (ResourceId key : toRemove) {
                        ResourceDataSourceImpl rdr = resources.remove(key);
                        if (rdr != null) {
                            getRepository().removeDataSource(rdr);
                        }
                    }
                    nextExpectedEventSequenceNumber.incrementAndGet();
                    return null;
                }
            });
        }
    }


    private class RemoveResourceAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final Resource r;

        public RemoveResourceAction(AbstractServiceEvent event, Resource r) {
            super(event);
            this.r = r;
        }

        @Override
        public void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (resources.containsKey(r.getId())) {
                        ResourceDataSourceImpl rdr = resources.remove(r.getId());
                        if (rdr != null) {
                            getRepository().removeDataSource(rdr);
//                    rdr.setVisible(false);
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class RemoveResourcesAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final String id;

        public RemoveResourcesAction(AbstractServiceEvent event, String id) {
            super(event);
            this.id = id;
        }

        public void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    Iterator<Map.Entry<ResourceId, ResourceDataSourceImpl>> i = resources.entrySet().iterator();
                    while (i.hasNext()) {
                        ResourceDataSourceImpl rdr = i.next().getValue();
                        if (rdr != null) {
                            getRepository().removeDataSource(rdr);
                            i.remove();
//                    rdr.setVisible(false);
                        }
                    }
                    return null;
                }
            });
        }
    }
}

