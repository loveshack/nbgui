/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.jvm;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectAddedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectChangedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectRemovedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.impl.cloud.CloudServiceImpl;
import com.sun.grid.grm.service.impl.ge.GEServiceContainer;
import com.sun.grid.grm.sparepool.SparePoolServiceImpl;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.jvm.JvmModel;
import com.sun.grid.sdm.api.jvm.JvmModelFactory;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.auth.JAASJmxEnvironmentFactory;
import com.sun.grid.sdm.onezeroufive.component.ca.CADataSourceImpl;
import com.sun.grid.sdm.onezeroufive.component.executor.ExecutorDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.component.general.GeneralComponentDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.component.reporter.ReporterDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.component.rp.ResourceProviderDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.service.cloud.CloudServiceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.service.ge.GEServiceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.service.general.GeneralServiceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.service.spare_pool.SparePoolServiceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.jmx.JmxApplicationException;
import com.sun.tools.visualvm.jmx.JmxApplicationsSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.callback.CallbackHandler;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;

/**
 * Helper class to allow to show the SDM JVMs in a view suitable for
 * HMACC.
 * 
 */
public class JvmDataSourceImpl extends JvmDataSource {

    private static final String BUNDLE = "com.sun.grid.sdm.onezeroufive.jvm.Bundle";
    private static final Logger log = Logger.getLogger(JvmDataSourceImpl.class.getName(), BUNDLE);
    private final ConfigurationServiceEventListener listener;
    private final ConfigurationServiceEventListener jvmlistener;
    private final ExecutionEnv env;
    private final CallbackHandler ch;
    private final JAASJmxEnvironmentFactory jmxEnvFactory;
    private final AtomicInteger state = new AtomicInteger(STATE_UNKNOWN);

    public JvmDataSourceImpl(String jvmName, String jvmHost, Application appl, boolean isCS, CallbackHandler ch) throws JmxApplicationException {
        super(jvmName, jvmHost, appl, isCS);
        this.env = ExecEnvUtil.getExecutionEnv(appl);
        this.listener = new CSEventListener();
        this.jvmlistener = new CSJVMEventListener();
        this.ch = ch;
        this.jmxEnvFactory = new JAASJmxEnvironmentFactory(env, System.getProperty("user.name"), ch);
    }

    public ExecutionEnv getEnv() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<ExecutionEnv>() {

            public ExecutionEnv execute() {
                return env;
            }
        });
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public String getVersionName(String version) {
        return Version.getVersionName(version);
    }

    public int getState() {
        return state.get();
    }

    private void setState(final int newState) {
        final int oldState = state.getAndSet(newState);
        if (DataSource.EVENT_QUEUE.isRequestProcessorThread()) {
            log.log(Level.FINE, "jvm: " + getName() + "@" + getHost() + " old state: " + oldState + ", jvm new state :" + newState);
            getChangeSupport().firePropertyChange(PROPERTY_STATE, null, newState);
        } else {
            DataSource.EVENT_QUEUE.post(new Runnable() {

                public void run() {
                    log.log(Level.FINE, "jvm: " + getName() + "@" + getHost() + " old state: " + oldState + ", jvm new state :" + newState);
                    getChangeSupport().firePropertyChange(PROPERTY_STATE, null, newState);
                }
            });
        }
    }

    /* helper class taking care of CS events */
    private class CSEventListener
            implements ConfigurationServiceEventListener {

        public void configurationObjectAdded(final ConfigurationObjectAddedEvent event) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (event.getHost().equals(getHost())) {
                        log.log(Level.FINE, "configuration object added for host: " + getHost() + ", event: " + event);
                    } else {
                        return null;
                    }
                    GlobalConfig gc = null;
                    try {
                        gc = env.getCommandService().execute(new GetGlobalConfigurationCommand()).getReturnValue();
                    } catch (Exception e) {
                        ErrorDisplayer.submitWarning(e.getLocalizedMessage());
                    }

                    List<ComponentInfo> cis = Collections.<ComponentInfo>emptyList();
                    AndFilter<ComponentInfo> f = new AndFilter<ComponentInfo>(3);
                    try {
                        f.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(getHost())));
                        f.add(GetComponentInfosCommand.newRegExpJvmFilter(getName()));
                        f.add(GetComponentInfosCommand.newNameFilter(event.getName()));
                        cis = ComponentServiceWithClassLoader.getComponentInfos(env, f);
                    } catch (Exception ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }

                    for (ComponentInfo ci : cis) {
                        if (event.getType().equals(Service.class.getName())) {
                            Class<?> serviceClass = getServiceImplementationClass(ci.getName(), getName(), gc);
                            // should not happen, we used jvm in filter
                            if (serviceClass != null) {
                                EVENT_QUEUE.post(new AddServiceAction(ci.getName(), serviceClass));
                            }
                            EVENT_QUEUE.post(new AddServiceAction(event.getName(), serviceClass));
                        } else if (event.getType().equals(GrmComponent.class.getName())) {
                            EVENT_QUEUE.post(new AddComponentAction(event.getName()));
                        } else if (event.getType().equals(Executor.class.getName())) {
                            EVENT_QUEUE.post(new AddComponentAction(event.getName()));
                        } else if (event.getType().equals(ResourceProvider.class.getName())) {
                            EVENT_QUEUE.post(new AddComponentAction(event.getName()));
                        } else if (event.getType().equals(GrmCA.class.getName())) {
                            EVENT_QUEUE.post(new AddComponentAction(event.getName()));
                        } else if (event.getType().equals(Reporter.class.getName())) {
                            EVENT_QUEUE.post(new AddComponentAction(event.getName()));
                        }
                    }
                    // will not probably work, but worth to try
                    if (event.getType().equals(JVM.class.getName()) && event.getContextPath().equals("active_jvm")) {
                        setState(STATE_AVAILABLE);
                    }
                    return null;
                }
            });
        }

        public void configurationObjectRemoved(final ConfigurationObjectRemovedEvent event) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (event.getHost().equals(getHost())) {
                        log.log(Level.FINE, "configuration object removed for host: " + getHost() + ", event: " + event);
                    } else {
                        return null;
                    }
                    if (event.getType().equals(Service.class.getName())) {
                        EVENT_QUEUE.post(new RemoveServiceAction(event.getName()));
                    } else if (event.getType().equals(GrmComponent.class.getName())) {
                        EVENT_QUEUE.post(new RemoveComponentAction(event.getName(), event.getHost()));
                    } else if (event.getType().equals(Executor.class.getName())) {
                        EVENT_QUEUE.post(new RemoveComponentAction(event.getName(), event.getHost()));
                    } else if (event.getType().equals(ResourceProvider.class.getName())) {
                        EVENT_QUEUE.post(new RemoveComponentAction(event.getName(), event.getHost()));
                    } else if (event.getType().equals(GrmCA.class.getName())) {
                        EVENT_QUEUE.post(new RemoveComponentAction(event.getName(), event.getHost()));
                    } else if (event.getType().equals(Reporter.class.getName())) {
                        EVENT_QUEUE.post(new RemoveComponentAction(event.getName(), event.getHost()));
                    } else if (event.getType().equals(JVM.class.getName()) && event.getContextPath().equals("active_jvm")) {
                        setState(STATE_UNAVAILABLE);
                    }
                    return null;
                }
            });
        }

        public void configurationObjectChanged(ConfigurationObjectChangedEvent event) {
//            if (event.getType().equals(Service.class.getName())) {
//              todo
//            }
        }
    }

    /* helper class taking care of CS events */
    private class CSJVMEventListener
            implements ConfigurationServiceEventListener {

        public void configurationObjectAdded(final ConfigurationObjectAddedEvent event) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (event.getType().equals(JVM.class.getName()) && event.getContextPath().equals("active_jvm")) {
                        log.log(Level.FINE, "jvm added: " + event);
                        SwingUtilities.invokeLater(new SyncJVMActionFromEvt(event.getName(), env));
                    }
                    return null;
                }
            });
        }

        public void configurationObjectRemoved(final ConfigurationObjectRemovedEvent event) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (event.getType().equals(JVM.class.getName()) && event.getContextPath().equals("active_jvm")) {
                        log.log(Level.FINE, "jvm removed: " + event);
                        SwingUtilities.invokeLater(new SyncJVMActionFromEvt(event.getName(), env));
                    }
                    return null;
                }
            });
        }

        public void configurationObjectChanged(final ConfigurationObjectChangedEvent event) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (event.getType().equals(JVM.class.getName()) && event.getContextPath().equals("active_jvm")) {
                        log.log(Level.FINE, "jvm changed: " + event);
                        SwingUtilities.invokeLater(new SyncJVMActionFromEvt(event.getName(), env));
                    }
                    return null;
                }
            });
        }
    }

    /* helper method for syncing active services */
    private void getServicesFromCS() {
        GlobalConfig gc = null;
        try {
            gc = env.getCommandService().execute(new GetGlobalConfigurationCommand()).getReturnValue();
        } catch (Exception ex) {
            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
        }
        List<ComponentInfo> cis = Collections.<ComponentInfo>emptyList();
        AndFilter<ComponentInfo> f = new AndFilter<ComponentInfo>(3);
        try {
            f.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(getHost())));
            f.add(GetComponentInfosCommand.newRegExpJvmFilter(getName()));
            f.add(GetComponentInfosCommand.newTypeFilter(Service.class));
            cis = ComponentServiceWithClassLoader.getComponentInfos(env, f);
        } catch (Exception ex) {
            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
        }
        for (ComponentInfo ci : cis) {
            Class<?> serviceClass = getServiceImplementationClass(ci.getName(), getName(), gc);
            // should not happen, we used jvm in filter
            if (serviceClass != null) {
                EVENT_QUEUE.post(new AddServiceAction(ci.getName(), serviceClass));
            }
        }
    }

    /* helper method for syncing active components */
    private void getComponentsFromCS() {
        List<ComponentInfo> cis = Collections.<ComponentInfo>emptyList();
        AndFilter<ComponentInfo> f = new AndFilter<ComponentInfo>(3);
        try {
            f.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(getHost())));
            f.add(GetComponentInfosCommand.newRegExpJvmFilter(getName()));
            f.add(GetComponentInfosCommand.newTypeFilter(GrmComponent.class));
            cis = ComponentServiceWithClassLoader.getComponentInfos(env, f);
        } catch (Exception ex) {
            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
        }
        for (ComponentInfo ci : cis) {
            if (!ci.isAssignableTo(Service.class) && !ci.isAssignableTo(JVM.class)) {
                EVENT_QUEUE.post(new AddComponentAction(ci.getName()));
            }
        }
    }

    /* helper method for syncing active jvms */
    private void getJvmsFromCS() {
        CancellGettingJvmsFromCS cancel = new CancellGettingJvmsFromCS();
        String label;
        final JvmModel m = JvmModelFactory.getDefault().getModel(getApplication());
        if (m != null) {
            label = NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Adding", new Object[]{m.getSystemName(), m.getPreferencesType()});
        } else {
            label = NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Adding", new Object[]{"unknown system name", "unknown preferences type"});
        }
        final ProgressHandle q = ProgressHandleFactory.createHandle(label, cancel); // NOI18N
        cancel.setProgressHandle(q);
        List<ActiveJvm> jvmsList = Collections.<ActiveJvm>emptyList();
        try {
            jvmsList = env.getCommandService().execute(new GetActiveJVMListCommand()).getReturnValue();
        } catch (GrmException ex) {
            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
        }
        int i = 0;
        final int amount = jvmsList.size() - 1;
        for (ActiveJvm tmp : jvmsList) {
            if (!SystemUtil.isCSJvmHostAndPort(env, tmp.getHost(), tmp.getPort())) {
                AddJVMAction action = new AddJVMAction(tmp, env, q, cancel);
                if (i == 0) {
                    action.doStart(amount);
                }
                if (i == (amount - 1)) {
                    action.doFinish();
                }
                // adding/removing jvms can not be done from DataSource.EVENT_QUEUE
                SwingUtilities.invokeLater(action);
                i++;
            }

        }
    }

    private class CancellGettingJvmsFromCS implements Cancellable {

        private ProgressHandle p;
        private boolean flag = false;

        public void setProgressHandle(ProgressHandle p) {
            this.p = p;
        }

        public boolean cancel() {
            flag = true;
            ErrorDisplayer.submitWarning(NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Cancelling_JVM_CS", getName()));
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    if (p != null) {
                        p.setDisplayName("Cancelling, please wait");
                    }
                }
            });
            EVENT_QUEUE.post(new Runnable() {

                public void run() {
                    DataSource.EVENT_QUEUE.post(new Runnable() {

                        public void run() {
                            if (JvmDataSourceImpl.this.getOwner() != null) {
                                JvmDataSourceImpl.this.getOwner().getRepository().removeDataSource(JvmDataSourceImpl.this);
                            }
                        }
                    });

                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            if (p != null) {
                                p.finish();
                            }
                        }
                    });
                }
            });

            return true;
        }

        public boolean isCancelled() {
            return flag;
        }
    }

    public synchronized void initialize() {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                if (env != null) {
                    try {

                        setState(STATE_AVAILABLE);
                        ConfigurationService cs = ComponentServiceWithClassLoader.getCS(env);
                        cs.addConfigurationServiceEventListener(listener);
                        if (isCS()) {
                            cs.addConfigurationServiceEventListener(jvmlistener);
                            getJvmsFromCS();
                        }
                        getServicesFromCS();
                        getComponentsFromCS();
                    } catch (Exception ex) {
                        ErrorDisplayer.submitWarning(NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Error_Registering_Listener", getName()));
                    }
                }
                return null;
            }
        });
    }

    @Override
    public synchronized void remove() {
        shutdown();
        super.remove();
        appWrapPairs.remove(getApplication());
    }

    public synchronized void shutdown() {
        if (!getApplication().isRemoved() && getApplication().getOwner() != null) {
            try {
                getApplication().getOwner().getRepository().removeDataSource(getApplication());
            } catch (final Exception ex) {
                ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
            }
        }
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                if (env != null) {
                    try {
                        ConfigurationService cs = ComponentServiceWithClassLoader.getCS(env);
                        if (isCS()) {
                            cs.removeConfigurationServiceEventListener(jvmlistener);
                        }
                        cs.removeConfigurationServiceEventListener(listener);
                    } catch (Exception ex) {
                        ErrorDisplayer.submitWarning(NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Error_Unregistering_Listener", getName()));
                    }
                }
                return null;
            }
        });
    }

    private class AddJVMAction implements Runnable {

        private final ActiveJvm jvm;
        private final ExecutionEnv env;
        private final ProgressHandle q;
        private boolean doStart;
        private boolean doFinish;
        private int amount;
        private final CancellGettingJvmsFromCS cancel;

        public AddJVMAction(ActiveJvm jvm, ExecutionEnv env, ProgressHandle q, CancellGettingJvmsFromCS cancel) {
            this.jvm = jvm;
            this.env = env;
            this.q = q;
            this.cancel = cancel;
        }

        public void doStart(int amount) {
            this.doStart = true;
            this.amount = amount;
        }

        public void doFinish() {
            this.doFinish = true;
        }

        public void run() {
            if (!cancel.isCancelled()) {
                if (doStart) {
                    q.start(amount);
                }
                q.progress(NbBundle.getMessage(JvmDataSourceImpl.class, "LBL_Added", jvm.getName(), jvm.getHost()), 1);

                ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                    public Void execute() {
                        try {
                            final String jvmurl = formatUrlString(env, jvm);
                            final String jvmname = formatJvmDisplayName(jvm);

                            List<String> theJvmHosts = jvmHosts.get(jvm.getName());
                            log.log(Level.FINEST, "add jvm action, jvm: " + jvm + ", jvm host: " + jvm.getHost() + ", known jvm hosts: " + theJvmHosts);
                            if (theJvmHosts == null || !theJvmHosts.contains(jvm.getHost()) && !isRemoved()) {
                                Application j = JmxApplicationsSupport.getInstance().createJmxApplication(jvmurl, jvmname, jmxEnvFactory, null, false, false);
                                j.setVisible(false);
                                JvmDataSourceImpl jds = new JvmDataSourceImpl(jvm.getName(), jvm.getHost(), j, false, ch);
                                jds.setVisible(false);
                                getRepository().addDataSource(jds);
                                jds.initialize();
                                if (theJvmHosts == null) {
                                    theJvmHosts = new LinkedList<String>();
                                }
                                theJvmHosts.add(jvm.getHost());
                                jvmHosts.put(jvm.getName(), theJvmHosts);
                                jvms.put(formatJvmId(jvm.getName(), jvm.getHost()), jds);
                            }
                        } catch (JmxApplicationException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        }
                        return null;
                    }
                });

                if (doFinish) {
                    q.finish();
                }
            }
        }
    }

    private class SyncJVMActionFromEvt implements Runnable {

        private final String name;
        private final ExecutionEnv env;

        public SyncJVMActionFromEvt(String jvm, ExecutionEnv env) {
            this.env = env;
            this.name = jvm;
        }

        @Override
        public void run() {
            final ProgressHandle q = ProgressHandleFactory.createHandle(NbBundle.getMessage(JvmDataSourceImpl.class,
                    "LBL_Syncing")); // NOI18N
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    List<ActiveJvm> jvmsList = Collections.<ActiveJvm>emptyList();
                    try {
                        GetActiveJVMListCommand cmd = new GetActiveJVMListCommand();
                        cmd.setJvmName(name);
                        jvmsList = env.getCommandService().execute(cmd).getReturnValue();
                    } catch (GrmException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }

                    /**
                     * list of active jvms of with the name that have been started, or
                     * list of hosts where the jvm has been shutdown
                     */
                    final List<ActiveJvm> toAdd = new LinkedList<ActiveJvm>();
                    final List<String> toRemove = new LinkedList<String>();

                    /**
                     * it is ok to work with the single jvm name - we believe that
                     * GetActiveJVMListCommand command retrieved only ActiveJvms
                     * with the name.
                     */
                    List<String> theJvmHosts = jvmHosts.get(name);
                    for (ActiveJvm jvm : jvmsList) {
                        if (theJvmHosts == null || !theJvmHosts.contains(jvm.getHost())) {
                            toAdd.add(jvm);
                        }
                    }

                    if (theJvmHosts != null) {
                        for (String host : theJvmHosts) {
                            if (!getAllJvmHosts(jvmsList).contains(host)) {
                                toRemove.add(host);
                            }
                        }
                    }

                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            q.setInitialDelay(0);
                            q.start(toAdd.size() + toRemove.size());
                        }
                    });


                    for (ActiveJvm jvm : toAdd) {
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                q.progress(1);
                            }
                        });
                        if (!jvm.getHost().equals(getHost()) || !jvm.getName().equals(getName())) {
                            try {
                                final String jvmurl = formatUrlString(env, jvm);
                                final String jvmname = formatJvmDisplayName(jvm);

                                Application j = JmxApplicationsSupport.getInstance().createJmxApplication(jvmurl, jvmname, jmxEnvFactory, null, false, false);
                                j.setVisible(false);
                                JvmDataSourceImpl jds = new JvmDataSourceImpl(jvm.getName(), jvm.getHost(), j, false, ch);
                                getRepository().addDataSource(jds);
                                jds.setVisible(false);
                                jds.initialize();
                                List<String> currHosts = jvmHosts.get(name);
                                if (currHosts == null) {
                                    currHosts = new LinkedList<String>();
                                    jvmHosts.put(jvm.getName(), currHosts);
                                }
                                currHosts.add(jvm.getHost());
                                jvms.put(formatJvmId(jvm.getName(), jvm.getHost()), jds);
                            } catch (JmxApplicationException ex) {
                                ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                            }
                        }
                    }

                    for (String host : toRemove) {
                        List<String> currHosts = jvmHosts.get(name);
                        if (currHosts != null) {
                            currHosts.remove(host);
                            if (currHosts.isEmpty()) {
                                jvmHosts.remove(name);
                            }
                        }
                        JvmDataSource jds = getJvmDataSource(name, host);
                        if (jds != null) {
                            getRepository().removeDataSource(jds);
                        }
                        jvms.remove(formatJvmId(name, host));
                    }
                    return null;
                }
            });

            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    q.finish();
                }
            });
        }
    }

    private class AddServiceAction implements
            Runnable {

        private final String serviceName;
        private final Class<?> serviceClass;

        /**
         * Workaround solution needed because there is no information about
         * implementing service class inside the componentinfo.
         *
         * @param sname name of the service
         * @param c global cofniguration object
         */
        public AddServiceAction(String sname, Class<?> c) {
            this.serviceName = sname;
            this.serviceClass = c;
        }

        public void run() {
            if (!services.containsKey(serviceName)) {
                ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                    public Void execute() {
                        try {
                            ServiceDataSource sds = null;
                            Service s = ComponentServiceWithClassLoader.<Service>getComponentByNameTypeAndHost(env, serviceName, Service.class, Hostname.getInstance(getHost()));
                            if (serviceClass.isAssignableFrom(SparePoolServiceImpl.class)) {
                                sds = new SparePoolServiceDataSourceImpl(env, serviceName, getName(), getHost(), s);
                            } else if (serviceClass.isAssignableFrom(GEServiceContainer.class)) {
                                sds = new GEServiceDataSourceImpl(env, serviceName, getName(), getHost(), s);
                            } else if (serviceClass.isAssignableFrom(CloudServiceImpl.class)) {
                                sds = new CloudServiceDataSourceImpl(env, serviceName, getName(), getHost(), s);
                            } else {
                                sds = new GeneralServiceDataSourceImpl(env, serviceName, getName(), getHost(), s);
                            }
                            sds.setVisible(false);
                            services.put(serviceName, sds);
                            getApplication().getRepository().addDataSource(sds);
                        } catch (final Exception ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        }
                        return null;
                    }
                });
            }
        }
    }

    private class RemoveServiceAction implements
            Runnable {

        private final String serviceName;

        public RemoveServiceAction(String sname) {
            this.serviceName = sname;
        }

        public void run() {
            if (services.containsKey(serviceName)) {
                ServiceDataSource sds = services.remove(serviceName);
                if (sds != null) {
                    getApplication().getRepository().removeDataSource(sds);
                }
            }
        }
    }

    private class AddComponentAction implements
            Runnable {

        private final String componentName;

        public AddComponentAction(String cname) {
            this.componentName = cname;
        }

        public void run() {
            if (!components.containsKey(componentName)) {
                ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                    public Void execute() {
                        try {
                            AndFilter<ComponentInfo> f = new AndFilter<ComponentInfo>(4);
                            f.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(getHost())));
                            f.add(GetComponentInfosCommand.newNameFilter(componentName));
                            f.add(GetComponentInfosCommand.newRegExpJvmFilter(getName()));
                            f.add(GetComponentInfosCommand.newTypeFilter(GrmComponent.class));

                            GetComponentInfosCommand gcic = new GetComponentInfosCommand(f);
                            for (ComponentInfo ci : env.getCommandService().execute(gcic).getReturnValue()) {
                                GrmComponent c = ComponentServiceWithClassLoader.<GrmComponent>getComponent(env, ci);
                                ComponentDataSource cds;
                                if (ci.isAssignableTo(ResourceProvider.class)) {
                                    cds = new ResourceProviderDataSourceImpl(env, componentName, getName(), getHost(), (ResourceProvider) c);
                                } else if (ci.isAssignableTo(Reporter.class)) {
                                    cds = new ReporterDataSourceImpl(env, componentName, getName(), getHost(), (Reporter) c);
                                } else if (ci.isAssignableTo(Executor.class)) {
                                    cds = new ExecutorDataSourceImpl(env, componentName, getName(), getHost(), (Executor) c);
                                } else if (ci.isAssignableTo(GrmCA.class)) {
                                    cds = new CADataSourceImpl(env, componentName, getName(), getHost(), (GrmCA) c);
                                } else {
                                    cds = new GeneralComponentDataSourceImpl(env, componentName, getName(), getHost(), c);
                                }
                                cds.setVisible(false);
                                components.put(componentName, cds);
                                getApplication().getRepository().addDataSource(cds);
                            }
                        } catch (final Exception ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        }
                        return null;
                    }
                });
            }
        }
    }

    private class RemoveComponentAction implements
            Runnable {

        private final String componentName;
        private final String hostname;

        public RemoveComponentAction(String cname, String hostname) {
            this.componentName = cname;
            this.hostname = hostname;
        }

        public void run() {
            if (components.containsKey(componentName) && getHost().equals(hostname)) {
                DataSource cds = components.remove(componentName);
                if (cds != null) {
                    getApplication().getRepository().removeDataSource(cds);
                }
            }
        }
    }

    /**
     * Helper method to format JMXServiceURL string.
     *
     * @param env execution environment of sdm system
     * @param jvm an active jvm instance
     * @return a formated string usable for jmx application creation
     */
    private String formatUrlString(ExecutionEnv env, ActiveJvm jvm) {
        final String urlPattern = "service:jmx:rmi://%s:%d/jndi/rmi://%s:%d/%s";
        return String.format(urlPattern, jvm.getHost(), jvm.getPort(), jvm.getHost(), jvm.getPort(), env.getSystemName());
    }

    /**
     * Helper method to format JMXServiceURL string.
     *
     * @param env execution environment of sdm system
     * @param jvm an active jvm instance
     * @return a formated string usable for jmx application creation
     */
    private String formatJvmId(String jvmName, String hostname) {
        final String pattern = "%s@%s";
        return String.format(pattern, jvmName, hostname);
    }

    /**
     * Helper method to format a display name for SDM JVM.
     *
     * @param jvm an active jvm instance
     * @return a formated string usable for jmx application creation
     */
    private String formatJvmDisplayName(ActiveJvm jvm) {
        String displayNamePattern = "name= %s, host= %s"; // NOI18N
        return String.format(displayNamePattern, jvm.getName(), jvm.getHost());
    }

    /**
     * helper method to get all hosts from a list of active jvms of the same name
     *
     * @param aj list of active jvms
     * @return set of hosts on which the jvm is actives
     */
    private Collection<String> getAllJvmHosts(List<ActiveJvm> aj) {
        Set<String> hosts = new HashSet<String>();
        for (ActiveJvm jvm : aj) {
            hosts.add(jvm.getHost());
        }
        return hosts;
    }

    private static Class<?> getServiceImplementationClass(String serviceName, String jvmName, GlobalConfig gc) {
        Class<?> clazz = null;
        if (gc == null) {
            return clazz;
        }
        for (JvmConfig jvm : gc.getJvm()) {
            if (jvm.getName().equals(jvmName)) {
                for (Component c : jvm.getComponent()) {
                    if (c.getName().equals(serviceName)) {
                        try {
                            clazz = Class.forName(c.getClassname());
                        } catch (ClassNotFoundException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        }
                        break;
                    }
                }
            }
        }
        return clazz;
    }
}


