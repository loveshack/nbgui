/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.service.ge;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.service.ge.GEServiceDataSource;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.service.ServiceDataSourceSupport;
import com.sun.grid.sdm.onezeroufive.util.HistorySupport;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;

public class GEServiceDataSourceImpl extends GEServiceDataSource implements HistoryObservable<Object[], Filter<Report>> {

    private Service s;
    private final HistorySupport hs;
    private final ServiceDataSourceSupport sdss;

    public GEServiceDataSourceImpl(ExecutionEnv env, String sName, String jvmName, String host, Service s) {
        super(sName, jvmName, host);
        this.s = s;
        this.sdss = new ServiceDataSourceSupport(env, sName, s, getRepository(), getChangeSupport());
        this.hs = new HistorySupport(env);
    }

    @Override
    public String getComponentState() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return s.getState().name();
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    return ComponentState.UNKNOWN.name();
                }
            }
        });
    }

    @Override
    public String getServiceState() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return s.getServiceState().name();
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    return ServiceState.UNKNOWN.name();
                }
            }
        });
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public String getVersionName(String version) {
        return Version.getVersionName(version);
    }

    @Override
    public void getHistory(Filter<Report> f) {
        hs.getHistory(f);
    }

    @Override
    public void cancelHistory() {
        hs.cancelHistory();
    }

    public void addHistoryListener(HistoryListener<Object[]> listener) {
        hs.addHistoryListener(listener);
    }

    public void removeHistoryListener(HistoryListener<Object[]> listener) {
        hs.removeHistoryListener(listener);
    }
}

