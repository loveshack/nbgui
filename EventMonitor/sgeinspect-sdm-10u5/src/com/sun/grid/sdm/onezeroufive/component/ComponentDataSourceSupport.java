/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.component;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.beans.PropertyChangeSupport;

public class ComponentDataSourceSupport {

    private final GrmComponent ex;
    private final PropertyChangeSupport pcs;
    private final ComponentEventListener cel;

    public ComponentDataSourceSupport(GrmComponent c, PropertyChangeSupport pcs) {
        this.pcs = pcs;
        this.ex = c;
        this.cel = new CEL();
        try {
            this.ex.addComponentEventListener(cel);
        } catch (GrmRemoteException grex) {
            ErrorDisplayer.submitWarning(grex.getLocalizedMessage());
        }
    }

    private class CEL implements ComponentEventListener {

        public void connectionClosed() {
        }

        public void connectionFailed() {
        }

        public void eventsLost() {
        }

        public void componentUnknown(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }

        public void componentStarting(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }

        public void componentStarted(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }

        public void componentStopping(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }

        public void componentStopped(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }

        public void componentReloading(final ComponentStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ComponentDataSource.COMPONENT_STATUS_PROPERTY, null, arg0.getNewState().toString());

                    return null;
                }
            });
        }
    }
}

