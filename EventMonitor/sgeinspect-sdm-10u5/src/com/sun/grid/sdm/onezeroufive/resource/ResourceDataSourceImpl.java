/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.resource;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.util.HistorySupport;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.util.Map;

public class ResourceDataSourceImpl extends com.sun.grid.sdm.api.resource.ResourceDataSource implements HistoryObservable<Object[], Filter<Report>> {

    private Resource r;
    private final HistorySupport hs;

    public ResourceDataSourceImpl(ExecutionEnv env, Resource r) {
        this.r = r;
        this.hs = new HistorySupport(env);
    }

    public void setResource(final Resource resource) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                Resource old = r;
                r = resource;
                getChangeSupport().firePropertyChange(ResourceDataSource.RESOURCE_PROPERTY, null, r);
                return null;
            }
        });
    }

    @Override
    public String getName() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                return r.getName();
            }
        });
    }

    public String getId() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                return r.getId().getId();
            }
        });
    }

    @Override
    public String getType() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                return r.getType().getName();
            }
        });
    }

    @Override
    public String getState() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                return r.getState().name();
            }
        });
    }

    @Override
    public int getUsageLevel() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                return r.getUsage().getLevel();
            }
        });
    }

    @Override
    public boolean isStatic() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                return r.isStatic();
            }
        });
    }

    @Override
    public boolean isAmbiguous() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                return r.isAmbiguous();
            }
        });
    }

    @Override
    public String getAnnotation() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                return r.getAnnotation();
            }
        });
    }

    @Override
    public Map<String, Object> getProperties() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Map<String, Object>>() {

            public Map<String, Object> execute() {
                return r.getProperties();
            }
        });
    }

    public void addHistoryListener(HistoryListener<Object[]> listener) {
        hs.addHistoryListener(listener);
    }

    public void removeHistoryListener(HistoryListener<Object[]> listener) {
        hs.removeHistoryListener(listener);
    }

    public void cancelHistory() {
        hs.cancelHistory();
    }

    public void getHistory(Filter<Report> u) {
        hs.getHistory(u);
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public String getVersionName(String version) {
        return Version.getVersionName(version);
    }
}

