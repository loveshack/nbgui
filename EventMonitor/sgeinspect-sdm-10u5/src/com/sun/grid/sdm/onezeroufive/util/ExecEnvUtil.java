/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.util;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.security.CachingClientSecurityContextFactory;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.sdm.api.jvm.JvmModel;
import com.sun.grid.sdm.api.jvm.JvmModelFactory;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import java.io.IOException;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.prefs.BackingStoreException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.openide.util.NbBundle;

public final class ExecEnvUtil {

    private static Map<Application, ExecutionEnv> cache;
    private static CachingClientSecurityContextFactory ccscf;

    static {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                cache = new WeakHashMap<Application, ExecutionEnv>();
                ccscf = new CachingClientSecurityContextFactory();
                return null;
            }
        });
    }
    
    private static final CallbackHandler dummyHandler = new CallbackHandler() {

        protected void handle(Callback cb)
                throws IOException, UnsupportedCallbackException {
            String user = System.getProperty("user.name");
            String pass = "";
            if (cb instanceof PasswordCallback) {
                PasswordCallback pwCallback = (PasswordCallback) cb;
                pwCallback.setPassword(pass.toCharArray());
            } else if (cb instanceof NameCallback) {
                NameCallback ncb = (NameCallback) cb;
                ncb.setName(user);
            } else {
                throw new UnsupportedCallbackException(cb);
            }
        }

        public void handle(Callback callbacks[])
                throws IOException, UnsupportedCallbackException {
            Callback arr$[] = callbacks;
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; i$++) {
                Callback cb = arr$[i$];
                handle(cb);
            }
        }
    };

    private ExecEnvUtil() {
    }

    public static ExecutionEnv getExecutionEnv(final Application app) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<ExecutionEnv>() {

            public ExecutionEnv execute() {
                ExecutionEnv ret = cache.get(app);
                if (ret != null) {
                    return ret;
                }
                try {
                    JvmModel model = JvmModelFactory.getDefault().getModel(app);
                    if (model != null) {
                        HostAndPort hp = HostAndPort.newInstance(model.getConnectionInfo());
                        ccscf.registerContext(hp, System.getProperty("user.name"), dummyHandler);
                        ret = ExecutionEnvFactory.newClientInstance(model.getSystemName(), PreferencesType.valueOf(model.getPreferencesType()), ccscf);
                        cache.put(app, ret);
                    }
                } catch (BackingStoreException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                } catch (PreferencesNotAvailableException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                } catch (GrmSecurityException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                } catch (GrmException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                }
                if (ret == null) {
                    ErrorDisplayer.submitWarning(NbBundle.getMessage(ExecEnvUtil.class, "MSG_eenv_problem", app.getId())); // NOI18N
                }
                return ret;
            }
        });
    }
}
