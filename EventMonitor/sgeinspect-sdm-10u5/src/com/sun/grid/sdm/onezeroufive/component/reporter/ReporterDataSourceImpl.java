/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.component.reporter;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.component.reporter.ReporterDataSource;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.component.ComponentDataSourceSupport;
import com.sun.grid.sdm.onezeroufive.util.HistorySupport;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;

public class ReporterDataSourceImpl extends ReporterDataSource implements HistoryObservable<Object[], Filter<Report>> {

    private final ExecutionEnv e;
    private final Reporter r;
    private final HistorySupport hs;
    private final ComponentDataSourceSupport cdss;

    public ReporterDataSourceImpl(ExecutionEnv env, String sName, String jvmName, String host, Reporter c) {
        super(sName, jvmName, host);
        this.e = env;
        this.r = c;
        this.hs = new HistorySupport(env);
        this.cdss = new ComponentDataSourceSupport(c, getChangeSupport());
    }

    @Override
    public void getHistory(Filter<Report> f) {
        hs.getHistory(f);
    }

    @Override
    public void cancelHistory() {
        hs.cancelHistory();
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public String getVersionName(String version) {
        return Version.getVersionName(version);
    }

    public void addHistoryListener(HistoryListener<Object[]> listener) {
        hs.addHistoryListener(listener);
    }

    public void removeHistoryListener(HistoryListener<Object[]> listener) {
        hs.removeHistoryListener(listener);
    }

    @Override
    public String getComponentState() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return r.getState().name();
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    return ComponentState.UNKNOWN.name();
                }
            }
        });
    }
}

