/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.util;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.reporting.RowIterator;
import com.sun.grid.grm.reporting.impl.ReportingVariableResolver;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.resource.BaseResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.api.util.CancellableHistoryRetriever;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.onezeroufive.jvm.ComponentServiceWithClassLoader;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.NbBundle;

public class HistorySupport implements HistoryObservable<Object[], Filter<Report>> {

    final static String FITLER = "description matches \".*name\\\\=%s,host\\\\=%s.*\"";
    private final ExecutionEnv e;
    protected CancellableHistoryRetriever currentRetriever = null;
    protected List<HistoryListener<Object[]>> listeners = new LinkedList<HistoryListener<Object[]>>();

    public HistorySupport(ExecutionEnv env) {
        this.e = env;
    }

    public void addHistoryListener(HistoryListener<Object[]> listener) {
        listeners.add(listener);
    }

    public void removeHistoryListener(HistoryListener<Object[]> listener) {
        listeners.remove(listener);
    }

    public synchronized void cancelHistory() {
        currentRetriever.cancel();
        currentRetriever = null;
    }

    public void getHistory(Filter<Report> f) {
        if (currentRetriever != null) {
            currentRetriever.cancel();
        }
        currentRetriever = new Retriever(f);
        DataSource.EVENT_QUEUE.post(currentRetriever);
    }

    private class Retriever implements CancellableHistoryRetriever {

        final Filter<Report> f;
        boolean cancelled = false;
        final ProgressHandle p;

        Retriever(Filter<Report> f) {
            this.f = f;
            this.p = ProgressHandleFactory.createHandle(NbBundle.getMessage(HistorySupport.class, "LBL_History"), this);
        }

        public void run() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    p.setInitialDelay(0);
                    p.start();
                }
            });
            try {
                for (HistoryListener<Object[]> l : listeners) {
                    l.historyReset();
                }
                Object[] row;
                Reporter reporter = ComponentServiceWithClassLoader.<Reporter>getComponentByType(e, Reporter.class);
                Report r = reporter.createReport(f);
                RowIterator iter = r.iterator();
                while (iter.hasNext(reporter)) {
                    row = iter.next(reporter);
                    if (row.length != 0 && !cancelled) {
                        for (HistoryListener<Object[]> l : listeners) {
                            l.historyRowsAdded(row);
                        }
                    }
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            p.progress(NbBundle.getMessage(HistorySupport.class, "LBL_History"));
                        }
                    });
                }
                for (HistoryListener<Object[]> l : listeners) {
                    l.historyRetrieved();
                }
            } catch (GrmException ex) {
                ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
            }
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    p.finish();
                }
            });
        }

        public synchronized boolean cancel() {
            cancelled = true;
            return true;
        }
    }

    public static <U extends ComponentDataSource> Filter<Report> createBasicComponentFilter(Date startTime, U ca) {
        Filter<Report> ca_regex;
        try {
            ca_regex = FilterHelper.<Report>parse(String.format(FITLER, ca.getComponentName().replace("_", "\\\\_"), ca.getHostname().replace("-", "\\\\-")));
        } catch (FilterException ex) {
            ca_regex = ConstantFilter.<Report>alwaysMatching();
        }
        final AndFilter<Report> filter = new AndFilter<Report>(2);
        /* not supported yet, has to be enabled on sdm side
        filter.add(ReportingVariableResolver.createServiceFilter(cds.getComponentName())); */
        filter.add(ca_regex);
        filter.add(ReportingVariableResolver.createStartimeFilter(startTime));
        return filter;
    }

    public static <U extends BaseResourceDataSource> Filter<Report> createBasicResourceFilter(Date startTime, U ca) {
        final AndFilter<Report> filter = new AndFilter<Report>(2);
        filter.add(ReportingVariableResolver.createResourceFilter(ca.getName().toLowerCase()));
        filter.add(ReportingVariableResolver.createStartimeFilter(startTime));
        return filter;
    }

    public static <U extends ServiceDataSource> Filter<Report> createBasicServiceFilter(Date startTime, U ca) {
        final AndFilter<Report> filter = new AndFilter<Report>(2);
        filter.add(ReportingVariableResolver.createServiceFilter(ca.getServiceName()));
        filter.add(ReportingVariableResolver.createStartimeFilter(startTime));
        return filter;
    }
}

