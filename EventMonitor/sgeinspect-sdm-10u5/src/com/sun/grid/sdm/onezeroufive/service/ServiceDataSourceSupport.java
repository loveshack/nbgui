/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.onezeroufive.service;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.ResourceProviderNotActiveException;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.impl.DefaultServiceSnapshot;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.sdm.api.resource.CachedResourceDataSource;
import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.jvm.ComponentServiceWithClassLoader;
import com.sun.grid.sdm.onezeroufive.resource.CachedResourceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.resource.ResourceDataSourceImpl;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.DataSourceContainer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceDataSourceSupport {

    private final static Logger LOGGER = Logger.getLogger(ServiceDataSourceSupport.class.getName());
    private final String name;
    private final Service s;
    private final ServiceEventListener sel;
    private final DataSourceContainer dsc;
    private final PropertyChangeSupport pcs;
    private final Map<ResourceId, ResourceDataSourceImpl> resources;
    private final Map<ResourceId, CachedResourceDataSourceImpl> cached;
    private final ExecutionEnv env;
    private final AtomicLong nextExpectedEventSequenceNumber = new AtomicLong(-1);

    public ServiceDataSourceSupport(ExecutionEnv env, String serviceName, final Service s, DataSourceContainer dsc, PropertyChangeSupport pcs) {
        this.name = serviceName;
        this.s = s;
        this.dsc = dsc;
        this.pcs = pcs;
        this.sel = new SEL();
        this.resources = new HashMap<ResourceId, ResourceDataSourceImpl>();
        this.cached = new HashMap<ResourceId, CachedResourceDataSourceImpl>();
        this.env = env;
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                DataSource.EVENT_QUEUE.post(new RefreshAction());
                DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                try {
                    s.addServiceEventListener(sel);
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                }
                return null;
            }
        });
    }

    public Service getService() {
        return s;
    }

    private class SEL implements ServiceEventListener {

        public void resourceRequest(final ResourceRequestEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_REQUEST_PROPERTY, null, arg0);
                    try {
                        List<SLOState> slostates = getService().getSLOStates();
                        pcs.firePropertyChange(ServiceDataSource.SERVICE_SLOSTATES_PROPERTY, null, slostates);
                    } catch (GrmRemoteException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }
                    return null;
                }
            });
        }

        public void addResource(final AddResourceEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceAdded(final ResourceAddedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void removeResource(final RemoveResourceEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceRemoved(final ResourceRemovedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceRejected(final ResourceRejectedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceError(final ResourceErrorEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceReset(final ResourceResetEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void resourceChanged(final ResourceChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new AddResourceAction(arg0, arg0.getResource()));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    return null;
                }
            });
        }

        public void serviceStarting(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourcesAction(arg0, "starting"));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }

        public void serviceRunning(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RefreshAction());
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }

        public void serviceUnknown(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourcesAction(arg0, "unknown"));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }

        public void serviceShutdown(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourcesAction(arg0, "shutdown"));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }

        public void serviceError(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourcesAction(arg0, "error"));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }

        public void serviceStopped(final ServiceStateChangedEvent arg0) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    DataSource.EVENT_QUEUE.post(new RemoveResourcesAction(arg0, "stopped"));
                    DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, arg0.getNewState().toString());
                    return null;
                }
            });
        }
    }

    // not used so far
    private class CEL implements ComponentEventListener {

        public void connectionClosed() {
            // todo what to do?
        }

        public void connectionFailed() {
            // todo what to do?
        }

        public void eventsLost() {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("events lost"));
            DataSource.EVENT_QUEUE.post(new RefreshAction());
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", 0, 1);
            pcs.firePropertyChange(evt);
        }

        public void componentUnknown(ComponentStateChangedEvent arg0) {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("component unknown"));
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", null, arg0.getNewState().toString());
            pcs.firePropertyChange(evt);
        }

        public void componentStarting(ComponentStateChangedEvent arg0) {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("component starting"));
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", null, arg0.getNewState().toString());
            pcs.firePropertyChange(evt);
        }

        public void componentStarted(ComponentStateChangedEvent arg0) {
            DataSource.EVENT_QUEUE.post(new RefreshAction());
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", null, arg0.getNewState().toString());
            pcs.firePropertyChange(evt);
        }

        public void componentStopping(ComponentStateChangedEvent arg0) {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("component stopping"));
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", null, arg0.getNewState().toString());
            pcs.firePropertyChange(evt);
        }

        public void componentStopped(ComponentStateChangedEvent arg0) {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("component stopped"));
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            PropertyChangeEvent evt = new PropertyChangeEvent(this, "Refresh", null, arg0.getNewState().toString());
            pcs.firePropertyChange(evt);
        }

        public void componentReloading(ComponentStateChangedEvent arg0) {
//            DataSource.EVENT_QUEUE.post(new RemoveResourcesAction("component reloading"));
            DataSource.EVENT_QUEUE.post(new SyncCachedResourcesAction());
            pcs.firePropertyChange("Refresh", null, arg0.getNewState().toString());
        }
    }

    private abstract class Action implements Runnable {

        /**
         * All 'unexpected' exceptions are consumed and logged by {@link Action#run()}  method.
         *
         * @throws java.lang.Exception if unexpected error occurs.
         */
        public abstract void execute() throws Exception;

        public final void run() {
            try {
//                if (LOGGER.isLoggable(Level.FINE)) {
//                    LOGGER.log(Level.FINE, "scp.action.exe", new Object[]{name, this.toString()});
//                }
                execute();
            } catch (Exception ex) {
//                if (LOGGER.isLoggable(Level.WARNING)) {
//                    LogRecord lr = new LogRecord(Level.WARNING, "scp.a.unexpected");
//                    lr.setParameters(new Object[]{name, this, ex});
//                    lr.setThrown(ex);
//                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
//                    LOGGER.log(lr);
//                }
            } finally {
            }
//            if (LOGGER.isLoggable(Level.FINE)) {
//                LOGGER.log(Level.FINE, "scp.action.fini", new Object[]{name, this.toString()});
//            }
        }
    }

    /**
     * Abstract base class of all actions which has been triggered by a ServiceEvent
     * or a ComponentEvent.
     */
    private abstract class AbstractEventTriggeredAction extends Action {

        /**
         * Get the sequence number of the event
         * @return the sequence number
         */
        protected abstract long getEventSequenceNumber();

        @Override
        public final void execute() throws Exception {
            long seqNo = getEventSequenceNumber();
            long expSeqNo = nextExpectedEventSequenceNumber.get();
            if (expSeqNo == -1) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ignore.initial", new Object[]{name, this.toString()});
                }
            } else if (seqNo == 0 && expSeqNo > 0) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.restart",
                            new Object[]{name, this.toString(), seqNo, expSeqNo});
                }
                // A new life cylce of the service has started
                DataSource.EVENT_QUEUE.post(new RefreshAction());
            } else if (seqNo < expSeqNo) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.outdated",
                            new Object[]{name, this.toString(), seqNo, expSeqNo});
                }
            } else if (seqNo > expSeqNo) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "event.ingore.unexpected",
                            new Object[]{name, this.toString(), seqNo, expSeqNo});
                }
                DataSource.EVENT_QUEUE.post(new RefreshAction());
            } else {
                nextExpectedEventSequenceNumber.incrementAndGet();
                executeAction();
            }
        }

        protected abstract void executeAction() throws Exception;
    }

    /**
     * Abstract base class of all actions triggered by a service event
     * @param <T> the type of the event
     */
    private abstract class AbstractServiceEventAction<T extends AbstractServiceEvent> extends AbstractEventTriggeredAction {

        protected final T event;

        protected AbstractServiceEventAction(T event) {
            if (event == null) {
                throw new IllegalArgumentException("Event must not be null!");
            }
            this.event = event;
        }

        protected long getEventSequenceNumber() {
            return event.getSequenceNumber();
        }

        @Override
        public final String toString() {
            return String.format("[%s:ev=%s]", getClass().getSimpleName(), event);
        }
    }

    private class AddResourceAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final Resource r;

        public AddResourceAction(AbstractServiceEvent event, Resource r) {
            super(event);
            this.r = r;
        }

        @Override
        protected void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    ResourceDataSourceImpl rdr = new ResourceDataSourceImpl(env, r);
                    if (!resources.containsKey(r.getId())) {
                        resources.put(r.getId(), rdr);
                        dsc.addDataSource(rdr);
                    } else {
                        rdr = resources.get(r.getId());
                        rdr.setResource(r);
                        pcs.firePropertyChange(ServiceDataSource.SERVICE_RESOURCE_PROPERTY, null, rdr);
                    }
                    return null;
                }
            });
        }
    }

    private class RefreshAction extends Action {

        public RefreshAction() {
        }

        @Override
        public void execute() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    ServiceSnapshot serviceSnapshot = null;
                    try {
                        serviceSnapshot = s.getSnapshot();
                    } catch (ServiceNotActiveException ex) {
                        LOGGER.log(Level.INFO, name + " is not active, message: " + ex.getLocalizedMessage());
                        serviceSnapshot = new DefaultServiceSnapshot(0, ComponentState.UNKNOWN, ServiceState.UNKNOWN,
                                Collections.<Resource>emptySet(),
                                Collections.<String, Collection<Need>>emptyMap());
                    } catch (GrmRemoteException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        serviceSnapshot = new DefaultServiceSnapshot(0, ComponentState.UNKNOWN, ServiceState.UNKNOWN,
                                Collections.<Resource>emptySet(),
                                Collections.<String, Collection<Need>>emptyMap());
                    }
                    Set<ResourceId> snapid = new HashSet<ResourceId>(serviceSnapshot.getResources().size());
                    for (Resource r : serviceSnapshot.getResources()) {
                        snapid.add(r.getId());
                        ResourceDataSourceImpl rdr = new ResourceDataSourceImpl(env, r);
                        if (!resources.containsKey(r.getId())) {
                            resources.put(r.getId(), rdr);
                            dsc.addDataSource(rdr);
                        } else {
                            rdr = resources.get(r.getId());
                            rdr.setResource(r);
                            pcs.firePropertyChange(ServiceDataSource.SERVICE_RESOURCE_PROPERTY, null, rdr);
                        }
                    }
                    List<ResourceId> toRemove = new LinkedList<ResourceId>();
                    for (ResourceId key : resources.keySet()) {
                        if (!snapid.contains(key)) {
                            toRemove.add(key);
                        }
                    }
                    for (ResourceId key : toRemove) {
                        ResourceDataSourceImpl rdr = resources.remove(key);
                        if (rdr != null) {
                            dsc.removeDataSource(rdr);
                        }
                    }

                    nextExpectedEventSequenceNumber.set(serviceSnapshot.getNextEventSequenceNumber());
                    pcs.firePropertyChange(ServiceDataSource.SERVICE_STATUS_PROPERTY, null, serviceSnapshot.getServiceState().toString());
                    return null;
                }
            });
        }
    }

    private class SyncCachedResourcesAction implements
            Runnable {

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    ResourceProvider tmp = null;
                    try {
                        tmp = ComponentServiceWithClassLoader.<ResourceProvider>getComponentByType(env, ResourceProvider.class);
                    } catch (GrmException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }
                    List<Resource> snap = Collections.<Resource>emptyList();

                    if (tmp != null) {
                        try {
                            snap = tmp.getResources(name, ConstantFilter.<Resource>alwaysMatching());
                        } catch (UnknownServiceException ex) {
                            LOGGER.log(Level.INFO, name + " is not yet available as SCP, message: " + ex.getLocalizedMessage());
                        } catch (ServiceNotActiveException ex) {
                            LOGGER.log(Level.INFO, name + " is not active, message: " + ex.getLocalizedMessage());
                        } catch (InvalidServiceException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        } catch (ResourceProviderNotActiveException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        } catch (GrmRemoteException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                        }
                    }

                    List<Resource> toAdd = new LinkedList<Resource>();
                    List<ResourceId> toRemove = new LinkedList<ResourceId>();

                    for (Resource r : snap) {
                        if (!cached.containsKey(r.getId())) {
                            toAdd.add(r);
                        } else {
                            CachedResourceDataSourceImpl crds = cached.get(r.getId());
                            crds.setResource(r);
                            pcs.firePropertyChange(ServiceDataSource.SERVICE_RESOURCE_PROPERTY, null, crds);
                        }
                    }

                    final List<ResourceId> idz = getIds(snap);
                    for (ResourceId id : cached.keySet()) {
                        if (!idz.contains(id)) {
                            toRemove.add(id);
                        }
                    }

                    for (Resource r : toAdd) {
                        CachedResourceDataSourceImpl crds = new CachedResourceDataSourceImpl(env, r);
                        cached.put(r.getId(), crds);
                        dsc.addDataSource(crds);
                    }

                    for (ResourceId id : toRemove) {
                        CachedResourceDataSource crds = cached.get(id);
                        cached.remove(id);
                        dsc.removeDataSource(crds);
                    }
                    return null;
                }
            });
        }
    }

    private class RemoveResourceAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final Resource r;

        public RemoveResourceAction(AbstractServiceEvent event, Resource r) {
            super(event);
            this.r = r;
        }

        @Override
        public void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    if (resources.containsKey(r.getId())) {
                        ResourceDataSource rdr = resources.remove(r.getId());
                        if (rdr != null && !rdr.isRemoved()) {
                            dsc.removeDataSource(rdr);
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class RemoveResourcesAction extends AbstractServiceEventAction<AbstractServiceEvent> {

        private final String id;

        public RemoveResourcesAction(AbstractServiceEvent event, String id) {
            super(event);
            this.id = id;
        }

        public void executeAction() throws Exception {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    Iterator<Map.Entry<ResourceId, ResourceDataSourceImpl>> i = resources.entrySet().iterator();
                    while (i.hasNext()) {
                        ResourceDataSource rdr = i.next().getValue();
                        if (rdr != null && !rdr.isRemoved()) {
                            dsc.removeDataSource(rdr);
                        }
                        i.remove();
                    }
                    return null;
                }
            });
        }
    }

    private List<ResourceId> getIds(Collection<Resource> rez) {
        List<ResourceId> list = new ArrayList<ResourceId>(rez.size());
        for (Resource r : rez) {
            list.add(r.getId());
        }
        return list;
    }
}

