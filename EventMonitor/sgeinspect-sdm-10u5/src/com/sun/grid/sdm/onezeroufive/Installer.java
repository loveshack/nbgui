/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.sdm.onezeroufive;

import com.sun.grid.sdm.onezeroufive.jvm.JvmDataSourceFactoryImpl;
import org.openide.modules.ModuleInstall;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    private static final long serialVersionUID = -2009100101L;

    @Override
    public void restored() {
        JvmDataSourceFactoryImpl.initialize();
    }
}
