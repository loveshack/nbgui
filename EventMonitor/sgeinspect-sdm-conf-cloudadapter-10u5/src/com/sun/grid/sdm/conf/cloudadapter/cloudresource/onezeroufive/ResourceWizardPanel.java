/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.cloudresource.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.grid.sdm.conf.parameter.onezeroufive.ParameterConfigHelper;
import java.awt.Component;
import org.openide.util.Exceptions;

public class ResourceWizardPanel extends ConfWizardPanel<ResourceVisualPanel> {

    private ParameterConfigHelper parameterConfigHelper;

    public ResourceWizardPanel() {
        super(new ResourceVisualPanel());
        try {
            this.parameterConfigHelper = new ParameterConfigHelper("Action Parameter");
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public void setupActionListeners() {

        this.addEditTableActionListeners(parameterConfigHelper, panel.tblResourceConfig_ResParams,
                panel.btnResourceConfig_ResParamsAdd,
                panel.btnResourceConfig_ResParamsRemove);
        
        this.setupCheckBoxControlledComponents(panel.chkResourceConfig_AmiIdIsSet, new Component[]{
                panel.lblResourceConfig_AmiId,  
                panel.txtResourceConfig_AmiId
        });

        this.setupCheckBoxControlledComponents(panel.chkResourceConfig_InstanceTypeIsSet, new Component[]{
                panel.lblResourceConfig_InstanceType,
                panel.cboResourceConfig_InstanceType
        });

        this.setupCheckBoxControlledComponents(panel.chkResourceConfig_UnboundNameIsSet, new Component[]{
                panel.lblResourceConfig_UnboundName,
                panel.txtResourceConfig_UnboundName
        });
        this.validateNonEmpty(panel.lblResourceConfig_UnboundName, panel.txtResourceConfig_UnboundName);
        this.validateNonEmptyNonWhiteSpace(panel.lblResourceConfig_AmiId, panel.txtResourceConfig_AmiId);
    }
}
