/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.advanced.onezeroufive;

import com.sun.grid.sdm.conf.cloudadapter.onezeroufive.CloudAdapterConfigHelper;
import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import java.util.List;
import org.openide.WizardDescriptor;
import org.openide.util.NbBundle;

public class CloudAdapterWizardPanel1 extends ConfWizardPanel<CloudAdapterVisualPanel1> {

    public CloudAdapterWizardPanel1() {
        super(new CloudAdapterVisualPanel1());
    }

    @Override
    protected void setupActionListeners() {
        this.validateNonEmptyNonWhiteSpace(panel.lblCloudAdapterConfiguration_Name, panel.txtCloudAdapterConfiguration_Name);
        @SuppressWarnings("unchecked")
        List<String> usedList = (List<String>) wd.getProperty(CloudAdapterConfigHelper.USED_SERVICE_NAMES);
        if (usedList != null) {
            this.validateNotOnUsedList(panel.lblCloudAdapterConfiguration_Name, panel.txtCloudAdapterConfiguration_Name, usedList);
        }
        this.validatePositiveInteger(panel.lblCloudAdapterConfiguration_MaxCloudHostsInSystemLimit, panel.txtCloudAdapterConfiguration_MaxCloudHostsInSystemLimit);

        //lets write a warning to the bottom line in case that the user disables the autostart feature!
         panel.chkCloudAdapterConfiguration_AutoStarting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(!panel.chkCloudAdapterConfiguration_AutoStarting.isSelected()){
                    String msg = NbBundle.getMessage(CloudAdapterWizardPanel1.class, "NO_UNBOUND_RESOURCE_CONFIGURATION_POSSIBLE_IF_\"START_IMMEDIATELY\"_OPTION_IS_DISABLED!");
                    wd.putProperty(WizardDescriptor.PROP_WARNING_MESSAGE, msg);
                }else{
                    wd.putProperty(WizardDescriptor.PROP_WARNING_MESSAGE, null);
                    
                }
            }
        });



    }
}

