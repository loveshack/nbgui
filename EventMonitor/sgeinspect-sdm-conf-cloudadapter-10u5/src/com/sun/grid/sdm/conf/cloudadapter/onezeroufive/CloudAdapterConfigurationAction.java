/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.ui.component.ModifyComponentConfigurationCommand;


import com.sun.grid.grm.ui.component.ReloadComponentCommand;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.api.service.cloud.CloudServiceDataSource;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.tools.visualvm.application.Application;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

abstract public class CloudAdapterConfigurationAction extends CloudAdapterAction {

    final ServiceData extractServiceData(Node[] activatedNodes) {
        return new ConfigurationServiceData(activatedNodes);
    }

    final CloudAdapterConfigWrapper getOldConfig(ServiceData serviceData) {
        try {
            GetConfigurationCommand<ComponentConfig> cmd = new GetConfigurationCommand<ComponentConfig>();
            cmd.setConfigName(serviceData.getServiceName());
            ComponentConfig conf = serviceData.getEnv().getCommandService().execute(cmd).getReturnValue();
            CloudAdapterConfigWrapper wrapper = newCloudAdapterConfigWrapperInstance();
            wrapper.wrapConfig((CloudAdapterConfig) conf);
            wrapper.setName(serviceData.getServiceName());
            wrapper.setHost(serviceData.getHost());
            wrapper.setJvm(serviceData.getJvm());
            return wrapper;
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    final void storeNewConfig(CloudAdapterConfigWrapper wrapper,ServiceData serviceData) {
        try {
            ModifyComponentConfigurationCommand mod = new ModifyComponentConfigurationCommand(wrapper.getName(), wrapper.unwrapConfig());
            //Modify the configuration of component
            Result res = serviceData.getEnv().getCommandService().execute(mod);
            //System.out.println("Successfully modified? " + res.getReturnValue());


            ReloadComponentCommand rel = new ReloadComponentCommand();
            rel.setComponentName(wrapper.getName());
            rel.setHostname(wrapper.getHost());
            rel.setForced(true);
            //Modify the configuration of component
            Result res2 = serviceData.getEnv().getCommandService().execute(rel);
            //System.out.println("Successfully updated? " + res2.getReturnValue());

            //TODO

        } catch (GrmException ex) {//TODO
            Exceptions.printStackTrace(ex);
        }
    }
    
    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{CloudServiceDataSource.class};//JvmWrapperDataSource.class};
    }

    protected boolean isEnabled(ServiceDataSource ds) {
        return true;
    }

    final class ConfigurationServiceData extends ServiceData {

        ConfigurationServiceData(Node[] activatedNodes) {

            ServiceDataSource serviceDS = getServiceDataSource(activatedNodes);
            env = ExecEnvUtil.getExecutionEnv((Application) serviceDS.getOwner());
            host = serviceDS.getHostname();
            jvm = serviceDS.getJvmName();
            csHost = env.getCSHost().getHostname();
            serviceName = getCloudAdapterServiceName(activatedNodes);
        }

        //HELPERS
        private ServiceDataSource getServiceDataSource(Node[] activatedNodes) {
            for (Node n : activatedNodes) {
                return n.getLookup().lookup(ServiceDataSource.class);
            }
            return null;
        }

        private String getCloudAdapterServiceName(Node[] activatedNodes) {
            for (Node n : activatedNodes) {
                return n.getDisplayName();
            }
            return null;
        }
    }
}
