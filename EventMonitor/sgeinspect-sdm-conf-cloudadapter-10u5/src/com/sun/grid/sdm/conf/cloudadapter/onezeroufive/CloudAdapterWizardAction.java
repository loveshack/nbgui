/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.config.gef.Parameter;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.impl.cloud.CloudServiceImpl;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.component.InstantiateComponentCommand;
import com.sun.grid.grm.ui.resource.AddResourceCommand;
import com.sun.grid.grm.ui.resource.ResourceActionResult;

import com.sun.grid.grm.util.Hostname;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.conf.cloudadapter.cloudresource.onezeroufive.ResourceConfig;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.grid.sdm.ui.navigator.node.CloudAdapterWizardCookie;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.io.InputStream;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

abstract public class CloudAdapterWizardAction extends CloudAdapterAction {

    ServiceData extractServiceData(Node[] activatedNodes) {
        return new WizardServiceData(activatedNodes);
    }

    final CloudAdapterConfigWrapper getOldConfig(ServiceData serviceData) {
        try {
            CloudAdapterConfig config = null;
            try {
                config = (CloudAdapterConfig) XMLUtil.load(getDefaultCloudAdapterConfigInputStream());
            } catch (ClassCastException ex) {
                Exceptions.printStackTrace(ex);
            }
            CloudAdapterConfigWrapper wrapper = newCloudAdapterConfigWrapperInstance();
            wrapper.wrapConfig(config);

            wrapper.setHost(serviceData.getHost());
            wrapper.setJvm(serviceData.getJvm());
            wrapper.flagWizardMode(true);

            return wrapper;
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    final void storeNewConfig(CloudAdapterConfigWrapper wrapper, ServiceData serviceData) {
        try {
            AddComponentWithConfigurationCommand aeCmd = AddComponentWithConfigurationCommand.newInstanceForSingleton().componentName(wrapper.getName()).classname(CloudServiceImpl.class.getName()).config(wrapper.unwrapConfig()).hostname(wrapper.getHost()).jvmName(wrapper.getJvm());
            //add service to Global config and create service config
            ExecutionEnv env = serviceData.getEnv();
            Result res = env.getCommandService().execute(aeCmd);



            //start the service if is AutoStarting
            if (wrapper.isAutoStarting()) {
                InstantiateComponentCommand cmd = new InstantiateComponentCommand();
                cmd.setComponentName(wrapper.getName());
                cmd.setHostName(wrapper.getHost());
                cmd.setJvmName(wrapper.getJvm());
                env.getCommandService().execute(cmd);

                //System.out.println("Successfully modified? " + res.getReturnValue());
                // now lets add all unbound resources:
                //do not take host /jvm /service name from ServieData because it might have been altered in the wizard!
                if (checkServiceRunning(env, wrapper.getHost(), wrapper.getJvm(), wrapper.getName())) {
                    addUnboundResources(wrapper, env);
                } else {
                    if (wrapper.getUnboundRes().size() != 0) {
                        ErrorDisplayer.submitWarning(NbBundle.getMessage(CloudAdapterWizardAction.class, "UNABLE_TO_ADD_UNBOUND_RESOURCES._SERVICE_{0}_IS_NOT_RUNNING!", wrapper.getName()));
                    }
                }
            }
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private boolean checkServiceRunning(ExecutionEnv env, String host, String jvm, String serviceName) {
        boolean retry = true;
        boolean firstTime = true;
        boolean running = false;
        boolean addedListener = false;
        ComponentState cs = ComponentState.UNKNOWN;
        ServiceState ss = ServiceState.UNKNOWN;

        ServiceRunningListener sel = new ServiceRunningListener();
        Service service = null;

        try {
            while (retry) {
                //try to get a service
                if (service == null) {
                    service = getService(env, host, jvm, serviceName);
                }
                //try to see if it is running
                if (service != null) {
                    service.addServiceEventListener(sel);
                    addedListener=true;
                    cs = service.getState();
                    ss = service.getServiceState();

                    if (ss == ServiceState.RUNNING) {
                        running=true;
                        break;
                    }
                }
                //if we have waited in the previous loop, ask before more waiting
                if (firstTime) {
                    firstTime = false;
                } else {
                    String message = NbBundle.getMessage(CloudAdapterWizardAction.class, "PROBLEM_TO_START_NEW_SERVICE_{0}._IT_IS_IN_THE_SERVICE_STATE_{1}._DO_YOU_WANT_TO_RETRY?", serviceName, ss); //+ ss.toString();
                    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(
                            message, NbBundle.getMessage(CloudAdapterWizardAction.class, "UNEXPECTED_PROBLEM"),
                            NotifyDescriptor.OK_CANCEL_OPTION);
                    retry = (DialogDisplayer.getDefault().notify(nd) == NotifyDescriptor.OK_OPTION);
                }
                // if we did not cancel until now we will wait 10 secs
                if (retry) {
                    try {
                        //wait 10 sec if service state does not switch to running!
                        //it will also wait for 10 secs if the service is null
                        if (sel.isRunning(10000)) {
                               running=true;
                               break;
                        }
                    } catch (InterruptedException ex) {
                        //for the sake of Unverbosity
                        //Exceptions.printStackTrace(ex);
                    }
                }
            }
        } catch (GrmException ex) {
            ErrorDisplayer.submitException(ex);
        } finally {
            //remove the SEL if it was added!
            if(service !=null && addedListener){
                try {
                    service.removeServiceEventListener(sel);
                } catch (GrmRemoteException ex) {
                    ErrorDisplayer.submitException(ex);
                }
            }
        }
        return running;
    }

    private Service getService(ExecutionEnv env, String host, String jvm, String serviceName) {
        try {
            ComponentInfo info = ComponentInfo.newInstance(env, Hostname.getInstance(host), jvm, serviceName, Service.class);
            return ComponentService.<Service>getComponent(env, info);
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{CloudAdapterWizardCookie.class};
    }

    /*
    protected boolean isEnabled(JvmWrapperDataSource ds) {
    return true;
    }
     */
    /*  # Default values for host resource
    #
    static = false
    unbound_name = <String, mandatory>
    # hardwareCpuArchitecture = <String, optional>
    # hardwareCpuCount = <Integer, optional>
    # hardwareCpuFrequency = <String, optional>
    # hardwareTotalMemory = <Double, optional>
    # operatingSystemName = <String, optional>
    # operatingSystemPatchlevel = <String, optional>
    # operatingSystemRelease = <String, optional>
    # operatingSystemVendor = <String, optional>
    # resourceHostname = <Hostname, optional>
    # resourceIPAddress = <String, optional>
    # totalSwap = <Double, optional>
    # totalVirtualMemory = <Double, optional> */
    private void addUnboundResources(CloudAdapterConfigWrapper wrapper, ExecutionEnv env) {
        try {
            ResourceType rt = HostResourceType.getInstance();
            int counter = 0;
            for (ResourceConfig resConf : wrapper.unwrapUnboundRes()) {
                Map<String, Object> props = new HashMap<String, Object>();
                props.put("static", resConf.isIsStatic());

                String resUnboundName = resConf.getUnboundName();
                if (resUnboundName == null) {
                    resUnboundName = wrapper.getName() + "" + (counter++);
                }
                props.put("unbound_name", resUnboundName);

                if (resConf.getAmiId() != null) {
                    props.put("amiId", resConf.getAmiId());
                }

                if (resConf.getInstanceType() != null) {
                    props.put("instanceType", resConf.getInstanceType().toString());
                }
                for (Parameter param : resConf.getResParams()) {
                    props.put(param.getName(), param.getValue());
                }
                AddResourceCommand cmd = new AddResourceCommand(wrapper.getName(), rt, props);
                Result<List<ResourceActionResult>> res = cmd.execute(env);
                for (ResourceActionResult r : res.getReturnValue()) {
                    //System.out.println("Successfully Added Message "+resUnboundName+"? " +r.getMessage());
                    //System.out.println("Resource "+resUnboundName+"? " +r.getResourceName());
                }
            }
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private InputStream getDefaultCloudAdapterConfigInputStream() {
        ClassLoader cl = getClass().getClassLoader();
        InputStream is = cl.getResourceAsStream("/com/sun/grid/sdm/conf/cloudadapter/onezeroufive/defaultCloudAdapterConfig.xml");

        return is;
    }

    //The Configure Actions are attached to the JVM data source
    class WizardServiceData extends ServiceData {
        WizardServiceData(Node[] activatedNodes) {
            JvmDataSource jvmDataSource = activatedNodes[0].getLookup().lookup(JvmDataSource.class);
            host = jvmDataSource.getHost();
            jvm = jvmDataSource.getName();
            env = ExecEnvUtil.getExecutionEnv(jvmDataSource.getApplication());
            csHost = env.getCSHost().getHostname();
        }
    }

    // this listener can detect a started service
    private class ServiceRunningListener extends ServiceEventAdapter {

        private final Lock lock = new ReentrantLock();
        private final Condition cond = lock.newCondition();
        boolean isRunning = false;

        @Override
        public void serviceRunning(ServiceStateChangedEvent evt) {
            lock.lock();
            try {
                isRunning = true;
                cond.signalAll();
            } finally {
                lock.unlock();
            }
        }

        public boolean isRunning(long timeOut) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeOut;
            while (true) {
                lock.lock();
                try {
                    if (isRunning) {
                        return true;
                    }
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        return false;
                    } else {
                        cond.await(rest, TimeUnit.MILLISECONDS);
                    }
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}
