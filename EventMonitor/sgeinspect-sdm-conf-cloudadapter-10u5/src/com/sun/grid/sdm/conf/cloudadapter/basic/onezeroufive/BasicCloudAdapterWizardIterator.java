/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sdm.conf.cloudadapter.basic.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardIterator;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;
import org.openide.util.NbBundle;

public final class BasicCloudAdapterWizardIterator extends ConfWizardIterator{


    private List<WizardDescriptor.Panel<WizardDescriptor>> panels;

    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     */
    public List<WizardDescriptor.Panel<WizardDescriptor>> getPanels() {
        if (panels == null) {
            panels = new ArrayList <WizardDescriptor.Panel<WizardDescriptor>>();
            BasicCloudAdapterWizardPanel1 panel1 = new BasicCloudAdapterWizardPanel1();

            //disable a set of components if in configuration mode!!
            panel1.getPanel().txtCloudAdapterConfiguration_Name.setEditable(!this.isConfigureMode());
            panel1.getPanel().chkCloudAdapterConfiguration_Ec2AmiIdIsSet.setEnabled(!this.isConfigureMode());
            panel1.getPanel().txtCloudAdapterConfiguration_Ec2AmiId.setEnabled(!this.isConfigureMode());
            panel1.getPanel().lblCloudAdapterConfiguration_Ec2AmiId.setEnabled(!this.isConfigureMode());
            panel1.getPanel().lblCreateDirectly.setEnabled(!this.isConfigureMode());
            //Default
            panel1.getPanel().chkCloudAdapterConfiguration_Ec2AmiIdIsSet.setSelected(false);



            panels.add(panel1);
            panels.add(new BasicCloudAdapterWizardPanel2());
            panels.add(new BasicCloudAdapterWizardPanel3());




            /*panels.add(new BasicCloudAdapterWizardPanel3());
            panels.add(new CloudAdapterWizardPanel4());
            panels.add(new CloudAdapterWizardPanel6());*/

            String[] steps = new String[panels.size()];
            for (int i = 0; i < panels.size(); i++) {
                Component c = panels.get(i).getComponent();
                // Default step name to component name of panel.
                steps[i] = c.getName();
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    // Sets step number of a component
                    // TODO if using org.openide.dialogs >= 7.8, can use WizardDescriptor.PROP_*:
                    jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                    // Sets steps names for a panel
                    jc.putClientProperty("WizardPanel_contentData", steps);
                    // Turn on subtitle creation on each step
                    jc.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
                    // Show steps on the left side with the image on the background
                    jc.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
                    // Turn on numbering of all steps
                    jc.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
                }
            }
        }
        return panels;
    }


    @Override
    public String name() {
        return NbBundle.getMessage(BasicCloudAdapterWizardIterator.class, "CLOUD_ADAPTER");
    }


}
