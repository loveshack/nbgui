/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.sdm.conf.cloudadapter.cloudresource.onezeroufive.ResourceConfig;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 */
public class CloudAdapterConfigWrapper {

    protected CloudAdapterConfig config;
    private String SERVICE_NAME_PLACE_HOLDER = "[SERVICE_NAME]";
    private final static long serialVersionUID = -2009191601L;
    @XmlElement(required = true)
    private String jvm;
    @XmlElement(required = true)
    private boolean autoStarting = true;
    @XmlElement(required = true)
    private String host;
    @XmlElement(required = true)
    private String name = "cloud_adapter";
    private boolean wizardMode=false;
    final protected List<ResourceConfig> unboundRes = new ArrayList<ResourceConfig>();
    public CloudAdapterConfigWrapper() {
        config = new CloudAdapterConfig();
    }

    final public String getJvm() {
        return jvm;
    }

    final public String getHost() {
        return host;
    }

    final public String getName() {
        return name;
    }

    final public void setHost(String host) {
        this.host = host;
    }

    final public boolean isAutoStarting() {
        return autoStarting;
    }

    final public void setAutoStarting(boolean autoStarting) {
        this.autoStarting = autoStarting;
    }

    final public void setJvm(String jvm) {
        this.jvm = jvm;
    }

    final public void setName(String name) {
        this.name = name;
    }

    final public int getMaxCloudHostsInSystemLimit() {
        return config.getMaxCloudHostsInSystemLimit();
    }

    final public boolean isSetMaxCloudHostsInSystemLimit() {
        return config.isSetMaxCloudHostsInSystemLimit();
    }

    final public void setMaxCloudHostsInSystemLimit(int value) {
        config.setMaxCloudHostsInSystemLimit(value);
    }


    final public List<ResourceConfig> getUnboundRes() {
        return unboundRes;
    }

    public List<ResourceConfig> unwrapUnboundRes() {
        return getUnboundRes();
    }

  
    //access to the internal data

     public CloudAdapterConfig unwrapConfig() {
        finishSLOSetup();
        return config;
    }

    protected void finishSLOSetup(){
        SLOSet set = config.getSlos();
        if (set != null) {
            for (SLOConfig slo : set.getSlo()) {
                if (slo.isSetRequest()) {
                    String request = slo.getRequest();
                    while (request.contains(SERVICE_NAME_PLACE_HOLDER)) {
                        request = request.replace(SERVICE_NAME_PLACE_HOLDER, this.name);
                    }
                    slo.setRequest(request);
                }
                if (slo.isSetResourceFilter()) {
                    String resFilter = slo.getResourceFilter();
                    while (resFilter.contains(SERVICE_NAME_PLACE_HOLDER)) {
                        resFilter = resFilter.replace(SERVICE_NAME_PLACE_HOLDER, this.name);
                    }
                    slo.setResourceFilter(resFilter);
                }
            }
        }
    }

    public boolean flaggedWizardMode() {
        return wizardMode;
    }

    public void flagWizardMode(boolean wizardMode) {
        this.wizardMode = wizardMode;
    }

    public void wrapConfig(CloudAdapterConfig config) {
        this.config = config;
    }
}
