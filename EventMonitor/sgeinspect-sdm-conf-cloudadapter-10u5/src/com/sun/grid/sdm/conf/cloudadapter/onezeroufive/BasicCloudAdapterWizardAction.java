/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.sdm.conf.cloudadapter.basic.onezeroufive.BasicCloudAdapterConfigHelper;
import com.sun.grid.sdm.conf.cloudadapter.basic.onezeroufive.BasicCloudAdapterConfiguration;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import org.openide.util.NbBundle;
//import com.sun.grid.shared.main.util.ClassLoaderAnchor;

final public class BasicCloudAdapterWizardAction extends CloudAdapterWizardAction {

    private static final long serialVersionUID = -200941001L;
    private static final BasicCloudAdapterWizardAction SINGLETON = new BasicCloudAdapterWizardAction();

    public static final BasicCloudAdapterWizardAction getInstance() {
        return SINGLETON;
    }

    private BasicCloudAdapterWizardAction() {
        this.setConfigureMode(false);
    }

    public String getName() {
        return NbBundle.getMessage(CloudAdapterWizardAction.class, "CTL_BasicCloudAdapterWizardAction");
    }

    String getTitle(String serviceName) {
        return getName() + "( " + serviceName + ")";
    }
    
    CloudAdapterConfigWrapper newCloudAdapterConfigWrapperInstance(){
        BasicCloudAdapterConfiguration conf = new BasicCloudAdapterConfiguration();
        return conf;
    }

    ConfHelper<?> newCloudAdapterConfHelperInstance(ServiceData serviceData) throws UnexpectedStructureException {
        //we set the cs host as the location for the cloud adapter in the 
        return new BasicCloudAdapterConfigHelper(serviceData.getEnv(),serviceData.getCsHost(),NbBundle.getMessage(BasicCloudAdapterWizardAction.class, "BASIC_CLOUD_ADAPTER_WIZARD"));
    }
}
