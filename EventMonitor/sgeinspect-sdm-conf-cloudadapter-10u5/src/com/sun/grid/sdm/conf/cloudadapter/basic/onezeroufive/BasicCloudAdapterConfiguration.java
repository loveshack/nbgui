/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.basic.onezeroufive;

import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.config.cloud.CloudGefConfig;
import com.sun.grid.grm.config.gef.Parameter;
import com.sun.grid.sdm.conf.cloudadapter.cloudresource.onezeroufive.ResourceConfig;
import com.sun.grid.sdm.conf.cloudadapter.onezeroufive.CloudAdapterConfigWrapper;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class BasicCloudAdapterConfiguration extends CloudAdapterConfigWrapper{
    private final static String PARAM_INSTANCE_TYPE = "instance_type";
    private final static String PARAM_JAVA_HOME= "ec2_java_home";
    private final static String PARAM_KEY_PAIR= "ec2_user_key_pair";
    private final static String PARAM_KEY_PAIR_FILE= "ec2_user_key_pair_file";
    private final static String PARAM_SECURITY_GROUP = "security_group";
    private final static String PARAM_EC2_INSTALL_DIR = "ec2_tools_install_dir";
    private final static String PARAM_CERTIFICATE = "ec2_user_certificate_file";
    private final static String PARAM_PRIVATE_KEY_FILE = "ec2_user_private_key_file";
    private final static String PARAM_VPN_BIN_DIR ="vpn_bin_dir";
    private final static String PARAM_VPN_CONFIG_DIR ="vpn_config_dir";

    private final static long serialVersionUID = -2009191601L;
    private static CloudAdapterConfig dirtySave;//todo
    private String ec2AmiId;

    private ParameterEditor paramEdit;

    public BasicCloudAdapterConfiguration() {
        paramEdit = new ParameterEditor(getGef().getParam());
    }
    
    @Override
    public void wrapConfig(CloudAdapterConfig config) {
        this.config = config;
        //this is usually called with a default config or an existing one
        dirtySave =config;
        paramEdit = new ParameterEditor(getGef().getParam());
    }

    @Override
    public void flagWizardMode(boolean wizardMode) {
        super.flagWizardMode(wizardMode);
        //if the wizard mode is set we wil reset the paramEdit! (no need for the @@@ placeholders @@@)
        if(wizardMode) {
           paramEdit.reset();
        }
    }



    @Override
    public CloudAdapterConfig unwrapConfig() {
        //the current configuration holds only the MaxCloudHostsInSystemLimit
        int maxCloudHostsInSystemLimit = config.getMaxCloudHostsInSystemLimit();

        //overwrite that poor config with one that is fully configured
        config = dirtySave;// this is the configuration object the dialog was based on
        // now the slo setup is parsed!
        finishSLOSetup(); 
        
        //adjust the gef settings
        CloudGefConfig gef = getGef();
        List<Parameter> param = gef.getParam();
        paramEdit.saveParameters(param);

        //set the max cloud hosts
         config.setMaxCloudHostsInSystemLimit(maxCloudHostsInSystemLimit);
        return config;

        
    }

    
    private CloudGefConfig getGef() {
        CloudGefConfig gef = config.getGef();
        if (gef == null) {
            gef = new CloudGefConfig();
            config.setGef(gef);
        }
        return gef;
    }

    @Override
    public List<ResourceConfig> unwrapUnboundRes() {
        //generate a default set of resources!
        List<ResourceConfig> list = getUnboundRes();
        if(this.ec2AmiId!=null && !"".equals(this.ec2AmiId)){
            for(int i=0;i<this.getMaxCloudHostsInSystemLimit();i++){
                ResourceConfig newRes =new ResourceConfig();
                newRes.setAmiId(ec2AmiId);
                newRes.setIsStatic(false);
                newRes.setUnboundName(this.getName()+""+i);
                list.add(newRes);
            }
        }
        return list;
    }


   



    /*
    <gef:param value="@@@Please_enter_your_install_directory_of_the_EC2_tools@@@" name="ec2_tools_install_dir"/>
    <gef:param value="@@@Please_enter_the_name_of_the_EC2_security_group@@@" name="security_group"/>
    <gef:param value="@@@Please_enter_the_name_of_your_EC2_key_pair@@@" name="ec2_user_key_pair"/>
    <gef:param value="@@@Please_enter_the_name_of_the_EC2_key_pair_file@@@" name="ec2_user_key_pair_file"/>
    <gef:param value="@@@Please_enter_the_path_to_the_EC2_user_private_key_file_(pk-*.pem)@@@" name="ec2_user_private_key_file"/>
    <gef:param value="@@@Please_enter_the_path_to_the_EC2_user_certificate_file_(cert-*.pem)@@@" name="ec2_user_certificate_file"/>
    <gef:param value="@@@Please_enter_your_VPN_configuration_directory@@@" name="vpn_config_dir"/>
    <gef:param value="@@@Please_enter_your_VPN_bin_directory@@@" name="vpn_bin_dir"/>
    <gef:param value="@@@Please_enter_the_JAVA_HOME_directory_path_for_the_EC2_TOOLS@@@" name="ec2_java_home"/>
    <gef:param value="m1.small" name="instance_type"/
     */



    public String getEc2InstanceType() {
        return paramEdit.get(PARAM_INSTANCE_TYPE);
    }

    public void setEc2InstanceType(String ec2InstanceType) {
        paramEdit.put(PARAM_INSTANCE_TYPE, ec2InstanceType);
    }

    public String getEc2JavaHome() {
        return paramEdit.get(PARAM_JAVA_HOME);
    }

    public void setEc2JavaHome(String ec2JavaHome) {
        paramEdit.put(PARAM_JAVA_HOME, ec2JavaHome);
    }

    public String getEc2KeyPair() {
        return paramEdit.get(PARAM_KEY_PAIR);
    }

    public void setEc2KeyPair(String ec2KeyPair) {
        paramEdit.put(PARAM_KEY_PAIR, ec2KeyPair);
    }

    public String getEc2KeyPairFile() {
        return paramEdit.get(PARAM_KEY_PAIR_FILE);
    }

    public void setEc2KeyPairFile(String ec2KeyPairFile) {
        paramEdit.put(PARAM_KEY_PAIR_FILE, ec2KeyPairFile);
    }

    public String getEc2SecurityGroup() {
         return paramEdit.get(PARAM_SECURITY_GROUP);
    }

    public void setEc2SecurityGroup(String ec2SecurityGroup) {
        paramEdit.put(PARAM_SECURITY_GROUP, ec2SecurityGroup);
    }

    public String getEc2ToolsInstallDir() {
        return paramEdit.get(PARAM_EC2_INSTALL_DIR);
    }

    public void setEc2ToolsInstallDir(String ec2ToolsInstallDir) {
        paramEdit.put(PARAM_EC2_INSTALL_DIR, ec2ToolsInstallDir);
    }

    public String getEc2UserCertificateFile() {
         return paramEdit.get(PARAM_CERTIFICATE);
    }

    public void setEc2UserCertificateFile(String ec2UserCertificateFile) {
        paramEdit.put(PARAM_CERTIFICATE, ec2UserCertificateFile);
    }

    public String getEc2UserPrivateKeyFile() {
         return paramEdit.get(PARAM_PRIVATE_KEY_FILE);
    }

    public void setEc2UserPrivateKeyFile(String ec2UserPrivateKeyFile) {
         paramEdit.put(PARAM_PRIVATE_KEY_FILE, ec2UserPrivateKeyFile);
    }

    public String getEc2VpnBinDir() {
         return paramEdit.get(PARAM_VPN_BIN_DIR);
    }

    public void setEc2VpnBinDir(String ec2VpnBinDir) {
         paramEdit.put(PARAM_VPN_BIN_DIR, ec2VpnBinDir);
    }

    public String getEc2VpnConfigurationDir() {
        return paramEdit.get(PARAM_VPN_CONFIG_DIR);
    }

    public void setEc2VpnConfigurationDir(String ec2VpnConfigurationDir) {
         paramEdit.put(PARAM_VPN_CONFIG_DIR, ec2VpnConfigurationDir);
    }

    public String getEc2AmiId() {
        return this.ec2AmiId;
    }

    public void setEc2AmiId(String ec2AmiId) {
         this.ec2AmiId =ec2AmiId;
    }

    

    private class ParameterEditor {

        HashMap<String, Parameter> lookup = new HashMap<String, Parameter>();

        ParameterEditor(List<Parameter> params) {
            // in case that the configuration contains the defaults .. delete the placeholders!
            for(Parameter param:params){
                String name =param.getName();
                if(name!=null &&!"".equals(name.trim())){
                    lookup.put(name, param);
                }
            }
        }

        String get(String name) {
            Parameter param = lookup.get(name);
            if (param == null) {
                return "";
            } else {
                return param.getValue();
            }

        }

        void put(String name, String value) {
            Parameter param = lookup.get(name);
            if (param == null) {
                param = new Parameter();
                lookup.put(name, param);
            }
            param.setName(name);
            param.setValue(value);
           
        }
        void reset(){
            this.lookup.clear();
        }

         void saveParameters( List<Parameter> params) {
            params.clear();// delete the existing ones!
            for (Parameter param : lookup.values()) {
                //dont save empty Strings! needed for optional Java Home 
                if(!"".equals(param.getValue())){
                    params.add(param);
                }
            }
        }
    }
}
