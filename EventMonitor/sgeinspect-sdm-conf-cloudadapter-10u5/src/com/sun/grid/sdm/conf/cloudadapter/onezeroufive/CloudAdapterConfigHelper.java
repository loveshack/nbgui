/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.sdm.conf.slo.onezeroufive.SloConfigHelper;
import com.sun.grid.sdm.conf.step.onezeroufive.StepConfigHelper;
import com.sun.grid.sdm.conf.utils.onezeroufive.CascadingComboBoxModel;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.ConfWizardIterator;
import com.sun.grid.shared.ui.confgui.SubClassRegistry;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

/**
 *
 */
abstract public class CloudAdapterConfigHelper<ConfObject extends CloudAdapterConfigWrapper> extends ConfHelper<ConfObject> {

    private ExecutionEnv env;
    private String host;
    public final static String USED_SERVICE_NAMES = "txtCloudAdapterConfiguration_Name_usedNamesList";

    public CloudAdapterConfigHelper(Class<? extends ConfWizardIterator> iteratorClass, Class<ConfObject> confClass, String confClassName, ConfObject[] defaultConfs, String wizardTitle) throws UnexpectedStructureException {
        super(iteratorClass, confClass, confClassName, defaultConfs, wizardTitle);
    }

    private enum JVM_NAMES {
        rp_vm, cs_vm, executor_vm
    };

    public void setEnv(ExecutionEnv env) {
        this.env = env;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    protected void storeModels(WizardDescriptor wd, ConfObject config, boolean isConfigMode) {
        if (isConfigMode) {
            //store only single entry combobox entries because no cooosing allowed!
            DefaultComboBoxModel cboHostModel = new DefaultComboBoxModel();
            cboHostModel.addElement(config.getHost());
            cboHostModel.setSelectedItem(config.getHost());
            wd.putProperty("cboCloudAdapterConfiguration_HostModel", cboHostModel);
            DefaultComboBoxModel cboJvmModel = new DefaultComboBoxModel();
            cboJvmModel.addElement(config.getJvm());
            cboJvmModel.setSelectedItem(config.getJvm());
            wd.putProperty("cboCloudAdapterConfiguration_JvmModel", cboJvmModel);
        } else {//new Cloud adapter Wizard
            List<String> usedNames = getServiceNames(); // store them as member add them in wd later
            if (usedNames != null) {
                String origName = config.getName();
                int nextIndex = 0;
                while (usedNames.contains(config.getName())) {
                    config.setName(origName + (nextIndex++));
                }
            }
            //first store the used service list for later validation
            wd.putProperty(USED_SERVICE_NAMES, usedNames);



            //then store the cbo models!
            CascadingComboBoxModel cboHostModel = new CascadingComboBoxModel();
            CascadingComboBoxModel cboJvmModel = cboHostModel.getCascadedModel();


            // set the master host as the default for to add the Service

            //Retrieve all JVM currently active in the System
            List<ActiveJvm> jvmsList = Collections.<ActiveJvm>emptyList();
            try {
                GetActiveJVMListCommand cmd = new GetActiveJVMListCommand();
                cmd.setJvmName(host);
                jvmsList = env.getCommandService().execute(new GetActiveJVMListCommand()).getReturnValue();
            } catch (GrmException ex) {
                Exceptions.printStackTrace(ex);
            }
            //Sort them that the JVMs are grouped according to their hosts.
            for (ActiveJvm aj : jvmsList) {
                String currentHost = aj.getHost();
                String currentJVM = aj.getName();
                cboHostModel.addCascadedModelElement(currentHost, currentJVM);
            }
            //set the defaults

            //host is master host
            cboHostModel.setSelectedItem(host);

            //jvm is rp_vm or cs_vm or none
            if (cboJvmModel.contains(JVM_NAMES.rp_vm.toString())) {
                //choose rp_vm as default
                cboJvmModel.setSelectedItem(JVM_NAMES.rp_vm.toString());
            } else if (cboJvmModel.contains(JVM_NAMES.cs_vm.toString())) {
                //it might be simple install, choose cs_vm
                cboJvmModel.setSelectedItem(JVM_NAMES.cs_vm.toString());
            }

            wd.putProperty("cboCloudAdapterConfiguration_HostModel", cboHostModel);
            wd.putProperty("cboCloudAdapterConfiguration_JvmModel", cboJvmModel);
        }
    }

    public static void setupSubClassRegistry() {
        SubClassRegistry registry = new SubClassRegistry();
        registry.registerSubClass(SLOConfig.class, SloConfigHelper.SUB_CLASS_ENTRY_FIXED_USAGE);
        registry.registerSubClass(SLOConfig.class, SloConfigHelper.SUB_CLASS_ENTRY_MIN_RESOURCE);
        registry.registerSubClass(SLOConfig.class, SloConfigHelper.SUB_CLASS_ENTRY_PERMANENT_REQUEST);
        //dont use it for cloud adapter!!
        //registry.registerSubClass(SLOConfig.class,SloConfigHelper.SUB_CLASS_ENTRY_MAX_PENDING_JOBS);

        registry.registerSubClass(AbstractStepConfig.class, StepConfigHelper.SUB_CLASS_ENTRY_JAVA_STEP);
        registry.registerSubClass(AbstractStepConfig.class, StepConfigHelper.SUB_CLASS_ENTRY_SCRIPT_STEP);
        CloudAdapterConfigHelper.setSubClassRegistry(registry);
    }

    private List<String> getServiceNames() {
        List<String> names = new ArrayList<String>();
        try {
            List<ComponentInfo> componentList = ComponentService.getComponentInfosByType(env, Service.class);
            for (ComponentInfo ci : componentList) {
                names.add(ci.getName());
            }
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
        return names;
    }
}
