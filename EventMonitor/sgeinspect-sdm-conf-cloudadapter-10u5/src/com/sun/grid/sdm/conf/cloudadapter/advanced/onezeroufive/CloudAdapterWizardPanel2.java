/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.advanced.onezeroufive;

import com.sun.grid.sdm.conf.action.onezeroufive.ActionConfigHelper;
import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.grid.sdm.conf.parameter.onezeroufive.ParameterConfigHelper;
import com.sun.grid.sdm.conf.protocoloption.onezeroufive.ProtocolOptionConfigHelper;
import java.awt.Component;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class CloudAdapterWizardPanel2 extends ConfWizardPanel<CloudAdapterVisualPanel2> {

    private ActionConfigHelper startupConfHelper;
    private ActionConfigHelper shutdownConfHelper;
    private ActionConfigHelper resetConfHelper;
    private ProtocolOptionConfigHelper onSuccessPOConfHelper;
    private ProtocolOptionConfigHelper onErrorPOConfHelper;
    public static final String ACTION_NAME_STARTUP = NbBundle.getMessage(CloudAdapterWizardPanel2.class, "STARTUP_ACTION");
    public static final String ACTION_NAME_SHUTDOWN = NbBundle.getMessage(CloudAdapterWizardPanel2.class, "SHUTDOWN_ACTION");
    public static final String ACTION_NAME_MONITOR = NbBundle.getMessage(CloudAdapterWizardPanel2.class, "MONITOR_ACTION");
    public static final String ACTION_NAME_RESET = NbBundle.getMessage(CloudAdapterWizardPanel2.class, "RESET_ACTION");
    private ParameterConfigHelper parameterConfigHelper;

    public CloudAdapterWizardPanel2() {
        super(new CloudAdapterVisualPanel2());
        try {
            this.onSuccessPOConfHelper = new ProtocolOptionConfigHelper(NbBundle.getMessage(CloudAdapterWizardPanel2.class, "ON_SUCCESS"));
            this.onErrorPOConfHelper = new ProtocolOptionConfigHelper(NbBundle.getMessage(CloudAdapterWizardPanel2.class, "ON_ERRROR"));
            this.parameterConfigHelper = new ParameterConfigHelper(NbBundle.getMessage(CloudAdapterWizardPanel2.class, "GLOBAL_PARAMETER"));


            startupConfHelper = new ActionConfigHelper(ACTION_NAME_STARTUP);
            shutdownConfHelper = new ActionConfigHelper(ACTION_NAME_SHUTDOWN);
            resetConfHelper = new ActionConfigHelper(ACTION_NAME_RESET);
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    @Override
    public void setupActionListeners() {
        this.addStartSubWizardActionListener(onSuccessPOConfHelper, panel.txtCloudAdapterConfiguration_Gef_Protocol_OnSuccess, panel.btnCloudAdapterConfiguration_Gef_Protocol_OnSuccess);
        this.addStartSubWizardActionListener(onErrorPOConfHelper, panel.txtCloudAdapterConfiguration_Gef_Protocol_OnError, panel.btnCloudAdapterConfiguration_Gef_Protocol_OnError);

        this.addEditTableActionListeners(parameterConfigHelper, panel.tblCloudAdapterConfiguration_Gef_Param, panel.btnCloudAdapterConfiguration_Gef_ParamAdd, panel.btnCloudAdapterConfiguration_Gef_ParamRemove);

        this.addStartSubWizardActionListener(startupConfHelper, panel.txtCloudAdapterConfiguration_Gef_StartupAction, panel.btnCloudAdapterConfiguration_Gef_StartupAction);
        this.addStartSubWizardActionListener(shutdownConfHelper, panel.txtCloudAdapterConfiguration_Gef_ShutdownAction, panel.btnCloudAdapterConfiguration_Gef_ShutdownAction);
        this.addStartSubWizardActionListener(resetConfHelper, panel.txtCloudAdapterConfiguration_Gef_ResetAction, panel.btnCloudAdapterConfiguration_Gef_ResetAction);
        this.setupCheckBoxControlledComponents(panel.chkCloudAdapterConfiguration_Gef_ResetActionIsSet, new Component[]{
                    panel.btnCloudAdapterConfiguration_Gef_ResetAction,
                    panel.txtCloudAdapterConfiguration_Gef_ResetAction,
                    panel.lblCloudAdapterConfiguration_Gef_ResetAction
                });
        this.validateNonEmptyNonWhiteSpace(panel.lblCloudAdapterConfiguration_Gef_ExecuteAs, panel.txtCloudAdapterConfiguration_Gef_ExecuteAs);

    }
}

