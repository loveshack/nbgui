/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.conf.confgui.onezeroufive.ConfElementFactoryExtensionSDM;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;
import org.openide.nodes.Node;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.confgui.ConfElementFactory;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CookieAction;

/**
 * Needed for the wizards!
 */
abstract public class CloudAdapterAction extends RelaxedCookieAction/*SingleDataSourceAction<JvmWrapperDataSource>*/ {

    final static String DEFAULT_CLOUD_ADAPTER_NAME = "cloud_adapter";
    private static final long serialVersionUID = -200941001L;
    private boolean configureMode = false;

    public void performAction(final Node[] activatedNodes) {
        final ServiceData serviceData = extractServiceData(activatedNodes);
        if (serviceData != null) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    try {
                        SDMOneZeroUFive.class.getClassLoader().loadClass("com.sun.xml.bind.marshaller.NamespacePrefixMapper");
                    } catch (ClassNotFoundException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    //register sdm specific ConfElements // this separation is needed because of clean Classloader issue
                    if (!ConfElementFactory.getFactoryExtensionsList().contains(ConfElementFactoryExtensionSDM.instance)) {
                        ConfElementFactory.getFactoryExtensionsList().add(ConfElementFactoryExtensionSDM.instance);
                    }

                    try {
                        //First register all subtypes of configurations that might be used!
                        CloudAdapterConfigHelper.setupSubClassRegistry();
                        //this will tell the basic mode to delete the placeholder defaults only in wizard mode!
                        //then start the wizard
                        CloudAdapterConfigWrapper oldConf = getOldConfig(serviceData);
                        ConfHelper confHelper = newCloudAdapterConfHelperInstance(serviceData);
                        @SuppressWarnings("unchecked")
                        CloudAdapterConfigWrapper newConf = (CloudAdapterConfigWrapper) confHelper.startWizardDialog(oldConf, true, configureMode);
                        if (newConf != null) {
                            storeNewConfig(newConf,serviceData);
                        }
                    } catch (UnexpectedStructureException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    return null;
                }
            });
        }
    }

    /* These helper methods are implemented differently for Wizard and Configure Actions!
     */
    
    
    abstract ServiceData extractServiceData(final Node[] activatedNodes);

    abstract CloudAdapterConfigWrapper getOldConfig(ServiceData data);

    abstract void storeNewConfig(CloudAdapterConfigWrapper config,ServiceData data);

    abstract CloudAdapterConfigWrapper newCloudAdapterConfigWrapperInstance();

    abstract ConfHelper<?> newCloudAdapterConfHelperInstance(ServiceData serviceData) throws UnexpectedStructureException;

    abstract String getTitle(String serviceName);




    /* Default implementations!
     */

    boolean isConfigureMode() {
        return configureMode;
    }

    void setConfigureMode(boolean configureMode) {
        this.configureMode = configureMode;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    /*
    @Override
    protected String iconResource() {
    return ICON_PATH;
    }
     */
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

   

    protected int mode() {
        //default
        return CookieAction.MODE_ALL;
    }


    abstract class ServiceData{
        protected String host;
        protected String jvm;
        protected ExecutionEnv env;
        protected String csHost;
        protected String serviceName;

        public String getServiceName() {
            return serviceName;
        }

        public String getCsHost() {
            return csHost;
        }

        public ExecutionEnv getEnv() {
            return env;
        }

        public String getHost() {
            return host;
        }

        public String getJvm() {
            return jvm;
        }
    }

}
