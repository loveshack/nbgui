/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.cloudadapter.onezeroufive;

import com.sun.grid.sdm.conf.cloudadapter.advanced.onezeroufive.AdvancedCloudAdapterConfiguration;
import com.sun.grid.sdm.conf.cloudadapter.advanced.onezeroufive.AdvancedCloudAdapterConfigHelper;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import org.openide.util.NbBundle;

/**
 *
 */
final public class AdvancedCloudAdapterWizardAction extends CloudAdapterWizardAction {

    private static final long serialVersionUID = -2009100801L;
    private static final AdvancedCloudAdapterWizardAction SINGLETON = new AdvancedCloudAdapterWizardAction();

    public static final AdvancedCloudAdapterWizardAction getInstance() {
        return SINGLETON;
    }

    private AdvancedCloudAdapterWizardAction() {
        this.setConfigureMode(false);
    }

    public String getName() {
        return NbBundle.getMessage(CloudAdapterWizardAction.class, "CTL_AdvancedCloudAdapterWizardAction");
    }

    String getTitle(String serviceName) {
        return getName() + "( " + serviceName + ")";
    }

    CloudAdapterConfigWrapper newCloudAdapterConfigWrapperInstance() {
        return new AdvancedCloudAdapterConfiguration();
    }

    ConfHelper<?> newCloudAdapterConfHelperInstance(ServiceData serviceData) throws UnexpectedStructureException {
        return new AdvancedCloudAdapterConfigHelper(serviceData.getEnv(), serviceData.host ,NbBundle.getMessage(AdvancedCloudAdapterWizardAction.class, "ADVANCED_CLOUD_ADAPTER_WIZARD"));
    }
}
