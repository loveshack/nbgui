/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.config.pe.impl;

import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx.JGDIMonitoredData;
import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.users.UserSet;
import java.util.List;
import com.sun.grid.jgdi.configuration.ParallelEnvironment;
import com.sun.grid.jgdi.management.mbeans.JGDIJMXMBean;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.sge.sixtwoufive.config.users.impl.UserSetImpl;
import com.sun.grid.sge.sixtwoufive.config.util.ExceptionHelper;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.util.Collections;
import java.util.LinkedList;

public class PEImpl extends PE {

    public PEImpl(String name) {
        super(name);
    }

    private JGDIMonitoredData getData() {
        synchronized ((ClusterImpl) getOwner()) {
            return ((ClusterImpl) getOwner()).getMonitoredData();
        }
    }

    @Override
    public void setName(String aName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        return getPEName();
    }

    @Override
    public void setSlots(final Integer aSlots) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setSlots(aSlots);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public int getSlots() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.getSlots();
                }
                return 0;
            }
        });
    }

    @Override
    public void addUser(UserSet auser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeAllUser() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeUser(UserSet auser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addXuser(UserSet axuser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeAllXuser() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeXuser(UserSet axuser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setStartProcArgs(final String aStartProcArgs) throws PEException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, PEException>() {

            public Void execute() throws PEException {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    if (aStartProcArgs.equalsIgnoreCase(PEWizardSettings.CMD_NONE) || aStartProcArgs.equals("")) {
                        pe.setStartProcArgs(null);
                    } else {
                       pe.setStartProcArgs(aStartProcArgs);
                    }
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    throw new PEException(ExceptionHelper.getMessage(ex), ex);
                }
                return null;
            }
        });
    }

    @Override
    public String getStartProcArgs() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    String args = pe.getStartProcArgs();
                    return (args == null) ? PEWizardSettings.CMD_NONE : args;
                }
                return "-";
            }
        });
    }

    @Override
    public void setStopProcArgs(final String aStopProcArgs) throws PEException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, PEException>() {

            public Void execute() throws PEException {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    if (aStopProcArgs.equalsIgnoreCase(PEWizardSettings.CMD_NONE) || aStopProcArgs.equals("")) {
                        pe.setStopProcArgs(null);
                    } else {
                       pe.setStopProcArgs(aStopProcArgs);
                    }
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                 } catch (Throwable ex) {
                    throw new PEException(ExceptionHelper.getMessage(ex), ex);
                }
                return null;
            }
        });
    }

    @Override
    public String getStopProcArgs() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    String args = pe.getStopProcArgs();
                    return (args == null) ? PEWizardSettings.CMD_NONE : args;
                }
                return "-";
            }
        });
    }

    @Override
    public void setAllocationRule(final String aAllocationRule) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setAllocationRule(aAllocationRule);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public String getAllocationRule() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.getAllocationRule();
                }
                return "-";
            }
        });
    }

    @Override
    public void setControlSlaves(final Boolean aControlSlaves) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setControlSlaves(aControlSlaves);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                 } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public Boolean getControlSlaves() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.isControlSlaves();
                }
                return false;
            }
        });
    }

    @Override
    public void setJobIsFirstTask(final Boolean aJobIsFirstTask) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setJobIsFirstTask(aJobIsFirstTask);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                 } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public Boolean getJobIsFirstTask() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.isJobIsFirstTask();
                }
                return false;
            }
        });
    }

    @Override
    public void setUrgencySlots(final String aUrgencySlots) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setUrgencySlots(aUrgencySlots);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public String getUrgencySlots() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.getUrgencySlots();
                }
                return "-";
            }
        });
    }

    @Override
    public void setAccountingSummary(final Boolean aAccountingSummary) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ClusterImpl cluster = (ClusterImpl) getOwner();
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    pe.setAccountingSummary(aAccountingSummary);
                }
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public Boolean getAccountingSummary() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    return pe.isAccountingSummary();
                }
                return false;
            }
        });
    }

    @Override
    public List<UserSet> getUserList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<UserSet>>() {

            public List<UserSet> execute() {
                List<UserSet> users = new LinkedList<UserSet>();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    List<com.sun.grid.jgdi.configuration.UserSet> jgdiList = pe.getUserList();
                    for (com.sun.grid.jgdi.configuration.UserSet jSet : jgdiList) {
                        users.add(new UserSetImpl(jSet.getName()));
                    }
                    return users;
                }
                return Collections.<UserSet>emptyList();
            }
        });
    }

    @Override
    public List<UserSet> getXuserList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<UserSet>>() {

            public List<UserSet> execute() {
                List<UserSet> users = new LinkedList<UserSet>();
                ParallelEnvironment pe = getData().getPE(getName());
                if (pe != null) {
                    List<com.sun.grid.jgdi.configuration.UserSet> jgdiList = pe.getXuserList();
                    for (com.sun.grid.jgdi.configuration.UserSet jSet : jgdiList) {
                        users.add(new UserSetImpl(jSet.getName()));
                    }
                    return users;
                }
                return Collections.<UserSet>emptyList();
            }
        });
    }

    @Override
    public void setUserList(final List<UserSet> list) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                ClusterImpl cluster = (ClusterImpl) getOwner();
                if (pe != null) {
                    pe.removeAllUser();
                    for (UserSet set : list) {
                        pe.addUser((com.sun.grid.jgdi.configuration.UserSet) cluster.getUserSet(set.getName()));
                    }
                }
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }

    @Override
    public void setXuserList(final List<UserSet> list) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                ParallelEnvironment pe = getData().getPE(getName());
                ClusterImpl cluster = (ClusterImpl) getOwner();
                if (pe != null) {
                    pe.removeAllXuser();
                    for (UserSet set : list) {
                        pe.addXuser((com.sun.grid.jgdi.configuration.UserSet) cluster.getUserSet(set.getName()));
                    }
                }
                JGDIJMXMBean proxy = cluster.getJGDICache().getProxy();
                try {
                    proxy.updateParallelEnvironment(pe);
                } catch (Throwable ex) {
                    ErrorDisplayer.submitWarning(ExceptionHelper.getMessage(ex));
                }
                return null;
            }
        });
    }
}
