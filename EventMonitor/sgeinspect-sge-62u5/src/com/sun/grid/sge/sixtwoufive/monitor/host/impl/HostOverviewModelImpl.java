/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.host.impl;

import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx.JGDIMonitoredData;
import com.sun.grid.jgdi.monitoring.HostInfo;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostOverview;
import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 */
public class HostOverviewModelImpl extends HostOverview {

    private final Host host;
    private final ClusterImpl cluster;
    private static Logger logger = Logger.getLogger(HostOverviewModelImpl.class.getName());

    public HostOverviewModelImpl(Host host) {
        this.host = host;
        this.cluster = (ClusterImpl) host.getOwner();
    }

    private JGDIMonitoredData getData() {
        synchronized (cluster) {
            return cluster.getMonitoredData();
        }
    }

    @Override
    public Object getHostValue(String name) {
        synchronized (cluster) {
            return getData().getHostValue(host, name);
        }
    }

    @Override
    public String getArch() {
        synchronized (cluster) {
            return host.getArch().toString();
        }
    }

    @Override
    public String getNumberOfProcessors() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

                public String execute() {
                    HostInfo info = getData().getHostInfo(host);

                    if (info != null) {
                        String numberOfProcessors = info.getNumberOfProcessors();
                        return numberOfProcessors;
                    }
                    return "-";
                }
            });
        }
    }

    @Override
    public double getLoadAvg() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    logger.entering(getClass().getName(), "getLoadAvg"); //NOI18L
                    double ret = Double.NaN;
                    String loadAvg = getData().getHostInfo(host).getLoadAvg();
                    if (!loadAvg.equals("-")) {
                        ret = Double.parseDouble(loadAvg);
                    }
                    logger.exiting(getClass().getName(), "getLoadAvg {0}", ret); // NOI18L
                    return ret;
                }
            });
        }
    }

    @Override
    public double getMemTotal() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return getData().parseMem(hostInfo.getMemTotal(), 1024 * 1024);
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public double getMemUsed() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return getData().parseMem(hostInfo.getMemUsed(), 1024 * 1024);
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public double getSwapTotal() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return getData().parseMem(hostInfo.getSwapTotal(), 1024 * 1024);
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public double getSwapUsed() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return getData().parseMem(hostInfo.getSwapUsed(), 1024 * 1024);
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public double getVirtualTotal() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    // Todo put to HostInfo, does not work with getHostValue since it sge_qhost only fills
                    // the default host values
                    if (hostInfo != null) {
                        return getData().parseMem((String) hostInfo.getResourceValue("hl", "virtual_total"), 1024 * 1024); //NOI18L
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public double getVirtualUsed() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

                public Double execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    // Todo put to HostInfo, does not work with getHostValue since it sge_qhost only fills
                    // the default host values
                    if (hostInfo != null) {
                        return getData().parseMem((String) hostInfo.getResourceValue("hl", "virtual_used"), 1024 * 1024); //NOI18L
                    }
                    return Double.NaN;
                }
            });
        }
    }

    @Override
    public int getHostValueCount() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

                public Integer execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return hostInfo.getHostValueCount();
                    }
                    return 0;
                }
            });
        }
    }

    @Override
    public Set<String> getHostValueKeys() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Set<String>>() {

                public Set<String> execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return hostInfo.getHostValueKeys();
                    }
                    return null;
                }
            });
        }
    }

    @Override
    public Object getResourceValue(final String dominance, final String name) {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Object>() {

                public Object execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return hostInfo.getResourceValue(dominance, name);
                    }
                    return null;
                }
            });
        }
    }

    @Override
    public Set<String> getDominanceSet() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Set<String>>() {

                public Set<String> execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return hostInfo.getDominanceSet();
                    }
                    return null;
                }
            });
        }
    }

    @Override
    public Set<String> getResourceValueNames(final String dominance) {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Set<String>>() {

                public Set<String> execute() {
                    HostInfo hostInfo = getData().getHostInfo(host);
                    if (hostInfo != null) {
                        return hostInfo.getResourceValueNames(dominance);
                    }
                    return null;
                }
            });
        }
    }

    @Override
    public String getHostname() {
        return host.getHostName();
    }

    @Override
    public List<Job> getRunningJobs() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getRunningJobs(host);
        }
    }

    @Override
    public int getRunningJobsCount() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getRunningJobsCount(host);
        }
    }

    @Override
    public int getQueueCount() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getQueueList(host).size();
        }
    }

    @Override
    public int getUsedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getUsedSlots(host);
        }
    }

    @Override
    public int getAvailableSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getAvailableSlots(host);
        }
    }

    @Override
    public int getReservedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getReservedSlots(host);
        }
    }

    @Override
    public int getTotalSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getTotalSlots(host);
        }
    }
}
