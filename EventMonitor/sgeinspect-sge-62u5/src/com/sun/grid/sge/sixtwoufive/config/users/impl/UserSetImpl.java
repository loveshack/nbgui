/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.sixtwoufive.config.users.impl;

import com.sun.grid.sge.api.config.users.UserSet;
import java.util.List;

public class UserSetImpl extends UserSet {

    public UserSetImpl(String name) {
        super(name);
    }

    @Override
    public void setType(int aType) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getType() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSetType() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setFshare(int aFshare) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getFshare() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSetFshare() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setOticket(int aOticket) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getOticket() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSetOticket() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setJobCnt(int aJobCnt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getJobCnt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSetJobCnt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getEntriesList() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getEntriesCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getEntries(int index) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addEntries(String aentries) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setEntries(int index, String aentries) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeAllEntries() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String removeEntries(int index) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeEntries(String aentries) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSetEntries() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String dump() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int compareTo(UserSet o) {
        return getName().compareTo(o.getName());
    }

    public String toString() {
        return this.getName();
    }

}
