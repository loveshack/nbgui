/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.sixtwoufive;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterOverview;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterOverviewModelImpl;
import com.sun.grid.sge.sixtwoufive.monitor.host.impl.HostImpl;
import com.sun.grid.sge.sixtwoufive.monitor.host.impl.HostOverviewModelImpl;
import com.sun.grid.sge.sixtwoufive.monitor.job.impl.JobImpl;
import com.sun.grid.sge.sixtwoufive.monitor.job.impl.JobOverviewModelImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueInstanceImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueInstanceOverviewModelImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueOverviewModelImpl;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import com.sun.tools.visualvm.core.model.Model;
import com.sun.tools.visualvm.core.model.ModelProvider;


public class SGEModelProvider extends AbstractModelProvider<Model, DataSource> {

    private final static SGEModelProvider instance = new SGEModelProvider();

    private SGEModelProvider() {

    }
    public static SGEModelProvider getInstance() {
        return instance;
    }
    /**
     * Default {@link ModelProvider} implementation, which creates
     * ClusterOverview for current cluster. If you want to extend ClusterModelProvider use
     * {@link ClusterModelProvider#registerProvider()} to register the new instances
     * of {@link ModelProvider} for the different types of {@link Cluster}.
     * @param cluster cluster
     * @return instance of {@link ClusterOverview} for cluster
     */
    public Model createModelFor(DataSource source) {
        if (source instanceof ClusterImpl) {
            return new ClusterOverviewModelImpl((Cluster)source);
        } else if (source instanceof HostImpl) {
            return new HostOverviewModelImpl((Host)source);
        } else if (source instanceof JobImpl) {
            return new JobOverviewModelImpl((Job)source);
        }  else if (source instanceof QueueImpl) {
            return new QueueOverviewModelImpl((Queue)source);
        } else  if (source instanceof QueueInstanceImpl) {
            return new QueueInstanceOverviewModelImpl((QueueInstance) source);
        }


        return null;
    }
}

