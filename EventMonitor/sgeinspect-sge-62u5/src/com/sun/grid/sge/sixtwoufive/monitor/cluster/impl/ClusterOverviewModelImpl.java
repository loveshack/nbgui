/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.cluster.impl;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterOverview;
import com.sun.grid.sge.api.monitor.job.Job;
import java.util.List;

public class ClusterOverviewModelImpl extends ClusterOverview {

    private final ClusterImpl cluster;

    public ClusterOverviewModelImpl(Cluster cluster) {
        this.cluster = (ClusterImpl) cluster;
    }

    @Override
    public String getClusterName() {
        return cluster.getDisplayName();
    }

    @Override
    public String getVersion() {
        return cluster.getVersion();
    }

    @Override
    public String getJmxMasterHost() {
        return cluster.getConfiguration().getJmxHost();
    }

    @Override
    public int getJmxPort() {
        return cluster.getConfiguration().getJmxPort();
    }

    @Override
    public String getSgeRoot() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSgeRoot();
        }
    }

    @Override
    public String getSgeCell() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSgeCell();
        }
    }

    @Override
    public String getSgeAdmin() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSgeAdmin();
        }
    }

    @Override
    public int getQmasterPort() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getQmasterPort();
        }
    }

    @Override
    public int getExecdPort() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getExecdPort();
        }
    }

    @Override
    public int getRunningJobsCount() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getClusterRunningJobsCount();
        }
    }

    @Override
    public int getZombieJobsCount() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getZombieJobsCount();
        }
    }

    @Override
    public int getPendingJobsCount() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getPendingJobsCount();
        }
    }

    @Override
    public int getAvailability() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getAvailability();
        }
    }

    @Override
    public int getOverload() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getOverload();
        }
    }

    @Override
    public int getSlotsUtilization() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSlotsUtilized();
        }
    }

    @Override
    public List<Job> getZombieJobs() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getZombieJobs();
        }
    }

    @Override
    public List<Job> getPendingJobs() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getPendingJobs();
        }
    }

    @Override
    public List<Job> getRunningJobs() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getClusterRunningJobs();
        }
    }
}
