/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.job.impl;

import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.sge.api.monitor.job.Job;
import java.util.Date;

public class JobImpl extends Job {

    private JobSummary jobSummary;

    public JobImpl(final JobSummary jobSummary, int id) {
        super(id);
        this.jobSummary = jobSummary;
    }

    public JobImpl(final JobSummary jobSummary, int id, String taskId) {
        super(id, taskId);
        this.jobSummary = jobSummary;
    }

    public boolean isRunning() {
        return jobSummary.isRunning();
    }

    public String getUser() {
        return jobSummary.getUser();
    }

    public Date getSubmitTime() {
        return jobSummary.getSubmitTime();
    }

    public String getState() {
        return jobSummary.getState();
    }

    public Date getStartTime() {
        return jobSummary.getStartTime();
    }

    public int getSlots() {
        return jobSummary.getSlots();
    }

    public String getQueue() {
        return jobSummary.getQueue();
    }

    public String getQinstanceName() {
        return jobSummary.getQinstanceName();
    }

    public String getProject() {
        return jobSummary.getProject();
    }

    public double getPriority() {
        return jobSummary.getNormalizedPriority();
    }

    public String getName() {
        String name = jobSummary.getName();
        if (name != null) {
            return name;
        } else {
            return Integer.toString(getId());
        }
    }

    public String getDepartment() {
        return jobSummary.getDepartment();
    }
}
