/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx;

import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.sixtwoufive.monitor.host.impl.HostImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueImpl;
import com.sun.grid.sge.sixtwoufive.monitor.queue.impl.QueueInstanceImpl;
import com.sun.grid.jgdi.JGDIException;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import com.sun.grid.jgdi.configuration.ParallelEnvironment;
import com.sun.grid.jgdi.configuration.QueueInstance;
import com.sun.grid.jgdi.event.ClusterQueueAddEvent;
import com.sun.grid.jgdi.event.ClusterQueueDelEvent;
import com.sun.grid.jgdi.event.ClusterQueueModEvent;
import com.sun.grid.jgdi.event.Event;
import com.sun.grid.jgdi.event.EventListener;
import com.sun.grid.jgdi.event.EventTypeEnum;
import com.sun.grid.jgdi.event.ExecHostAddEvent;
import com.sun.grid.jgdi.event.ExecHostDelEvent;
import com.sun.grid.jgdi.event.ExecHostModEvent;
import com.sun.grid.jgdi.event.JobAddEvent;
import com.sun.grid.jgdi.event.JobDelEvent;
import com.sun.grid.jgdi.event.JobFinalUsageEvent;
import com.sun.grid.jgdi.event.JobModEvent;
import com.sun.grid.jgdi.event.JobTaskAddEvent;
import com.sun.grid.jgdi.event.JobTaskDelEvent;
import com.sun.grid.jgdi.event.JobTaskModEvent;
import com.sun.grid.jgdi.event.ParallelEnvironmentAddEvent;
import com.sun.grid.jgdi.event.ParallelEnvironmentListEvent;
import com.sun.grid.jgdi.event.ParallelEnvironmentModEvent;
import com.sun.grid.jgdi.event.QueueInstanceAddEvent;
import com.sun.grid.jgdi.event.QueueInstanceDelEvent;
import com.sun.grid.jgdi.event.QueueInstanceModEvent;
import com.sun.grid.jgdi.event.UserSetAddEvent;
import com.sun.grid.jgdi.event.UserSetDelEvent;
import com.sun.grid.jgdi.event.UserSetListEvent;
import com.sun.grid.jgdi.event.UserSetModEvent;
import com.sun.grid.jgdi.management.JGDIProxy;
import com.sun.grid.jgdi.management.mbeans.JGDIJMXMBean;
import com.sun.grid.jgdi.monitoring.HostInfo;
import com.sun.grid.jgdi.monitoring.QHostResult;
import com.sun.grid.jgdi.monitoring.QueueInfo;
import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostTypeFilter;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.sge.sixtwoufive.config.pe.impl.PEImpl;
import com.sun.grid.sge.sixtwoufive.config.users.impl.UserSetImpl;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle;

/**
 *
 */
public class JGDICache implements PropertyChangeListener {

    private static final Logger logger = Logger.getLogger(JGDICache.class.getName());
    private Map<String, HostImpl> hostMap = new HashMap<String, HostImpl>();
    private Map<String, QueueImpl> queueMap = new HashMap<String, QueueImpl>();
    private Map<String, PEImpl> peMap = new HashMap<String, PEImpl>();
    private Map<String, UserSetImpl> userMap = new HashMap<String, UserSetImpl>();
    private final Object syncObject = new Object();
    private Timer timer;
    // private int updateInterval = 60000;
    private int updateInterval = 100;
    private final JGDIConnectionController jgdiConnectionController;
    private final ControllerListener controllerListener = new ControllerListener();
    private final ConnectNotificationListener connectNotificationListener = new ConnectNotificationListener();
    private final ClusterImpl cluster;
    private final List<ChangeListener> listeners = Collections.synchronizedList(new LinkedList<ChangeListener>());
    private volatile List<Queue> queueList = Collections.<Queue>emptyList();
    private volatile List<Host> hostList = Collections.<Host>emptyList();
    private volatile List<PE> peList = Collections.<PE>emptyList();
    private volatile List<UserSet> userList = Collections.<UserSet>emptyList();
    private volatile boolean doFetch = true;
    private volatile boolean ongoingUpdate = false;

    public JGDICache(Cluster cluster) {
        this.cluster = (ClusterImpl) cluster;
        jgdiConnectionController = new JGDIConnectionController(this.cluster);
        jgdiConnectionController.addListener(controllerListener);
        jgdiConnectionController.addEventListener(connectNotificationListener);
    }

    public JGDIJMXMBean getProxy() {
        return jgdiConnectionController.getProxy().getProxy();
    }

    public void connect() {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                String jmxHost = cluster.getConfiguration().getJmxHost();
                int jmxPort = cluster.getConfiguration().getJmxPort();
                String caPath = cluster.getConfiguration().getCaPath();
                String keystorePath = cluster.getConfiguration().getKeystorePath();
                String username = cluster.getConfiguration().getUserName();
                char[] keystorePassword = "".toCharArray();
                boolean useSSL = cluster.getConfiguration().getSSL();

                if (cluster.getConfiguration().getKeystorePassword() != null) {
                    keystorePassword = cluster.getConfiguration().getKeystorePassword().toCharArray();
                }
                Object credentials = null;
                if (useSSL) {
                    try {
                        KeyStore ks = createKeyStore(keystorePath, username, keystorePassword);
                        credentials = JGDIProxy.createCredentialsFromKeyStore(ks, username, keystorePassword);
                    } catch (JGDIException ex) {
                        cluster.getConfiguration().setKeystorePassword(null);
                        handleError(ex);
                        return null;
                    } catch (SecurityException ex) {
                        cluster.getConfiguration().setKeystorePassword(null);
                        handleError(ex);
                        return null;
                    }
                } else {
                    credentials = new String[]{cluster.getConfiguration().getUserName(), cluster.getConfiguration().getUserPassword()};
                }

                if (!useSSL) {
                    jgdiConnectionController.connect(jmxHost, jmxPort, credentials, null, null, null);
                } else {
                    jgdiConnectionController.connect(jmxHost, jmxPort, credentials, caPath, keystorePath, keystorePassword);
                }
                return null;
            }
        });
    }

    private void handleError(final Throwable ex) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                // TODO: stopUpdateTimer();
                NotifyDescriptor d = new NotifyDescriptor.Exception(ex);
                DialogDisplayer.getDefault().notify(d);
            }
        });
    }

    private KeyStore createKeyStore(String path, String username, char[] pw) throws SecurityException {
        FileInputStream fin = null;
        KeyStore ret = null;
        try {
            File keyStoreFile = new File(path);
            fin = new FileInputStream(keyStoreFile);
            ret = KeyStore.getInstance(KeyStore.getDefaultType());
            ret.load(fin, pw);
            if (!ret.isKeyEntry(username)) {
                throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_user", path, username));
            }
        } catch (FileNotFoundException ex) {
            throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_notfound", path));
        } catch (KeyStoreException ex) {
            throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_exception", ex.getLocalizedMessage()));
        } catch (NoSuchAlgorithmException ex) {
            throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_exception", ex.getLocalizedMessage()));
        } catch (CertificateException ex) {
            throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_exception", ex.getLocalizedMessage()));
        } catch (IOException ex) {
            throw new SecurityException(NbBundle.getMessage(JGDICache.class, "keystore_exception", ex.getLocalizedMessage()));
        } finally {
            try {
                fin.close();
            } catch (Exception ex) {
                // ignore
            }
        }
        return ret;
    }

    private void subscribeEvents(boolean subscribe) {
        Set<EventTypeEnum> subscriptions = new HashSet<EventTypeEnum>();

        subscriptions.add(EventTypeEnum.QueueInstanceAdd);
        subscriptions.add(EventTypeEnum.QueueInstanceDel);
        subscriptions.add(EventTypeEnum.QueueInstanceMod);

        subscriptions.add(EventTypeEnum.ClusterQueueAdd);
        subscriptions.add(EventTypeEnum.ClusterQueueDel);
        subscriptions.add(EventTypeEnum.ClusterQueueMod);

        subscriptions.add(EventTypeEnum.ExecHostAdd);
        subscriptions.add(EventTypeEnum.ExecHostDel);
        subscriptions.add(EventTypeEnum.ExecHostMod);

        subscriptions.add(EventTypeEnum.JobAdd);
        subscriptions.add(EventTypeEnum.JobDel);
        subscriptions.add(EventTypeEnum.JobMod);
        subscriptions.add(EventTypeEnum.JobFinalUsage);
        subscriptions.add(EventTypeEnum.JobTaskMod);
        // ????
        subscriptions.add(EventTypeEnum.JobTaskAdd);
        subscriptions.add(EventTypeEnum.JobTaskDel);

        subscriptions.add(EventTypeEnum.ParallelEnvironmentAdd);
        subscriptions.add(EventTypeEnum.ParallelEnvironmentDel);
        subscriptions.add(EventTypeEnum.ParallelEnvironmentList);
        subscriptions.add(EventTypeEnum.ParallelEnvironmentMod);

        subscriptions.add(EventTypeEnum.UserSetAdd);
        subscriptions.add(EventTypeEnum.UserSetDel);
        subscriptions.add(EventTypeEnum.UserSetList);
        subscriptions.add(EventTypeEnum.UserSetMod);


        if (subscribe) {
            jgdiConnectionController.subscribe(subscriptions);
        } else {
            jgdiConnectionController.unsubscribe(subscriptions);
        }
    }

    public void disconnect() {
        stopUpdateTimer();
        jgdiConnectionController.disconnect();
    }

    public boolean isConnected() {
        boolean isConnected = jgdiConnectionController.isConnected();
        logger.log(Level.FINEST, "isConnected {0}", isConnected); //NOI18L
        return isConnected;
    }

    synchronized private void stopUpdateTimer() {
        if (timer != null) {
            logger.log(Level.FINER, "stopUpdateTimer"); //NOI18L
            timer.stop();
        }
    }

    synchronized private void startUpdateTimer() {
        logger.log(Level.FINER, "startUpdateTimer"); //NOI18L
        if (isConnected()) {
            stopUpdateTimer();
            timer = new Timer(updateInterval, new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (doFetch) {
                        if (ongoingUpdate) {
                            logger.log(Level.FINER, "+++++++++++++++++ skipping updateMonitoredData ongoingUpdate ++");
                        } else {
                            logger.log(Level.FINER, "+++++++++++++++++ trigger updateMonitoredData +++++++++++++++++");
                            // assure that the last events are not thrown away
                            if (jgdiConnectionController.updateMonitoredData()) {
                                doFetch = false;
                            }
                        }
                    }
                }
            });
            timer.setCoalesce(true);
            timer.setInitialDelay(0);
            timer.start();
        }
    }

    private void runUpdate(JGDIMonitoredData data) {
        logger.entering(getClass().getName(), "runUpdate"); //NOI18L
        synchronized (syncObject) {
            //Parallel Environment
            List<ParallelEnvironment> parErList = data.getPEList();
            Set<String> removePE = new HashSet<String>(peMap.keySet());
            for (ParallelEnvironment pe : parErList) {

                if (removePE.contains(pe.getName())) {
                    removePE.remove(pe.getName());
                }
                PEImpl pei = peMap.get(pe.getName());
                if (pei == null) {
                    pei = new PEImpl(pe.getName());
                    peMap.put(pe.getName(), pei);
                    cluster.getRepository().addDataSource(pei);
                }
            }

            //remove absolete PE
            for (String peName : removePE) {
                cluster.getRepository().removeDataSource(peMap.get(peName));
                peMap.remove(peName);
            }

            //User List
            List<com.sun.grid.jgdi.configuration.UserSet> users = data.getUserList();
            Set<String> removeUser = new HashSet<String>(userMap.keySet());

            for (com.sun.grid.jgdi.configuration.UserSet user : users) {
                if (removeUser.contains(user.getName())) {
                    removeUser.remove(user.getName());
                }
                UserSetImpl usi = userMap.get(user.getName());
                if (usi == null) {
                    usi = new UserSetImpl(user.getName());
                    userMap.put(user.getName(), usi);
//                    cluster.getRepository().addDataSource(usi);
                }
            }

            //remove absolete UserLists
            for (String userName : removeUser) {
                peMap.remove(userName);
            }

            List<ClusterQueue> clusterQueueList = data.getClusterQueueList();
            QHostResult qhostResult = data.getQhostResult();

            // build new Queue list that contains QueueInstances
            Set<String> queueMapKeyRemoveSet = new HashSet<String>(queueMap.keySet());
            Set<QueueImpl> addQueueSet = new HashSet<QueueImpl>();
            for (ClusterQueue cq : clusterQueueList) {

                if (queueMapKeyRemoveSet.contains(cq.getName())) {
                    queueMapKeyRemoveSet.remove(cq.getName());
                }
                QueueImpl queue = queueMap.get(cq.getName());
                if (queue == null) {
                    queue = new QueueImpl(cq.getName());
                    queueMap.put(cq.getName(), queue);
                    addQueueSet.add(queue);
                }
                // Queue Instances
                List<com.sun.grid.sge.api.monitor.queue.QueueInstance> queueInstances = queue.getQueueInstances();
                Set<com.sun.grid.sge.api.monitor.queue.QueueInstance> removeQueueInstances = new HashSet<com.sun.grid.sge.api.monitor.queue.QueueInstance>(queueInstances);
                Set<com.sun.grid.sge.api.monitor.queue.QueueInstance> addQueueInstances = new HashSet<com.sun.grid.sge.api.monitor.queue.QueueInstance>();
                for (QueueInstance qi : cq.getQinstancesList()) {
                    // TODO should be done in jgdi already
                    String qtype = qtypeToString(qi);
                    QueueInstanceImpl queueInstance = new QueueInstanceImpl(cluster, qi.getFullName(), qtype);
                    // logger.log(Level.FINEST, "qi.getName() " + qi.getFullName() + " queueInstance:  " + queueInstance.getQueueName());
                    if (!queueInstances.contains(queueInstance)) {
                        queueInstances.add(queueInstance);
                        addQueueInstances.add(queueInstance);
                    } else {
                        removeQueueInstances.remove(queueInstance);
                    }
                }
                queueInstances.removeAll(removeQueueInstances);
                queue.getRepository().updateDataSources(addQueueInstances, removeQueueInstances);

                if (logger.isLoggable(Level.FINEST)) {
                    for (com.sun.grid.sge.api.monitor.queue.QueueInstance queueInstance : queueInstances) {
                        logger.log(Level.FINEST, "queueInstance: " + queueInstance.getQueueName()); //NOI18N
                        logger.log(Level.FINEST, "queueInstanceDataSource: " + queue.getRepository().getDataSources());
                    }
                }
            }
            // delete obsolete queues from queueMap
            Set<QueueImpl> removeQueueSet = new HashSet<QueueImpl>();
            for (String queueName : queueMapKeyRemoveSet) {
                QueueImpl queue = queueMap.get(queueName);
                removeQueueSet.add(queue);
                queueMap.remove(queueName);
            }
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST, "queueMap size: " + queueMap.size());
            }
            cluster.getRepository().updateDataSources(addQueueSet, removeQueueSet);

            if (qhostResult != null) {
                // remove obsolete entries from cluster repository and from hostMap
                Set<String> hostInfoHostNames = qhostResult.getHostNames();
                Set<Host> removeHosts = new HashSet<Host>();
                Set<Host> addHosts = new HashSet<Host>();
                for (String hostName : hostMap.keySet()) {
                    if (!hostInfoHostNames.contains(hostName)) {
                        Host host = hostMap.get(hostName);
                        removeHosts.add(host);
                    }
                }
                for (Host host : removeHosts) {
                    hostMap.remove(host.getHostName());
                }

                for (HostInfo hi : qhostResult.getHostInfo()) {
                    if (!hi.getHostname().equals("global")) {
                        HostImpl host;
                        // try to find the host if it had been added before
                        // added otherwise to the hostMap and to cluster repository
                        host = hostMap.get(hi.getHostname());
                        if (host == null) {
                            // TODO map host info entries for arch and type
                            EnumSet<HostTypeFilter> types = EnumSet.noneOf(HostTypeFilter.class);
                            types.add(HostTypeFilter.EXECD);
                            host = new HostImpl(hi.getHostname(), types, hi.getArch());
                            hostMap.put(hi.getHostname(), host);
                            addHosts.add(host);
                        }

                        List<Queue> queues = host.getQueueList();
                        HashSet<Queue> removeQueues = new HashSet<Queue>(queues);
                        for (QueueInfo qi : hi.getQueueList()) {
                            QueueImpl queue = queueMap.get(qi.getQname());
                            //cannot find the qstat queue in the qhost queue map
                            if (queue != null) {
                                //if host is known ==> remove from removeQueues ==> queue is known and still there
                                if (removeQueues.contains(queue)) {
                                    removeQueues.remove(queue);
                                } else {
                                    //queue is unknown lets add it to the new ones
                                    queues.add(queue);
                                }
                            } else {
                                // Todo: Preliminary workaround for the not in sync problem of qhost -q and qstat -f
                                //       fetches for jgdi. The correct solution should be a complete snapshot at a
                                //       certain point in time to have no difference in the data delivered by the qhost
                                //       and qstat --> extension of jgdi needed (snapshot for subsequent calls or a special
                                //       call to get the information in one sweep)
                                // The current workaround is to refetch once again to sync up the data if some missing queues
                                // i.e. queue==null are detected
                                if (logger.isLoggable(Level.FINEST)) {
                                    logger.log(Level.FINEST, "QueueInfo qi not contained in queueMap: " + qi.getQname()); //NOI18N
                                }
                                doFetch = true;
                            }

                        }
                        if (logger.isLoggable(Level.FINEST)) {
                            logger.log(Level.FINEST, "removeQueues size: " + removeQueues.size() + " are " + removeQueues); //NOI18N
                        }

                        queues.removeAll(removeQueues);

                        if (logger.isLoggable(Level.FINEST)) {
                            logger.log(Level.FINEST, "hostViewMap: Host: {0} Queues({1}) : {2}", new Object[]{host, queues.size(), queues}); //NOI18N
                        }
                    }
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.log(Level.FINEST, "queueMap size 2: " + queueMap.size()); //NOI18N
                    }
                }
                cluster.getRepository().updateDataSources(addHosts, removeHosts);
            }
            queueList = Collections.unmodifiableList(new ArrayList<Queue>(queueMap.values()));
            hostList = Collections.unmodifiableList(new ArrayList<Host>(hostMap.values()));
            peList = Collections.unmodifiableList(new ArrayList<PE>(peMap.values()));
            userList = Collections.unmodifiableList(new ArrayList<UserSet>(userMap.values()));
            // assure that fireChangeEvent is triggered only at the end of runUpdate
            // after all DataSource.EventQueue.post() calls have been finished
            logger.log(Level.FINER, "before ongoingUpdate: " + ongoingUpdate);
            DataSource.EVENT_QUEUE.post(new LastFiresChangedEvent());
        }
        logger.log(Level.FINER, "after ongoingUpdate: " + ongoingUpdate);
        logger.exiting(getClass().getName(), "runUpdate"); //NOI18N
    }

    //------------------------------------------------------------------------
    // Queue/Host List
    //------------------------------------------------------------------------
    /**
     *
     * @return unmodifiable list with all queues
     */
    public List<Queue> getQueueList() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Queue>>() {

                public List<Queue> execute() {
                    return queueList;
                }
            });
        }
    }

    /**
     *
     * @return unmodifiable list with all hosts
     */
    public List<Host> getHostList() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Host>>() {

                public List<Host> execute() {
                    return hostList;
                }
            });
        }
    }

    /**
     *
     * @return unmodifiable list with all PEs
     */
    public List<PE> getPEList() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<PE>>() {

                public List<PE> execute() {
                    return peList;
                }
            });
        }
    }

    /**
     *
     * @return unmodifiable list with all UserSets
     */
    public List<UserSet> getUserList() {
        synchronized (cluster) {
            return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<UserSet>>() {

                public List<UserSet> execute() {
                    return userList;
                }
            });
        }
    }

    private static String qtypeToString(QueueInstance queueInstance) {
        int qtype = queueInstance.getQtype();
        StringBuilder qtypeString = new StringBuilder();
        // possible queue types: BATCH, INTERACTIVE, PARALLEL, CHECKPOINT
        if ((qtype & 0x0001) == 0x0001) {
            qtypeString.append("B");
        }
        if ((qtype & 0x0002) == 0x0002) {
            qtypeString.append("I");
        }
        if (queueInstance.getPeCount() > 0) {
            qtypeString.append("P");
        }
        if (queueInstance.getCkptCount() > 0) {
            qtypeString.append("C");
        }
        return qtypeString.toString();
    }

    private class ControllerListener implements JGDIConnectionController.Listener {

        public void connected(final String host, final int port, final Set<EventTypeEnum> subscription) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                        public Void execute() {
                            logger.log(Level.FINER, NbBundle.getMessage(JGDICache.class, "Property_Changed", Cluster.PROPERTY_CLUSTER_CONNECTED, false, true));
                            cluster.firePropertyChange(Cluster.PROPERTY_CLUSTER_CONNECTED, false, true);
                            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JGDICache.class, "Connected_to", host, Integer.toString(port)));
                            subscribeEvents(true);
                            doFetch = true;
                            ongoingUpdate = false;
                            startUpdateTimer();
                            return null;
                        }
                    });
                }
            });
        }

        public void disconnected() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                        public Void execute() {
                            stopUpdateTimer();
                            subscribeEvents(false);
                            logger.log(Level.FINER, NbBundle.getMessage(JGDICache.class, "Property_Changed", Cluster.PROPERTY_CLUSTER_CONNECTED, true, false));
                            cluster.firePropertyChange(Cluster.PROPERTY_CLUSTER_CONNECTED, true, false);
                            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JGDICache.class, "Disconnected",
                                    cluster.getConfiguration().getJmxHost(),
                                    Integer.toString(cluster.getConfiguration().getJmxPort())));
                            return null;
                        }
                    });
                }
            });
        }

        public void subscribed(Set<EventTypeEnum> types) {
            final Set<EventTypeEnum> tmpTypes = new HashSet<EventTypeEnum>(types);
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                        public Void execute() {
                            if (logger.isLoggable(Level.FINER)) {
                                for (EventTypeEnum evt : tmpTypes) {
                                    logger.log(Level.FINER, "susbscribe {0}", evt); // NOI18N
                                }
                            }
                            return null;
                        }
                    });
                }
            });
        }

        public void unsubscribed(final Set<EventTypeEnum> types) {

            final Set<EventTypeEnum> tmpTypes = new HashSet<EventTypeEnum>(types);
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                        public Void execute() {
                            if (logger.isLoggable(Level.FINER)) {
                                for (EventTypeEnum evt : tmpTypes) {
                                    logger.log(Level.FINER, NbBundle.getMessage(JGDICache.class, "unsusbscribe", evt)); // NOI18N
                                }
                            }
                            return null;
                        }
                    });
                }
            });
        }

        public void errorOccured(final Throwable ex) {
            handleError(ex);
        }

        public void clearSubscription() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JGDICache.class, "stopped_client"));
                }
            });

        }

        public void subscriptionSet(Set<EventTypeEnum> types) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JGDICache.class, "subscription_changed"));
                }
            });
        }

        public void updateFinished(JGDIMonitoredData data) {
            logger.log(Level.FINE, "+++++++++++++++++ updateFinished +++++++++++++++++");
            synchronized (cluster) {
                cluster.setMonitoredData(data);
                runUpdate(data);
            }
            // The DataSource post into every DataSource.EventQueue to add/remove/update
            // therefore we must assure that fireChangeEvent is triggered after the last post for all
            // changed DataSources -> see LastFiresChangedEvent
            // inside of runUpdate()
            // fireChangeEvent();
        }
    }

    private class ConnectNotificationListener implements EventListener {

        public void eventOccured(final Event evt) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                        public Void execute() {
                            if (evt != null) {
                                if (logger.isLoggable(Level.FINEST)) {
                                    logger.log(Level.FINEST, NbBundle.getMessage(JGDICache.class, "Received_Event",
                                            new Object[]{evt.getEventId(), evt.getType(), new Date(1000 * evt.getTimestamp()).toString(),
                                                evt.getClass().getName()}));
                                }

                                if (evt instanceof ClusterQueueAddEvent || evt instanceof ClusterQueueModEvent || evt instanceof ClusterQueueDelEvent ||
                                        evt instanceof QueueInstanceAddEvent || evt instanceof QueueInstanceDelEvent || evt instanceof QueueInstanceModEvent ||
                                        evt instanceof ExecHostAddEvent || evt instanceof ExecHostModEvent || evt instanceof ExecHostDelEvent ||
                                        evt instanceof JobAddEvent || evt instanceof JobDelEvent || evt instanceof JobModEvent ||
                                        evt instanceof JobTaskAddEvent || evt instanceof JobTaskModEvent || evt instanceof JobTaskDelEvent ||
                                        evt instanceof JobFinalUsageEvent || evt instanceof ParallelEnvironmentAddEvent || evt instanceof ParallelEnvironmentListEvent ||
                                        evt instanceof ParallelEnvironmentModEvent || evt instanceof UserSetAddEvent || evt instanceof UserSetDelEvent ||
                                        evt instanceof UserSetListEvent || evt instanceof UserSetModEvent) {
                                    doFetch = true;
                                }
                            }
                            return null;
                        }
                    });
                }
            });
        }
    }

    public void fireChangeEvent() {
        logger.log(Level.FINER, NbBundle.getMessage(JGDICache.class, "fireChangeEvent"), listeners.size());
        synchronized (listeners) { // according to Collections.synchronizedList()
            for (ChangeListener l : listeners) {
                l.stateChanged(new ChangeEvent(this));
            }
        }
    }

    public void addChangeListener(ChangeListener cl) {
        logger.log(Level.FINE, NbBundle.getMessage(JGDICache.class, "addChangeListener"), cl);
        listeners.add(cl);
        if (listeners.size() > 0 && timer == null) {
            startUpdateTimer();
        }
        doFetch = true;
    }

    public void removeChangeListener(ChangeListener cl) {
        logger.log(Level.FINE, NbBundle.getMessage(JGDICache.class, "removeChangeListener"), cl);
        listeners.remove(cl);
//        prevents Navigator from updates
//        if (listeners.size() == 0) {
//            stopUpdateTimer();
//        }
    }

    // listen to TopComponent change events
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(TopComponent.Registry.PROP_ACTIVATED)) {
            logger.log(Level.FINE, "######### TopComponent {0} activated", evt.getPropertyName());
        }
    }

    private class LastFiresChangedEvent implements Runnable {

        public LastFiresChangedEvent() {
            ongoingUpdate = true;
        }

        public synchronized void run() {
//            try {
//                Thread.sleep(50000);
//            } catch (InterruptedException ex) {
//                Exceptions.printStackTrace(ex);
//            }
            fireChangeEvent();
            logger.log(Level.FINER, "fireChangeEvent()");
            ongoingUpdate = false;
        }
    }
}
