/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.queue.impl;

import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueOverview;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;

public class QueueOverviewModelImpl extends QueueOverview {

    private final Queue queue;
    private final ClusterImpl cluster;

    public QueueOverviewModelImpl(Queue queue) {
        this.queue = queue;
        this.cluster = (ClusterImpl) queue.getOwner();
    }

    @Override
    public String getName() {
        return queue.getDisplayName();
    }

    @Override
    public double getLoad() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getLoad(queue);
        }
    }

    @Override
    public boolean isLoadSet() {
        synchronized (cluster) {
            return cluster.getMonitoredData().isLoadSet(queue);
        }
    }

    @Override
    public int getReservedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getReservedSlots(queue);
        }
    }

    @Override
    public int getUsedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getUsedSlots(queue);
        }
    }

    @Override
    public int getTotalSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getTotalSlots(queue);
        }
    }

    @Override
    public int getAvailableSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getAvailableSlots(queue);
        }
    }

    @Override
    public int getTempDisabled() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getTempDisabled(queue);
        }
    }

    @Override
    public int getManualIntervention() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getManualIntervention(queue);
        }
    }

    @Override
    public int getSuspendManual() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSuspendManual(queue);
        }
    }

    @Override
    public int getSuspendThreshold() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSuspendThreshold(queue);
        }
    }

    @Override
    public int getSuspendOnSubordinate() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSuspendOnSubordinate(queue);
        }
    }

    @Override
    public int getSuspendByCalendar() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSuspendByCalendar(queue);
        }
    }

    @Override
    public int getUnknown() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getUnknown(queue);
        }
    }

    @Override
    public int getLoadAlarm() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getLoadAlarm(queue);
        }
    }

    @Override
    public int getDisabledManual() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getDisabledManual(queue);
        }
    }

    @Override
    public int getDisabledByCalendar() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getDisabledByCalendar(queue);
        }
    }

    @Override
    public int getAmbiguous() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getAmbiguous(queue);
        }
    }

    @Override
    public int getOrphaned() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getOrphaned(queue);
        }
    }

    @Override
    public int getError() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getError(queue);
        }
    }
}
