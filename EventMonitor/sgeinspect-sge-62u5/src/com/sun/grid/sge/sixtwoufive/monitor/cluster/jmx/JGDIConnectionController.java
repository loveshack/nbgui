/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx;

import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.jgdi.JGDIFactory;
import com.sun.grid.jgdi.event.ConnectionClosedEvent;
import com.sun.grid.jgdi.event.ConnectionFailedEvent;
import com.sun.grid.jgdi.event.Event;
import com.sun.grid.jgdi.event.EventListener;
import com.sun.grid.jgdi.event.EventTypeEnum;
import com.sun.grid.jgdi.event.QmasterGoesDownEvent;
import com.sun.grid.jgdi.event.ShutdownEvent;
import com.sun.grid.jgdi.management.JGDIProxy;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.grid.jgdi.JGDIException;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import java.io.PrintWriter;
import com.sun.grid.jgdi.monitoring.ClusterQueueSummary;
import com.sun.grid.jgdi.monitoring.ClusterQueueSummaryOptions;
import com.sun.grid.jgdi.monitoring.QHostOptions;
import com.sun.grid.jgdi.monitoring.QHostResult;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryOptions;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import com.sun.grid.jgdi.monitoring.filter.JobStateFilter;
import com.sun.grid.jgdi.monitoring.filter.QueueFilter;
import com.sun.grid.jgdi.monitoring.filter.ResourceAttributeFilter;
import com.sun.grid.sge.api.monitor.util.ExceptionHelper;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.io.InvalidClassException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.SocketException;
import java.rmi.UnknownHostException;
import java.util.Iterator;
import java.util.concurrent.Future;
import org.openide.util.NbBundle;

/**
 *
 */
public class JGDIConnectionController {

    private final static Logger log = Logger.getLogger(JGDIConnectionController.class.getName());
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Set<Listener> listeners = new HashSet<Listener>();
    private final Set<EventListener> eventListeners = new HashSet<EventListener>();
    private volatile JGDIProxy jgdiProxy;
    private boolean useSSL = false;
    private File caTop;
    private String serverHostname;
    private int serverPort;
    private ClusterImpl cluster;
    private volatile Future<?> doUpdateFuture;
    private final EventListener shutdownListener = new EventListener() {

        public void eventOccured(Event evt) {
            if (evt instanceof QmasterGoesDownEvent || evt instanceof ConnectionClosedEvent || evt instanceof ConnectionFailedEvent || evt instanceof ShutdownEvent) {
                disconnect();
//            } else if (evt instanceof ShutdownEvent) {
//                executor.submit(new ClearSubscriptionAction());
            }
        }
    };

    synchronized public JGDIProxy getProxy() {
        return jgdiProxy;
    }

    public JGDIConnectionController(ClusterImpl cluster) {
        this.cluster = cluster;
        eventListeners.add(shutdownListener);
    }

    public void connect(String host, int port, Object credentials, String caTop, String keyStore, char[] pw) {
        executor.submit(new ConnectAction(host, port, credentials, caTop, keyStore, pw));
    }

    public void disconnect() {
        executor.submit(new DisconnectAction());
    }

    synchronized public boolean isConnected() {
        return (jgdiProxy != null && jgdiProxy.isConnected());
    }

    public void setSubscription(Set<EventTypeEnum> types) {
        executor.submit(new SetSubscriptionAction(types));
    }

    public void subscribeAll() {
        EventTypeEnum[] values = EventTypeEnum.values();
        Set<EventTypeEnum> subs = new HashSet<EventTypeEnum>(values.length);
        for (EventTypeEnum value : values) {
            subs.add(value);
        }
        subscribe(subs);
    }

    public void unsubscribeAll() {
        executor.submit(new UnsubscribeAllAction());
    }

    public void subscribe(Set<EventTypeEnum> types) {
        executor.submit(new SubscribeAction(types, true));
    }

    public void unsubscribe(Set<EventTypeEnum> types) {
        executor.submit(new SubscribeAction(types, false));
    }

    public void addListener(Listener lis) {
        synchronized (listeners) {
            listeners.add(lis);
        }
    }

    public void removeListener(Listener lis) {
        synchronized (listeners) {
            listeners.remove(lis);
        }
    }

    private List<Listener> getListeners() {
        synchronized (listeners) {
            return new ArrayList<Listener>(listeners);
        }
    }

    public void addEventListener(EventListener lis) {
        eventListeners.add(lis);
    }

    public void removeEventListener(EventListener lis) {
        eventListeners.remove(lis);
    }

    private class UnsubscribeAllAction implements Runnable {

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            if (jgdiProxy != null) {
                                jgdiProxy.getProxy().setSubscription(Collections.<EventTypeEnum>emptySet());
                            }
                            for (Listener lis : getListeners()) {
                                lis.clearSubscription();
                            }
                        } catch (Exception ex) {
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(ex);
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class ClearSubscriptionAction implements Runnable {

        public void run() {
            for (Listener lis : getListeners()) {
                lis.clearSubscription();
            }
        }
    }

    private class SetSubscriptionAction implements Runnable {

        private final Set<EventTypeEnum> types;

        public SetSubscriptionAction(Set<EventTypeEnum> types) {
            this.types = types;

        }

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            if (jgdiProxy != null) {
                                jgdiProxy.getProxy().setSubscription(types);
                                for (Listener lis : getListeners()) {
                                    lis.subscriptionSet(types);
                                }
                            }
                        } catch (Exception ex) {
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(ex);
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class SubscribeAction implements Runnable {

        private final Set<EventTypeEnum> types;
        private final boolean subscribe;

        public SubscribeAction(Set<EventTypeEnum> types, boolean subscribe) {
            this.types = types;
            this.subscribe = subscribe;
        }

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            if (jgdiProxy != null) {

                                if (subscribe) {
                                    jgdiProxy.getProxy().subscribe(types);
                                    for (Listener lis : getListeners()) {
                                        lis.subscribed(types);
                                    }
                                } else {
                                    jgdiProxy.getProxy().unsubscribe(types);
                                    for (Listener lis : getListeners()) {
                                        lis.unsubscribed(types);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(ex);
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class DisconnectAction implements Runnable {

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            if (jgdiProxy != null) {
                                jgdiProxy.close();
                                if (useSSL) {
                                    JGDIProxy.resetSSL(serverHostname, serverPort, caTop);
                                }
                            }
                        } catch (Exception ex) {
                            log.log(Level.WARNING, NbBundle.getMessage(JGDIConnectionController.class, "connector.close_failed"), ex);
                        } finally {
                            jgdiProxy = null;
                            for (Listener lis : getListeners()) {
                                lis.disconnected();
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    private class ConnectAction implements Runnable {

        private final Object credentials;
        private final File keyStore;
        private final char[] pw;

        public ConnectAction(String myhost, int myport, Object credentials, String caTopPath, String keyStorePath, char[] pw) {
            serverHostname = myhost;
            serverPort = myport;
            this.credentials = credentials;
            if (caTopPath != null) {
                useSSL = true;
                caTop = new File(caTopPath);
                this.keyStore = new File(keyStorePath);
                this.pw = pw;
            } else {
                useSSL = false;
                caTop = null;
                this.keyStore = null;
                this.pw = null;
            }
        }

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            if (useSSL) {
                                if (!caTop.isDirectory()) {
                                    throw new SecurityException(NbBundle.getMessage(JGDIConnectionController.class, "ca_notfound", caTop.getAbsolutePath()));
                                }
                                JGDIProxy.setupSSL(serverHostname, serverPort, caTop, keyStore, pw);
                            }
                            jgdiProxy = JGDIFactory.newJMXInstance(serverHostname, serverPort, credentials);
                            for (EventListener lis : eventListeners) {
                                jgdiProxy.addEventListener(lis);
                            }
                            jgdiProxy.getProxy().getCurrentJGDIVersion();
                            Set<EventTypeEnum> subscription = jgdiProxy.getProxy().getSubscription();
                            for (Listener lis : getListeners()) {
                                lis.connected(serverHostname, serverPort, subscription);
                            }
                        } catch (UndeclaredThrowableException ex) {
                            JGDIException jex = new JGDIException(ex.getCause(), NbBundle.getMessage(JGDIConnectionController.class, "undeclared_throwable_exception"));
                            cluster.getConfiguration().setUserPassword(null);
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(jex);
                            }
                        } catch (SecurityException ex) {
                            JGDIException jex = new JGDIException(NbBundle.getMessage(JGDIConnectionController.class, "security_exception", ex.getLocalizedMessage()));
                            cluster.getConfiguration().setUserPassword(null);
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(jex);
                            }
                        } catch (JGDIException ex) {
                            JGDIException jex = new JGDIException(constructExceptionMessage(ex));

                            cluster.getConfiguration().setUserPassword(null);
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(jex);
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    /**
     * Constructs a message to the given {@link Throwable}
     * @param source The exception which needs a more precise message
     * @return The appripriate message for the given exception
     */
    private String constructExceptionMessage(Throwable source) {
        // Incompatible version
        InvalidClassException invalidClassException = ExceptionHelper.findCauseByType(InvalidClassException.class, source);
        if (invalidClassException != null) {
            return NbBundle.getMessage(JGDIConnectionController.class, "known_connect_exception",
                    NbBundle.getMessage(JGDIConnectionController.class, "incompatible_version_exception", cluster.getVersion()));
        }

        // Wrong keystore
        SocketException socketException = ExceptionHelper.findCauseByType(SocketException.class, source);
        if (socketException != null) {
            if (socketException.getMessage().endsWith("Broken pipe")) { // NOI18L
                return NbBundle.getMessage(JGDIConnectionController.class, "known_connect_exception",
                        NbBundle.getMessage(JGDIConnectionController.class, "keystore_wrong_excpetion", cluster.getConfiguration().getKeystorePath()));
            }
        }

        // Unknown host
        UnknownHostException unknownHostException = ExceptionHelper.findCauseByType(UnknownHostException.class, source);
        if (unknownHostException != null) {
            return NbBundle.getMessage(JGDIConnectionController.class, "known_connect_exception",
                    NbBundle.getMessage(JGDIConnectionController.class, "unknown_host_exception", cluster.getConfiguration().getJmxHost()));
        }

        // Default
        return NbBundle.getMessage(JGDIConnectionController.class, "connect_exception", source.getLocalizedMessage());
    }

    public boolean updateMonitoredData() {
        if (doUpdateFuture == null || doUpdateFuture.isDone() || doUpdateFuture.isCancelled()) {
            log.log(Level.FINE, "+++++++++++++++++ submit UpdateMonitoredDataAction +++++++++++++++++");
            doUpdateFuture = executor.submit(new UpdateMonitoredDataAction());
            return true;
        } else {
            log.log(Level.FINE, "+++++++++++++++++ skip submit UpdateMonitoredDataAction +++++++++++++++++");
            return false;
        }
    }

    private class UpdateMonitoredDataAction implements Runnable {

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
//                log.log(Level.FINE, "fetchQueueInstance {0}", mconfig.isFetchQueueInstance());
//                log.log(Level.FINE, "fetchClusterQueue {0}", mconfig.isFetchClusterQueue());
//                log.log(Level.FINE, "fetchExecHost {0}", mconfig.isFetchExecHost());
//                log.log(Level.FINE, "fetchJobs {0}", mconfig.isFetchJobs());

                            List<ClusterQueue> clusterQueueList = cluster.getMonitoredData().getClusterQueueList();
                            List<ClusterQueueSummary> clusterQueueSummary = cluster.getMonitoredData().getClusterQueueSummaryList();
                            for (ClusterQueueSummary cqs : clusterQueueSummary) {
                                log.log(Level.FINER, "clusterQueueSummary {0}", cqs.getName());
                            }
//                if (mconfig.isFetchClusterQueue() || clusterQueueSummary == null) {
                            // get Queue list and associated QueueInstances
                            clusterQueueList = jgdiProxy.getProxy().getClusterQueueList();

                            log.log(Level.FINEST, "clusterQueueList size {0}", clusterQueueList.size());

                            // ClusterQueueSummary list
                            ClusterQueueSummaryOptions cqOptions = new ClusterQueueSummaryOptions();
                            cqOptions.setShowAdditionalAttributes(true);
                            // workaround for getting clusterQueueList and clusterQueueSummary out of sync
                            // due to two jgdiProxy calls - should be fetched together to assure consistency
                            // of data
                            // using queueFilter to only fetch the Summary for the queues we fetched before
                            // it may happen that the queue has been removed meanwhile though
                            QueueFilter queueFilter = new QueueFilter();
                            for (ClusterQueue cq : clusterQueueList) {
                                queueFilter.addQueue(cq.getName());
                            }
                            cqOptions.setQueueFilter(queueFilter);
                            clusterQueueSummary = jgdiProxy.getProxy().getClusterQueueSummary(cqOptions);

                            if (clusterQueueList.size() > clusterQueueSummary.size()) {
                                // workaround
                                // some cluster queues disappeared meanwhile readjust
                                for (Iterator<ClusterQueue> it = clusterQueueList.iterator(); it.hasNext();) {
                                    ClusterQueue cq = it.next();
                                    boolean found = false;
                                    for (ClusterQueueSummary cqs : clusterQueueSummary) {
                                        if (cqs.getName().equals(cq.getName())) {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found) {
                                        log.log(Level.FINEST, "Queue not contained in clusterQueueSummary {0}", cq.getName());
                                        it.remove();
                                    }
                                }
                            }


                            log.log(Level.FINEST, "clusterQueueList size {0}", clusterQueueList.size());
                            log.log(Level.FINEST, "clusterQueueSummary size {0}", clusterQueueSummary.size());
//                }

                            QueueInstanceSummaryResult queueInstanceSummaryResult = cluster.getMonitoredData().getQueueInstanceSummaryResult();
//                if (mconfig.isFetchQueueInstance() || queueInstanceSummaryResult == null) {
                            // get finished, pending and error jobs
                            QueueInstanceSummaryOptions qiOptions = new QueueInstanceSummaryOptions();
                            qiOptions.setShowFullOutput(true);
                            qiOptions.setShowEmptyQueues(true);
                            qiOptions.setShowAdditionalAttributes(true);
                            qiOptions.setShowExtendedSubTaskInfo(true);
                            JobStateFilter jobStateFilter = JobStateFilter.fill("prsz");
                            qiOptions.setJobStateFilter(jobStateFilter);
                            // options.setJobUserFilter(jobUserFilter);
                            queueInstanceSummaryResult = jgdiProxy.getProxy().getQueueInstanceSummary(qiOptions);
//                }

                            QHostResult qhostResult = cluster.getMonitoredData().getQhostResult();
//                if (mconfig.isFetchExecHost() || qhostResult == null) {
                            // get Host list and associated queues
                            // resource attribute filter is needed to get virtual_total and other
                            // non standard attributes
                            QHostOptions qhostOptions = new QHostOptions();
                            qhostOptions.setIncludeQueue(true);
                            qhostOptions.setIncludeJobs(true);
                            ResourceAttributeFilter raf = new ResourceAttributeFilter();
                            qhostOptions.setResourceAttributeFilter(raf);
                            qhostResult = jgdiProxy.getProxy().execQHost(qhostOptions);
//                }
                            // fill into monitoredData to update listeners
                            JGDIMonitoredData monitoredData = new JGDIMonitoredData();
                            monitoredData.setExecdPort(jgdiProxy.getProxy().getSgeExecdPort());
                            monitoredData.setQmasterPort(jgdiProxy.getProxy().getSgeQmasterPort());
                            monitoredData.setSgeAdmin(jgdiProxy.getProxy().getAdminUser());
                            monitoredData.setSgeRoot(jgdiProxy.getProxy().getSGERoot().getAbsolutePath());
                            monitoredData.setSgeCell(jgdiProxy.getProxy().getSGECell());
                            monitoredData.setClusterQueueList(clusterQueueList);
                            monitoredData.setClusterQueueSummaryList(clusterQueueSummary);
                            monitoredData.setQhostResult(qhostResult);
                            monitoredData.setQueueInstanceSummaryResult(queueInstanceSummaryResult);
                            monitoredData.setPEList(jgdiProxy.getProxy().getParallelEnvironmentList());
                            monitoredData.setUserList(jgdiProxy.getProxy().getUserSetList());
                            monitoredData.prepareJobs(cluster);
                            monitoredData.preparePEMap();
                            monitoredData.prepareUserMap();

                            for (Listener lis : getListeners()) {
                                lis.updateFinished(monitoredData);
                            }

                        } catch (JGDIException ex) {
                            log.log(Level.SEVERE, null, ex);
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(ex);
                            }
                        } catch (Throwable ex) {
                            log.log(Level.SEVERE, null, ex);
                            for (Listener lis : getListeners()) {
                                lis.errorOccured(ex);
                            }
                        }
                    }
                    return null;
                }
            });
        }
    }

    public void qstatgc(PrintWriter out) {
        executor.submit(new QstatgcAction(out));
    }

    private class QstatgcAction implements Runnable {

        private final PrintWriter out;

        public QstatgcAction(PrintWriter out) {
            this.out = out;
        }

        public void run() {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    synchronized (JGDIConnectionController.this) {
                        try {
                            ClusterQueueSummaryOptions cqOptions = new ClusterQueueSummaryOptions();
                            cqOptions.setShowAdditionalAttributes(true);
                            List<ClusterQueueSummary> res = jgdiProxy.getProxy().getClusterQueueSummary(cqOptions);

                            if (res.size() == 0) {
                                out.printf("list size is 0"); //NOI18L
                                return null;
                            }
                            out.printf("%s%s%n", "CLUSTER QUEUE                   CQLOAD   USED    RES  AVAIL  TOTAL aoACDS  cdsuE", cqOptions.showAdditionalAttributes() ? "     s     A     S     C     u     a     d     D     c     o     E" : ""); //NOI18L
                            out.printf("%s%s%n", "--------------------------------------------------------------------------------", cqOptions.showAdditionalAttributes() ? "------------------------------------------------------------------" : ""); //NOI18L
                            for (ClusterQueueSummary elem : res) {
                                out.printf("%-30.30s ", elem.getName()); //NOI18L
                                if (elem.isLoadSet()) {
                                    out.printf("%7.2f ", elem.getLoad()); //NOI18L
                                } else {
                                    out.printf("%7s ", "-NA-"); //NOI18L
                                }
                                out.printf("%6d %6d %6d %6d %6d %6d ", elem.getUsedSlots(), elem.getReservedSlots(), elem.getAvailableSlots(), elem.getTotalSlots(), elem.getTempDisabled(), elem.getManualIntervention()); //NOI18L
                                if (cqOptions.showAdditionalAttributes()) {
                                    out.printf("%5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d ", elem.getSuspendManual(), elem.getSuspendThreshold(), elem.getSuspendOnSubordinate(), elem.getSuspendByCalendar(), elem.getUnknown(), elem.getLoadAlarm(), elem.getDisabledManual(), elem.getDisabledByCalendar(), elem.getAmbiguous(), elem.getOrphaned(), elem.getError()); //NOI18L
                                }
                                out.printf("%n"); //NOI18L
                            }
                        } catch (JGDIException ex) {
                            ex.printStackTrace(out);
                            log.log(Level.SEVERE, null, ex);
                        }
                    }
                    return null;
                }
            });
        }
    }

    public static interface Listener {

        public void connected(String host, int port, Set<EventTypeEnum> subscription);

        public void disconnected();

        public void subscriptionSet(Set<EventTypeEnum> types);

        public void subscribed(Set<EventTypeEnum> types);

        public void unsubscribed(Set<EventTypeEnum> types);

        public void errorOccured(Throwable ex);

        public void clearSubscription();

        public void updateFinished(JGDIMonitoredData data);
    }
}
