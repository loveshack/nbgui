/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.cluster.impl;

import com.sun.grid.jgdi.configuration.ParallelEnvironment;
import com.sun.grid.jgdi.management.mbeans.JGDIJMXMBean;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx.JGDICache;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx.JGDIMonitoredData;
import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.sge.sixtwoufive.config.util.ExceptionHelper;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.tools.visualvm.core.datasource.Storage;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.event.ChangeListener;

/**
 *
 */
public class ClusterImpl extends Cluster {

    private JGDICache jgdiCache;
    private final static JGDIMonitoredData NULL_HELPER = new JGDIMonitoredData();
    private final AtomicReference<JGDIMonitoredData> monitoredData = new AtomicReference<JGDIMonitoredData>(NULL_HELPER);

    public ClusterImpl(ClusterConfiguration config, Storage givenStorage) {
        super(config, givenStorage);
        jgdiCache = ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<JGDICache>() {

            public JGDICache execute() {
                return new JGDICache(ClusterImpl.this);
            }
        });

        //TODO read it from the cluster
        version = "62u5";
    }

    public void setMonitoredData(JGDIMonitoredData data) {
        monitoredData.set(data);
    }

    public JGDIMonitoredData getMonitoredData() {
        return monitoredData.get();
    }

    @Override
    public void connect() {
        jgdiCache.connect();
    }

    @Override
    public boolean isConnected() {
        return jgdiCache.isConnected();
    }

    @Override
    public void disconnect() {
        jgdiCache.disconnect();
    }

    public JGDICache getJGDICache() {
        return jgdiCache;
    }

    //------------------------------------------------------------------------
    // Queue/Host/PE List
    //------------------------------------------------------------------------
    @Override
    public List<Queue> getQueueList() {
        return jgdiCache.getQueueList();
    }

    @Override
    public List<Host> getHostList() {
        return jgdiCache.getHostList();
    }

    @Override
    protected void fireChangeEvent() {
        jgdiCache.fireChangeEvent();
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
        jgdiCache.addChangeListener(cl);
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
        jgdiCache.removeChangeListener(cl);
    }

    @Override
    public List<PE> getPEList() {
        return jgdiCache.getPEList();
    }

    @Override
    public boolean isPENameExists(String name) {
        boolean exists = false;
        for (PE pe : getPEList()) {
            exists = pe.getName().equals(name);
            if (exists) {
                break;
            }
        }
        return exists;
    }

    @Override
    public List<UserSet> getUserList() {
        return jgdiCache.getUserList();
    }

    public void createPE(final Object pe) throws PEException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, PEException>() {

            public Void execute() throws PEException {
                ParallelEnvironment per = (ParallelEnvironment) pe;
                JGDIJMXMBean proxy = jgdiCache.getProxy();
                try {
                    proxy.addParallelEnvironment(per);
                    proxy.updateParallelEnvironment(per);
                } catch (Throwable ex) {
                     throw new PEException(ExceptionHelper.getMessage(ex), ex);
                }
                return null;
            }
        });
    }

   public void deletePE(final String name) throws PEException {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlockWithException<Void, PEException>() {

            public Void execute() throws PEException {
               JGDIJMXMBean proxy = jgdiCache.getProxy();
                try {                   
                    proxy.deleteParallelEnvironment(name);
                } catch (Throwable ex) {
                    throw new PEException(ExceptionHelper.getMessage(ex), ex);
                }
                return null;
            }
        });
    }

    @Override
    public Object getUserSet(final String name) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Object>() {

            public Object execute() {
                return getMonitoredData().getUserSet(name);
            }
        });
    }
}
