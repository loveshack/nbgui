/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;
import com.sun.grid.sge.api.monitor.cluster.ClusterFactory;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.tools.visualvm.core.datasource.Storage;

/**
 *  Factory for creating the cluster instance
 */
public class ClusterFactoryImpl extends ClusterFactory {
    private static ClusterFactoryImpl INSTANCE;

    public ClusterFactoryImpl() {
        super(Version.supportedVersions);
    }

    public static ClusterFactoryImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClusterFactoryImpl();
        }

        return INSTANCE;
    }

    public Cluster createClusterDataSource(final ClusterConfiguration config, final Storage storage) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Cluster>() {

            public Cluster execute() {
                return new ClusterImpl(config, storage);
            }
        });
    }
}
