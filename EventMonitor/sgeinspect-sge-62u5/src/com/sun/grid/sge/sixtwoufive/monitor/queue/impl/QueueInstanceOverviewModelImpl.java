/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.queue.impl;

import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueInstanceOverview;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import java.util.List;
import java.util.Set;

public class QueueInstanceOverviewModelImpl extends QueueInstanceOverview {

    private QueueInstanceImpl queueInstance;
    private final ClusterImpl cluster;

    public QueueInstanceOverviewModelImpl(QueueInstance queueInstance) {
        this.queueInstance = (QueueInstanceImpl) queueInstance;
        cluster = (ClusterImpl) queueInstance.getOwner().getOwner();
    }

    @Override
    public String getName() {
        return queueInstance.getDisplayName();
    }

    @Override
    public String getQueueType() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getQueueType(queueInstance);
        }
    }

    @Override
    public int getReservedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getReservedSlots(queueInstance);
        }
    }

    @Override
    public int getUsedSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getUsedSlots(queueInstance);
        }
    }

    @Override
    public int getFreeSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getFreeSlots(queueInstance);
        }
    }

    @Override
    public String getArch() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getArch(queueInstance);
        }
    }

    @Override
    public String getState() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getState(queueInstance);
        }
    }

    @Override
    public String getLoadAvgStr() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getLoadAvgStr(queueInstance);
        }
    }

    @Override
    public boolean hasLoadValue() {
        synchronized (cluster) {
            return cluster.getMonitoredData().hasLoadValue(queueInstance);
        }
    }

    @Override
    public boolean isHasLoadValueFromObject() {
        synchronized (cluster) {
            return cluster.getMonitoredData().isHasLoadValueFromObject(queueInstance);
        }
    }

    @Override
    public double getLoadAvg() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getLoadAvg(queueInstance);
        }
    }

    @Override
    public String getLoadAlarmReason() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getLoadAlarmReason(queueInstance);
        }
    }

    @Override
    public String getSuspendAlarmReason() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSuspendAlarmReason(queueInstance);
        }
    }

    @Override
    public List<String> getExplainMessageList() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getExplainMessageList(queueInstance);
        }
    }

    @Override
    public Set<String> getResourceDominanceSet() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getResourceDominanceSet(queueInstance);
        }
    }

    @Override
    public Set<String> getResourceNames(String dom) {
        synchronized (cluster) {
            return cluster.getMonitoredData().getResourceNames(queueInstance, dom);
        }
    }

    @Override
    public String getResourceValue(String dom, String name) {
        synchronized (cluster) {
            return cluster.getMonitoredData().getResourceValue(queueInstance, dom, name);
        }
    }

    @Override
    public double getSlotUsage() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getSlotUsage(queueInstance);
        }
    }

    @Override
    public int getTotalSlots() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getTotalSlots(queueInstance);
        }
    }

    @Override
    public List<Job> getRunningJobs() {
        synchronized (cluster) {
            return cluster.getMonitoredData().getRunningJobs(queueInstance);
        }
    }
}
