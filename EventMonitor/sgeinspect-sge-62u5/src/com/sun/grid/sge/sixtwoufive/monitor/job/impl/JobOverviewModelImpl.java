/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.job.impl;

import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.api.monitor.job.JobOverview;
import java.util.Date;

public class JobOverviewModelImpl extends JobOverview {

    private JobImpl job;

    public JobOverviewModelImpl(Job job) {
        this.job = (JobImpl) job;
    }

    @Override
    public int getId() {
        return job.getId();
    }

    @Override
    public String getTaskId() {
        return job.getTaskId();
    }

    @Override
    public String getProject() {
        return job.getProject();
    }

    @Override
    public String getDepartment() {
        return job.getDepartment();
    }

    @Override
    public boolean isRunning() {
        return job.isRunning();
    }

    @Override
    public double getPriority() {
        return job.getPriority();
    }

    @Override
    public String getName() {
        return job.getName();
    }

    @Override
    public String getUser() {
        return job.getUser();
    }

    @Override
    public String getState() {
        return job.getState();
    }

    @Override
    public String getQueue() {
        return job.getQueue();
    }

    @Override
    public String getQinstanceName() {
        return job.getQinstanceName();
    }

    @Override
    public Date getSubmitTime() {
        return job.getSubmitTime();
    }

    @Override
    public Date getStartTime() {
        return job.getStartTime();
    }

    @Override
    public int getSlots() {
        return job.getSlots();
    }
}
