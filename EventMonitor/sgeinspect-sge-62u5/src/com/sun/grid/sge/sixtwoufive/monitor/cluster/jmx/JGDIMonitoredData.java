/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.cluster.jmx;

import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.sixtwoufive.monitor.job.impl.JobImpl;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import com.sun.grid.jgdi.configuration.ParallelEnvironment;
import com.sun.grid.jgdi.configuration.UserSet;
import com.sun.grid.jgdi.monitoring.ClusterQueueSummary;
import com.sun.grid.jgdi.monitoring.HostInfo;
import com.sun.grid.jgdi.monitoring.JobInfo;
import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.jgdi.monitoring.QHostResult;
import com.sun.grid.jgdi.monitoring.QueueInfo;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.libs.sixtwoufive.SGESixTwoUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.NbBundle;

public class JGDIMonitoredData {

    private static Logger logger = Logger.getLogger(JGDIMonitoredData.class.getName());
    private List<ClusterQueue> clusterQueueList = Collections.<ClusterQueue>emptyList();
    private List<ParallelEnvironment> peList = Collections.<ParallelEnvironment>emptyList();
    private List<UserSet> userList = Collections.<UserSet>emptyList();
    private QHostResult qhostResult;
    private List<ClusterQueueSummary> clusterQueueSummaryList = Collections.<ClusterQueueSummary>emptyList();
    private QueueInstanceSummaryResult queueInstanceSummaryResult;
    private int qmasterPort = -1;
    private int execdPort = -1;
    private String sgeRoot = "-NA-";
    private String sgeCell = "-NA-";
    private String sgeAdmin = "-NA-";
    private int finishedJobsCount = 0;
    private int pendingJobsCount = 0;
    private int errorJobsCount = 0;
    private int clusterRunningJobsCount = 0;
    private int zombieJobsCount = 0;
    private final List<Job> finishedJobs = new LinkedList<Job>();
    private final List<Job> pendingJobs = new LinkedList<Job>();
    private final List<Job> errorJobs = new LinkedList<Job>();
    private final List<Job> clusterRunningJobs = new LinkedList<Job>();
    private final List<Job> zombieJobs = new LinkedList<Job>();
    private int slotsUtilized = 0;
    private int availability = 0;
    private int overload = 0;
    private int usedSlots = 0;
    private int reservedSlots = 0;
    private int totalSlots = 0;
    private int availableSlots = 0;
    private final Map<String, HostInfo> hostInfoMap = new HashMap<String, HostInfo>();
    private final Map<String, ClusterQueueSummary> clusterQueueSummaryMap = new HashMap<String, ClusterQueueSummary>();
    private final Map<String, QueueInstanceSummary> queueInstanceSummaryMap = new HashMap<String, QueueInstanceSummary>();
    private final Map<String, ParallelEnvironment> PEMap = new HashMap<String, ParallelEnvironment>();
    private final Map<String, UserSet> userMap = new HashMap<String, UserSet>();

    public int getExecdPort() {
        return execdPort;
    }

    public void setExecdPort(int execdPort) {
        this.execdPort = execdPort;
    }

    public int getQmasterPort() {
        return qmasterPort;
    }

    public void setQmasterPort(int qmasterPort) {
        this.qmasterPort = qmasterPort;
    }

    public String getSgeAdmin() {
        return sgeAdmin;
    }

    public void setSgeAdmin(String sgeAdmin) {
        this.sgeAdmin = sgeAdmin;
    }

    public String getSgeCell() {
        return sgeCell;
    }

    public void setSgeCell(String sgeCell) {
        this.sgeCell = sgeCell;
    }

    public String getSgeRoot() {
        return sgeRoot;
    }

    public void setSgeRoot(String sgeRoot) {
        this.sgeRoot = sgeRoot;
    }

    public List<ClusterQueue> getClusterQueueList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<ClusterQueue>>() {

            public List<ClusterQueue> execute() {
                return clusterQueueList;
            }
        });
    }

    public void setClusterQueueList(final List<ClusterQueue> cql) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                clusterQueueList = cql;
                return null;
            }
        });
    }

    public List<ParallelEnvironment> getPEList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<ParallelEnvironment>>() {

            public List<ParallelEnvironment> execute() {
                return peList;
            }
        });
    }

    public void setPEList(final List<ParallelEnvironment> pel) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                peList = pel;
                return null;
            }
        });
    }

    public List<UserSet> getUserList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<UserSet>>() {

            public List<UserSet> execute() {
                return userList;
            }
        });
    }

    public void setUserList(final List<UserSet> userl) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                userList = userl;
                return null;
            }
        });
    }

    public List<ClusterQueueSummary> getClusterQueueSummaryList() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<ClusterQueueSummary>>() {

            public List<ClusterQueueSummary> execute() {
                return clusterQueueSummaryList;
            }
        });
    }

    public void setClusterQueueSummaryList(final List<ClusterQueueSummary> clusterQueueSummaryL) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                clusterQueueSummaryList = clusterQueueSummaryL;
                return null;
            }
        });
    }

    public QHostResult getQhostResult() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<QHostResult>() {

            public QHostResult execute() {
                return qhostResult;
            }
        });
    }

    public void setQhostResult(final QHostResult qhostR) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                qhostResult = qhostR;
                return null;
            }
        });
    }

    public QueueInstanceSummaryResult getQueueInstanceSummaryResult() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<QueueInstanceSummaryResult>() {

            public QueueInstanceSummaryResult execute() {
                return queueInstanceSummaryResult;
            }
        });
    }

    public void setQueueInstanceSummaryResult(final QueueInstanceSummaryResult queueInstanceSummaryR) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                queueInstanceSummaryResult = queueInstanceSummaryR;
                return null;
            }
        });
    }

    public int getAvailability() {
        return availability;
    }

    public List<Job> getClusterRunningJobs() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Job>>() {

            public List<Job> execute() {
                return clusterRunningJobs;
            }
        });
    }

    public int getClusterRunningJobsCount() {
        return clusterRunningJobsCount;
    }

    public List<Job> getErrorJobs() {
        return errorJobs;
    }

    public int getErrorJobsCount() {
        return errorJobsCount;
    }

    public List<Job> getFinishedJobs() {
        return finishedJobs;
    }

    public int getFinishedJobsCount() {
        return finishedJobsCount;
    }

    public int getOverload() {
        return overload;
    }

    public List<Job> getPendingJobs() {
        return pendingJobs;
    }

    public int getPendingJobsCount() {
        return pendingJobsCount;
    }

    public int getReservedSlots() {
        return reservedSlots;
    }

    public int getSlotsUtilized() {
        return slotsUtilized;
    }

    public int getTotalSlots() {
        return totalSlots;
    }

    public int getUsedSlots() {
        return usedSlots;
    }

    public List<Job> getZombieJobs() {
        return zombieJobs;
    }

    public int getZombieJobsCount() {
        return zombieJobsCount;
    }

    public HostInfo getHostInfo(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<HostInfo>() {

            public HostInfo execute() {
                return hostInfoMap.get(host.getHostName());
            }
        });
    }

    public ParallelEnvironment getPE(final String name) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<ParallelEnvironment>() {

            public ParallelEnvironment execute() {
                return PEMap.get(name);
            }
        });
    }

    public UserSet getUserSet(final String name) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<UserSet>() {

            public UserSet execute() {
                return userMap.get(name);
            }
        });
    }

    public ClusterQueueSummary getClusterQueueSummary(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<ClusterQueueSummary>() {

            public ClusterQueueSummary execute() {
                return clusterQueueSummaryMap.get(queue.getQueueName());
            }
        });
    }

    public QueueInstanceSummary getQueueInstanceSummary(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<QueueInstanceSummary>() {

            public QueueInstanceSummary execute() {
                return queueInstanceSummaryMap.get(queueInstance.getQueueName());
            }
        });
    }

    public List<Job> getRunningJobs(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Job>>() {

            public List<Job> execute() {
                List<Job> hostJobs = new LinkedList<Job>();
                HostInfo hi = hostInfoMap.get(host.getHostName());
                if (hi != null) {
                    List<JobInfo> jobs = hi.getJobList();
                    if (jobs != null && clusterRunningJobs != null) {
                        for (Job job : clusterRunningJobs) {
                            for (JobInfo ji : jobs) {
                                String jiTaskId = ji.getTaskId();
                                String jobTaskId = job.getTaskId();
                                if (ji.getId() == job.getId() && ((jiTaskId == null) || (jiTaskId.equals(jobTaskId)))) {
                                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "getRunningJobs(host)", job.getId()));
                                    hostJobs.add(job);
                                }
                            }
                        }
                    }
                }
                return hostJobs;
            }
        });
    }

    public int getRunningJobsCount(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                HostInfo hi = hostInfoMap.get(host.getHostName());
                if (hi != null) {
                    return hi.getJobCount();
                }
                return 0;
            }
        });
    }

    public List<Job> getRunningJobs(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Job>>() {

            public List<Job> execute() {
                List<Job> queueInstanceJobs = new LinkedList<Job>();
                QueueInstanceSummary queueInstanceSummary = queueInstanceSummaryMap.get(queueInstance.getQueueName());
                if (queueInstanceSummary != null) {
                    List<JobSummary> jobs = queueInstanceSummary.getJobList();
                    if (jobs != null) {
                        for (Job job : clusterRunningJobs) {
                            for (JobSummary js : jobs) {
                                if (js.getId() == job.getId() && (!js.isArray() || js.getTaskId().equals(job.getTaskId()))) {
                                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "getRunningJobs(queueInstance)", job.getId()));
                                    queueInstanceJobs.add(job);
                                }
                            }
                        }
                    }
                }
                return queueInstanceJobs;
            }
        });
    }

    void prepareJobs(final ClusterImpl cluster) {

        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {

                if (queueInstanceSummaryResult != null) {
                    finishedJobsCount = queueInstanceSummaryResult.getFinishedJobs().size();
                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "count_finished", finishedJobsCount));
                    finishedJobs.clear();
                    for (JobSummary jobSummary : queueInstanceSummaryResult.getFinishedJobs()) {
                        Job job = new JobImpl(jobSummary, jobSummary.getId(), jobSummary.getTaskId());
                        finishedJobs.add(job);
                    }

                    pendingJobsCount = queueInstanceSummaryResult.getPendingJobs().size();
                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "count_pending", pendingJobsCount));
                    pendingJobs.clear();
                    for (JobSummary jobSummary : queueInstanceSummaryResult.getPendingJobs()) {
                        Job job = new JobImpl(jobSummary, jobSummary.getId(), jobSummary.getTaskId());
                        pendingJobs.add(job);
                    }

                    errorJobsCount = queueInstanceSummaryResult.getErrorJobs().size();
                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "count_error", errorJobsCount));
                    errorJobs.clear();
                    for (JobSummary jobSummary : queueInstanceSummaryResult.getErrorJobs()) {
                        Job job = new JobImpl(jobSummary, jobSummary.getId(), jobSummary.getTaskId());
                        errorJobs.add(job);
                    }

                    zombieJobsCount = queueInstanceSummaryResult.getZombieJobs().size();
                    logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "count_zombie", zombieJobsCount));
                    zombieJobs.clear();
                    for (JobSummary jobSummary : queueInstanceSummaryResult.getZombieJobs()) {
                        Job job = new JobImpl(jobSummary, jobSummary.getId(), jobSummary.getTaskId());
                        zombieJobs.add(job);
                    }

                    clusterRunningJobs.clear();
                    clusterRunningJobsCount = 0;

                    // running jobs summary for all queue instances
                    for (QueueInstanceSummary qi : queueInstanceSummaryResult.getQueueInstanceSummary()) {

                        queueInstanceSummaryMap.put(qi.getName(), qi);

                        for (JobSummary jobSummary : qi.getJobList()) {
                            Job job = new JobImpl(jobSummary, jobSummary.getId(), jobSummary.getTaskId());
                            clusterRunningJobsCount++;
                            clusterRunningJobs.add(job);
                            logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "job_qi_summary", jobSummary.getId()));
                        }
                    }
                }

                if (qhostResult != null) {
                    for (HostInfo hi : qhostResult.getHostInfo()) {
                        if (!hi.getHostname().equals("global")) {
                            hostInfoMap.put(hi.getHostname(), hi);
                        }
                    }
                }

                /*
                 * calculate available, overload and slotsUtilzed values from
                 * cluster queues
                 */

                int manualIntervention = 0;
                int tempDisabled = 0;
                double loadAverage = 0.0;
                int queuesWithLoad = 0;

                // remove no longer contained cluster qeues from clusterQueueSummaryMap
                for (ClusterQueueSummary cqs : clusterQueueSummaryList) {
                    if (clusterQueueSummaryMap.get(cqs.getName()) == null) {
                        clusterQueueSummaryMap.remove(cqs.getName());
                    }
                }

                for (ClusterQueueSummary cqSummary : clusterQueueSummaryList) {
                    logger.log(Level.FINEST, "cqSummary.getManualIntervention(): {0}", cqSummary.getManualIntervention());
                    clusterQueueSummaryMap.put(cqSummary.getName(), cqSummary);

                    totalSlots += cqSummary.getTotalSlots();
                    usedSlots += cqSummary.getUsedSlots();
                    reservedSlots += cqSummary.getReservedSlots();
                    // availableSlots += cqSummary.getAvailableSlots();
                    manualIntervention += cqSummary.getManualIntervention();
                    tempDisabled += cqSummary.getTempDisabled();
                    availableSlots = totalSlots - manualIntervention - tempDisabled;
                    if (cqSummary.isLoadSet()) {
                        loadAverage += cqSummary.getLoad();
                        queuesWithLoad++;
                    }
                }
                logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "used_slots", usedSlots));
                logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "total_slots", totalSlots));
                if (totalSlots > 0) {
                    // principal availability independent of load states
                    // availability = (totalSlots - manualIntervention) * 100 / totalSlots;
                    // availability as displayed in qstat -g c for whole cluster
                    availability = availableSlots * 100 / totalSlots;
                }
                if (availableSlots > 0) {
                    slotsUtilized = usedSlots * 100 / availableSlots;
                }
                if (queuesWithLoad > 0) {
                    overload = (int) ((loadAverage / queuesWithLoad) * 100);
                }
                logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "slots_utilized", slotsUtilized));
                logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "availability", availability));
                logger.log(Level.FINEST, NbBundle.getMessage(JGDIMonitoredData.class, "overload", overload));
                return null;
            }
        });
    }

    void preparePEMap() {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
//         remove no longer contained PE  from Map
                for (ParallelEnvironment pe : peList) {
                    if (PEMap.get(pe.getName()) == null) {
                        PEMap.remove(pe.getName());
                    }
                }

                for (ParallelEnvironment pe : peList) {
                    PEMap.put(pe.getName(), pe);
                }
                return null;
            }
        });
    }

    void prepareUserMap() {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
//         remove no longer contained Users  from Map
                for (UserSet user : userList) {
                    if (userMap.get(user.getName()) == null) {
                        userMap.remove(user.getName());
                    }
                }
                for (UserSet user : userList) {
                    userMap.put(user.getName(), user);
                }
                return null;
            }
        });
    }
//------------------------------------------------------------------------
// ClusterOverviewModel - overall cluster metrics
//------------------------------------------------------------------------
//------------------------------------------------------------------------
// HostOverviewModel
//------------------------------------------------------------------------

    public Object getHostValue(final Host host, final String name) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Object>() {

            public Object execute() {
                return getHostInfo(host).getHostValue(name);
            }
        });
    }

    public String getArch(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    return hostInfo.getArch();
                }
                return "-";
            }
        });
    }

    public int getHostValueCount(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    return hostInfo.getHostValueCount();
                }
                return 0;
            }
        });
    }

    public List<Queue> getQueueList(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<Queue>>() {

            public List<Queue> execute() {
                return host.getQueueList();
            }
        });
    }

    public int getQueueCount(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                return host.getQueueList().size();
            }
        });
    }

    public int getAvailableSlots(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                int availableHostSlots = 0;
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    for (QueueInfo qi : hostInfo.getQueueList()) {
                        // reserved slots are not subtracted similar to qstat -g c behavior
                        availableHostSlots += (qi.getTotalSlots() - qi.getUsedSlots());
                    }
                }
                return availableHostSlots;
            }
        });
    }

// TODO per host slot value for ReservedSlost in jgdi and sge_qhost
    public int getReservedSlots(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                int reservedHostSlots = 0;
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    for (QueueInfo qi : hostInfo.getQueueList()) {
                        reservedHostSlots += qi.getReservedSlots();
                    }
                }
                return reservedHostSlots;
            }
        });
    }

    public int getTotalSlots(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                int totalHostSlots = 0;
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    for (QueueInfo qi : hostInfo.getQueueList()) {
                        totalHostSlots += qi.getTotalSlots();
                    }

                }
                return totalHostSlots;
            }
        });
    }

    public int getUsedSlots(final Host host) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                int usedHostSlots = 0;
                HostInfo hostInfo = getHostInfo(host);
                if (hostInfo != null) {
                    for (QueueInfo qi : hostInfo.getQueueList()) {
                        usedHostSlots += qi.getUsedSlots();
                    }

                }
                return usedHostSlots;
            }
        });
    }
//------------------------------------------------------------------------
// QueueOverviewModel
//------------------------------------------------------------------------
    public double getLoad(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

            public Double execute() {
                try {
                    return getClusterQueueSummary(queue).getLoad();
                } catch (NullPointerException npe) {
                    return 0.0;
                }

            }
        });
    }

    public boolean isLoadSet(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                try {
                    return getClusterQueueSummary(queue).isLoadSet();
                } catch (NullPointerException npe) {
                    return false;
                }

            }
        });
    }

    public int getReservedSlots(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getReservedSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getUsedSlots(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getUsedSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getTotalSlots(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getTotalSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getAvailableSlots(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getAvailableSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getTempDisabled(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getTempDisabled();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getManualIntervention(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getManualIntervention();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getSuspendManual(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getSuspendManual();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getSuspendThreshold(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getSuspendThreshold();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getSuspendOnSubordinate(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getSuspendOnSubordinate();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getSuspendByCalendar(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getSuspendByCalendar();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getUnknown(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getUnknown();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getLoadAlarm(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getLoadAlarm();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getDisabledManual(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getDisabledManual();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getDisabledByCalendar(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getDisabledByCalendar();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public int getAmbiguous(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getAmbiguous();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getOrphaned(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getOrphaned();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getError(final Queue queue) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getClusterQueueSummary(queue).getError();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

//------------------------------------------------------------------------
// QueueInstanceOverviewModel
//------------------------------------------------------------------------
    public String getQueueType(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getQueueType();
                } catch (NullPointerException npe) {
                    return "";
                }
            }
        });
    }

    public int getReservedSlots(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getReservedSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getUsedSlots(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getUsedSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    public int getFreeSlots(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    QueueInstanceSummary qis = getQueueInstanceSummary(queueInstance);
                    // reserved slots are not subtracted like in qstat -g c
                    return qis.getTotalSlots() - qis.getUsedSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }

            }
        });
    }

    public String getArch(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getArch();
                } catch (NullPointerException npe) {
                    return "";
                }

            }
        });
    }

    public String getState(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getState();
                } catch (NullPointerException npe) {
                    return "";
                }

            }
        });
    }

    public String getLoadAvgStr(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getLoadAvgStr();
                } catch (NullPointerException npe) {
                    return "";
                }

            }
        });
    }

    public boolean hasLoadValue(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).hasLoadValue();
                } catch (NullPointerException npe) {

                    return false;
                }

            }
        });
    }

    public boolean isHasLoadValueFromObject(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Boolean>() {

            public Boolean execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).isHasLoadValueFromObject();
                } catch (NullPointerException npe) {
                    return false;
                }

            }
        });
    }

    public double getLoadAvg(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

            public Double execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getLoadAvg();
                } catch (NullPointerException npe) {
                    return 0.0;
                }

            }
        });
    }

    public String getLoadAlarmReason(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getLoadAlarmReason();
                } catch (NullPointerException npe) {
                    return "";
                }

            }
        });
    }

    public String getSuspendAlarmReason(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getSuspendAlarmReason();
                } catch (NullPointerException npe) {
                    return "";
                }

            }
        });
    }

    public List<String> getExplainMessageList(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<List<String>>() {

            public List<String> execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getExplainMessageList();
                } catch (NullPointerException npe) {
                    return Collections.emptyList();
                }

            }
        });
    }

    public Set<String> getResourceDominanceSet(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Set<String>>() {

            public Set<String> execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getResourceDominanceSet();
                } catch (NullPointerException npe) {
                    return Collections.emptySet();
                }
            }
        });
    }

    public Set<String> getResourceNames(final QueueInstance queueInstance, final String dom) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Set<String>>() {

            public Set<String> execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getResourceNames(dom);
                } catch (NullPointerException npe) {
                    return Collections.emptySet();
                }
            }
        });
    }

    public String getResourceValue(final QueueInstance queueInstance, final String dom, final String name) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<String>() {

            public String execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getResourceValue(dom, name);
                } catch (NullPointerException npe) {
                    return "";
                }
            }
        });
    }

    public double getSlotUsage(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Double>() {

            public Double execute() {
                try {
                    int myTotalSlots = getTotalSlots(queueInstance);
                    int myUsedSlots = getUsedSlots(queueInstance);
                    double slotUsage = 0.0;
                    if (myTotalSlots > 0) {
                        slotUsage = (double) myUsedSlots / (double) myTotalSlots;
                    }

                    return slotUsage;
                } catch (NullPointerException npe) {
                    return 0.0;
                }

            }
        });
    }

    public int getTotalSlots(final QueueInstance queueInstance) {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SGESixTwoUFive.class, new ClassLoaderAnchor.CommandBlock<Integer>() {

            public Integer execute() {
                try {
                    return getQueueInstanceSummary(queueInstance).getTotalSlots();
                } catch (NullPointerException npe) {
                    return 0;
                }
            }
        });
    }

    /**
     *
     *  returns a memory value as double
     *
     *  @param mem the memory value as string e.g. 100G
     *  @param unit the memory unit to base on e.g. "k"
     *  unit
     *  k : Multiplier = 1000
     *  K : Multiplier = 1024
     *  m : Multiplier = 1000*1000
     *  M : Multiplier = 1024*1024
     *  g : Multiplier = 1000*1000*1000
     *  G : Multiplier = 1024*1024*1024
     *
     *  @return the memory value as double based on unit
     */
    public double parseMem(String mem, double unit) {

        if (mem != null && !mem.equals("-")) { //NOI18L
            double multiplier = 1.0;
            if (mem.trim().endsWith("k")) { //NOI18L
                multiplier = 1000;
                mem = mem.replace("k", ""); //NOI18L
            } else if (mem.trim().endsWith("K")) { //NOI18L
                multiplier = 1024;
                mem = mem.replace("K", ""); //NOI18L
            } else if (mem.trim().endsWith("m")) { //NOI18L
                multiplier = 1000 * 1000;
                mem = mem.replace("m", ""); //NOI18L
            } else if (mem.trim().endsWith("M")) { //NOI18L
                multiplier = 1024 * 1024;
                mem = mem.replace("M", ""); //NOI18L
            } else if (mem.trim().endsWith("g")) { //NOI18L
                multiplier = 1000 * 1000 * 1000;
                mem = mem.replace("g", ""); //NOI18L
            } else if (mem.trim().endsWith("G")) { //NOI18L
                multiplier = 1024 * 1024 * 1024;
                mem = mem.replace("G", ""); //NOI18L
            }
            double memValue = Double.parseDouble(mem);

            return memValue * multiplier / unit;
        } else {
            return Double.NaN;
        }
    }
}


