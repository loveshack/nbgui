/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.sixtwoufive.monitor.queue.impl;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;

public class QueueInstanceImpl extends QueueInstance {

    public QueueInstanceImpl(Cluster cluster, String name, String type) {
        super(cluster, name, type);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof QueueInstanceImpl && getQueueName() != null) {
            return getQueueName().equals(((QueueInstance) obj).getQueueName()) &&
                   (getCluster() == ((QueueInstance) obj).getCluster());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = 17;
        int c = 0;
        if (getQueueName() != null) {
            c = getQueueName().hashCode() + getCluster().hashCode();
        }
        result = 31 * result + c;
        return result;
    }
}
