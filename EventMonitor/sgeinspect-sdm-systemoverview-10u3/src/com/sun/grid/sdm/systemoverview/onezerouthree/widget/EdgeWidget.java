/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.edge.*;
import java.awt.Color;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.PointShape;
import org.netbeans.api.visual.widget.ConnectionWidget;

public class EdgeWidget extends ConnectionWidget {

    private Edge edge;

    public EdgeWidget(SdmGraphScene scene, Edge edge) {
        super(scene);
        this.edge = edge;
        this.setSourceAnchor(AnchorFactory.createCenterAnchor(this.edge.getSource()));
        this.setTargetAnchor(AnchorFactory.createCenterAnchor(this.edge.getTarget()));
        this.setTargetAnchorShape(AnchorShape.TRIANGLE_HOLLOW);
        this.setLineColor(Color.LIGHT_GRAY);
        this.setEndPointShape(PointShape.SQUARE_FILLED_SMALL);
    }

    public Edge getEdge() {
        return this.edge;
    }
}
