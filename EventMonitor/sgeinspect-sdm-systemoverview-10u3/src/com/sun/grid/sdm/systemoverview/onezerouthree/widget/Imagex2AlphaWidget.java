/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import javax.swing.GrayFilter;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.ErrorManager;

public class Imagex2AlphaWidget extends Widget {

    public static final int TARGET_WIDGET = 0;
    public static final int TARGET_BACKGROUND = 1;
    public static final int TARGET_BORDER = 2;
    public static final int TARGET_CHILDREN = 3;
    private float walphaa, balpha, bkgalpha, calpha = 1.0f;
    private Image imagea;
    private Image imageb;
    private Image disabledImagea;
    private Image disabledImageb;
    private int width, height;
    private boolean paintAsDisabled;
    private final ImageObserver observera = new ImageObserver() {

        public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
            setImageCore(imagea);
            getScene().validate();
            return (infoflags & (ImageObserver.ABORT | ImageObserver.ERROR)) == 0;
        }
    };
    private final ImageObserver observerb = new ImageObserver() {

        public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
            setImageCore(imageb);
            getScene().validate();
            return (infoflags & (ImageObserver.ABORT | ImageObserver.ERROR)) == 0;
        }
    };

    public Imagex2AlphaWidget(Scene scene, Image imagea, Image imageb, float alpha) {
        super(scene);
        this.imagea = imagea;
        this.imageb = imageb;
        alpha = Math.max(Math.min(1.0f, alpha), 0.0f);
        this.walphaa = alpha;
        this.balpha = alpha;
        this.bkgalpha = alpha;
        this.calpha = alpha;
    }

    public Imagex2AlphaWidget(Scene scene, Image imagea, Image imageb, float w, float b, float bkg, float c) {
        super(scene);
        this.imagea = imagea;
        this.imageb = imageb;
        walphaa = Math.max(Math.min(1.0f, w), 0.0f);
        balpha = Math.max(Math.min(1.0f, b), 0.0f);
        bkgalpha = Math.max(Math.min(1.0f, bkg), 0.0f);
        calpha = Math.max(Math.min(1.0f, c), 0.0f);
    }

    public void setAlpha(float alpha, int target) {
        alpha = Math.max(Math.min(1.0f, alpha), 0.0f);
        switch (target) {
            case TARGET_WIDGET:
                walphaa = alpha;
                break;
            case TARGET_BACKGROUND:
                bkgalpha = alpha;
                break;
            case TARGET_BORDER:
                balpha = alpha;
                break;
            case TARGET_CHILDREN:
                calpha = alpha;
                break;
            default:
                walphaa = alpha;
                balpha = alpha;
                calpha = alpha;
                bkgalpha = alpha;
                break;
        }
    }

    public float getAlpha(int target) {
        switch (target) {
            case TARGET_WIDGET:
                return walphaa;
            case TARGET_BACKGROUND:
                return bkgalpha;
            case TARGET_BORDER:
                return balpha;
            case TARGET_CHILDREN:
                return calpha;
            default:
                return 100;
        }
    }

    /**
     * Returns an image.
     * @return the image
     */
    public Image getImageA() {
        return imagea;
    }

    /**
     * Returns an image.
     * @return the image
     */
    public Image getImageB() {
        return imageb;
    }

    /**
     * Sets an image
     * @param image the image
     */
    public void setImageA(Image image) {
        if (this.imagea == image) {
            return;
        }
        setImageCore(image);
    }

    /**
     * Sets an image
     * @param image the image
     */
    public void setImageB(Image image) {
        if (this.imageb == image) {
            return;
        } else {
//            if (image.getWidth(null) != imagea.getWidth(null) || image.getHeight(null) != imagea.getHeight(null)) {
//                image = image.getScaledInstance(imagea.getWidth(null), imagea.getHeight(null), Image.SCALE_SMOOTH);
//            }
            this.imageb = image;
        }
    }

    private void setImageCore(Image image) {
        int oldWidth = width;
        int oldHeight = height;

        this.imagea = image;
        this.disabledImagea = null;
        width = image != null ? image.getWidth(null) : 0;
        height = image != null ? image.getHeight(null) : 0;

        if (oldWidth == width && oldHeight == height) {
            repaint();
        } else {
            revalidate();
        }
    }

    /**
     * Returns whether the label is painted as disabled.
     * @return true, if the label is painted as disabled
     */
    public boolean isPaintAsDisabled() {
        return paintAsDisabled;
    }

    /**
     * Sets whether the label is painted as disabled.
     * @param paintAsDisabled if true, then the label is painted as disabled
     */
    public void setPaintAsDisabled(boolean paintAsDisabled) {
        boolean repaint = this.paintAsDisabled != paintAsDisabled;
        this.paintAsDisabled = paintAsDisabled;
        if (repaint) {
            repaint();
        }
    }

    /**
     * Calculates a client area of the image
     * @return the calculated client area
     */
    @Override
    protected Rectangle calculateClientArea() {
        if (imagea != null) {
            return new Rectangle(0, 0, width, height);
        }
        return super.calculateClientArea();
    }

    @Override
    public void paintWidget() {
        if (imagea == null || imageb == null) {
            return;
        }

        if (imagea != null && imageb != null) {
            Graphics2D gr = getGraphics();

            if (paintAsDisabled) {
                if (disabledImagea == null) {
                    disabledImagea = GrayFilter.createDisabledImage(imagea);
                    MediaTracker tracker = new MediaTracker(getScene().getView());
                    tracker.addImage(disabledImagea, 0);
                    try {
                        tracker.waitForAll();
                    } catch (InterruptedException e) {
                        ErrorManager.getDefault().notify(e);
                    }
                }
                if (disabledImageb == null) {
                    disabledImageb = GrayFilter.createDisabledImage(imageb);
                    MediaTracker tracker = new MediaTracker(getScene().getView());
                    tracker.addImage(disabledImageb, 0);
                    try {
                        tracker.waitForAll();
                    } catch (InterruptedException e) {
                        ErrorManager.getDefault().notify(e);
                    }
                }
                Composite previous = gr.getComposite();
                Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, walphaa);
                gr.setComposite(current);
                gr.drawImage(disabledImagea, 0, 0, observera);
                current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f - walphaa);
                gr.setComposite(current);
                gr.drawImage(disabledImageb, 0, 0, observerb);
                gr.setComposite(previous);
            } else {
                Composite previous = gr.getComposite();
                Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, walphaa);
                gr.setComposite(current);
                gr.drawImage(imagea, 0, 0, observera);
                current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f - walphaa);
                gr.setComposite(current);
                gr.drawImage(imageb, 0, 0, observerb);
                gr.setComposite(previous);
            }
        }
    }

    @Override
    public void paintBorder() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, balpha);
        gr.setComposite(current);
        super.paintBorder();
        gr.setComposite(previous);
    }

    @Override
    public void paintBackground() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, bkgalpha);
        gr.setComposite(current);
        super.paintBackground();
        gr.setComposite(previous);
    }

    @Override
    public void paintChildren() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, calpha);
        gr.setComposite(current);
        super.paintChildren();
        gr.setComposite(previous);
    }
}

