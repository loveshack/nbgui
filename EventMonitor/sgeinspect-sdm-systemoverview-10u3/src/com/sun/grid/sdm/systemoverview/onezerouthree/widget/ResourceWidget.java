/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.options.SystemOverviewOptionOptionsPanelController;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator.AlphaWidgetAnimator;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import java.awt.Image;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.animator.AnimatorEvent;
import org.netbeans.api.visual.animator.AnimatorListener;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.ImageWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;

public class ResourceWidget extends AlphaWidget implements DataSourceWidget<ResourceDataSource> {

    private final ImageWidget img;
    private ResourceDataSource ds;
    private final LabelWidget name;
    private static final Image HOST = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_server_32.png");
    private static final Logger log = Logger.getLogger(ResourceWidget.class.getName());
    private final PCL pcl = new PCL();

    public ResourceWidget(SdmGraphScene scene, ResourceDataSource node) {
        super(scene);
        NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).addPreferenceChangeListener(new SDMGSL());
        this.img = new ImageWidget(scene);
        this.ds = node;
        this.ds.addPropertyChangeListener(pcl);
        this.name = new LabelWidget(scene);
        this.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 4));
        @SuppressWarnings("unchecked")
        DataSourceDescriptor<ResourceDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
        if (NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.DEMO_MODE, false)) {
            if (dsd != null) {
                img.setImage(HOST);
            }
        } else {
            if (dsd != null) {
                img.setImage(dsd.getIcon());
            }
        }
        setToolTipText(ds.getId());
        name.setLabel(ds.getName());
        this.addChild(img);
        this.addChild(name);
        name.setVisible(NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.SHOW_RESOURCE_NAMES, false));
        this.setPreferredLocation(new Point(0, 0));
    }

    @Override
    public ResourceDataSource getDataSource() {
        return ds;
    }

    public SdmGraphScene getSdmScene() {
        return (SdmGraphScene) getScene();
    }

    public void addToContainer(final ResourceContainerWidget<? extends DataSource> rcw) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                setAlpha(0.0f, 4);
                rcw.getContainerWidget().addChild(ResourceWidget.this);
                getScene().validate();
                AlphaWidgetAnimator animator = new AlphaWidgetAnimator(getScene().getSceneAnimator());
                animator.setAlpha(ResourceWidget.this, 0.0f, 1.0f);
//                setAlpha(0.0f, 4);
//                int x = 0, y = 0;
//                if (rcw.getLocation() != null && rcw.getBounds() != null && getBounds() != null) {
//                    x = (int) rcw.getLocation().getX() + (int) rcw.getBounds().getWidth() / 2 - (int) getBounds().getWidth() / 2;
//                    y = (int) rcw.getLocation().getY() + (int) rcw.getBounds().getHeight() / 2 - (int) getBounds().getHeight() / 2;
//                } else if (getScene().getView() != null) {
//                    x = getScene().getView().getWidth() / 2;
//                    y = getScene().getView().getHeight() / 2;
//                }
//                Point p = new Point(x, y);
//                PreferredLocationAlphaWidgetAnimator animator = new PreferredLocationAlphaWidgetAnimator(getScene().getSceneAnimator());
//                AAL aal = new AAL();
//                animator.addAnimatorListener(aal);
//                aal.setTask(new Runnable() {
//
//                    public void run() {
//                        if (ResourceWidget.this.getParentWidget() != null) {
//                            ResourceWidget.this.getParentWidget().removeChild(ResourceWidget.this);
//                            ResourceWidget.this.getScene().validate();
//                        }
//                        rcw.getContainerWidget().addChild(ResourceWidget.this);
//                        ResourceWidget.this.getScene().validate();
//                    }
//                });
//                animator.setAlpha(ResourceWidget.this, 0.0f, 1.0f, p);
            }
        });
    }

    public void removeFromContainer(final ResourceContainerWidget<? extends DataSource> rcw) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                AlphaWidgetAnimator animator = new AlphaWidgetAnimator(getScene().getSceneAnimator());
                RemoveResourceAL ral = new RemoveResourceAL(rcw, ResourceWidget.this);
                animator.addAnimatorListener(ral);
                animator.setAlpha(ResourceWidget.this, 0.0f);
                ds.removePropertyChangeListener(pcl);
//                PreferredLocationAlphaWidgetAnimator animator = new PreferredLocationAlphaWidgetAnimator(getScene().getSceneAnimator());
//                RemoveResourceAL ral = new RemoveResourceAL(rcw, ResourceWidget.this);
//                animator.addAnimatorListener(ral);
//                int middle = getScene().getView().getHeight() / 2;
//                animator.setAlpha(ResourceWidget.this, 0.0f, new Point((int) getLocation().getX(), middle));
            }
        });
    }

    private static class RemoveResourceAL implements AnimatorListener {

        private final ResourceWidget rw;
        private final ResourceContainerWidget<? extends DataSource> rcw;

        RemoveResourceAL(ResourceContainerWidget<? extends DataSource> c, ResourceWidget r) {
            rcw = c;
            rw = r;
        }

        public void animatorStarted(AnimatorEvent arg0) {
        }

        public void animatorReset(AnimatorEvent arg0) {
        }

        public void animatorFinished(AnimatorEvent arg0) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    if (rw.getParentWidget() != null) {
                        rcw.getContainerWidget().removeChild(rw);
                        rcw.getScene().validate();
                    }
                }
            });
        }

        public void animatorPreTick(AnimatorEvent arg0) {
        }

        public void animatorPostTick(AnimatorEvent arg0) {
        }
    }

    private class SDMGSL implements PreferenceChangeListener {

        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.SHOW_RESOURCE_NAMES)) {
                name.setVisible(Boolean.parseBoolean(evt.getNewValue()));
                name.repaint();
                getScene().validate();
            } else if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.DEMO_MODE)) {
                if (Boolean.parseBoolean(evt.getNewValue())) {
                    img.setImage(HOST);
                } else {
                    @SuppressWarnings("unchecked")
                    DataSourceDescriptor<ResourceDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
                    if (dsd != null) {
                        img.setImage(dsd.getIcon());
                    }
                }
            }
        }
    }

    private class AAL implements AnimatorListener {

        private AtomicBoolean isFinished = new AtomicBoolean(false);
        private Runnable task;

        public void animatorStarted(AnimatorEvent arg0) {
        }

        public void animatorReset(AnimatorEvent arg0) {
        }

        public void animatorFinished(AnimatorEvent arg0) {
            isFinished.set(true);
            if (task != null) {
                startTask();
            }
        }

        public boolean isFinished() {
            return isFinished.get();
        }

        public void setTask(Runnable r) {
            task = r;
            startTask();
        }

        private void startTask() {
            if (isFinished()) {
                SwingUtilities.invokeLater(task);
            }
        }

        public void animatorPreTick(AnimatorEvent arg0) {
        }

        public void animatorPostTick(AnimatorEvent arg0) {
        }
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            setToolTipText(ds.getId());
            name.setLabel(ds.getName());
        }
    }
}
