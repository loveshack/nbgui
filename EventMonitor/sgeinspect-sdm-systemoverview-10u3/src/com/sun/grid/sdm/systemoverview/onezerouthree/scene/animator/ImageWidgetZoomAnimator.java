/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator;

import java.awt.Dimension;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.animator.Animator;
import org.netbeans.api.visual.animator.SceneAnimator;
import org.netbeans.api.visual.widget.ImageWidget;

public class ImageWidgetZoomAnimator extends Animator {

    private HashMap<ImageWidget, Dimension> sourceDimensions = new HashMap<ImageWidget, Dimension>();
    private HashMap<ImageWidget, Image> origImage = new HashMap<ImageWidget, Image>();
    private HashMap<ImageWidget, Dimension> targetDimensions = new HashMap<ImageWidget, Dimension>();

    public ImageWidgetZoomAnimator(SceneAnimator sceneAnimator) {
        super(sceneAnimator);
    }

    public void setImageSize(ImageWidget widget, Dimension imageSize) {
        assert widget != null;
        assert imageSize != null;
        sourceDimensions.clear();
        targetDimensions.put(widget, imageSize);
        start();
    }

    public void setImageSize(ImageWidget widget, Image img, Dimension imageSize) {
        assert widget != null;
        assert imageSize != null;
        sourceDimensions.clear();
        origImage.put(widget, img);
        targetDimensions.put(widget, imageSize);
        start();
    }

    @Override
    protected void tick(double progress) {
        for (Map.Entry<ImageWidget, Dimension> entry : targetDimensions.entrySet()) {
            ImageWidget widget = entry.getKey();
            Dimension sourceDimension = sourceDimensions.get(widget);
            if (sourceDimension == null) {
                sourceDimension = new Dimension(widget.getImage().getWidth(null), widget.getImage().getHeight(null));
                sourceDimensions.put(widget, sourceDimension);
            }
            Dimension targetDimension = entry.getValue();
            Dimension boundary;
            if (progress >= 1.0) {
                boundary = targetDimension;
            } else {
                boundary = new Dimension(
                        (int) (sourceDimension.width + progress * progress * (targetDimension.width - sourceDimension.width)),
                        (int) (sourceDimension.height + progress * progress * (targetDimension.width - sourceDimension.height)));
            }
            Image img = origImage.get(widget);
            if (img == null) {
                img = widget.getImage();
            }
            widget.setImage(img.getScaledInstance(boundary.width, boundary.height, Image.SCALE_SMOOTH));
            getScene().validate();
        }
        if (progress >= 1.0) {
            origImage.clear();
            sourceDimensions.clear();
            targetDimensions.clear();
        }
    }
}
