/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator;

import com.sun.grid.sdm.systemoverview.onezerouthree.widget.AlphaWidget;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.animator.Animator;
import org.netbeans.api.visual.animator.SceneAnimator;

public class PreferredLocationAlphaWidgetAnimator extends Animator {

    private HashMap<AlphaWidget, Target> sourceAlpha = new HashMap<AlphaWidget, Target>();
    private HashMap<AlphaWidget, Target> targetAlpha = new HashMap<AlphaWidget, Target>();

    public PreferredLocationAlphaWidgetAnimator(SceneAnimator sceneAnimator) {
        super(sceneAnimator);
    }

    public void setAlpha(AlphaWidget widget, Float sourcealpha, Float targetalpha, Point targetLocation) {
        assert widget != null;
        assert sourcealpha != null;
        assert targetalpha != null;
        sourceAlpha.put(widget, new Target(sourcealpha, widget.getLocation()));
        targetAlpha.put(widget, new Target(targetalpha, targetLocation));
        start();
    }

    public void setAlpha(AlphaWidget widget, Float targetalpha, Point targetLocation) {
        assert widget != null;
        assert targetalpha != null;
        sourceAlpha.clear();
        targetAlpha.put(widget, new Target(targetalpha, targetLocation));
        start();
    }

    @Override
    protected void tick(double progress) {
        for (Map.Entry<AlphaWidget, Target> entry : targetAlpha.entrySet()) {
            AlphaWidget widget = entry.getKey();
            Target sourcealpha = sourceAlpha.get(widget);
            if (sourcealpha == null) {
                sourcealpha = new Target(1.0f, widget.getLocation());
                sourceAlpha.put(widget, sourcealpha);
            }
            Target targetalpha = entry.getValue();
            Target boundary;
            if (progress >= 1.0) {
                boundary = targetalpha;
            } else {
                Point p = new Point((int) (sourcealpha.getTargetLocation().getX() + progress * (targetalpha.getTargetLocation().getX() - sourcealpha.getTargetLocation().getX())),
                        (int) (sourcealpha.getTargetLocation().getY() + progress * (targetalpha.getTargetLocation().getY() - sourcealpha.getTargetLocation().getY())));
                boundary = new Target(sourcealpha.getTargetAlpha() + (float) progress * (targetalpha.getTargetAlpha() - sourcealpha.getTargetAlpha()), p);
            }
            widget.setAlpha(boundary.getTargetAlpha(), 4);
            widget.setPreferredLocation(boundary.getTargetLocation());
            widget.getScene().validate();
        }
        if (progress >= 1.0) {
            sourceAlpha.clear();
            targetAlpha.clear();
        }
    }

    private class Target {

        private Float flo;
        private Point loc;

        public Target(Float f, Point p) {
            this.flo = f;
            this.loc = p;
        }

        public Float getTargetAlpha() {
            return flo;
        }

        public Point getTargetLocation() {
            return loc;
        }
    }
}
