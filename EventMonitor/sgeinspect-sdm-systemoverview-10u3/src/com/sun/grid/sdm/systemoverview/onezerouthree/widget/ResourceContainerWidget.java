/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.systemoverview.onezerouthree.layout.ContainerLayout;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.border.GradientRoundedBorder;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.SystemColor;
import javax.swing.JPopupMenu;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.action.TwoStateHoverProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.LevelOfDetailsWidget;
import org.netbeans.api.visual.widget.Widget;

public abstract class ResourceContainerWidget<T extends DataSource> extends Widget implements DataSourceWidget<T> {

    private T ds;
    private final Widget container;

    public ResourceContainerWidget(SdmGraphScene s, T ds) {
//        super(s, 0.5, 1, Double.MAX_VALUE, Double.MAX_VALUE);
        super(s);
        this.ds = ds;
//        this.setBorder(new GradientRoundedBorder(Color.BLACK, 5, Color.decode("0xFFFFFF"), Color.decode("0xAAAAFF"), 5, 5, 25, 25));
        this.setBackground(SystemColor.window);
        this.setBorder(new GradientRoundedBorder(SystemColor.white, 1, SystemColor.window, SystemColor.window, 0, 0, 10, 10));
        this.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 5));
        this.setPreferredSize(new Dimension(100, 100));
        Widget labelPlaceHolder = new Widget(s);
        labelPlaceHolder.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 4));
        labelPlaceHolder.addChild(getLabelWidget());
        this.addChild(labelPlaceHolder);

        this.container = new LevelOfDetailsWidget(s, 0.5, 1, Double.MAX_VALUE, Double.MAX_VALUE);
//        this.container.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 4));
        this.container.setLayout(new ContainerLayout(2, 10, 4));
        this.container.setOpaque(false);
        this.addChild(container);

        setOpaque(true);

        WidgetAction hoverAction = ActionFactory.createHoverAction(new MyHoverProvider());
        s.getActions().addAction(hoverAction);
        getActions().addAction(hoverAction);

//        WidgetAction popupMenuAction = ActionFactory.createPopupMenuAction(new MyPopupProvider());
//        getActions().addAction(popupMenuAction);

    }

    protected abstract Widget getLabelWidget();

    public SdmGraphScene getSdmScene() {
        return (SdmGraphScene) getScene();
    }

    public T getDataSource() {
        return ds;
    }

    public Widget getContainerWidget() {
        return container;
    }

    private static class MyHoverProvider implements TwoStateHoverProvider {

        public void unsetHovering(Widget widget) {
//            if (widget != null) {
//                ((ResourceContainerWidget) widget).icon.getImageWidget().setBackground(Color.WHITE);
//                ((ResourceContainerWidget) widget).icon.getImageWidget().setForeground(Color.BLACK);
////                ((DataSourceWidget) widget).icon.getLabelWidget().setVisible(false);
//            }
        }

        public void setHovering(Widget widget) {
//            if (widget != null) {
//                ((ResourceContainerWidget) widget).icon.getImageWidget().setBackground(new Color(52, 124, 150));
//                ((ResourceContainerWidget) widget).icon.getImageWidget().setForeground(Color.WHITE);
////                ((DataSourceWidget) widget).icon.getLabelWidget().setVisible(true);
//            }
        }
    }

    private static class MyPopupProvider implements PopupMenuProvider {

        public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {
            JPopupMenu menu = new JPopupMenu();
            menu.add("Open");
            return menu;
        }
    }
}
