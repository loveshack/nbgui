/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.layout;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.edge.Edge;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.DataSourceWidget;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.netbeans.api.visual.graph.layout.GraphLayout;
import org.netbeans.api.visual.graph.layout.UniversalGraph;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.visual.widget.Widget;

/**
 * Performs layout of SdmScene.
 */
public final class ContainerSceneLayout extends GraphLayout<DataSource, Edge> {

    private int verticalGap;
    private int horizontalGap;
    private static final Logger log = Logger.getLogger(ContainerSceneLayout.class.getName());

    public ContainerSceneLayout(int verticalGap, int horizontalGap) {
        this.verticalGap = verticalGap;
        this.horizontalGap = horizontalGap;
    }

    public void setProperties(int verticalGap, int horizontalGap) {
        this.verticalGap = verticalGap;
        this.horizontalGap = horizontalGap;
    }

    protected void performGraphLayout(UniversalGraph<DataSource, Edge> graph) {
        SdmGraphScene scene;
        /* sdmscene layout works just for SdmScene */
        if (!(graph.getScene() instanceof SdmGraphScene)) {
            return;
        } else {
            scene = (SdmGraphScene) graph.getScene();
        }

        Collection<DataSource> nodes = scene.getNodes();
        if (nodes.isEmpty()) {
            return;
        }

        DataSource node = scene.getSystem();

        List<DataSourceWidget<ComponentDataSource>> compos = scene.getComponentWidgets();
        List<DataSourceWidget<ServiceDataSource>> services = scene.getServiceWidgets();
        Widget system = scene.getSystemWidget();

        // put "Service Domain Manager" into center of the screen
        Point origin = new Point(horizontalGap, verticalGap);
        setResolvedNodeLocation(graph, node, origin);

        Collections.sort(services, new ServiceNameComparator());
        Collections.sort(compos, new ComponentNameComparator());

        List<DataSourceWidget<? extends DataSource>> all = new ArrayList<DataSourceWidget<? extends DataSource>>(compos.size() + services.size());
        all.addAll(compos);
        all.addAll(services);

        if (all.size() == 0) {
            return;
        }

        // split widgets into upper and lower
        int half = (int) Math.ceil(((double) all.size()) / 2);

        if (half == 0) {
            return;
        }

        // compute the height of resource container widget
        int rcheight = Math.max((scene.getView().getHeight() - 4 * verticalGap - 50) / 2, 100);
        log.log(Level.FINEST, "view area height: " + scene.getView().getHeight());
        log.log(Level.FINEST, "rc area height  : " + rcheight);


        // compute the width of resource container widget
        int rcwidth_u = Math.max((scene.getView().getWidth() - (half + 1) * horizontalGap) / half, 100);
        log.log(Level.FINEST, "view area width : " + scene.getView().getWidth());
        log.log(Level.FINEST, "rc area width u : " + rcwidth_u);
        // widgets above SDM
        for (int i = 0; i < half; i++) {
            Point above = new Point();
            above.setLocation(origin.getX() + i * horizontalGap + i * rcwidth_u, origin.getY() - verticalGap - rcheight);
            setResolvedNodeLocation(graph, (all.get(i)).getDataSource(), above);
            scene.getSceneAnimator().animatePreferredBounds((Widget) all.get(i), new Rectangle(rcwidth_u, rcheight));
        }

        if (all.size() > half) {
            // compute the width of resource container widget
            int rcwidth_d = Math.max((scene.getView().getWidth() - ((all.size() - half) + 1) * horizontalGap) / (all.size() - half), 100);
            log.log(Level.FINEST, "view area width : " + scene.getView().getWidth());
            log.log(Level.FINEST, "rc area width d : " + rcwidth_d);
            // widgets below SDM
            for (int i = 0; i < (all.size() - half); i++) {
                Point below = new Point();
                below.setLocation(origin.getX() + i * horizontalGap + i * rcwidth_d, origin.getY() + verticalGap + 50);
                setResolvedNodeLocation(graph, (all.get(i + half)).getDataSource(), below);
                scene.getSceneAnimator().animatePreferredBounds((Widget) all.get(i + half), new Rectangle(rcwidth_d, rcheight));
            }

        }


        // resize the SDM widget
        scene.getSceneAnimator().animatePreferredBounds(system, new Rectangle(scene.getView().getWidth() - 2 * horizontalGap, 50));

        scene.repaint();
    }

    @Override
    protected void performNodesLayout(UniversalGraph<DataSource, Edge> universalGraph, Collection<DataSource> nodes) {
        // TODO
        Logger.getLogger(getClass().getName()).log(Level.WARNING, org.openide.util.NbBundle.getMessage(ContainerSceneLayout.class, "performNodesLayout_no_supported"));
    }

    private class ServiceNameComparator implements
            Comparator<DataSourceWidget<ServiceDataSource>> {

        public int compare(DataSourceWidget<ServiceDataSource> o1, DataSourceWidget<ServiceDataSource> o2) {
            return o1.getDataSource().getServiceName().compareTo(o2.getDataSource().getServiceName());
        }
    }

    private class ComponentNameComparator implements
            Comparator<DataSourceWidget<ComponentDataSource>> {

        public int compare(DataSourceWidget<ComponentDataSource> o1, DataSourceWidget<ComponentDataSource> o2) {
            return o1.getDataSource().getComponentName().compareTo(o2.getDataSource().getComponentName());
        }
    }
}
