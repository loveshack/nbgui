/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.DataSourceViewsManager;
import com.sun.tools.visualvm.core.ui.PluggableDataSourceViewProvider;
import java.util.Set;

/**
 *
 * @author mb199851
 */
public class JvmSceneViewProvider extends PluggableDataSourceViewProvider<JvmDataSource> {

    @Override
    public Set<Integer> getPluggableLocations(DataSourceView view) {
        return ALL_LOCATIONS;
    }

    private static final class Singleton {

        private static final JvmSceneViewProvider INSTANCE = new JvmSceneViewProvider();

        private Singleton() {
        }
    }

    private JvmSceneViewProvider() {
    }

    protected DataSourceView createView(JvmDataSource app) {
        return new JvmSceneView(app);
    }

    protected boolean supportsViewFor(JvmDataSource app) {
        return app != null && app.isCS();
    }

    public static void initialize() {
        DataSourceViewsManager.sharedInstance().addViewProvider(Singleton.INSTANCE, JvmDataSource.class);
    }

    public static void shutdown() {
        DataSourceViewsManager.sharedInstance().removeViewProvider(Singleton.INSTANCE);
    }
}

