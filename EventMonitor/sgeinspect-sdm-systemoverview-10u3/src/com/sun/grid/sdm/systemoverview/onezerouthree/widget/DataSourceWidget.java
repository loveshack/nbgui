/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.tools.visualvm.core.datasource.DataSource;

/**
 *
 * @author mb199851
 */
public interface DataSourceWidget<T extends DataSource> {

    T getDataSource();

    SdmGraphScene getSdmScene();

}
