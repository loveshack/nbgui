/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.options.SystemOverviewOptionOptionsPanelController;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.awt.Image;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.ImageWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;

public class ResourceProviderWidget extends ResourceContainerWidget<ResourceProviderDataSource> {

    private static final Logger log = Logger.getLogger(ResourceProviderWidget.class.getName());
    private final DCL_RDS drl;
    private Widget label;

    public ResourceProviderWidget(SdmGraphScene scene, ResourceProviderDataSource node) {
        super(scene, node);
        drl = new DCL_RDS(this);
        initialize();
    }

    private void initialize() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getDataSource().getRepository().addDataChangeListener(drl, ResourceDataSource.class);
                for (ResourceDataSource rds : getDataSource().getRepository().getDataSources(ResourceDataSource.class)) {
                    ResourceWidget rw = new ResourceWidget(getSdmScene(), rds);
                    getContainerWidget().addChild(rw);
                }
            }
        });

    }

    private static class DCL_RDS implements DataChangeListener<ResourceDataSource> {

        ResourceProviderWidget rpw;

        private DCL_RDS(ResourceProviderWidget rpw) {
            this.rpw = rpw;
        }

        public void dataChanged(final DataChangeEvent<ResourceDataSource> arg0) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    for (ResourceDataSource remd : arg0.getRemoved()) {
                        List<ResourceWidget> toRemove = new LinkedList<ResourceWidget>();
                        for (Widget w : rpw.getContainerWidget().getChildren()) {
                            if (((ResourceWidget) w).getDataSource().equals(remd)) {
                                toRemove.add((ResourceWidget) w);
                            }
                        }
                        for (ResourceWidget w : toRemove) {
                            w.removeFromContainer(rpw);
                        }
                    }
                    List<ResourceDataSource> childRDSs = new ArrayList<ResourceDataSource>(rpw.getContainerWidget().getChildren().size());
                    for (Widget w : rpw.getContainerWidget().getChildren()) {
                        childRDSs.add(((ResourceWidget) w).getDataSource());
                    }
                    List<ResourceWidget> toAdd = new LinkedList<ResourceWidget>();
                    for (ResourceDataSource addd : arg0.getAdded()) {
                        if (!childRDSs.contains(addd)) {
                            ResourceWidget rw = new ResourceWidget(rpw.getSdmScene(), addd);
                            toAdd.add(rw);
                        }
                    }
                    for (ResourceWidget w : toAdd) {
                        w.addToContainer(rpw);
                    }
                }
            });
        }
    }

    protected synchronized Widget getLabelWidget() {
         if (label == null) {
            label = new LW(getSdmScene(), getDataSource());
        }
        return label;
    }

    private static class LW extends AlphaWidget {

        private final ResourceProviderDataSource ds;
        private final LabelWidget name;
        private final ImageWidget icon;
        private final AlphaWidget info;
        private final LabelWidget anno;
        private static final Image DEMO_RP = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_rp_48x32.png");

        public LW(SdmGraphScene s, ResourceProviderDataSource ds) {
            super(s, 1.0f);
            NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).addPreferenceChangeListener(new SDMGSL());
            this.ds = ds;
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 4));
            icon = new ImageWidget(getScene());
            info = new AlphaWidget(getScene(), 0.0f);
            info.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 0));
            name = new LabelWidget(s);
            anno = new LabelWidget(s);
            addChild(icon);
            addChild(name);
            addChild(info);
            addChild(anno);
            name.setVisible(NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.SHOW_SERVICE_NAMES, false));

            initialize();
        }

        private void setupLabelNameAndIcon(boolean demo) {
            if (demo) {
                icon.setImage(DEMO_RP);
                @SuppressWarnings("unchecked")
                DataSourceDescriptor<? extends ResourceProviderDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
                if (dsd != null) {
                    icon.setToolTipText(dsd.getName());
                    name.setLabel(dsd.getName() + " [ " + dsd.getDescription() + " ]");
                }
            } else {
                @SuppressWarnings("unchecked")
                DataSourceDescriptor<? extends ResourceProviderDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
                if (dsd != null) {
                    icon.setImage(dsd.getIcon());
                    icon.setToolTipText(dsd.getName());
                    name.setLabel(dsd.getName() + " [ " + dsd.getDescription() + " ]");
                }
            }
        }

        private void initialize() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {

                    setupLabelNameAndIcon(NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.DEMO_MODE, false));
                }
            });
        }

        private class SDMGSL implements PreferenceChangeListener {

            public void preferenceChange(PreferenceChangeEvent evt) {
                if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.SHOW_SERVICE_NAMES)) {
                    name.setVisible(Boolean.parseBoolean(evt.getNewValue()));
                    name.repaint();
                    getScene().validate();
                } else if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.DEMO_MODE)) {
                    setupLabelNameAndIcon(Boolean.parseBoolean(evt.getNewValue()));
                    getScene().validate();
                }
            }
        }
    }
}
