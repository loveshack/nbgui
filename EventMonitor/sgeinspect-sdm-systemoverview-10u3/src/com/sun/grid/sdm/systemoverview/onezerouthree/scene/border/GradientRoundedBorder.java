/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.border;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.RoundRectangle2D;
import org.netbeans.api.visual.border.Border;

public class GradientRoundedBorder implements Border {

    private final int arcWidth;
    private final int arcHeight;
    private final Color colorBorder;
    private final Insets insets;
    private final Stroke stroke;
    private final Color color1;
    private final Color color2;
//    private Color color3;
//    private Color color4;
//    private Color color5;

    /**
     * Creates a rounded border filled with gradient.
     *
     * @param colorBorder color of the border
     * @param thickness line thickness of the border
     * @param color1 top color of the border
     * @param color2 bottom color of the gradient
     * @param insetWidth inset width
     * @param insetHeight inset height
     * @param arcWidth widht of the arc of the border
     * @param arcHeight height of the arc of the border
     */
    public GradientRoundedBorder(Color colorBorder, int thickness, Color color1, Color color2, int insetWidth, int insetHeight, int arcWidth, int arcHeight) {
        this.colorBorder = colorBorder;
        this.insets = new Insets(insetHeight, insetWidth, insetHeight, insetWidth);
        this.stroke = new BasicStroke(thickness);
        this.color1 = color1;
        this.color2 = color2;
        this.arcWidth = arcWidth;
        this.arcHeight = arcHeight;
//        this.color3 = color3;
//        this.color4 = color4;
//        this.color5 = color5;
    }

    public Insets getInsets() {
        return insets;
    }

    public void paint(Graphics2D gr, Rectangle bounds) {
        Shape previousClip = gr.getClip();
        gr.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gr.clip(new RoundRectangle2D.Float(bounds.x, bounds.y, bounds.width, bounds.height, arcWidth, arcHeight));

        drawGradient(gr, bounds, color1, color2, 0f, 1);
//        drawGradient(gr, bounds, color2, color3, 0.3f, 0.764f);
//        drawGradient(gr, bounds, color3, color4, 0.764f, 0.927f);
//        drawGradient(gr, bounds, color4, color5, 0.927f, 1f);

        gr.setColor(colorBorder);
        Stroke previousStroke = gr.getStroke();
        gr.setStroke(stroke);
        gr.draw(new RoundRectangle2D.Float(bounds.x + 0.5f, bounds.y + 0.5f, bounds.width - 1, bounds.height - 1, arcWidth, arcHeight));
        gr.setStroke(previousStroke);

        gr.setClip(previousClip);
    }

    private void drawGradient(Graphics2D gr, Rectangle bounds, Color color1, Color color2, float y1, float y2) {
        y1 = bounds.y + y1 * bounds.height;
        y2 = bounds.y + y2 * bounds.height;
        gr.setPaint(new GradientPaint(bounds.x, y1, color1, bounds.x, y2, color2));
        gr.fill(new Rectangle.Float(bounds.x, y1, bounds.x + bounds.width, y2));
    }

    public boolean isOpaque() {
        return true;
    }
}
