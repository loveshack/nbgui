/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.border.GradientRoundedBorder;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;

public class SystemWidget extends LabelWidget implements DataSourceWidget<JvmDataSource> {

    private JvmDataSource ds;

    public SystemWidget(SdmGraphScene scene, JvmDataSource node) {
        super(scene);
        this.ds = node;
        this.setPreferredSize(new Dimension((int) scene.getClientArea().getWidth(), 50));
        this.setBackground(SystemColor.window);
        this.setBorder(new GradientRoundedBorder(SystemColor.white, 2, SystemColor.window, SystemColor.window, 0, 0, 25, 25));
        @SuppressWarnings("unchecked")
        DataSourceDescriptor<JvmDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
        if (dsd != null) {
            setToolTipText(dsd.getName());
        }
        Map<TextAttribute, Object> fontattr = new HashMap<TextAttribute, Object>();
        fontattr.put(TextAttribute.FOREGROUND, Color.WHITE);
        fontattr.put(TextAttribute.FAMILY, Font.MONOSPACED);
        fontattr.put(TextAttribute.SIZE, 36);
        Font h = Font.getFont(fontattr);
        setFont(h);
        setLabel("Service Domain Manager");
        setAlignment(Alignment.CENTER);
        setVerticalAlignment(VerticalAlignment.CENTER);
    }

    public JvmDataSource getDataSource() {
        return ds;
    }

    public SdmGraphScene getSdmScene() {
        return (SdmGraphScene) getScene();
    }
}
