/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator;

import com.sun.grid.sdm.systemoverview.onezerouthree.widget.AlphaWidget;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.animator.Animator;
import org.netbeans.api.visual.animator.SceneAnimator;

public class AlphaWidgetAnimator extends Animator {

    private HashMap<AlphaWidget, Float> sourceAlpha = new HashMap<AlphaWidget, Float>();
    private HashMap<AlphaWidget, Float> targetAlpha = new HashMap<AlphaWidget, Float>();

    public AlphaWidgetAnimator(SceneAnimator sceneAnimator) {
        super(sceneAnimator);
    }

    public void setAlpha(AlphaWidget widget, Float sourcealpha, Float targetalpha) {
        assert widget != null;
        assert sourcealpha != null;
        assert targetalpha != null;
        sourceAlpha.put(widget, sourcealpha);
        targetAlpha.put(widget, targetalpha);
        start();
    }

    public void setAlpha(AlphaWidget widget, Float targetalpha) {
        assert widget != null;
        assert targetalpha != null;
        sourceAlpha.clear();
        targetAlpha.put(widget, targetalpha);
        start();
    }

    @Override
    protected void tick(double progress) {
        for (Map.Entry<AlphaWidget, Float> entry : targetAlpha.entrySet()) {
            AlphaWidget widget = entry.getKey();
            Float sourcealpha = sourceAlpha.get(widget);
            if (sourcealpha == null) {
                sourcealpha = 1.0f;
                sourceAlpha.put(widget, sourcealpha);
            }
            Float targetalpha = entry.getValue();
            Float boundary;
            if (progress >= 1.0) {
                boundary = targetalpha;
            } else {
                boundary = sourcealpha + (float) progress * (targetalpha - sourcealpha);
            }
            widget.setAlpha(boundary, 4);
            widget.repaint();
        }
        if (progress >= 1.0) {
            sourceAlpha.clear();
            targetAlpha.clear();
        }
    }
}
