/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics2D;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class AlphaWidget extends Widget {

    public static final int TARGET_WIDGET = 0;
    public static final int TARGET_BACKGROUND = 1;
    public static final int TARGET_BORDER = 2;
    public static final int TARGET_CHILDREN = 3;
    private float walpha, balpha, bkgalpha, calpha = 1.0f;

    public AlphaWidget(Scene scene) {
        super(scene);
    }

    public AlphaWidget(Scene scene, float alpha) {
        super(scene);
        alpha = Math.max(Math.min(1.0f, alpha), 0.0f);
        this.walpha = alpha;
        this.balpha = alpha;
        this.bkgalpha = alpha;
        this.calpha = alpha;
    }

    public AlphaWidget(Scene scene, float w, float b, float bkg, float c) {
        super(scene);
        walpha = Math.max(Math.min(1.0f, w), 0.0f);
        balpha = Math.max(Math.min(1.0f, b), 0.0f);
        bkgalpha = Math.max(Math.min(1.0f, bkg), 0.0f);
        calpha = Math.max(Math.min(1.0f, c), 0.0f);
    }

    public void setAlpha(float alpha, int target) {
        alpha = Math.max(Math.min(1.0f, alpha), 0.0f);
        switch (target) {
            case TARGET_WIDGET:
                walpha = alpha;
                break;
            case TARGET_BACKGROUND:
                bkgalpha = alpha;
                break;
            case TARGET_BORDER:
                balpha = alpha;
                break;
            case TARGET_CHILDREN:
                calpha = alpha;
                break;
            default:
                walpha = alpha;
                balpha = alpha;
                calpha = alpha;
                bkgalpha = alpha;
                break;
        }
    }

    public float getAlpha(int target) {
        switch (target) {
            case TARGET_WIDGET:
                return walpha;
            case TARGET_BACKGROUND:
                return bkgalpha;
            case TARGET_BORDER:
                return balpha;
            case TARGET_CHILDREN:
                return calpha;
            default:
                return 100;
        }
    }

    @Override
    public void paintWidget() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, walpha);
        gr.setComposite(current);
        super.paintWidget();
        gr.setComposite(previous);
    }

    @Override
    public void paintBorder() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, balpha);
        gr.setComposite(current);
        super.paintBorder();
        gr.setComposite(previous);
    }

    @Override
    public void paintBackground() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, bkgalpha);
        gr.setComposite(current);
        super.paintBackground();
        gr.setComposite(previous);
    }

    @Override
    public void paintChildren() {
        Graphics2D gr = getGraphics();
        Composite previous = gr.getComposite();
        Composite current = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, calpha);
        gr.setComposite(current);
        super.paintChildren();
        gr.setComposite(previous);
    }
}

