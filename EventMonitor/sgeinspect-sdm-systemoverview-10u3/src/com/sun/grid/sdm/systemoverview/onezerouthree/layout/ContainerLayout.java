/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.layout;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collection;

import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.widget.Widget;

/**
 * Performs layout of SdmScene.
 */
public final class ContainerLayout implements Layout {

    private int columns;
    private int hgap, vgap;

    public ContainerLayout(int columns, int hgap, int vgap) {
        this.columns = columns;
        this.hgap = hgap;
        this.vgap = vgap;
    }

    public void layout(Widget widget) {
        Collection<Widget> children = widget.getChildren();
        int posX = 0;
        int posY = 0;
        int col = 0;
        int maxWidth = 0;
        int maxHeight = 0;
        for (Widget child : children) {
            Rectangle preferredBounds = child.getPreferredBounds();
            int width = preferredBounds.width;
            int height = preferredBounds.height;
            if (height > maxHeight) {
                maxHeight = height;
            }
            if (width > maxWidth) {
                maxWidth = width;
            }
        }

        for (Widget child : children) {
            Rectangle preferredBounds = child.getPreferredBounds();
            int x = preferredBounds.x;
            int y = preferredBounds.y;
            int width = preferredBounds.width;
            int height = preferredBounds.height;
            int lx = posX - x;
            int ly = posY - y;
            if (child.isVisible()) {
                child.resolveBounds(new Point(lx, ly), new Rectangle(x, y, width, height));
                posX += (maxWidth + hgap);
            } else {
                child.resolveBounds(new Point(lx, ly), new Rectangle(x, y, 0, 0));
            }
            col++;
            if (col == columns) {
                col = 0;
                posX = 0;
                posY += (maxHeight + vgap);
            }
        }

    }

    public boolean requiresJustification(Widget widget) {
        return false;
    }

    public void justify(Widget widget) {
    }
}
