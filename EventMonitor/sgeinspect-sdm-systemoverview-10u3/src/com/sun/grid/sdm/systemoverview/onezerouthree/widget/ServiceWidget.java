/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.widget;

import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.api.service.cloud.CloudServiceDataSource;
import com.sun.grid.sdm.api.service.ge.GEServiceDataSource;
import com.sun.grid.sdm.api.service.spare_pool.SparePoolServiceDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.options.SystemOverviewOptionOptionsPanelController;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.SdmGraphScene;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator.AlphaWidgetAnimatorx2;
import com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator.Imagex2AlphaWidgetAnimator;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.ImageWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;

public class ServiceWidget extends ResourceContainerWidget<ServiceDataSource> {

    private static final Logger log = Logger.getLogger(ServiceWidget.class.getName());
    private final DCL_RDS drl;
    private Widget label;

    public ServiceWidget(SdmGraphScene scene, ServiceDataSource node) {
        super(scene, node);
        drl = new DCL_RDS(this);
        initialize();
    }

    private void initialize() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getDataSource().getRepository().addDataChangeListener(drl, ResourceDataSource.class);
                for (ResourceDataSource rds : getDataSource().getRepository().getDataSources(ResourceDataSource.class)) {
                    ResourceWidget rw = new ResourceWidget(getSdmScene(), rds);
                    getContainerWidget().addChild(rw);
//                    rw.addToContainer(ServiceWidget.this);
                }
            }
        });

    }

    private static class DCL_RDS implements DataChangeListener<ResourceDataSource> {

        ServiceWidget sw;

        private DCL_RDS(ServiceWidget sw) {
            this.sw = sw;
        }

        public void dataChanged(final DataChangeEvent<ResourceDataSource> arg0) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    for (ResourceDataSource remd : arg0.getRemoved()) {
                        List<ResourceWidget> toRemove = new LinkedList<ResourceWidget>();
                        for (Widget w : sw.getContainerWidget().getChildren()) {
                            if (((ResourceWidget) w).getDataSource().equals(remd)) {
                                toRemove.add((ResourceWidget) w);
                            }
                        }
                        for (ResourceWidget w : toRemove) {
                            w.removeFromContainer(sw);
                        }
                    }
                    List<ResourceDataSource> childRDSs = new ArrayList<ResourceDataSource>(sw.getContainerWidget().getChildren().size());
                    for (Widget w : sw.getContainerWidget().getChildren()) {
                        childRDSs.add(((ResourceWidget) w).getDataSource());
                    }
                    List<ResourceWidget> toAdd = new LinkedList<ResourceWidget>();
                    for (ResourceDataSource addd : arg0.getAdded()) {
                        if (!childRDSs.contains(addd)) {
                            ResourceWidget rw = new ResourceWidget(sw.getSdmScene(), addd);
                            toAdd.add(rw);
                        }
                    }
                    for (ResourceWidget w : toAdd) {
                        w.addToContainer(sw);
                    }
                }
            });
        }
    }

    protected synchronized Widget getLabelWidget() {
        if (label == null) {
            label = new LW(getSdmScene(), getDataSource());
        }
        return label;
    }

    private static class LW extends AlphaWidget {

        private final ServiceDataSource ds;
        private final LabelWidget name;
        private final ImageWidget icon, infoimage;
        private final Imagex2AlphaWidget status;
        private final AlphaWidget info;
        private final LabelWidget anno;
        private static final Image SS_REQUEST = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_service_request_1.png");
        private static final Image SS_UNKNOWN = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_unknown.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image SS_STARTING = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_starting.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image SS_RUNNING = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_running.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image SS_SHUTDOWN = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_shutdown.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image SS_STOPPED = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_stopped.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image SS_ERROR = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/ss_error.png").getScaledInstance(24, 24, Image.SCALE_SMOOTH);
        private static final Image DEMO_CLOUD = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_cloud_demo.png");
        private static final Image DEMO_GE = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_ge_service_demo.png");
        private static final Image DEMO_SPARE_POOL = ImageUtilities.loadImage("com/sun/grid/sdm/systemoverview/onezerouthree/resources/icon_spare_pool_demo.png");

        public LW(SdmGraphScene s, ServiceDataSource ds) {
            super(s, 1.0f);
            NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).addPreferenceChangeListener(new SDMGSL());
            this.ds = ds;
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 4));
            icon = new ImageWidget(getScene());
            status = new Imagex2AlphaWidget(getScene(), SS_UNKNOWN, SS_UNKNOWN, 1.0f);
            info = new AlphaWidget(getScene(), 0.0f);
            infoimage = new ImageWidget(s, SS_REQUEST);
            info.addChild(infoimage);
            info.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 0));
            name = new LabelWidget(s);
            anno = new LabelWidget(s);
            addChild(icon);
            addChild(name);
            addChild(status);
            addChild(info);
            addChild(anno);
            name.setVisible(NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.SHOW_SERVICE_NAMES, false));

            initialize();
        }

        private void setupLabelNameAndIcon(boolean demo) {
            if (demo) {
                if (ds instanceof SparePoolServiceDataSource) {
                    icon.setImage(DEMO_SPARE_POOL);
                } else if (ds instanceof CloudServiceDataSource) {
                    icon.setImage(DEMO_CLOUD);
                } else if (ds instanceof GEServiceDataSource) {
                    icon.setImage(DEMO_GE);
                }
                @SuppressWarnings("unchecked")
                DataSourceDescriptor<? extends ServiceDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
                if (dsd != null) {
                    icon.setToolTipText(dsd.getName());
                    name.setLabel(dsd.getName() + " [ " + dsd.getDescription() + " ]");
                }
                infoimage.setImage(SS_REQUEST);
            } else {
                @SuppressWarnings("unchecked")
                DataSourceDescriptor<? extends ServiceDataSource> dsd = DataSourceDescriptorFactory.getDescriptor(ds);
                if (dsd != null) {
                    icon.setImage(dsd.getIcon());
                    icon.setToolTipText(dsd.getName());
                    name.setLabel(dsd.getName() + " [ " + dsd.getDescription() + " ]");
                }
                infoimage.setImage(SS_REQUEST.getScaledInstance(20, 20, Image.SCALE_SMOOTH));
            }
        }

        private void initialize() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {

                    setupLabelNameAndIcon(NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.DEMO_MODE, false));

                    try {
                        swapServiceStateIcon(ds.getServiceState(), NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.DEMO_MODE, false));
                    } catch (Exception e) {
                        Exceptions.printStackTrace(e);
                    }
                    ds.addPropertyChangeListener(new PropertyChangeListener() {

                        public void propertyChange(final PropertyChangeEvent evt) {
                            SwingUtilities.invokeLater(new Runnable() {

                                public void run() {
                                    if (evt.getPropertyName().equals(ServiceDataSource.SERVICE_STATUS_PROPERTY)) {
                                        swapServiceStateIcon((String) evt.getNewValue(), NbPreferences.forModule(SystemOverviewOptionOptionsPanelController.class).getBoolean(SystemOverviewOptionOptionsPanelController.SHOW_SERVICE_NAMES, false));
                                    } else if (evt.getPropertyName().equals(ServiceDataSource.SERVICE_REQUEST_PROPERTY)) {
//                                        ResourceRequestEvent rre = (ResourceRequestEvent) evt.getNewValue();
//                                        if (rre.getSLOName().equals("fixed_usage")) {
//                                            // just sge workshop workaround, we do not want to see
//                                            // empty request coming from fixed_usage each 30 secs.
//                                            return;
//                                        }
//                                        if (rre.getNeeds().isEmpty()) {
                                            blinkServiceRequestIcon(null, 0.25f);
//                                        } else {
//                                            blinkServiceRequestIcon(rre.getSLOName() + " -> " + rre.getNeeds().toString(), 0.25f);
//                                        }
                                    } else if (evt.getPropertyName().equals(ServiceDataSource.SERVICE_SLOSTATES_PROPERTY)) {
//                                        slocomponent.updateModel((List<SLOState>) evt.getNewValue());
                                    }
                                }
                            });

                        }
                    });
                }
            });
        }

        private void swapServiceStateIcon(String serviceStatus, boolean demo) {
            Imagex2AlphaWidgetAnimator animator = new Imagex2AlphaWidgetAnimator(getScene().getSceneAnimator());
            if (demo) {
                if ("RUNNING".equals(serviceStatus)) {
                    animator.setImageB(status, SS_RUNNING);
                } else if ("STARTING".equals(serviceStatus)) {
                    animator.setImageB(status, SS_STARTING);
                } else if ("STOPPED".equals(serviceStatus)) {
                    animator.setImageB(status, SS_STOPPED);
                } else if ("ERROR".equals(serviceStatus)) {
                    animator.setImageB(status, SS_ERROR);
                } else if ("SHUTDOWN".equals(serviceStatus)) {
                    animator.setImageB(status, SS_SHUTDOWN);
                } else {
                    animator.setImageB(status, SS_UNKNOWN);
                }
            } else {
                if ("RUNNING".equals(serviceStatus)) {
                    animator.setImageB(status, SS_RUNNING.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                } else if ("STARTING".equals(serviceStatus)) {
                    animator.setImageB(status, SS_STARTING.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                } else if ("STOPPED".equals(serviceStatus)) {
                    animator.setImageB(status, SS_STOPPED.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                } else if ("ERROR".equals(serviceStatus)) {
                    animator.setImageB(status, SS_ERROR.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                } else if ("SHUTDOWN".equals(serviceStatus)) {
                    animator.setImageB(status, SS_SHUTDOWN.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                } else {
                    animator.setImageB(status, SS_UNKNOWN.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                }
            }
        }

        private void blinkServiceRequestIcon(String tooltip, float targetAlpha) {
            AlphaWidgetAnimatorx2 animator = new AlphaWidgetAnimatorx2(getScene().getSceneAnimator());
            info.setToolTipText(tooltip);
            animator.setAlpha(info, 1.0f, targetAlpha);
        }

        private class SDMGSL implements PreferenceChangeListener {

            public void preferenceChange(PreferenceChangeEvent evt) {
                if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.SHOW_SERVICE_NAMES)) {
                    name.setVisible(Boolean.parseBoolean(evt.getNewValue()));
                    name.repaint();
                    getScene().validate();
                } else if (evt.getKey().equals(SystemOverviewOptionOptionsPanelController.DEMO_MODE)) {
                    setupLabelNameAndIcon(Boolean.parseBoolean(evt.getNewValue()));
                    swapServiceStateIcon(ds.getServiceState(), Boolean.parseBoolean(evt.getNewValue()));
                    getScene().validate();
                }
            }
        }
    }
}

