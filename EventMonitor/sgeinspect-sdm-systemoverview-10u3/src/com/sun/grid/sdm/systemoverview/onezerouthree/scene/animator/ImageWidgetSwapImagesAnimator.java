/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator;

import java.awt.Dimension;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.animator.Animator;
import org.netbeans.api.visual.animator.SceneAnimator;
import org.netbeans.api.visual.widget.ImageWidget;

public class ImageWidgetSwapImagesAnimator extends Animator {

    private HashMap<ImageWidget, Image> sourceImage = new HashMap<ImageWidget, Image>();
    private HashMap<ImageWidget, Image> targetImage = new HashMap<ImageWidget, Image>();

    public ImageWidgetSwapImagesAnimator(SceneAnimator sceneAnimator) {
        super(sceneAnimator);
    }

    public void setNewImage(ImageWidget widget, Image newImage) {
        assert widget != null;
        assert newImage != null;
        sourceImage.clear();
        sourceImage.put(widget, widget.getImage());
        targetImage.put(widget, newImage);
        start();
    }

    public void setNewImage(ImageWidget widget, Image originalImage, Image newImage) {
        assert widget != null;
        assert newImage != null;
        sourceImage.clear();
        sourceImage.put(widget, originalImage);
        targetImage.put(widget, newImage);
        start();
    }

    @Override
    protected void tick(double progress) {
        if (progress < 0.5) {
            for (Map.Entry<ImageWidget, Image> entry : sourceImage.entrySet()) {
                ImageWidget widget = entry.getKey();
                Dimension sourceDimension = new Dimension(widget.getImage().getWidth(null), widget.getImage().getHeight(null));
                Dimension boundary = new Dimension(
                        (int) (Math.max(sourceDimension.width - 2 * progress * sourceDimension.width, 1)),
                        (int) (Math.max(sourceDimension.height - 2 * progress * sourceDimension.width, 1)));
                Image img = sourceImage.get(widget);
                if (img == null) {
                    img = widget.getImage();
                }
                widget.setImage(img.getScaledInstance(boundary.width, boundary.height, Image.SCALE_SMOOTH));
                getScene().validate();
            }
        } else if (progress < 1.0) {
            for (Map.Entry<ImageWidget, Image> entry : targetImage.entrySet()) {
                ImageWidget widget = entry.getKey();
                Image img = targetImage.get(widget);
                if (img == null) {
                    img = widget.getImage();
                }
                Dimension targetDimension = new Dimension(img.getWidth(null), img.getHeight(null));
                Dimension boundary = new Dimension(
                        (int) (Math.max(2 * (progress - 0.5) * targetDimension.width, 1)),
                        (int) (Math.max(2 * (progress - 0.5) * targetDimension.height, 1)));
                widget.setImage(img.getScaledInstance(boundary.width, boundary.height, Image.SCALE_SMOOTH));
                getScene().validate();
            }
        } else {
            for (Map.Entry<ImageWidget, Image> entry : targetImage.entrySet()) {
                ImageWidget widget = entry.getKey();
                Image img = targetImage.get(widget);
                if (img == null) {
                    img = widget.getImage();
                }
                Dimension targetDimension = new Dimension(img.getWidth(null), img.getHeight(null));
                Dimension boundary = new Dimension(targetDimension.width, targetDimension.height);
                widget.setImage(img.getScaledInstance(boundary.width, boundary.height, Image.SCALE_SMOOTH));
                getScene().validate();
            }
            sourceImage.clear();
            targetImage.clear();
        }
    }
}
