/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.systemoverview.onezerouthree.edge.Edge;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.EdgeWidget;
import com.sun.grid.sdm.systemoverview.onezerouthree.layout.ContainerSceneLayout;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.DataSourceWidget;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.ResourceProviderWidget;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.ResourceWidget;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.ServiceWidget;
import com.sun.grid.sdm.systemoverview.onezerouthree.widget.SystemWidget;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.awt.SystemColor;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.graph.layout.GraphLayout;
import org.netbeans.api.visual.graph.layout.GraphLayoutFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;

public class SdmGraphScene extends GraphScene<DataSource, Edge> {

    public static final String PROP_SO_SHOW_SERVICE_NAMES = "soShowServiceNames";
    public static final String PROP_SO_SHOW_RESOURCE_NAMES = "soShowResourceNames";
    private JvmDataSource system;
    private SystemWidget systemWidget;
    private LayerWidget componentsLayer;
    private LayerWidget servicesLayer;
    private LayerWidget resourcesLayer;
    private LayerWidget cachedResourcesLayer;
    private LayerWidget connectionLayer;
    private LayerWidget interactionLayer;
    private WidgetAction moveAction;
    private final DCL_SDS dcl_sds = new DCL_SDS();
    private final DCL_RPDS dcl_rpds = new DCL_RPDS();
    private final DRL drl = new DRL();
    private SceneLayout sceneLayout;
    private final GraphLayout<DataSource, Edge> graphLayout = GraphLayoutFactory.createTreeGraphLayout(100, 100, 50, 50, false);
    private final GraphLayout<DataSource, Edge> sdmLayout = new ContainerSceneLayout(5, 5);

    public SdmGraphScene(JvmDataSource jvm) {
//        myLayout = SdmSceneSupportedLayoutType.SDM_LAYOUT;
        this.setBackground(SystemColor.window);
        this.system = jvm;

        cachedResourcesLayer = new LayerWidget(this);
        componentsLayer = new LayerWidget(this);
        servicesLayer = new LayerWidget(this);
        resourcesLayer = new LayerWidget(this);

        connectionLayer = new LayerWidget(this);
        interactionLayer = new LayerWidget(this);

        addChild(cachedResourcesLayer);
        addChild(componentsLayer);
        addChild(servicesLayer);
        addChild(resourcesLayer);

        addChild(connectionLayer);
        addChild(interactionLayer);

//        moveAction = ActionFactory.createAlignWithMoveAction(componentsLayer, interactionLayer, null);

        getActions().addAction(ActionFactory.createZoomAction(1.2, false));
        getActions().addAction(ActionFactory.createPanAction());

        sceneLayout = LayoutFactory.createSceneGraphLayout(this, sdmLayout);

        system.getRepository().addDataChangeListener(dcl_sds, ServiceDataSource.class);
//        system.getRepository().addDataChangeListener(dcl_rpds, ResourceProviderDataSource.class);
//        system.getRepository().addDataChangeListener(dcl_rds, ResourceDataSource.class);
    }

    public void initialize() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                systemWidget = (SystemWidget) addNode(system);
//                fillRPs();
                fillServices();
            }
        });
    }

    public DataSource getSystem() {
        return this.system;
    }

    public void performLayout() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                sceneLayout.invokeLayout();
                validate();
            }
        });
    }

    public void setCachedLayerVisible(boolean visible) {
        findWidget(cachedResourcesLayer).setVisible(visible);
    }

    public void setNonCachedLayerVisible(boolean visible) {
        findWidget(cachedResourcesLayer).setVisible(visible);
    }

    @Override
    protected Widget attachNodeWidget(DataSource node) {
        Widget widget = null;
        if (node instanceof ServiceDataSource) {
            widget = new ServiceWidget(this, (ServiceDataSource) node);
            servicesLayer.addChild(widget);
//            WidgetAction menuAction = ActionFactory.createPopupMenuAction(new _cls1());
//            widget.getActions().addAction(menuAction);
        } else if (node instanceof ResourceProviderDataSource) {
            widget = new ResourceProviderWidget(this, (ResourceProviderDataSource) node);
            componentsLayer.addChild(widget);
//            WidgetAction menuAction = ActionFactory.createPopupMenuAction(new _cls2());
//            widget.getActions().addAction(menuAction);
        } else if (node instanceof ResourceDataSource) {
            widget = new ResourceWidget(this, (ResourceDataSource) node);
            resourcesLayer.addChild(widget);
//            WidgetAction menuAction = ActionFactory.createPopupMenuAction(new _cls3());
//            widget.getActions().addAction(menuAction);
        } else if (node instanceof JvmDataSource) {
            widget = new SystemWidget(this, (JvmDataSource) node);
            cachedResourcesLayer.addChild(widget);
        }
        if (widget != null) {
            //single-click, the event is not consumed:
            widget.getActions().addAction(createSelectAction());
            //mouse-dragged, the event is consumed while mouse is dragged:
//            widget.getActions().addAction(moveAction);
            //mouse-over, the event is consumed while the mouse is over the widget:
            widget.getActions().addAction(createWidgetHoverAction());
        }
        validate();
        performLayout();
        return widget;
    }

    @Override
    protected void detachNodeWidget(DataSource node, Widget widget) {
        if (widget != null) {
            widget.removeFromParent();
        }
        validate();
        performLayout();
    }

    @Override
    protected void detachEdgeWidget(Edge e, Widget widget) {
        if (widget != null) {
            widget.removeFromParent();
        }
        validate();
        performLayout();
    }

    @Override
    protected Widget attachEdgeWidget(Edge edge) {
        EdgeWidget connection = new EdgeWidget(this, edge);
        connectionLayer.addChild(connection);
        validate();
        performLayout();
        return connection;
    }

    @Override
    protected void attachEdgeSourceAnchor(Edge edge, DataSource old, DataSource source) {
        Widget w = source != null ? findWidget(source) : null;
        ((ConnectionWidget) findWidget(edge)).setSourceAnchor(AnchorFactory.createRectangularAnchor(w));
        validate();
    }

    @Override
    protected void attachEdgeTargetAnchor(Edge edge, DataSource old, DataSource target) {
        Widget w = target != null ? findWidget(target) : null;
        ((ConnectionWidget) findWidget(edge)).setTargetAnchor(AnchorFactory.createRectangularAnchor(w));
        validate();
    }

    private void fillServices() {
        for (ServiceDataSource ds : system.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
//            ds.notifyWhenRemoved(drl);
            Collection<DataSource> nodez = getNodes();
            if (!nodez.contains(ds)) {
                addNode(ds);
            }
        }
        for (JvmDataSource ds : system.getRepository().getDataSources(JvmDataSource.class)) {
            for (ServiceDataSource sds : ds.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
//            ds.notifyWhenRemoved(drl);
                Collection<DataSource> nodez = getNodes();
                if (!nodez.contains(sds)) {
                    addNode(sds);
                }
            }
        }
    }

    private void fillRPs() {
        for (ResourceProviderDataSource ds : system.getApplication().getRepository().getDataSources(ResourceProviderDataSource.class)) {
//            ds.notifyWhenRemoved(drl);
            if (!getNodes().contains(ds)) {
                addNode(ds);
            }
        }
        for (JvmDataSource ds : system.getRepository().getDataSources(JvmDataSource.class)) {
            for (ResourceProviderDataSource rpds : ds.getApplication().getRepository().getDataSources(ResourceProviderDataSource.class)) {
//            ds.notifyWhenRemoved(drl);
                if (!getNodes().contains(rpds)) {
                    addNode(rpds);
                }
            }
        }
    }

    private final class DRL implements DataRemovedListener<Object> {

        public void dataRemoved(Object arg0) {
            // data source is removed
        }
    }

    private class DCL_RDS implements DataChangeListener<ResourceDataSource> {

        public void dataChanged(DataChangeEvent<ResourceDataSource> arg0) {
            // resource data source is added, removed etc.
        }
    }

    private class DCL_SDS implements DataChangeListener<ServiceDataSource> {

        public void dataChanged(DataChangeEvent<ServiceDataSource> arg0) {
            for (ServiceDataSource ds : arg0.getAdded()) {
                Collection<DataSource> nodez = getNodes();
                if (!nodez.contains(ds)) {
                    addNode(ds);
                    performLayout();
                }
            }
            for (ServiceDataSource ds : arg0.getRemoved()) {
                Collection<DataSource> nodez = getNodes();
                if (nodez.contains(ds)) {
                    removeNode(ds);
                    performLayout();
                }
            }
        }
    }

    private class DCL_RPDS implements DataChangeListener<ResourceProviderDataSource> {

        public void dataChanged(final DataChangeEvent<ResourceProviderDataSource> arg0) {
            // rp data source is added, removed ..
        }
    }

    /***
     * Exposed for layout purposes
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<DataSourceWidget<ServiceDataSource>> getServiceWidgets() {
        List<Widget> tmp = new LinkedList<Widget>(servicesLayer.getChildren());
        List<DataSourceWidget<ServiceDataSource>> ret = new LinkedList<DataSourceWidget<ServiceDataSource>>();
        for (Widget w : tmp) {
            ret.add((DataSourceWidget<ServiceDataSource>) w);
        }
        return ret;
    }

    /***
     * Exposed for layout purposes
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<DataSourceWidget<ComponentDataSource>> getComponentWidgets() {
        List<Widget> tmp = new LinkedList<Widget>(componentsLayer.getChildren());
        List<DataSourceWidget<ComponentDataSource>> ret = new LinkedList<DataSourceWidget<ComponentDataSource>>();
        for (Widget w : tmp) {
            ret.add((DataSourceWidget<ComponentDataSource>) w);
        }
        return ret;
    }

    /***
     * Exposed for layout purposes
     * @return
     */
    public SystemWidget getSystemWidget() {
        return systemWidget;
    }
}
