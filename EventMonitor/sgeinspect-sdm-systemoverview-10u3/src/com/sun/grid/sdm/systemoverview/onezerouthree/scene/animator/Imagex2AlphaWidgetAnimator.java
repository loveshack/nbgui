/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.systemoverview.onezerouthree.scene.animator;

import com.sun.grid.sdm.systemoverview.onezerouthree.widget.Imagex2AlphaWidget;
import java.awt.Image;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.api.visual.animator.Animator;
import org.netbeans.api.visual.animator.SceneAnimator;

public class Imagex2AlphaWidgetAnimator extends Animator {

    private List<Imagex2AlphaWidget> targets = new LinkedList<Imagex2AlphaWidget>();

    public Imagex2AlphaWidgetAnimator(SceneAnimator sceneAnimator) {
        super(sceneAnimator);
    }

    public void setImageB(Imagex2AlphaWidget widget, Image image) {
        assert widget != null;
        assert image != null;
        widget.setImageB(image);
        targets.add(widget);
        start();
    }

    @Override
    protected void tick(double progress) {
        for (Imagex2AlphaWidget widget : targets) {
            Float boundary;
            if (progress >= 1.0) {
                boundary = 1.0f;
                widget.setImageA(widget.getImageB());
            } else {
                boundary = (float) progress;
            }
            widget.setAlpha(boundary, 4);
            widget.repaint();
        }
        if (progress >= 1.0) {
            targets.clear();
        }
    }
}
