/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.conf.sixtwoufive.pe;

import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.config.users.UserSet;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;

public class PEWizardPanel3 implements WizardDescriptor.Panel<WizardDescriptor> {

    private Cluster cluster;
//    JList allowedList = ((PEVisualPanel3) getComponent()).getAllowedList();
//    JList disallowedList = ((PEVisualPanel3) getComponent()).getDisallowedList();
    DefaultListModel allowedModel, disallowedModel, availableModel;
    List<String> dictionary = new ArrayList<String>();
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private PEVisualPanel3 component;

    public PEWizardPanel3(Cluster cluster) {
        this.cluster = cluster;
        populateList();
        component.setButtonState();
        component.setTextDocuments();
        component.txtAvailable.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                change();
            }

            public void removeUpdate(DocumentEvent e) {
                change();
            }

            public void changedUpdate(DocumentEvent e) {
                change();
            }
        });
        component.txtAllowed.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                changeAllowed();
            }

            public void removeUpdate(DocumentEvent e) {
                changeAllowed();
            }

            public void changedUpdate(DocumentEvent e) {
                changeAllowed();
            }
        });
        component.txtDisallowed.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                changeDisallowed();
            }

            public void removeUpdate(DocumentEvent e) {
                changeDisallowed();
            }

            public void changedUpdate(DocumentEvent e) {
                changeDisallowed();
            }
        });
    }

    public void change() {
        component.getAvailableList().setSelectedValue(component.txtAvailable.getText(), true);
    }

    public void changeAllowed() {
        component.getAllowedList().setSelectedValue(component.txtAllowed.getText(), true);
    }

    public void changeDisallowed() {
        component.getDisallowedList().setSelectedValue(component.txtDisallowed.getText(), true);
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new PEVisualPanel3();

        }
        return component;
    }

    private void populateList() {
        if (cluster != null) {
            List<UserSet> userList = cluster.getUserList();
            availableModel = ((PEVisualPanel3) getComponent()).getAvailableModel();
            for (UserSet user : userList) {
                availableModel.addElement(user.getName());
            }

        }
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
        // If it depends on some condition (form filled out...), then:
        // return someCondition();
        // and when this condition changes (last form field filled in...) then:
        // fireChangeEvent();
        // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
    /*
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    public final void addChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.add(l);
    }
    }
    public final void removeChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.remove(l);
    }
    }
    protected final void fireChangeEvent() {
    Iterator<ChangeListener> it;
    synchronized (listeners) {
    it = new HashSet<ChangeListener>(listeners).iterator();
    }
    ChangeEvent ev = new ChangeEvent(this);
    while (it.hasNext()) {
    it.next().stateChanged(ev);
    }
    }
     */

    // You can use a settings object to keep track of state. Normally the
    // settings object will be the WizardDescriptor, so you can use
    // WizardDescriptor.getProperty & putProperty to store information entered
    // by the user.
    public void readSettings(WizardDescriptor wizardDescriptor) {

        allowedModel = new DefaultListModel();
        allowedModel = (DefaultListModel) wizardDescriptor.getProperty(PEWizardSettings.PROP_USER_LIST);
        if (allowedModel != null) {
            component.allowedModel = allowedModel;
        }

        disallowedModel = new DefaultListModel();
        disallowedModel = (DefaultListModel) wizardDescriptor.getProperty(PEWizardSettings.PROP_XUSER_LIST);
        if (disallowedModel != null) {
            component.disallowedModel = disallowedModel;
        }
    }

    public void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty(PEWizardSettings.PROP_USER_LIST, component.allowedModel);
        wizardDescriptor.putProperty(PEWizardSettings.PROP_XUSER_LIST, component.disallowedModel);
    }
}

