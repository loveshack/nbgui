/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.conf.sixtwoufive.pe;

import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import java.awt.Component;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class PEWizardPanel1 implements WizardDescriptor.FinishablePanel<WizardDescriptor> {
    private Cluster cluster;
    JTextField nameField = ((PEVisualPanel1) getComponent()).getPENameField();
    JTextField locationField = ((PEVisualPanel1) getComponent()).getPELocationField();
    private boolean isValid = false;
    private String nameError = "";
    WizardDescriptor descriptor;

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private PEVisualPanel1 component;

    public PEWizardPanel1(Cluster cluster) {
       this.cluster = cluster;
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new PEVisualPanel1();
            component.getPENameField().getDocument().addDocumentListener(new DocumentListener() {

                public void insertUpdate(DocumentEvent e) {
                    change();
                }

                public void removeUpdate(DocumentEvent e) {
                    change();
                }

                public void changedUpdate(DocumentEvent e) {
                    change();
                }
            });
        }
        return component;
    }

   private void change() {
        validateAll();
    }

     private void setValid(boolean val) {
        if (isValid != val) {
            isValid = val;
            fireChangeEvent();
        }
    }

    private void validateAll() {
        boolean valid = true;
        String name = nameField.getText().trim();

        if (name == null || name.length() <= 0) {
            valid = false;
            nameError = NbBundle.getMessage(PEWizardPanel1.class, "Null_Name");
        } else if (cluster.isPENameExists(name)){
            valid = false;
            nameError = NbBundle.getMessage(PEWizardPanel1.class, "Name_Exists");
        } else if (name.contains(" ")) {
            valid = false;
            nameError = NbBundle.getMessage(PEWizardPanel1.class, "Name_Space");
        }

        if (!valid) {
            descriptor.putProperty(WizardDescriptor.PROP_ERROR_MESSAGE,
                  "<html>" + nameError + "</html>");
        } else {
            descriptor.putProperty(WizardDescriptor.PROP_ERROR_MESSAGE, "");
        }
        setValid(valid);

    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {    
        return isValid;
    }

    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0

    public final void addChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }
    public final void removeChangeListener(ChangeListener l) {
        synchronized (listeners) {
        listeners.remove(l);
        }
    }

    protected final void fireChangeEvent() {
        Iterator<ChangeListener> it;
        synchronized (listeners) {
            it = new HashSet<ChangeListener>(listeners).iterator();
        }
        ChangeEvent ev = new ChangeEvent(this);
            while (it.hasNext()) {
            it.next().stateChanged(ev);
        }
    }
     

    // You can use a settings object to keep track of state. Normally the
    // settings object will be the WizardDescriptor, so you can use
    // WizardDescriptor.getProperty & putProperty to store information entered
    // by the user.
    public void readSettings(WizardDescriptor settings) {
        descriptor = settings;
        nameField.setText(NbPreferences.forModule(PEWizardPanel1.class).get(PEWizardSettings.PROP_NAME, ""));
        locationField.setText(NbPreferences.forModule(PEWizardPanel1.class).get(PEWizardSettings.PROP_LOCATION, ""));
    }

    public void storeSettings(WizardDescriptor settings) {
        NbPreferences.forModule(PEWizardPanel1.class).put(PEWizardSettings.PROP_NAME, nameField.getText().trim());
        NbPreferences.forModule(PEWizardPanel1.class).put(PEWizardSettings.PROP_LOCATION, locationField.getText());
    }

    public boolean isFinishPanel() {
        String name = nameField.getText();
        if (cluster.isPENameExists(name)){
            return false;
        }
        return true;
    }
}

