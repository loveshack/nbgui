/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.conf.sixtwoufive.pe.actions;

import com.sun.grid.jgdi.configuration.ParallelEnvironment;
import com.sun.grid.jgdi.configuration.ParallelEnvironmentImpl;
import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.jgdi.configuration.UserSet;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.conf.sixtwoufive.pe.PEWizardPanel1;
import com.sun.grid.sge.conf.sixtwoufive.pe.PEWizardPanel2;
import com.sun.grid.sge.conf.sixtwoufive.pe.PEWizardPanel3;
import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.sge.sixtwoufive.monitor.cluster.impl.ClusterImpl;
import com.sun.grid.sge.ui.resources.ResourceHelper;
import com.sun.grid.shared.api.layer.OpenSingleDataSourceAction;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

// An example action demonstrating how the wizard could be called from within
// your code. You can copy-paste the code below wherever you need.
public final class PEWizardAction extends OpenSingleDataSourceAction<ClusterImpl> {

    private WizardDescriptor.Panel[] panels;
    private static final String SMALL_IMAGE_PATH = "com/sun/grid/sge/ui/resources/parallel_environment_16.png";  // NOI18N
    private static final Image SMALL_IMAGE = ResourceHelper.createBadgedIcon(SMALL_IMAGE_PATH);  // NOI18N
    //private static final Image LARGE_IMAGE = ResourceHelper.createBadgedIcon("com/sun/grid/sge/ui/resources/parallel_environment_24.png");  // NOI18N
    private final PropertyChangeListener propertyListener = new ClusterPropertyListener();
    private static PEWizardAction INSTANCE;
    private Cluster cluster;
    private WizardDescriptor wizardDescriptor;

    public void actionPerformed(final ClusterImpl cluster, ActionEvent actionEvent) {
        this.cluster = cluster;
        wizardDescriptor = new WizardDescriptor(getPanels());
        // {0} will be replaced by WizardDesriptor.Panel.getComponent().getName()
        wizardDescriptor.setTitleFormat(new MessageFormat("{0}"));
        wizardDescriptor.setTitle(NbBundle.getMessage(PEWizardAction.class, "Dialog_PE"));
        NbPreferences.forModule(PEWizardPanel1.class).put(PEWizardSettings.PROP_NAME, "");
        NbPreferences.forModule(PEWizardPanel1.class).put(PEWizardSettings.PROP_LOCATION, cluster.getDisplayName());

        Dialog dialog = DialogDisplayer.getDefault().createDialog(wizardDescriptor);
        dialog.setVisible(true);
        dialog.toFront();
        boolean cancelled = wizardDescriptor.getValue() != WizardDescriptor.FINISH_OPTION;

        if (!cancelled) {
            RequestProcessor.getDefault().post(new Runnable() {

                public void run() {
                    try {
                        cluster.createPE(createPE());
                        StatusDisplayer.getDefault().setStatusText(
                              NbBundle.getMessage(PEWizardAction.class, "Creating_PE"));
                    } catch (PEException pex) {
                        ErrorDisplayer.submitWarning(pex.getMessage());
                    }
                }
            });


        }
    }

    private ParallelEnvironment createPE() {
        ParallelEnvironment pe = new ParallelEnvironmentImpl();
        pe.setName(NbPreferences.forModule(PEWizardPanel1.class).get(PEWizardSettings.PROP_NAME, ""));
        pe.setSlots(NbPreferences.forModule(PEWizardPanel2.class).getInt(PEWizardSettings.PROP_SLOTS, 999));
        pe.setUrgencySlots(NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_URGENCY, "min"));
        pe.setAllocationRule(NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_ALLOCATION, "$pe_slots"));
        String startCmd = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_START_CMD, "NONE");
        String startArgs = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_START_AREA, "");
        if (startCmd.equalsIgnoreCase(PEWizardSettings.CMD_NONE) || startCmd.equals("")) {
            pe.setStartProcArgs(null);
        } else {
            pe.setStartProcArgs((startCmd + " " + startArgs).trim());
        }
        String stopCmd = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_STOP_CMD, "NONE");
        String stopArgs = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_STOP_AREA, "");
        if (stopCmd.equalsIgnoreCase(PEWizardSettings.CMD_NONE) || startCmd.equals("")) {
            pe.setStopProcArgs(null);
        } else {
            pe.setStopProcArgs((stopCmd + " " + stopArgs).trim());
        }
        pe.setAccountingSummary(NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_ACCOUNTING, false));
        pe.setJobIsFirstTask(NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_IS_TASK, true));
        pe.setControlSlaves(NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_SLAVES, false));

        //Allowed Users
        DefaultListModel allowed = (DefaultListModel) wizardDescriptor.getProperty(PEWizardSettings.PROP_USER_LIST);
        pe.removeAllUser();

        List<UserSet> allowedSet = getUserSets(allowed);
        for (UserSet user : allowedSet) {
            pe.addUser(user);
        }

        //Disallowed Users
        DefaultListModel disallowed = (DefaultListModel) wizardDescriptor.getProperty(PEWizardSettings.PROP_XUSER_LIST);
        pe.removeAllXuser();

        List<UserSet> disallowedSet = getUserSets(disallowed);
        for (UserSet user : disallowedSet) {
            pe.addXuser(user);
        }

        return pe;
    }

    private List<UserSet> getUserSets(DefaultListModel model) {
        List<UserSet> users = new LinkedList<UserSet>();
        if (model != null) {
            Object[] names = model.toArray();

            for (Object name : names) {
                users.add((UserSet) cluster.getUserSet(name.toString()));
            }
        }

        return users;
    }

    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     */
    private WizardDescriptor.Panel[] getPanels() {
        if (panels == null) {
            panels = new WizardDescriptor.Panel[]{
                      new PEWizardPanel1(cluster),
                      new PEWizardPanel2(),
                      new PEWizardPanel3(cluster)
                  };
            String[] steps = new String[panels.length];
            for (int i = 0; i < panels.length; i++) {
                Component c = panels[i].getComponent();
                // Default step name to component name of panel. Mainly useful
                // for getting the name of the target chooser to appear in the
                // list of steps.
                steps[i] = c.getName();
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    // Sets step number of a component
                    // TODO if using org.openide.dialogs >= 7.8, can use WizardDescriptor.PROP_*:
                    jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                    // Sets steps names for a panel
                    jc.putClientProperty("WizardPanel_contentData", steps);
                    // Turn on subtitle creation on each step
                    jc.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
                    // Show steps on the left side with the image on the background
                    jc.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
                    // Turn on numbering of all steps
                    jc.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
                }
            }
        }
        return panels;
    }

    public String getName() {
        return NbBundle.getMessage(PEWizardAction.class, "LBL_New_PE");
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean isEnabled(ClusterImpl cluster) {
        cluster.addPropertyChangeListener(propertyListener);
        return cluster.isConnected();
    }

    private class ClusterPropertyListener implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            INSTANCE.setEnabled(((Cluster) evt.getSource()).isConnected());
        }
    }

    public static synchronized PEWizardAction createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PEWizardAction();
        }
        return INSTANCE;
    }

    private PEWizardAction() {
        super(ClusterImpl.class);
        putValue(NAME, NbBundle.getMessage(PEWizardAction.class, "LBL_New_PE"));  // NOI18N
        putValue(SHORT_DESCRIPTION, NbBundle.getMessage(PEWizardAction.class, "ToolTip_New_PE")); // NOI18N
        putValue(SMALL_ICON, new ImageIcon(SMALL_IMAGE));

        // Customize Toolbar dialog needs it
        putValue("iconBase", ""); // NOI18N
    }
}
