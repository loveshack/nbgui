/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.conf.sixtwoufive.pe;

import com.sun.grid.sge.api.config.pe.PEWizardSettings;
import com.sun.grid.sge.api.config.pe.PEValidator;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class PEWizardPanel2 implements WizardDescriptor.FinishablePanel<WizardDescriptor>, ActionListener {

    JTextField slotsField = ((PEVisualPanel2) getComponent()).getTxtSlots();
    JComboBox urgencyCombo = ((PEVisualPanel2) getComponent()).getCbUrgency();
    JComboBox allocationCombo = ((PEVisualPanel2) getComponent()).getCbAllocation();
    JTextField startField = ((PEVisualPanel2) getComponent()).getTxtStartCmd();
    JTextField stopField = ((PEVisualPanel2) getComponent()).getTxtStopCmd();
    JComboBox startArgsCombo = ((PEVisualPanel2) getComponent()).getCbStartArgs();
    JComboBox stopArgsCombo = ((PEVisualPanel2) getComponent()).getCbStopArgs();
    JCheckBox chbAccounting = ((PEVisualPanel2) getComponent()).getChbAccounting();
    JCheckBox chbTask = ((PEVisualPanel2) getComponent()).getChbFirstTask();
    JCheckBox chbSlaves = ((PEVisualPanel2) getComponent()).getChbSlaves();
    JTextArea taStartLine = ((PEVisualPanel2) getComponent()).getTaStartLine();
    JTextArea taStopLine = ((PEVisualPanel2) getComponent()).getTaStopLine();
    WizardDescriptor descriptor;
    private int slotsInt;
    private boolean isValid = false;
    private String slotsError = "";
    private String urgencyError = "";
    private String allocationError = "";
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private PEVisualPanel2 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new PEVisualPanel2();
            component.getTxtSlots().getDocument().addDocumentListener(new DocumentListener() {

                public void insertUpdate(DocumentEvent e) {
                    change();
                }

                public void removeUpdate(DocumentEvent e) {
                    change();
                }

                public void changedUpdate(DocumentEvent e) {
                    change();
                }
            });
        }
        component.getCbUrgency().addActionListener(this);
        component.getCbAllocation().addActionListener(this);
        return component;
    }

    private void change() {
        validateAll();
    }

    private void setValid(boolean val) {
        if (isValid != val) {
            isValid = val;
            fireChangeEvent();
        }
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        return isValid;
    }
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0

    public final void addChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public final void removeChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    protected final void fireChangeEvent() {
        Iterator<ChangeListener> it;
        synchronized (listeners) {
            it = new HashSet<ChangeListener>(listeners).iterator();
        }
        ChangeEvent ev = new ChangeEvent(this);
        while (it.hasNext()) {
            it.next().stateChanged(ev);
        }
    }

    // You can use a settings object to keep track of state. Normally the
    // settings object will be the WizardDescriptor, so you can use
    // WizardDescriptor.getProperty & putProperty to store information entered
    // by the user.
    public void readSettings(WizardDescriptor settings) {
        descriptor = settings;
        String slots = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_SLOTS, PEWizardSettings.SLOTS_DEFAULT);
        slotsField.setText(slots);
        String urgency = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_URGENCY, PEWizardSettings.URGENCY_DEFAULT);
        urgencyCombo.setSelectedItem(urgency);
        String allocation = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_ALLOCATION, PEWizardSettings.ALLOCATION_DEFAULT);
        allocationCombo.setSelectedItem(allocation);
        String start = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_START_CMD, PEWizardSettings.START_CMD_DEFAULT);
        startField.setText(start);
        String startArgs = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_START_ARGS, PEWizardSettings.ARGS_DEFAULT);
        startArgsCombo.setSelectedItem(startArgs);
        String startLine = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_START_AREA, "");
        taStartLine.setText(startLine);
        String stop = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_STOP_CMD, PEWizardSettings.STOP_CMD_DEFAULT);
        stopField.setText(stop);
        String stopArgs = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_STOP_ARGS, PEWizardSettings.ARGS_DEFAULT);
        stopArgsCombo.setSelectedItem(stopArgs);
        String stopLine = NbPreferences.forModule(PEWizardPanel2.class).get(PEWizardSettings.PROP_STOP_AREA, "");
        taStopLine.setText(stopLine);
        boolean accounting = NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_ACCOUNTING, false);
        chbAccounting.setSelected(accounting);
        boolean task = NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_IS_TASK, true);
        chbTask.setSelected(task);
        boolean slaves = NbPreferences.forModule(PEWizardPanel2.class).getBoolean(PEWizardSettings.PROP_SLAVES, false);
        chbSlaves.setSelected(slaves);
    }

    public void storeSettings(WizardDescriptor settings) {
        NbPreferences.forModule(PEWizardPanel2.class).putInt(PEWizardSettings.PROP_SLOTS, slotsInt);
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_URGENCY, urgencyCombo.getSelectedItem().toString());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_ALLOCATION, allocationCombo.getSelectedItem().toString());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_START_CMD, startField.getText().trim());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_START_ARGS, startArgsCombo.getSelectedItem().toString());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_START_AREA, taStartLine.getText().trim());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_STOP_CMD, stopField.getText().trim());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_STOP_ARGS, stopArgsCombo.getSelectedItem().toString());
        NbPreferences.forModule(PEWizardPanel2.class).put(PEWizardSettings.PROP_STOP_AREA, taStopLine.getText().trim());
        NbPreferences.forModule(PEWizardPanel2.class).putBoolean(PEWizardSettings.PROP_ACCOUNTING, chbAccounting.isSelected());
        NbPreferences.forModule(PEWizardPanel2.class).putBoolean(PEWizardSettings.PROP_IS_TASK, chbTask.isSelected());
        NbPreferences.forModule(PEWizardPanel2.class).putBoolean(PEWizardSettings.PROP_SLAVES, chbSlaves.isSelected());
    }

    private void validateAll() {
        //validate slots
        String slots = slotsField.getText();
        boolean valid = true;
         if (slots != null && slots.length() > 0) {
            try {
                slotsInt = Integer.parseInt(slots);
                if (!PEValidator.validateSlots(slotsInt)) {
                    valid = false;
                    slotsError = NbBundle.getMessage(PEWizardPanel2.class, "Invalid_Slots");
                } else {
                    valid = true;
                    slotsError = "";
                }
            } catch (NumberFormatException nfe) {
                 valid = false;
                 slotsError = NbBundle.getMessage(PEWizardPanel2.class, "Invalid_Slots");
            }
        } else {
            valid = false;
            slotsError = NbBundle.getMessage(PEWizardPanel2.class, "Slots_Empty");
        }

        if (!PEValidator.validateUrgency(urgencyCombo.getModel().getSelectedItem().toString())) {
            valid = false;
            urgencyError = NbBundle.getMessage(PEWizardPanel2.class, "Invalid_Urgency");
        } else {
            urgencyError = "";
        }
        if (!PEValidator.validateAllocation(allocationCombo.getModel().getSelectedItem().toString())) {
            valid = false;
            allocationError = NbBundle.getMessage(PEWizardPanel2.class, "Invalid_Allocation");
        } else {
            allocationError = "";
        }
        if (!valid) {
            descriptor.putProperty(WizardDescriptor.PROP_ERROR_MESSAGE,
                  "<html>" + slotsError + "<br>" + urgencyError + "<br>" + allocationError + "</html>");
        } else {
            descriptor.putProperty(WizardDescriptor.PROP_ERROR_MESSAGE, "");
        }

        setValid(valid);
    }

    public boolean isFinishPanel() {
        validateAll();
        return isValid;
    }

    public void actionPerformed(ActionEvent e) {
        validateAll();
    }
              
}

