

Copyright © 2009 Sun Microsystems, Inc.  All rights reserved.  Sun
Microsystems, Inc. has intellectual property rights relating to technology
embodied in the product that is described in this document. In particular,
and without limitation, these intellectual property rights may include one
or more of the U.S. patents listed at http://www.sun.com/patents and one or
more additional patents or pending patent applications in the U.S. and in
other countries. U.S. Government Rights - Commercial software.  Government
users are subject to the Sun Microsystems, Inc. standard license agreement
and applicable provisions of the FAR and its supplements.  Use is subject to
license terms.  This distribution may include materials developed by third
parties.Portions may be derived from Berkeley BSD systems, licensed from U.
of CA.  Sun, Sun Microsystems, the Sun logo, Java and Solaris are trademarks
or registered trademarks of Sun Microsystems, Inc. in the U.S. and other
countries.  All SPARC trademarks are used under license and are trademarks
or registered trademarks of SPARC International, Inc. in the U.S. and other
countries.
