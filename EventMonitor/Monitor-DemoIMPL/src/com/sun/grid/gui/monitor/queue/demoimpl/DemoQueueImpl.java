/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.queue.demoimpl;

import com.sun.grid.gui.monitor.queue.Queue;
import com.sun.grid.gui.monitor.queue.QueueInstance;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DemoQueueImpl extends Queue {

    List<QueueInstance> qInstances = Collections.synchronizedList(new LinkedList<QueueInstance>());

    public DemoQueueImpl(String name) {
        super(name);
        fillQueueInstances();
    }

    public void fillQueueInstances() {
        String host = "latte";
        QueueInstance qInstance;
        for (int i = 0; i < 10; i++) {
            String queueName = getQueueName();
            if (queueName.equals("all.q")) {
                qInstance = new DemoQueueInstanceImpl(queueName + "@" + host + i, "(BIP)");
                qInstances.add(qInstance);
                this.getRepository().addDataSource(qInstance);
            } else if (queueName.equals("INTERACTIVE")) {
                if (i % 2 == 0) {
                    qInstance = new DemoQueueInstanceImpl(queueName + "@" + host + i, "(BIP)");
                    qInstances.add(qInstance);
                    this.getRepository().addDataSource(qInstance);
                }
            } else {
                if (i % 2 != 0) {
                    qInstance = new DemoQueueInstanceImpl(queueName + "@" + host + i, "(BIP)");
                    qInstances.add(qInstance);
                    this.getRepository().addDataSource(qInstance);
                }
            }
        }

    }

    @Override
    public List<QueueInstance> getQueueInstances() {
        return qInstances;
    }
}
