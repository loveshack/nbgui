/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.host.demoimpl;

import com.sun.grid.gui.monitor.host.*;
import com.sun.grid.gui.monitor.job.Job;
import com.sun.grid.gui.monitor.job.demoimpl.DemoJobImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class DemoHostOverviewModel extends HostOverview {

    private static int jobs = 0;
    private String hostName = "latte";
    private static int count = 1;
    private String architecture = "x86+64 64bit";
    private String os = "Solaris";
    private String IP = "192.168.178.101";
    private String numProcessors = "2";
    private double memSize = 4096.00;
    private double swapSize = memSize * 2.00;
    private double virtSize = 567.00;
    Random generator = new Random(System.currentTimeMillis());
    private List<Job> runningJobs;
    private List<Job> finishedJobs;

    public DemoHostOverviewModel(Host host) {
        hostName = host.getHostName();    
        runningJobs = new ArrayList<Job>();
        finishedJobs = new ArrayList<Job>();
        fillJobs();
    }

    private void fillJobs() {
        for (int i = 0; i < 10; i++) {
            Job job = new DemoJobImpl(i);
            runningJobs.add(job);
        }
    }
    
    @Override
    public Object getHostValue(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getArch() {
        return architecture;
    }

    @Override
    public String getNumberOfProcessors() {
        return numProcessors;
    }

    @Override
    public double getLoadAvg() {
        return generator.nextDouble();
    }

    @Override
    public double getMemTotal() {
        return memSize;
    }

    @Override
    public double getMemUsed() {
        return generator.nextDouble() * 1000.00;
    }

    @Override
    public double getSwapTotal() {
        return swapSize;
    }

    @Override
    public double getSwapUsed() {
        return generator.nextDouble() * 1000.00;
    }

    @Override
    public int getHostValueCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<String> getHostValueKeys() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getResourceValue(String dominance, String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<String> getDominanceSet() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<String> getResourceValueNames(String dominance) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getHostname() {
        return hostName;
    }

    @Override
    public double getVirtualTotal() {
        return virtSize;
    }

    @Override
    public double getVirtualUsed() {
        return generator.nextDouble();
    }

    @Override
    public List<Job> getRunningJobs() {
        jobs++;
        return runningJobs;
    }

    @Override
    public int getRunningJobsCount() {
        return runningJobs.size();
    }

    @Override
    public int getUsedSlots() {
        return 5;
    }

    @Override
    public int getAvailableSlots() {
        return 10;
    }

    @Override
    public int getReservedSlots() {
        return 1;
    }

    @Override
    public int getTotalSlots() {
        return 16;
    }

    @Override
    public int getQueueCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
