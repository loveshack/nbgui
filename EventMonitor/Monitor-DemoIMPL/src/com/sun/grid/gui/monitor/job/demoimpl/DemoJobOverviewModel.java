/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.job.demoimpl;

import com.sun.grid.gui.monitor.job.Job;
import com.sun.grid.gui.monitor.job.JobOverview;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class DemoJobOverviewModel extends JobOverview {

    Random generator = new Random(System.currentTimeMillis());
    private static int count = 1;
    private String project = "project";
    private String department = "department";
    private String jobName = "test";
    private String user = "jo195647";
    int id;
    String taskId = "";
    SimpleDateFormat df;
    private String queueInstanceName;

    public DemoJobOverviewModel() {
        count++;
        project = project + count;
        department = department + count;
        queueInstanceName = "queue" + count + "@xyz.sun.com";
        jobName = jobName + count;
        df = new SimpleDateFormat();
    }

    public DemoJobOverviewModel(Job job) {
        this.id = job.getId();
    }

    public int getId() {
        return id;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public String getProject() {
        return project;
    }

    @Override
    public String getDepartment() {
        return department;
    }

    @Override
    public boolean isRunning() {
        return true;
    }

    @Override
    public double getPriority() {
        return generator.nextDouble();
    }

    @Override
    public String getName() {
        return jobName;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public String getState() {
        return "state";
    }

    @Override
    public String getQueue() {
        return "all.queue";
    }

    @Override
    public String getQinstanceName() {
        return queueInstanceName;
    }

    @Override
    public Date getSubmitTime() {
        return new Date(System.currentTimeMillis());
    }

    @Override
    public Date getStartTime() {
        return new Date(System.currentTimeMillis());
    }

    @Override
    public int getSlots() {
        return 1;
    }

//    @Override
//    public String getJobType() {
//        return "type";
//    }
//
//    @Override
//    public int getExitCode() {
//        return 0;
//    }
//
//    @Override
//    public Date getEndTime() {
//        return new Date(System.currentTimeMillis() + 10000);
//    }
//
//    @Override
//    public String getFinishedStatus() {
//        return "finished status";
//    }
//
//    @Override
//    public int getAR() {
//        return 0;
//    }


}
