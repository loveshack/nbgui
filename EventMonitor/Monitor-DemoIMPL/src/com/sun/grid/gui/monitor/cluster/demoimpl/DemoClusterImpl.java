/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.cluster.demoimpl;

import com.sun.grid.gui.monitor.cluster.*;
import com.sun.grid.gui.monitor.cluster.Cluster;
import com.sun.grid.gui.monitor.host.Host;
import com.sun.grid.gui.monitor.host.HostArchFilter;
import com.sun.grid.gui.monitor.host.HostTypeFilter;
import com.sun.grid.gui.monitor.host.demoimpl.DemoHostImpl;
import com.sun.grid.gui.monitor.host.demoimpl.DemoHostProperties;
import com.sun.grid.gui.monitor.queue.Queue;
import com.sun.grid.gui.monitor.queue.QueueInstance;
import com.sun.grid.gui.monitor.queue.demoimpl.DemoQueueImpl;
import com.sun.tools.visualvm.core.datasource.DataSourceContainer;
import com.sun.tools.visualvm.core.datasource.Storage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

final public class DemoClusterImpl extends Cluster {

    private List<Host> hosts = Collections.synchronizedList(new LinkedList<Host>());
    private List<Queue> queues = Collections.synchronizedList(new LinkedList<Queue>());
    private boolean connected = false;
    private static boolean done = false;
    private DataSourceContainer repository = this.getRepository();
    Queue queue2;
    Queue queue1;
    Timer timer;

    DemoClusterImpl(ClusterConfiguration config, Storage givenStorage) {
        super(config, givenStorage);
        fillQueues();
        fillHosts();       
    }
   
   private void startUpdateTimer() {
       //so we see some data changing
        timer = new Timer(10000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                final long time = System.currentTimeMillis();

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if (queues.size() == 2 && !DemoClusterImpl.done) {
                            List<QueueInstance> qis = queue1.getQueueInstances();
                            if (qis.size() > 0) {
                                QueueInstance qi = qis.remove(qis.size() - 1);
                                if (!qi.isRemoved()) {
                                    queue1.getRepository().removeDataSource(qi);
                                }
                            }
                        }
                        if (hosts.size() > 0 && !DemoClusterImpl.done) {
                            Host host = hosts.remove(hosts.size() - 1);
                            if (!host.isRemoved()) {
                                repository.removeDataSource(host);
                            }
                        } else if (!DemoClusterImpl.done) {
                            fillHosts();
                            if (queues.contains(queue2)) {
                                queues.remove(queue2);
                                repository.removeDataSource(queue2);
                            }
                            DemoClusterImpl.done = true;
                        }
                        fireChangeEvent();
                    }
                });
            }
        });
        timer.setInitialDelay(800);
        timer.start();      
   }
   
   private void stopUpdateTimer() {
        if (timer != null) {
            timer.stop();
        }
    }

    @Override
    public List<Queue> getQueueList() {
        return queues;
    }

    @Override
    public List<Host> getHostList() {
        return hosts;
    }

    private void fillQueues() {
        // dummy data to fill cluster
        queue1 = new DemoQueueImpl("all.q");
        queues.add(queue1);
        this.getRepository().addDataSource(queue1);
        queue2 = new DemoQueueImpl("INTERACTIVE");
        queues.add(queue2);
        this.getRepository().addDataSource(queue2);
        Queue queue3 = new DemoQueueImpl("Fast");
        queues.add(queue3);
        this.getRepository().addDataSource(queue3);
    }

    private void fillHosts() {
        for (int i = 0; i < 10; i++) {
            Host host;
            DemoHostProperties props = new DemoHostProperties();
            String name = props.getHostName();
            EnumSet<HostTypeFilter> types = props.getTypes();
            HostArchFilter arch = props.getArch();
            if (i % 2 == 0) {
                host = new DemoHostImpl(name, types, arch, queues);
            } else {
                List<Queue> subList = Collections.synchronizedList(queues.subList(0, 2));
                host = new DemoHostImpl(name, types, arch, subList);
            }
            hosts.add(host);
            this.getRepository().addDataSource(host);
        }
    }
    //TODO: Not sure if it is neccessary to remove all registered DataSources if the top data source is removed
    //Not sure if we should also do it when the connection si disconnected
    //It should be moved to abstract class, if we need it and is the same for both models
    private void cleanUp() {
        //remove all datasources and clear lists
        for (Host host : hosts) {
            if (!host.isRemoved()) {
                hosts.remove(host);
                repository.removeDataSource(host);
            }
        }

        for (Queue queue : queues) {
            List<QueueInstance> qis = queue.getQueueInstances();
            for (QueueInstance qi : qis) {
                if (!qi.isRemoved()) {
                    qis.remove(qi);
                    queue.getRepository().removeDataSource(qi);
                }
                
                if (!queue.isRemoved()) {
                    queues.remove(queue);
                    repository.removeDataSource(queue);
                }
            }
        }
    }

    @Override
    public void connect() {
        connected = true;
        firePropertyChange(Cluster.PROPERTY_CLUSTER_CONNECTED, !connected, connected);
        startUpdateTimer();
    }

    @Override
    public void disconnect() {
        connected = false;
        firePropertyChange(Cluster.PROPERTY_CLUSTER_CONNECTED, !connected, connected);
        stopUpdateTimer();
    }

    @Override
    public boolean isConnected() {
        return connected;
    }
}
