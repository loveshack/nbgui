/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.cluster.demoimpl;

import com.sun.grid.gui.monitor.cluster.ClusterConfiguration;


public class DemoClusterConfiguration extends ClusterConfiguration {

    private String clusterName = "p";
    private static int count = 6000;
    private String caPath = "/home/user123/sge/default/common/sgeCA";
    private String userName = "user123";
    private String userPassword = "bla";
    private String keystorePath = "/var/lib/sgeCA/port1234/default/private/keystore";
    private String keystorePassword = "changeit";
    private String jmxHost = "host";
    private int jmxPort = 54322;
    private String displayName;
    private boolean isSSL = false;

    public DemoClusterConfiguration() {
        clusterName = clusterName + count++;
        displayName = clusterName + "@" + jmxHost + ":" + jmxPort;
    }

    @Override
    public String getClusterName() {
        return clusterName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getUserPassword() {
        return userPassword;
    }

    @Override
    public String getKeystorePassword() {
        return keystorePassword;
    }

    @Override
    public String getKeystorePath() {
        return keystorePath;
    }

    @Override
    public String getCaPath() {
        return caPath;
    }

    @Override
    public String getJmxHost() {
        return jmxHost;
    }

    @Override
    public int getJmxPort() {
        return jmxPort;
    }

    @Override
    public boolean getSSL() {
        return isSSL;
    }

    @Override
    public void setClusterName(String clusterName) {
       this.clusterName = clusterName;
    }

    @Override
    public void setDisplayName(String displayName) {
       this.displayName = displayName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public void setKeystorePassword(String keystorePassword) {
       this.keystorePassword = keystorePassword;
    }

    @Override
    public void setCaPath(String caPath) {
        this.caPath = caPath;
    }

    @Override
    public void setJmxHost(String jmxHost) {
        this.jmxHost = jmxHost;
    }

    @Override
    public void setJmxPort(String jmxPort) {
         this.jmxPort = Integer.parseInt(jmxPort);
    }

    @Override
    public void setJmxPort(int jmxPort) {
        this.jmxPort = jmxPort;
    }

    @Override
    public void setKeystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
    }

    @Override
    public void setSSL(boolean isSSL) {
        this.isSSL = isSSL;
    }
}
