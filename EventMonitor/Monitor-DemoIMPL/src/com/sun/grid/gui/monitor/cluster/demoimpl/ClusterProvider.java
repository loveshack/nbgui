/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.cluster.demoimpl;

import com.sun.grid.gui.monitor.cluster.Cluster;
import com.sun.grid.gui.monitor.cluster.ClusterConfiguration;
import com.sun.grid.gui.monitor.cluster.ClusterContainer;
import com.sun.grid.gui.monitor.cluster.Provider;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.Storage;
import com.sun.tools.visualvm.core.datasupport.Utils;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public class ClusterProvider extends Provider {

    public ClusterProvider() {
        super();
    }

    public Cluster createCluster(ClusterConfiguration config) {
        waitForInitialization();

        final String clusterName = config.getClusterName();
        LOGGER.log(Level.FINE, "clusterName: " + clusterName);
        if (clusterName != null) {
            final Cluster knownCluster = getClusterByName(clusterName);
            if (knownCluster != null) {
                LOGGER.log(Level.FINE, "knownCluster: " + knownCluster.getClusterName());
                return knownCluster;
            } else {
                File customPropertiesStorage = Utils.getUniqueFile(ClusterSupport.getStorageDirectory(), clusterName, Storage.DEFAULT_PROPERTIES_EXT);
                Storage storage = new Storage(customPropertiesStorage.getParentFile(), customPropertiesStorage.getName());

                config.store(storage);

                Cluster newCluster = null;

                try {
                    newCluster = new DemoClusterImpl(config, storage);
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.log(Level.SEVERE, "Error creating cluster", e);
                }
                if (newCluster != null) {
                    newCluster.connect();
                    ClusterContainer.sharedInstance().getRepository().addDataSource(newCluster);
                }
                return newCluster;
            }
        }
        return null;
    }

    // get the stored clusters
    protected void initStoredClusters() {

        if (ClusterSupport.storageDirectoryExists()) {
            File[] files = ClusterSupport.getStorageDirectory().listFiles(new FilenameFilter() {

                public boolean accept(File dir, String name) {
                    return name.endsWith(Storage.DEFAULT_PROPERTIES_EXT);
                }
            });

            Set<File> unresolvedClustersF = new HashSet<File>();
            Set<String> unresolvedClustersS = new HashSet<String>();

            Set<Cluster> clusters = new HashSet<Cluster>();
            for (File file : files) {
                Storage storage = new Storage(file.getParentFile(), file.getName());
                String clusterName = storage.getCustomProperty(ClusterConfiguration.PROPERTY_CLUSTERNAME);
                Cluster persistedCluster = null;
                try {
                    ClusterConfiguration config = new DemoClusterConfiguration();
                    config.load(storage);

                    persistedCluster = new DemoClusterImpl(config, storage);
                    // TODO connect to the previous cluster if possible ????
                    persistedCluster.connect();
                } catch (Exception e) {
                    LOGGER.throwing(ClusterProvider.class.getName(), "initPersistedClusters", e);    // NOI18N
                    unresolvedClustersF.add(file);
                    unresolvedClustersS.add(clusterName);
                }

                if (persistedCluster != null) {
                    clusters.add(persistedCluster);
                }
            }

            if (!unresolvedClustersF.isEmpty()) {
                notifyUnresolvedClusters(unresolvedClustersF, unresolvedClustersS);
            }
            ClusterContainer.sharedInstance().getRepository().addDataSources(clusters);
        }

        DataSource.EVENT_QUEUE.post(new Runnable() {

            public void run() {
                initializingClustersSemaphore.release();
                initializingClusters = false;
            }
        });
    }

    public void initialize() {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable() {

            public void run() {
                RequestProcessor.getDefault().post(new Runnable() {

                    public void run() {
                        initCluster();
                        initStoredClusters();
                    }
                });
            }
        });
    }

}
