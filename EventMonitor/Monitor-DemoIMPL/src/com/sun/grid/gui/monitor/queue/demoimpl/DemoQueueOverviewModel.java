/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.queue.demoimpl;

import com.sun.grid.gui.monitor.queue.QueueOverview;
import com.sun.grid.gui.monitor.queue.Queue;
import java.util.Random;

public class DemoQueueOverviewModel extends QueueOverview {

    private String queueName;
    Random generator = new Random(System.currentTimeMillis());


    public DemoQueueOverviewModel(Queue queue) {
        queueName = queue.getDisplayName();
    }
    
    @Override
    public String getName() {
        return queueName;
    }

    @Override
    public double getLoad() {
        return generator.nextDouble();
    }

    @Override
    public boolean isLoadSet() {
        return true;
    }

    @Override
    public int getReservedSlots() {
        return generator.nextInt(1000);
    }

    @Override
    public int getUsedSlots() {
        return generator.nextInt(1000);
    }

    @Override
    public int getTotalSlots() {
        return generator.nextInt(1000);
    }

    @Override
    public int getAvailableSlots() {
        return generator.nextInt(1000);
    }

    @Override
    public int getTempDisabled() {
        return generator.nextInt(10);
    }

    @Override
    public int getManualIntervention() {
        return generator.nextInt(10);
    }

    @Override
    public int getSuspendManual() {
        return generator.nextInt(1000);
    }

    @Override
    public int getSuspendThreshold() {
        return generator.nextInt(1000);
    }

    @Override
    public int getSuspendOnSubordinate() {
        return generator.nextInt(1000);
    }

    @Override
    public int getSuspendByCalendar() {
        return generator.nextInt(1000);
    }

    @Override
    public int getUnknown() {
        return generator.nextInt(1000);
    }

    @Override
    public int getLoadAlarm() {
        return generator.nextInt(1000);
    }

    @Override
    public int getDisabledManual() {
        return generator.nextInt(1000);
    }

    @Override
    public int getDisabledByCalendar() {
        return generator.nextInt(1000);
    }

    @Override
    public int getAmbiguous() {
        return generator.nextInt(1000);
    }

    @Override
    public int getOrphaned() {
        return generator.nextInt(1000);
    }

    @Override
    public int getError() {
        return generator.nextInt(100);
    }
}
