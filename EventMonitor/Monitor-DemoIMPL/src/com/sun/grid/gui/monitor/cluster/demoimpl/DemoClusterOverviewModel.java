/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.cluster.demoimpl;

//used only for testing, uses dummy data
import com.sun.grid.gui.monitor.cluster.ClusterOverview;
import com.sun.grid.gui.monitor.cluster.Cluster;
import com.sun.grid.gui.monitor.job.Job;
import com.sun.grid.gui.monitor.job.demoimpl.DemoJobImpl;
import com.sun.grid.jgdi.monitoring.ClusterQueueSummary;
import com.sun.grid.jgdi.monitoring.ClusterQueueSummaryOptions;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DemoClusterOverviewModel extends ClusterOverview {

    private static int jobs = 0;
    private String clusterName = "p";
    private static int count = 6000;
    private static int qmasterPort = 5999;
    private static int execdPort = 6000;
    private static int jmxPort = 6001;
    private String sgeRoot = "/opt/sge";
    private String sgeCell = "default";
    private String sgeAdmin = "jo195647";
    private String jmxMasterHost = "gluck";
    Random generator = new Random(System.currentTimeMillis());
    ClusterQueueSummary queueSummary;
    ClusterQueueSummaryOptions queueOptions;
    private List<Job> runningJobs;
    private List<Job> pendingJobs;
    private List<Job> zombieJobs;

    public DemoClusterOverviewModel(Cluster cluster) {
        clusterName = cluster.getDisplayName();
        qmasterPort++;
        execdPort++;
        jmxPort++;
        runningJobs = new ArrayList<Job>();
        pendingJobs = new ArrayList<Job>();
        zombieJobs = new ArrayList<Job>();
        fillJobs();
    }

    private void fillJobs() {
        for (int i = 0; i < 11; i++) {
            Job job = new DemoJobImpl(i);
            pendingJobs.add(job);
        }
    }


    public String getClusterName() {
        return clusterName;
    }

    public String getSgeRoot() {
        return sgeRoot;
    }

    public String getSgeCell() {
        return sgeCell;
    }

    public String getSgeAdmin() {
        return sgeAdmin;
    }

    public int getQmasterPort() {
        return qmasterPort;

    }
    
    public int getExecdPort() {
        return execdPort;
    }

    public String getJmxMasterHost() {
        return jmxMasterHost;
    }

    public int getJmxPort() {
        return jmxPort;
    }

    public int getAvailability() {
//        return 50;
        return generator.nextInt(100);
    }

    public int getOverload() {
        return generator.nextInt(100);
    }

    public int getSlotsUtilization() {
        return generator.nextInt(100);
    }

    public ClusterQueueSummary getClusterQueueSummary() {
        return queueSummary;
    }

    public ClusterQueueSummaryOptions getQueueOptions() {
        return new ClusterQueueSummaryOptions();
    }

    public int getRunningJobsCount() {
        return runningJobs.size();
    }

    public int getZombieJobsCount() {
        return zombieJobs.size();
    }

    public int getPendingJobsCount() {
        return pendingJobs.size();
    }

    public List<Job> getZombieJobs() {
        return zombieJobs;
    }

    public List<Job> getPendingJobs() {
        jobs++;
        return pendingJobs;
    }

    public List<Job> getRunningJobs() {
        Job job;
        Job job2;
        if (pendingJobs.size() > 0) {
            job = pendingJobs.remove(0);
            runningJobs.add(job);
        }

        if (jobs > 5 && runningJobs.size() > 0) {
            job2 = runningJobs.remove(0);
            zombieJobs.add(job2);
        }

        return runningJobs;
    }
}
