/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.cluster.demoimpl;

import com.sun.grid.gui.monitor.cluster.*;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import com.sun.tools.visualvm.core.model.ModelProvider;

public class ClusterModelProvider extends AbstractModelProvider<ClusterOverview, Cluster> {

    /**
     * Default {@link ModelProvider} implementation, which creates 
     * ClusterOverview for current cluster. If you want to extend ClusterModelProvider use 
     * {@link ClusterModelProvider#registerProvider()} to register the new instances
     * of {@link ModelProvider} for the different types of {@link Cluster}.
     * @param cluster cluster
     * @return instance of {@link ClusterOverview} for cluster
     */
    public ClusterOverview createModelFor(Cluster cluster) {
        if (cluster instanceof DemoClusterImpl) {
            return new DemoClusterOverviewModel(cluster);
        }
        return null;
    }
}

