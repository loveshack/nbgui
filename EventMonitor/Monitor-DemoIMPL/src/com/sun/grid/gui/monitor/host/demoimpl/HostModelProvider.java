/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.host.demoimpl;

import com.sun.grid.gui.monitor.host.Host;
import com.sun.grid.gui.monitor.host.HostOverview;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import com.sun.tools.visualvm.core.model.ModelProvider;

public class HostModelProvider extends AbstractModelProvider<HostOverview, Host> {

    /**
     * Default {@link ModelProvider} implementation, which creates 
     * HostOverview for current host. If you want to extend HostModelProvider use 
     * {@link HostModelProvider#registerProvider()} to register the new instances
     * of {@link ModelProvider} for the different types of {@link Host}.
     * @param host host
     * @return instance of {@link HostOverview} for host
     */
    public HostOverview createModelFor(Host host) {
        if (host instanceof DemoHostImpl) {
            return new DemoHostOverviewModel(host);
        }
        return null;
    }
}

