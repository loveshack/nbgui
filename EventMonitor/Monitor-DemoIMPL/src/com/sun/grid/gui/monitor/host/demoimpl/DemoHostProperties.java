/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.host.demoimpl;

import com.sun.grid.gui.monitor.host.HostArchFilter;
import com.sun.grid.gui.monitor.host.HostTypeFilter;
import java.util.EnumSet;

public class DemoHostProperties {

    private String hostName = "latte";
    private static int count = 1;
    private final EnumSet<HostTypeFilter> types = EnumSet.noneOf(HostTypeFilter.class);
    private HostArchFilter arch = null;

    public DemoHostProperties() {
        HostTypeFilter type;
        if ((count % 2) == 0) {
            type = HostTypeFilter.EXECD;
            arch = HostArchFilter.LX24_AMD64;
        } else {
            type = HostTypeFilter.SUBMIT;
            arch = HostArchFilter.SOL_SPARC;
        }
        types.add(type);
        hostName = hostName + count++;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public EnumSet<HostTypeFilter> getTypes() {
        return types;
    }

    public HostArchFilter getArch() {
        return arch;
    }
}
