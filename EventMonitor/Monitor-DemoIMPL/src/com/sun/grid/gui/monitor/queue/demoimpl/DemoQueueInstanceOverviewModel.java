/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.gui.monitor.queue.demoimpl;

import com.sun.grid.gui.monitor.job.Job;
import com.sun.grid.gui.monitor.job.demoimpl.DemoJobImpl;
import com.sun.grid.gui.monitor.queue.QueueInstance;
import com.sun.grid.gui.monitor.queue.QueueInstanceOverview;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class DemoQueueInstanceOverviewModel extends QueueInstanceOverview {

    private String queueInstanceName;
    Random generator = new Random(System.currentTimeMillis());
    List<Job> runningJobs;
    private static int jobs = 0;

    public DemoQueueInstanceOverviewModel(QueueInstance queueInstance) {
        queueInstanceName = queueInstance.getDisplayName();
        runningJobs = new LinkedList<Job>();
        fillJobs();
    }

    private void fillJobs() {
        for (int i = 0; i < 10; i++) {
            Job job = new DemoJobImpl(i);
            runningJobs.add(job);
        }
    }

    @Override
    public String getName() {
        return queueInstanceName;
    }

    @Override
    public String getQueueType() {
        return "BIP";
    }

    @Override
    public int getReservedSlots() {
        return generator.nextInt(100);
    }

    @Override
    public int getUsedSlots() {
        return generator.nextInt(100);
    }

    @Override
    public int getFreeSlots() {
        return generator.nextInt(100);
    }

    @Override
    public String getArch() {
        return "some arch";
    }

    @Override
    public String getState() {
        return "some state";
    }

    @Override
    public String getLoadAvgStr() {
        return "load_avg";
    }

    @Override
    public boolean hasLoadValue() {
        return true;
    }

    @Override
    public boolean isHasLoadValueFromObject() {
        return true;
    }

    @Override
    public double getLoadAvg() {
        return generator.nextDouble();
    }

    @Override
    public String getLoadAlarmReason() {
        return "bla";
    }

    @Override
    public String getSuspendAlarmReason() {
        return "bla";
    }

    @Override
    public List<String> getExplainMessageList() {
        return Collections.<String>emptyList();
    }

    @Override
    public Set<String> getResourceDominanceSet() {
        return Collections.<String>emptySet();
    }

    @Override
    public Set<String> getResourceNames(String dom) {
        return Collections.<String>emptySet();
    }

    @Override
    public String getResourceValue(String dom, String name) {
        return name;
    }

    @Override
    public double getSlotUsage() {
        return generator.nextDouble();
    }

    @Override
    public int getTotalSlots() {
        return generator.nextInt(100);
    }

    @Override
    public List<Job> getRunningJobs() {
        jobs++;
        return runningJobs;
    }
}
