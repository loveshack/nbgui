/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.gui.monitor.cluster.demoimpl;

import com.sun.grid.gui.monitor.cluster.ClusterAction;
import com.sun.grid.gui.monitor.cluster.ClusterContainer;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class AddNewClusterAction extends ClusterAction {

    private static final CreateAbstractNewClusterAction action = new CreateAbstractNewClusterAction();
    private static final CreateSingleNewClusterAction dsAction  = new CreateSingleNewClusterAction();
    
     public static AbstractAction getAction() {
        return action;
    }

    public static SingleDataSourceAction<ClusterContainer> getDSAction() {
        return dsAction;
    }
    
    private static void performAction(ActionEvent actionEvent) {
        
            RequestProcessor.getDefault().post(new Runnable() {

                public void run() {
                    com.sun.grid.gui.monitor.cluster.demoimpl.ClusterSupport.getInstance().createCluster(new DemoClusterConfiguration());
                }
            });
    }

     private static class CreateAbstractNewClusterAction extends AbstractAction {

        private static final long serialVersionUID = -2009040101L;

        private CreateAbstractNewClusterAction() {
            super();
            putValue(NAME, NbBundle.getMessage(CreateSingleNewClusterAction.class, "CTL_CreateDemoClusterConnection"));
            putValue(SMALL_ICON, new ImageIcon(ImageUtilities.loadImage(ICON_PATH)));
            putValue("iconBase", ICON_PATH);  // NOI18N
        }

        public void actionPerformed(ActionEvent event) {
            performAction(event);
        }
    }
     
    private static class CreateSingleNewClusterAction extends SingleDataSourceAction<ClusterContainer> {

        private static final long serialVersionUID = -2009040101L;

        private CreateSingleNewClusterAction() {
            super(ClusterContainer.class);
            putValue(NAME, NbBundle.getMessage(CreateSingleNewClusterAction.class, "CTL_CreateDemoClusterConnection"));
            putValue(SMALL_ICON, new ImageIcon(ImageUtilities.loadImage(ICON_PATH)));
            putValue("iconBase", ICON_PATH);  // NOI18N
        }

        public void actionPerformed(ClusterContainer ds, ActionEvent event) {
            performAction(event);
        }

        protected boolean isEnabled(ClusterContainer ds) {
            return true;
        }
    }
}

