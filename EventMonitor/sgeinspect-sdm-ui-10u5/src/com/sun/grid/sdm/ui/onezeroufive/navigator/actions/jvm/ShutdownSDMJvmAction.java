/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.navigator.actions.jvm;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.component.JVMResultObject;
import com.sun.grid.grm.ui.component.StopJVMCommand;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class ShutdownSDMJvmAction extends RelaxedCookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final ShutdownSDMJvmAction SINGLETON = new ShutdownSDMJvmAction();

    public void performAction(Node[] activatedNodes) {
        final JvmDataSource serviceDataSource = activatedNodes[0].getLookup().lookup(JvmDataSource.class);
        if (serviceDataSource != null) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    StopJVMCommand sjc = new StopJVMCommand();
                    sjc.setHostname(serviceDataSource.getHost());
                    sjc.setJVMName(serviceDataSource.getName());
                    ExecutionEnv env = ExecEnvUtil.getExecutionEnv((Application) serviceDataSource.getOwner());
                    try {
                        List<JVMResultObject> jro = env.getCommandService().execute(sjc).getReturnValue();
                    } catch (GrmException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }
                    return null;
                }
            });
        }
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    public String getName() {
        return NbBundle.getMessage(ShutdownSDMJvmAction.class, "CTL_ShutdownSDMServiceAction");
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{ServiceDataSource.class};
    }

    public static final ShutdownSDMJvmAction getInstance() {
        return SINGLETON;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

