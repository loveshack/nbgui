/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.component.reporter;

import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.component.reporter.ReporterDataSourceImpl;
import com.sun.grid.sdm.ui.onezeroufive.util.HistoryPanelSupport;
import com.sun.grid.sdm.ui.util.ComboHelper;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.Icon;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ReporterHistoryTablePanel extends JPanel {

    public static final long serialVersionUID = -2009100101L;
    private final HistoryPanelSupport<ReporterDataSourceImpl> tod;
    private final PropertyChangeListener listener = new HPSListener();
    private final Icon down_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/down_arrow.png"));
    private final Icon up_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/up_arrow.png"));
    private final ReporterDataSourceImpl rds;
    private final DateFormat SF = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");

    /** Creates new form ModulesTablePanel */
    public ReporterHistoryTablePanel(ReporterDataSourceImpl rds) {
        initComponents();
        this.tod = new HistoryPanelSupport<ReporterDataSourceImpl>(rds);
        this.tod.addPropertyChangeListener(listener);
        this.tod.setRowsPerPage(getRowsPerPage());
        this.otlPanel.add(tod.getComponent());
        this.rds = rds;
        this.updateHistory();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel = new javax.swing.JPanel();
        btnPrev = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        lblRows = new javax.swing.JLabel();
        cbRowsPerPage = new javax.swing.JComboBox();
        mainPanel = new javax.swing.JPanel();
        otlPanel = new javax.swing.JPanel();
        btnPanel = new javax.swing.JPanel();
        tglPanel = new javax.swing.JPanel();
        cmbProperty = new javax.swing.JComboBox();
        lblProperty = new javax.swing.JLabel();
        cmbOperator = new javax.swing.JComboBox();
        lblValue = new javax.swing.JLabel();
        txtValue = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtFilter = new javax.swing.JTextArea();
        lblFilter = new javax.swing.JLabel();
        btnOr = new javax.swing.JButton();
        btLBrckt = new javax.swing.JButton();
        btnRBrckt = new javax.swing.JButton();
        btnAnd = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnHide = new javax.swing.JToggleButton();

        btnPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_previous.png"))); // NOI18N
        btnPrev.setMnemonic('P');
        btnPrev.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnPrev.text")); // NOI18N
        btnPrev.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnPrev.toolTipText")); // NOI18N
        btnPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrevActionPerformed(evt);
            }
        });

        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_next.png"))); // NOI18N
        btnNext.setMnemonic('N');
        btnNext.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnNext.text")); // NOI18N
        btnNext.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnNext.toolTipText")); // NOI18N
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_refresh.png"))); // NOI18N
        btnRefresh.setMnemonic('R');
        btnRefresh.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnRefresh.text")); // NOI18N
        btnRefresh.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnRefresh.toolTipText")); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        lblRows.setDisplayedMnemonic('a');
        lblRows.setLabelFor(cbRowsPerPage);
        lblRows.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.lblRows.text")); // NOI18N

        cbRowsPerPage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10", "20", "50" }));
        cbRowsPerPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbRowsPerPageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnPrev)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNext)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefresh)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 498, Short.MAX_VALUE)
                .addComponent(lblRows)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbRowsPerPage, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbRowsPerPage, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblRows))
                    .addComponent(btnPrev)
                    .addComponent(btnNext)
                    .addComponent(btnRefresh))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnPrev.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnPrev.AccessibleContext.accessibleName")); // NOI18N
        btnNext.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnNext.AccessibleContext.accessibleName")); // NOI18N
        btnRefresh.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnRefresh.AccessibleContext.accessibleName")); // NOI18N

        otlPanel.setLayout(new java.awt.BorderLayout());

        cmbProperty.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "resource", "service", "type", "time", "description" }));
        cmbProperty.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.cmbProperty.toolTipText")); // NOI18N

        lblProperty.setLabelFor(cmbProperty);
        lblProperty.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.lblProperty.text")); // NOI18N

        cmbOperator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=", "!=", "<=", "<", ">", ">=", "matches" }));
        cmbOperator.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.cmbOperator.toolTipText")); // NOI18N

        lblValue.setLabelFor(txtValue);
        lblValue.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.lblValue.text")); // NOI18N

        txtValue.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.txtValue.text")); // NOI18N
        txtValue.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.txtValue.toolTipText")); // NOI18N

        jScrollPane1.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.jScrollPane1.toolTipText")); // NOI18N

        txtFilter.setColumns(20);
        txtFilter.setRows(5);
        jScrollPane1.setViewportView(txtFilter);

        lblFilter.setLabelFor(txtFilter);
        lblFilter.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.lblFilter.text")); // NOI18N

        btnOr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_or.png"))); // NOI18N
        btnOr.setMnemonic('O');
        btnOr.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnOr.text")); // NOI18N
        btnOr.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnOr.toolTipText")); // NOI18N
        btnOr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrActionPerformed(evt);
            }
        });

        btLBrckt.setMnemonic('L');
        btLBrckt.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btLBrckt.text")); // NOI18N
        btLBrckt.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btLBrckt.toolTipText")); // NOI18N
        btLBrckt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLBrcktActionPerformed(evt);
            }
        });

        btnRBrckt.setMnemonic('R');
        btnRBrckt.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnRBrckt.text")); // NOI18N
        btnRBrckt.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnRBrckt.toolTipText")); // NOI18N
        btnRBrckt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRBrcktActionPerformed(evt);
            }
        });

        btnAnd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_and.png"))); // NOI18N
        btnAnd.setMnemonic('A');
        btnAnd.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAnd.text")); // NOI18N
        btnAnd.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAnd.toolTipText")); // NOI18N
        btnAnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAndActionPerformed(evt);
            }
        });

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_add.png"))); // NOI18N
        btnAdd.setMnemonic('D');
        btnAdd.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAdd.text")); // NOI18N
        btnAdd.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAdd.toolTipText")); // NOI18N
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_clean.png"))); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnClear.text")); // NOI18N
        btnClear.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnClear.toolTipText")); // NOI18N
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tglPanelLayout = new javax.swing.GroupLayout(tglPanel);
        tglPanel.setLayout(tglPanelLayout);
        tglPanelLayout.setHorizontalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblFilter)
                    .addComponent(lblProperty))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tglPanelLayout.createSequentialGroup()
                        .addComponent(cmbProperty, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbOperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblValue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(tglPanelLayout.createSequentialGroup()
                                .addComponent(btnAnd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnOr)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btLBrckt)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRBrckt)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
                                .addComponent(btnClear))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tglPanelLayout.createSequentialGroup()
                                .addComponent(txtValue, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAdd))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE)))
        );
        tglPanelLayout.setVerticalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tglPanelLayout.createSequentialGroup()
                        .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblProperty)
                            .addComponent(cmbProperty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbOperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblValue)
                            .addComponent(txtValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btLBrckt)
                            .addComponent(btnOr)
                            .addComponent(btnAnd)
                            .addComponent(btnRBrckt)))
                    .addGroup(tglPanelLayout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnClear)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblFilter)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        cmbOperator.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.cmbOperator.AccessibleContext.accessibleName")); // NOI18N
        btnOr.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnOr.AccessibleContext.accessibleName")); // NOI18N
        btnAnd.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAnd.AccessibleContext.accessibleName")); // NOI18N
        btnAdd.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnAdd.AccessibleContext.accessibleName")); // NOI18N
        btnClear.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnClear.AccessibleContext.accessibleName")); // NOI18N

        btnHide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/up_arrow.png"))); // NOI18N
        btnHide.setText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnHide.text")); // NOI18N
        btnHide.setToolTipText(org.openide.util.NbBundle.getMessage(ReporterHistoryTablePanel.class, "ReporterHistoryTablePanel.btnHide.toolTipText")); // NOI18N
        btnHide.setBorderPainted(false);
        btnHide.setContentAreaFilled(false);
        btnHide.setFocusPainted(false);
        btnHide.setMaximumSize(new java.awt.Dimension(32767, 32767));
        btnHide.setMinimumSize(new java.awt.Dimension(0, 0));
        btnHide.setPreferredSize(new java.awt.Dimension(100, 10));
        btnHide.setVerifyInputWhenFocusTarget(false);
        btnHide.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHideMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHideMouseExited(evt);
            }
        });
        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout btnPanelLayout = new javax.swing.GroupLayout(btnPanel);
        btnPanel.setLayout(btnPanelLayout);
        btnPanelLayout.setHorizontalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tglPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnHide, javax.swing.GroupLayout.DEFAULT_SIZE, 788, Short.MAX_VALUE)
        );
        btnPanelLayout.setVerticalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnPanelLayout.createSequentialGroup()
                .addComponent(tglPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 788, Short.MAX_VALUE)
            .addComponent(btnPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(btnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mainPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnHideMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseEntered
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseEntered

    private void btnHideMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseExited
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseExited

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        tglPanel.setVisible(!(tglPanel.isVisible()));
        if (tglPanel.isVisible()) {
            btnHide.setIcon(up_arrow);
        } else {
            btnHide.setIcon(down_arrow);
        }
}//GEN-LAST:event_btnHideActionPerformed

    private void btnAndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAndActionPerformed
        txtFilter.setText(txtFilter.getText() + " &");
    }//GEN-LAST:event_btnAndActionPerformed

    private void btnOrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrActionPerformed
        txtFilter.setText(txtFilter.getText() + " |");
    }//GEN-LAST:event_btnOrActionPerformed

    private void btLBrcktActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLBrcktActionPerformed
        txtFilter.setText(txtFilter.getText() + " (");
    }//GEN-LAST:event_btLBrcktActionPerformed

    private void btnRBrcktActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRBrcktActionPerformed
        txtFilter.setText(txtFilter.getText() + ")");
    }//GEN-LAST:event_btnRBrcktActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        updateFilter();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        updateHistory();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        txtFilter.setText("");
}//GEN-LAST:event_btnClearActionPerformed

    private void btnPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrevActionPerformed
        tod.navigateBackward();
    }//GEN-LAST:event_btnPrevActionPerformed

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        tod.navigateForward();
    }//GEN-LAST:event_btnNextActionPerformed

    private void cbRowsPerPageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbRowsPerPageActionPerformed
        tod.setRowsPerPage(getRowsPerPage());
    }//GEN-LAST:event_cbRowsPerPageActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btLBrckt;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAnd;
    private javax.swing.JButton btnClear;
    private javax.swing.JToggleButton btnHide;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnOr;
    private javax.swing.JPanel btnPanel;
    private javax.swing.JButton btnPrev;
    private javax.swing.JButton btnRBrckt;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JComboBox cbRowsPerPage;
    private javax.swing.JComboBox cmbOperator;
    private javax.swing.JComboBox cmbProperty;
    private javax.swing.JPanel jPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblFilter;
    private javax.swing.JLabel lblProperty;
    private javax.swing.JLabel lblRows;
    private javax.swing.JLabel lblValue;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel otlPanel;
    private javax.swing.JPanel tglPanel;
    private javax.swing.JTextArea txtFilter;
    private javax.swing.JTextField txtValue;
    // End of variables declaration//GEN-END:variables

    private Filter<Report> getFilter() {
        return ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Filter<Report>>() {

            public Filter<Report> execute() {
                final String filter_string = txtFilter.getText();
                Filter<Report> filter = ConstantFilter.<Report>alwaysMatching();
                if (filter_string != null && filter_string.length() != 0) {
                    try {
                        filter = FilterHelper.<Report>parse(txtFilter.getText());
                    } catch (FilterException ex) {
                        txtFilter.setText(NbBundle.getMessage(ReporterHistoryTablePanel.class, "Filter_is_not_valid"));
                    }
                }
                return filter;
            }
        });
    }

    /**
     *
     * @return -1 if all
     */
    public synchronized int getRowsPerPage() {
        String o = cbRowsPerPage.getSelectedItem().toString();
        if (o.equals(ComboHelper.ALL)) {
            return -1;
        } else {
            try {
                return Integer.parseInt(o);
            } catch (NumberFormatException numberFormatException) {
                return -1;
            }
        }
    }

    private synchronized void updateHistory() {
        tod.getHistory(getFilter());
        enableNavigationControls(false);
    }

    private void updateFilter() {
        String value;
        if (cmbProperty.getSelectedItem().equals("time")) {
            value = String.format("TS{%s}", txtValue.getText());
        } else {
            value = "\"" + txtValue.getText() + "\"";
        }
        txtFilter.setText(txtFilter.getText() + " " + cmbProperty.getSelectedItem() + cmbOperator.getSelectedItem() + value);
        txtValue.setText("");
    }

    private void enableNavigationControls(boolean enabled) {
        this.btnNext.setEnabled(enabled);
        this.btnPrev.setEnabled(enabled);
        this.btnRefresh.setEnabled(enabled);
    }

    private class HPSListener implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(HistoryPanelSupport.INPROGRESS)) {
                if (evt.getNewValue() == Boolean.TRUE) {
                    enableNavigationControls(false);
                } else {
                    enableNavigationControls(true);
                }
            }
            if (evt.getPropertyName().equals(HistoryPanelSupport.BEGINNINGREACHED)) {
                if (evt.getNewValue() == Boolean.TRUE) {
                    btnPrev.setEnabled(false);
                } else {
                    btnPrev.setEnabled(true);
                }
            }
            if (evt.getPropertyName().equals(HistoryPanelSupport.ENDREACHED)) {
                if (evt.getNewValue() == Boolean.TRUE) {
                    btnNext.setEnabled(false);
                } else {
                    btnNext.setEnabled(true);
                }
            }
        }
    }
}
