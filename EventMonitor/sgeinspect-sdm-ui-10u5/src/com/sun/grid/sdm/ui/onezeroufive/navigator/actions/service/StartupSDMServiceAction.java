/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.navigator.actions.service;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.ui.component.ServiceResultObject;
import com.sun.grid.grm.ui.component.StartComponentCommand;
import com.sun.grid.grm.ui.component.StartServiceCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class StartupSDMServiceAction extends RelaxedCookieAction {

    private static final String BUNDLE = "com.sun.grid.sdm.ui.onezeroufive.navigator.actions.service.Bundle";
    private static final Logger log = Logger.getLogger(StartupSDMServiceAction.class.getName(), BUNDLE);
    private final static long serialVersionUID = -2009100101L;
    private static final StartupSDMServiceAction SINGLETON = new StartupSDMServiceAction();

    public void performAction(Node[] activatedNodes) {
        final ServiceDataSource serviceDataSource = activatedNodes[0].getLookup().lookup(ServiceDataSource.class);
        if (serviceDataSource != null) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {
                public Void execute() {
                    StartServiceCommand ssc = new StartServiceCommand(serviceDataSource.getServiceName());
                    ExecutionEnv env = ExecEnvUtil.getExecutionEnv((Application) serviceDataSource.getOwner());
                    try {
                        if(ensureComponentStarted(serviceDataSource,env)){
                            ServiceResultObject sro = env.getCommandService().execute(ssc).getReturnValue();
                        }
                    } catch (GrmException ex) {
                        log.log(Level.INFO, "startup service error", ex);
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(StartupSDMServiceAction.class, "MSG_StartupSDMServiceAction", serviceDataSource.getServiceName()));
                    return null;
                }
            });
        }
    }

    private boolean ensureComponentStarted(ServiceDataSource serviceDataSource, ExecutionEnv env) {
        int tries = 2;
        ComponentState componentState = ComponentState.UNKNOWN;
        String serviceName = serviceDataSource.getServiceName();
        String host = serviceDataSource.getHostname();

        try {

            Service service = getService(serviceDataSource, env);
            if (service != null) {
                for (int i = 0; i < tries; i++) {
                    componentState = service.getState();
                    if (componentState == ComponentState.STARTED) {
                        return true;
                    } else if (componentState == componentState.STOPPED || componentState == componentState.UNKNOWN) {
                        //startup component
                        startupComponent(env, serviceName, host);
                    }
                    // should now be in starting
                    //wait for 5 seconds
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
        } catch (GrmException ex) {
            log.log(Level.INFO, "Error testing the component state", ex);
        }
        String message = NbBundle.getMessage(StartupSDMServiceAction.class, "MSG_ServiceComponentCannotStart",serviceName, componentState);
        ErrorDisplayer.submitWarning(message);
        return false;
    }

    private Service getService(ServiceDataSource serviceDataSource, ExecutionEnv env) throws GrmException {
        String serviceName = serviceDataSource.getServiceName();
        String host = serviceDataSource.getHostname();
        String jvm = serviceDataSource.getJvmName();
        ComponentInfo info = ComponentInfo.newInstance(
                env,
                Hostname.getInstance(host),
                jvm,
                serviceName,
                Service.class);

        return ComponentService.<Service>getComponent(env, info);
    }
    
    private void startupComponent(ExecutionEnv env,String serviceName, String host) {
        try {
            StartComponentCommand scc = new StartComponentCommand();
            scc.setComponentName(serviceName);
            scc.setHostname(host);
            env.getCommandService().execute(scc);
        } catch (GrmException ex) {
            log.log(Level.INFO, "startup component error", ex);
            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
        }

    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        boolean enable = super.enable(activatedNodes);
        if (enable) {
            ServiceDataSource serviceDataSource = activatedNodes[0].getLookup().lookup(ServiceDataSource.class);
            if (serviceDataSource != null) {
                return (serviceDataSource.getServiceState().equals("STOPPED") ||
                        serviceDataSource.getServiceState().equals("UNKNOWN") ||
                        serviceDataSource.getServiceState().equals("ERROR"));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return NbBundle.getMessage(StartupSDMServiceAction.class, "CTL_StartupSDMServiceAction");
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{ServiceDataSource.class};
    }

    public static final StartupSDMServiceAction getInstance() {
        return SINGLETON;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

