/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.service.cloud;

import com.sun.grid.sdm.api.service.cloud.CloudServiceDataSource;
import com.sun.grid.sdm.onezeroufive.service.cloud.CloudServiceDataSourceImpl;
import com.sun.grid.sdm.onezeroufive.util.Version;
import com.sun.grid.sdm.ui.util.VersionableDataSourceViewFactory;
import com.sun.tools.visualvm.core.ui.DataSourceView;

public class CloudServiceDataSourceResourceViewFactoryImpl implements VersionableDataSourceViewFactory<CloudServiceDataSource> {

    private final static CloudServiceDataSourceResourceViewFactoryImpl INSTANCE = new CloudServiceDataSourceResourceViewFactoryImpl();

    private CloudServiceDataSourceResourceViewFactoryImpl() {
    }

    public static final CloudServiceDataSourceResourceViewFactoryImpl getInstance() {
        return INSTANCE;
    }

    public String getVersion() {
        return Version.getVersions();
    }

    public DataSourceView createView(CloudServiceDataSource ds) {
        return new CloudServiceDataSourceResourceView((CloudServiceDataSourceImpl)ds);
    }
}
