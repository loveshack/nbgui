/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.util;

import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.sdm.api.base.HistoryObservable;
import com.sun.grid.sdm.api.util.HistoryListener;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class HistoryPanelSupport<T extends HistoryObservable<Object[], Filter<Report>>> implements HistoryListener<Object[]> {

    public final static String INPROGRESS = "inprogress";
    public final static String BEGINNINGREACHED = "beginningreached";
    public final static String ENDREACHED = "endreached";
    private T cds;
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private OutlineDynamic o;
    private final DateFormat SF = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
    private final ArrayList<Object[]> row_cache = new ArrayList<Object[]>();
    private int offset;
    private int rowsPerPage;
    private static final Image MESSAGE_IMAGE = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_message.png");
    private JScrollPane scroller;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public HistoryPanelSupport(T c) {
        this.cds = c;
        this.cds.addHistoryListener(this);
        this.o = new OutlineDynamic();
    }

    public synchronized Component getComponent() {
        if (scroller == null) {
            scroller = new JScrollPane(o);
        }
        return scroller;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public synchronized void getHistory(final Filter<Report> f) {
        ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

            public Void execute() {
                cds.getHistory(f);
                return null;
            }
        });
        pcs.firePropertyChange(INPROGRESS, false, true);
    }

    public void historyRowsAdded(Object[] added) {
        List<Object[]> newCache = new LinkedList<Object[]>(row_cache);
        newCache.addAll(Collections.<Object[]>singletonList(added));
        updateRowCache(newCache);
    }

    public void historyReset() {
        updateRowCache(Collections.<Object[]>emptyList());
    }

    public void historyRetrieved() {
        pcs.firePropertyChange(INPROGRESS, true, false);
        setOffset(row_cache.size());
    }

    public synchronized void setRowsPerPage(int o) {
        synchronized (row_cache) {
            if (o == -1) {
                this.rowsPerPage = row_cache.size();
            } else {
                this.rowsPerPage = o;
            }
        }
        this.o.fillModel();
    }

    /**
     *
     * @return true if beginning has been reached
     */
    public void navigateForward() {
        setOffset(Math.min(offset + rowsPerPage, row_cache.size()));
    }

    /**
     *
     * @return true if end has been reached
     */
    public void navigateBackward() {
        setOffset(Math.max(rowsPerPage, offset - rowsPerPage));
    }

    private void setOffset(int o) {
        synchronized (row_cache) {
            this.offset = Math.max(rowsPerPage, o);
            this.offset = Math.min(offset, row_cache.size());
        }
        this.o.fillModel();
        if (o <= rowsPerPage) {
            pcs.firePropertyChange(BEGINNINGREACHED, false, true);
        } else {
            pcs.firePropertyChange(BEGINNINGREACHED, true, false);
        }
        if (o >= row_cache.size()) {
            pcs.firePropertyChange(ENDREACHED, false, true);
        } else {
            pcs.firePropertyChange(ENDREACHED, true, false);
        }
    }

    private static class RowNode {

        final private Object[] rw;

        public RowNode(Object[] rw) {
            this.rw = rw;
        }
    }

    private static class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        public String getDisplayName(Object node) {
            if (((DefaultMutableTreeNode) node).getUserObject() instanceof RowNode) {
                RowNode rn = (RowNode) ((DefaultMutableTreeNode) node).getUserObject();
                Object[] cr = rn.rw;
                return SF.format((Date) cr[0]);
            } else {
                return "";
            }
        }

        public boolean isHtmlDisplayName(Object node) {
            return false;
        }

        public Color getBackground(Object node) {
            return Color.WHITE;
        }

        public Color getForeground(Object node) {
            return Color.BLACK;
        }

        public String getTooltipText(Object node) {
            return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_History");
        }

        public Icon getIcon(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof RowNode) {
                return ImageUtilities.image2Icon(MESSAGE_IMAGE);
            } else {
                return null;
            }
        }
    }

    private class OutlineDynamic extends Outline {

        private final static long serialVersionUID = -2009100101L;
        private TreeModel treeMdl;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            treeMdl = new DefaultTreeModel(root, false);
            OutlineModel mdl = DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), true, NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Date"));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);
            setModel(mdl);

        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        private class NodeRowModel implements RowModel {

            public Class<?> getColumnClass(int column) {
                switch (column) {
                    default:
                        return String.class;
                }
            }

            public int getColumnCount() {
                return 5;
            }

            public String getColumnName(int column) {
                switch (column) {
                    case 0:
                        return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Event_Type");
                    case 1:
                        return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Service");
                    case 2:
                        return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Resource_name");
                    case 3:
                        return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Resource");
                    case 4:
                        return NbBundle.getMessage(HistoryPanelSupport.class, "LBL_Description");
                    default:
                        assert false;
                }
                return "";
            }

            public Object getValueFor(Object node, int column) {
                Object o = ((DefaultMutableTreeNode) node).getUserObject();
                if (o instanceof RowNode) {
                    RowNode rn = (RowNode) o;
                    Object[] cr = rn.rw;
                    switch (column) {
                        case 0:
                            return rn.rw[1];
                        case 1:
                            return rn.rw[2];
                        case 2:
                            return rn.rw[3];
                        case 3:
                            return rn.rw[4];
                        case 4:
                            return rn.rw[5];
                        default:
                            return "";
                    }
                } else if (o instanceof RootNode) {
                    switch (column) {
                        default:
                            return "";
                    }
                }

                return null;
            }

            public boolean isCellEditable(Object node, int column) {
                return false;
            }

            public void setValueFor(Object node, int column, Object value) {
            }
        }

        private void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    synchronized (row_cache) {
                        DefaultTreeModel dtm = (DefaultTreeModel) treeMdl;
                        root.removeAllChildren();
                        dtm.reload(root);
                        int showablerows = Math.min(rowsPerPage, row_cache.size());
                        for (int y = 0; y < showablerows; y++) {
                            Object[] ar = row_cache.get(offset - showablerows + y);
                            RowNode rn = new RowNode(ar);
                            DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rn);
                            root.add(rmtn);
                            int i = root.getChildCount();
                            dtm.nodesWereInserted(root, new int[]{i - 1});

                        }
                        expandRoot();
                    }
                }
            });

        }
    }

    private void expandRoot() {
        o.expandPath(new TreePath(root));
    }

    private synchronized void updateRowCache(List<Object[]> newCache) {
        synchronized (row_cache) {
            row_cache.clear();
            row_cache.addAll(newCache);
            offset = row_cache.size();
        }
        o.fillModel();
    }
}
