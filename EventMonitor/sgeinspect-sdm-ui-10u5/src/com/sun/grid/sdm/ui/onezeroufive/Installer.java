/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive;

import com.sun.grid.sdm.onezeroufive.jvm.JvmDataSourceImpl;
import com.sun.grid.sdm.ui.component.ca.CADataSourceViewProvider;
import com.sun.grid.sdm.ui.component.executor.ExecutorDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.general.GeneralComponentDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.reporter.ReporterDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.rp.ResourceProviderDataSourceViewProvider;
import com.sun.grid.sdm.ui.jvm.SDMSystemViewProvider;
import com.sun.grid.sdm.ui.navigator.NavigatorSupport;
import com.sun.grid.sdm.ui.onezeroufive.component.ca.CADataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.component.executor.ExecutorDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.component.general.GeneralComponentDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.component.reporter.ReporterDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.component.rp.ResourceProviderDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.jvm.SDMSystemViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.jvm.overview.ModulesOverviewPluginProvider;
import com.sun.grid.sdm.ui.onezeroufive.resource.CachedResourceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.resource.ResourceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.cloud.CloudServiceDataSourceResourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.cloud.CloudServiceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.ge.GEServiceDataSourceResourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.ge.GEServiceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.general.GeneralServiceDataSourceResourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.general.GeneralServiceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.spare_pool.SparePoolServiceDataSourceResourceViewFactoryImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.spare_pool.SparePoolServiceDataSourceViewFactoryImpl;
import com.sun.grid.sdm.ui.resource.CachedResourceDataSourceViewProvider;
import com.sun.grid.sdm.ui.resource.ResourceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.cloud.CloudServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.cloud.CloudServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.ge.GEServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.ge.GEServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.general.GeneralServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.general.GeneralServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceViewProvider;
import org.openide.modules.ModuleInstall;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    private final static long serialVersionUID = -2009102101L;

    @Override
    public void restored() {
        ModulesOverviewPluginProvider.initialize();
        SDMSystemViewProvider.getSharedInstance().registerCVDataSourceViewFactory(SDMSystemViewFactoryImpl.getInstance());

        CADataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(CADataSourceViewFactoryImpl.getInstance());
        ExecutorDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(ExecutorDataSourceViewFactoryImpl.getInstance());
        GeneralComponentDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(GeneralComponentDataSourceViewFactoryImpl.getInstance());
        ReporterDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(ReporterDataSourceViewFactoryImpl.getInstance());
        ResourceProviderDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(ResourceProviderDataSourceViewFactoryImpl.getInstance());

        GeneralServiceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(GeneralServiceDataSourceViewFactoryImpl.getInstance());
        CloudServiceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(CloudServiceDataSourceViewFactoryImpl.getInstance());
        GEServiceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(GEServiceDataSourceViewFactoryImpl.getInstance());
        SparePoolServiceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(SparePoolServiceDataSourceViewFactoryImpl.getInstance());

        GeneralServiceDataSourceResourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(GeneralServiceDataSourceResourceViewFactoryImpl.getInstance());
        CloudServiceDataSourceResourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(CloudServiceDataSourceResourceViewFactoryImpl.getInstance());
        GEServiceDataSourceResourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(GEServiceDataSourceResourceViewFactoryImpl.getInstance());
        SparePoolServiceDataSourceResourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(SparePoolServiceDataSourceResourceViewFactoryImpl.getInstance());

        ResourceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(ResourceDataSourceViewFactoryImpl.getInstance());
        CachedResourceDataSourceViewProvider.getSharedInstance().registerCVDataSourceViewFactory(CachedResourceDataSourceViewFactoryImpl.getInstance());

        NavigatorSupport.getInstance(JvmDataSourceImpl.class).initialize();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void uninstalled() {
        super.uninstalled();
        ModulesOverviewPluginProvider.shutdown();
        SDMSystemViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(SDMSystemViewFactoryImpl.getInstance());

        CADataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(CADataSourceViewFactoryImpl.getInstance());
        ExecutorDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(ExecutorDataSourceViewFactoryImpl.getInstance());
        GeneralComponentDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(GeneralComponentDataSourceViewFactoryImpl.getInstance());
        ReporterDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(ReporterDataSourceViewFactoryImpl.getInstance());
        ResourceProviderDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(ResourceProviderDataSourceViewFactoryImpl.getInstance());

        GeneralServiceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(GeneralServiceDataSourceViewFactoryImpl.getInstance());
        CloudServiceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(CloudServiceDataSourceViewFactoryImpl.getInstance());
        GEServiceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(GEServiceDataSourceViewFactoryImpl.getInstance());
        SparePoolServiceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(SparePoolServiceDataSourceViewFactoryImpl.getInstance());

        GeneralServiceDataSourceResourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(GeneralServiceDataSourceResourceViewFactoryImpl.getInstance());
        CloudServiceDataSourceResourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(CloudServiceDataSourceResourceViewFactoryImpl.getInstance());
        GEServiceDataSourceResourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(GEServiceDataSourceResourceViewFactoryImpl.getInstance());
        SparePoolServiceDataSourceResourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(SparePoolServiceDataSourceResourceViewFactoryImpl.getInstance());

        ResourceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(ResourceDataSourceViewFactoryImpl.getInstance());
        CachedResourceDataSourceViewProvider.getSharedInstance().unregisterCVDataSourceViewFactory(CachedResourceDataSourceViewFactoryImpl.getInstance());

        NavigatorSupport.getInstance(JvmDataSourceImpl.class).shutdown();
    }
}
