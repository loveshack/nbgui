/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.jvm;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent.DetailsView;
import org.openide.util.NbBundle;

public class SDMSystemView extends com.sun.grid.sdm.ui.jvm.SDMSystemView {

    private ResourceTypesTablePanel resource_types;
    private ResourcesTablePanel resources;

    public SDMSystemView(JvmDataSource jvm) {
        super(jvm);
    }

    @Override
    protected DataViewComponent createComponent() {
        DataViewComponent dvc = super.createComponent();
        dvc.addDetailsView(getResourceTypesView(), DataViewComponent.TOP_LEFT);
        return dvc;
    }

    protected DetailsView getResourceTypesView() {
        if (resource_types == null) {
            resource_types = new ResourceTypesTablePanel();
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(SDMSystemView.class, "Resource_Types"), NbBundle.getMessage(SDMSystemView.class, "System_Resource_Types"), 10, resource_types, null);
    }

    @Override
    protected DetailsView getResourcesView() {
        if (resources == null) {
            resources = new ResourcesTablePanel((JvmDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(SDMSystemView.class, "Resources"), NbBundle.getMessage(SDMSystemView.class, "System_Resources"), 11, resources, null);
    }
}
