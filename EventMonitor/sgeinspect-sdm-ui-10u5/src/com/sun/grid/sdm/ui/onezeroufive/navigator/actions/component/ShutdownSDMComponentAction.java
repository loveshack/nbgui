/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.navigator.actions.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.component.ComponentChangeResult;
import com.sun.grid.grm.ui.component.StopComponentCommand;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.libs.onezeroufive.SDMOneZeroUFive;
import com.sun.grid.sdm.onezeroufive.util.ExecEnvUtil;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;
import com.sun.grid.shared.main.util.ClassLoaderAnchor;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.application.Application;
import java.util.List;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class ShutdownSDMComponentAction extends RelaxedCookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/stop_16.png";
    private static final ShutdownSDMComponentAction SINGLETON = new ShutdownSDMComponentAction();

    public void performAction(Node[] activatedNodes) {
        final ComponentDataSource componentDataSource = activatedNodes[0].getLookup().lookup(ComponentDataSource.class);
        if (componentDataSource != null) {
            ClassLoaderAnchor.executeInSDMVersionedClassLoader(SDMOneZeroUFive.class, new ClassLoaderAnchor.CommandBlock<Void>() {

                public Void execute() {
                    StopComponentCommand scc = new StopComponentCommand();
                    scc.setComponentName(componentDataSource.getComponentName());
                    scc.setHostname(componentDataSource.getHostname());
                    ExecutionEnv env = ExecEnvUtil.getExecutionEnv((Application) componentDataSource.getOwner());
                    try {
                        List<ComponentChangeResult> ccr = env.getCommandService().execute(scc).getReturnValue();
                    } catch (GrmException ex) {
                        ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                    }
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(ShutdownSDMComponentAction.class, "MSG_ShutdownSDMComponentAction", new Object[] {componentDataSource.getComponentName(), componentDataSource.getHostname()}));
                    return null;
                }
            });
        }
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        boolean enable = super.enable(activatedNodes);
        if (enable) {
            ComponentDataSource componentDataSource = activatedNodes[0].getLookup().lookup(ComponentDataSource.class);
            if (componentDataSource != null) {
                return (componentDataSource.getComponentState().equals("STARTED"));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return NbBundle.getMessage(ShutdownSDMComponentAction.class, "CTL_ShutdownSDMComponentAction");
    }

    @Override
    protected String iconResource() {
        return ICON_PATH;
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{ComponentDataSource.class};
    }

    public static final ShutdownSDMComponentAction getInstance() {
        return SINGLETON;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

