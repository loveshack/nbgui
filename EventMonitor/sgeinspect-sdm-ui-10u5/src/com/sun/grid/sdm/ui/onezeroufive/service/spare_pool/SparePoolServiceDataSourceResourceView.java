/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.service.spare_pool;

import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.onezeroufive.service.spare_pool.SparePoolServiceDataSourceImpl;
import com.sun.grid.sdm.ui.onezeroufive.service.ServiceCachedResourcesTablePanel;
import com.sun.grid.sdm.ui.onezeroufive.service.ServiceResourcesTablePanel;
import com.sun.grid.sdm.ui.service.ServiceDataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent.DetailsView;
import org.openide.util.NbBundle;

public class SparePoolServiceDataSourceResourceView extends com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceResourceView {

    private ServiceResourcesTablePanel resources;
    private ServiceCachedResourcesTablePanel cached;

    public SparePoolServiceDataSourceResourceView(SparePoolServiceDataSourceImpl sds) {
        super(sds);
    }

    @Override
    protected void added() {
        // todo register details view to listen to service state changes
    }

    @Override
    protected void removed() {
        // todo unregister details view service listener
    }

    @Override
    protected DetailsView getServiceResourcesView() {
        if (resources == null) {
            resources = new ServiceResourcesTablePanel((ServiceDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ServiceDataSourceView.class, "Resources"),
                NbBundle.getMessage(ServiceDataSourceView.class, "Service_Resources"), 11, resources, null);
    }

    @Override
    protected DetailsView getServiceCachedResourcesView() {
        if (cached == null) {
            cached = new ServiceCachedResourcesTablePanel((ServiceDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ServiceDataSourceView.class, "Cached_Resources"),
                NbBundle.getMessage(ServiceDataSourceView.class, "Service_Cached_Resources"), 11, cached, null);
    }
}

