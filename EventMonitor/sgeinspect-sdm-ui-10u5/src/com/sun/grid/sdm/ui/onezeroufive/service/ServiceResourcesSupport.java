/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.service;

import com.sun.grid.sdm.ui.resource.*;
import com.sun.grid.sdm.api.resource.BaseResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ServiceResourcesSupport<T extends BaseResourceDataSource> {

    public static final String SELECTED_RESOURCE = "selected_resource";
    private final JScrollPane scroller;
    private ServiceDataSource rds;
    private Class<T> rt;
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private OutlineDynamic o;
    private final PCL pcl = new PCL();
    private final DRL drl = new DRL();
    private final DCL dcl = new DCL();
    private final Image resources_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/onezeroufive/resources/icon_resources_all.png");
    private final Image resource_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/onezeroufive/resources/icon_host_resource.png");
    private final Image resource_property_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/onezeroufive/resources/icon_host_resource_property.png");
    private boolean expandResources = false;
    private final Map<T, DefaultMutableTreeNode> rmap = new HashMap<T, DefaultMutableTreeNode>();
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final AtomicBoolean refilling = new AtomicBoolean(false);

    public ServiceResourcesSupport(ServiceDataSource c, Class<T> resourcetype) {
        this.rds = c;
        this.rt = resourcetype;
        this.rds.addPropertyChangeListener(pcl);
        this.rds.notifyWhenRemoved(drl);
        this.rds.getRepository().addDataChangeListener(dcl, resourcetype);
        this.o = new OutlineDynamic();
        this.scroller = new JScrollPane(o);
    }

    public Component getComponent() {
        return scroller;
    }

    public void setExpandResources(boolean expand) {
        expandResources = expand;
        if (expandResources) {
            expandResources();
        } else {
            collapseResources();
        }
        setupColumns();
    }

    @SuppressWarnings("unchecked")
    public T getSelectedResource() {
        DefaultMutableTreeNode dmtn = o.getSelectedNode();
        if (dmtn != null) {
            if (dmtn.getUserObject() instanceof BaseResourceDataSource) {
                return (T) dmtn.getUserObject();
            }
        }
        return null;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    private static class PropertyNode {

        final private String key;
        final private Object value;

        public PropertyNode(String k, Object v) {
            this.key = k;
            this.value = v;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PropertyNode other = (PropertyNode) obj;
            if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
                return false;
            }
            if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 61 * hash + (this.key != null ? this.key.hashCode() : 0);
            hash = 61 * hash + (this.value != null ? this.value.hashCode() : 0);
            return hash;
        }
    }

    private static class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        public String getDisplayName(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return ((BaseResourceDataSource) arg0).getId();
            } else {
                return "";
            }
        }

        public boolean isHtmlDisplayName(Object node) {
            return false;
        }

        public Color getBackground(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof PropertyNode) {
                return UIManager.getColor("control");
            } else {
                return Color.WHITE;
            }
        }

        public Color getForeground(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof PropertyNode) {
                return Color.BLACK;
            } else {
                return Color.DARK_GRAY;
            }
        }

        public String getTooltipText(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "show_hide");
            } else if (arg0 instanceof PropertyNode) {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "resource_property");
            } else {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "resource_details");
            }
        }

        public Icon getIcon(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return ImageUtilities.image2Icon(resource_image);
            } else if (arg0 instanceof PropertyNode) {
                return ImageUtilities.image2Icon(resource_property_image);
            } else if (arg0 instanceof RootNode) {
                return ImageUtilities.image2Icon(resources_image);
            } else {
                return null;
            }
        }
    }

    private class OutlineDynamic extends Outline {

        private final static long serialVersionUID = -2009100101L;
        private TreeModel treeMdl;
        private TableColumn kC;
        private TableColumn vC;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            setLayout(new BorderLayout());
            treeMdl = new DefaultTreeModel(root, false);
            OutlineModel mdl = DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), true, NbBundle.getMessage(ServiceResourcesSupport.class, "ID"));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);
            setModel(mdl);
            getSelectionModel().addListSelectionListener(new LSL());

            kC = getColumnModel().getColumn(7);
            vC = getColumnModel().getColumn(8);
        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        private class NodeRowModel implements RowModel {

            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 4:
                        return Integer.class;
                    default:
                        return String.class;
                }
            }

            public int getColumnCount() {
                return 8;
            }

            public String getColumnName(int column) {
                switch (column) {
                    case 0:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Name");
                    case 1:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Type");
                    case 2:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Static");
                    case 3:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Status");
                    case 4:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Usage");
                    case 5:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Annotation");
                    case 6:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Property_Name");
                    case 7:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Property_Value");
                    default:
                        assert false;
                }
                return "";
            }

            public Object getValueFor(Object node, int column) {
                Object o = ((DefaultMutableTreeNode) node).getUserObject();
                if (o instanceof BaseResourceDataSource) {
                    switch (column) {
                        case 0:
                            return ((BaseResourceDataSource) o).getName();
                        case 1:
                            return ((BaseResourceDataSource) o).getType();
                        case 2:
                            return String.valueOf(((BaseResourceDataSource) o).isStatic());
                        case 3:
                            return ((BaseResourceDataSource) o).getState();
                        case 4:
                            return ((BaseResourceDataSource) o).getUsageLevel();
                        case 5:
                            return ((BaseResourceDataSource) o).getAnnotation();
                        default:
                            return "";
                    }
                } else if (o instanceof RootNode) {
                    switch (column) {
                        default:
                            return "";
                    }
                } else if (o instanceof PropertyNode) {
                    switch (column) {
                        case 6:
                            return ((PropertyNode) o).key;
                        case 7:
                            if (((PropertyNode) o).value != null) {
                                return ((PropertyNode) o).value.toString();
                            } else {
                                return "null";
                            }
                        default:
                            return "";
                    }
                }

                return null;
            }

            public boolean isCellEditable(Object node, int column) {
                return column == 1;
            }

            public void setValueFor(Object node, int column, Object value) {
            }
        }

        private void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                @SuppressWarnings("unchecked")
                public void run() {
                    refilling.set(true);
                    DefaultTreeModel dtm = (DefaultTreeModel) treeMdl;
                    for (int x = 0; x < root.getChildCount(); x++) {
                        DefaultMutableTreeNode tn = (DefaultMutableTreeNode) root.getChildAt(x);
                        rmap.remove(((T) tn.getUserObject()));
                    }
                    root.removeAllChildren();
                    for (T ds : rds.getRepository().getDataSources(rt)) {
                        ds.notifyWhenRemoved(drl);
                        DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(ds);
                        root.add(rmtn);
                        int i = root.getChildCount();
//                        dtm.nodesWereInserted(root, new int[]{i - 1});
                        for (Map.Entry<String, Object> property : ds.getProperties().entrySet()) {
                            PropertyNode pn = new PropertyNode(property.getKey(), property.getValue());
                            DefaultMutableTreeNode pmtn = new DefaultMutableTreeNode(pn);
                            rmtn.add(pmtn);
//                            dtm.nodesWereInserted(rmtn, new int[]{rmtn.getChildCount() - 1});
                        }
                        rmap.put(ds, rmtn);
                    }
                    dtm.reload(root);
                    if (expandResources) {
                        expandResources();
                    } else {
                        expandRoot();
                    }
                    refilling.set(false);
                }
            });
        }
    }

    private void expandRoot() {
        o.expandPath(new TreePath(root));
        setupColumns();
    }

    private void expandResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            o.expandPath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void collapseResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            o.collapsePath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void setupColumns() {
        if (o.getColumnModel() instanceof ETableColumnModel) {
            ((ETableColumnModel) o.getColumnModel()).setColumnHidden(o.kC, !expandResources);
            ((ETableColumnModel) o.getColumnModel()).setColumnHidden(o.vC, !expandResources);
        }
    }

    private class DRL implements DataRemovedListener<DataSource> {

        public void dataRemoved(DataSource arg0) {
            if (arg0.equals(rds)) {
                rds.removePropertyChangeListener(pcl);
            }
            o.fillModel();
        }
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(final PropertyChangeEvent evt) {
            o.fillModel();
        }
    }

    private class DCL implements DataChangeListener<T> {

        public void dataChanged(final DataChangeEvent<T> arg0) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    refilling.set(true);
                    DefaultTreeModel dtm = (DefaultTreeModel) o.treeMdl;
                    if (!arg0.getAdded().isEmpty()) {
                        for (T ds : arg0.getAdded()) {
                            if (!rmap.containsKey(ds)) {
                                ds.notifyWhenRemoved(drl);
                                DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(ds);
                                root.add(rmtn);
                                int i = root.getChildCount();
//                                dtm.nodesWereInserted(root, new int[]{i - 1});
                                for (Map.Entry<String, Object> property : ds.getProperties().entrySet()) {
                                    PropertyNode pn = new PropertyNode(property.getKey(), property.getValue());
                                    DefaultMutableTreeNode pmtn = new DefaultMutableTreeNode(pn);
                                    rmtn.add(pmtn);
//                                    dtm.nodesWereInserted(rmtn, new int[]{rmtn.getChildCount() - 1});
                                }
                                rmap.put(ds, rmtn);
                            }
                        }
                    } else if (!arg0.getRemoved().isEmpty()) {
                        for (T ds : arg0.getRemoved()) {
                            if (rmap.containsKey(ds)) {
                                DefaultMutableTreeNode dmtn = rmap.remove(ds);
                                int index = root.getIndex(dmtn);
                                root.remove(dmtn);
                                dtm.nodesWereRemoved(root, new int[]{index}, new Object[]{dmtn});
                            }
                        }
                    }
                    if (expandResources) {
                        expandResources();
                    } else {
                        expandRoot();
                    }
                    dtm.reload(root);
                    refilling.set(false);
                }
            });
        }
    }

    private class LSL implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {
            if (!refilling.get() && !e.getValueIsAdjusting()) {
                pcs.firePropertyChange(SELECTED_RESOURCE, null, getSelectedResource());
            }
        }
    }
}
