/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.onezeroufive.service;

import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

public class ServiceResourcesTablePanel extends javax.swing.JPanel {

    private final static long serialVersionUID = -2009100101L;
    private final ServiceResourcesSupport<ResourceDataSource> tod;
    private final ServiceDataSource sds;
    private final Icon down_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/onezeroufive/resources/down_arrow.png"));
    private final Icon up_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/onezeroufive/resources/up_arrow.png"));

    /** Creates new form ModulesTablePanel
     * @param sds 
     */
    public ServiceResourcesTablePanel(ServiceDataSource sds) {
        initComponents();
        this.btnShowResource.setEnabled(false);
        this.sds = sds;
        this.tod = new ServiceResourcesSupport<ResourceDataSource>(sds, ResourceDataSource.class);
        this.tod.setExpandResources(tglExpandResources.isSelected());
        this.otlPanel.add(tod.getComponent());
        this.tod.addPropertyChangeListener(new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(ServiceResourcesSupport.SELECTED_RESOURCE)) {
                    btnShowResource.setEnabled(evt.getNewValue() != null);
                }
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        otlPanel = new javax.swing.JPanel();
        btnPanel = new javax.swing.JPanel();
        btnHide = new javax.swing.JToggleButton();
        tglPanel = new javax.swing.JPanel();
        tglExpandResources = new javax.swing.JToggleButton();
        btnShowResource = new javax.swing.JButton();

        setName("jPanel"); // NOI18N

        otlPanel.setLayout(new java.awt.BorderLayout());

        btnHide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/down_arrow.png"))); // NOI18N
        btnHide.setText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.btnHide.text")); // NOI18N
        btnHide.setToolTipText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.btnHide.toolTipText")); // NOI18N
        btnHide.setBorderPainted(false);
        btnHide.setContentAreaFilled(false);
        btnHide.setFocusPainted(false);
        btnHide.setMaximumSize(new java.awt.Dimension(32767, 32767));
        btnHide.setMinimumSize(new java.awt.Dimension(0, 0));
        btnHide.setPreferredSize(new java.awt.Dimension(100, 10));
        btnHide.setVerifyInputWhenFocusTarget(false);
        btnHide.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHideMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHideMouseExited(evt);
            }
        });
        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        tglExpandResources.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_host_resource_property.png"))); // NOI18N
        tglExpandResources.setMnemonic('S');
        tglExpandResources.setText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.tglExpandResources.text")); // NOI18N
        tglExpandResources.setToolTipText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.tglExpandResources.toolTipText")); // NOI18N
        tglExpandResources.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglExpandResourcesActionPerformed(evt);
            }
        });

        btnShowResource.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/onezeroufive/resources/icon_view_host_resource.png"))); // NOI18N
        btnShowResource.setMnemonic('D');
        btnShowResource.setText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.btnShowResource.text")); // NOI18N
        btnShowResource.setToolTipText(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.btnShowResource.toolTipText")); // NOI18N
        btnShowResource.setMaximumSize(new java.awt.Dimension(26, 26));
        btnShowResource.setMinimumSize(new java.awt.Dimension(26, 26));
        btnShowResource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowResourceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tglPanelLayout = new javax.swing.GroupLayout(tglPanel);
        tglPanel.setLayout(tglPanelLayout);
        tglPanelLayout.setHorizontalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(tglExpandResources)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnShowResource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(422, Short.MAX_VALUE))
        );
        tglPanelLayout.setVerticalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnShowResource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tglExpandResources))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tglExpandResources.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.tglExpandResources.AccessibleContext.accessibleName")); // NOI18N
        btnShowResource.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ServiceResourcesTablePanel.class, "ServiceResourcesTablePanel.btnShowResource.AccessibleContext.accessibleName")); // NOI18N

        javax.swing.GroupLayout btnPanelLayout = new javax.swing.GroupLayout(btnPanel);
        btnPanel.setLayout(btnPanelLayout);
        btnPanelLayout.setHorizontalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnHide, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
            .addComponent(tglPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        btnPanelLayout.setVerticalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnPanelLayout.createSequentialGroup()
                .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tglPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                    .addComponent(btnPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnHideMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseEntered
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseEntered

    private void btnHideMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseExited
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseExited

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        tglPanel.setVisible(!(tglPanel.isVisible()));
        if (tglPanel.isVisible()) {
            btnHide.setIcon(down_arrow);
        } else {
            btnHide.setIcon(up_arrow);
        }
}//GEN-LAST:event_btnHideActionPerformed

    private void tglExpandResourcesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglExpandResourcesActionPerformed
        tod.setExpandResources(tglExpandResources.isSelected());
    }//GEN-LAST:event_tglExpandResourcesActionPerformed

    private void btnShowResourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowResourceActionPerformed
        ResourceDataSource r = tod.getSelectedResource();
        if (r != null) {
            DataSourceWindowManager.sharedInstance().openDataSource(r);
        }
    }//GEN-LAST:event_btnShowResourceActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnHide;
    private javax.swing.JPanel btnPanel;
    private javax.swing.JButton btnShowResource;
    private javax.swing.JPanel otlPanel;
    private javax.swing.JToggleButton tglExpandResources;
    private javax.swing.JPanel tglPanel;
    // End of variables declaration//GEN-END:variables
}
