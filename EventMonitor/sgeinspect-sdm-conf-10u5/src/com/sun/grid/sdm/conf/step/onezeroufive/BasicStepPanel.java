/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.step.onezeroufive;

import javax.swing.JPanel;
import org.openide.util.NbBundle;

public final class BasicStepPanel extends JPanel {

    /** Creates new form SloVisualPanel1 */
    public BasicStepPanel() {
        initComponents();
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(BasicStepPanel.class, "ACTION_STEP_CONFIGURATION");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        cboStepConfig_SubType = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        lblStepConfig_Name = new javax.swing.JLabel();
        txtStepConfig_Name = new javax.swing.JTextField();
        lblStepConfig_MaxTries = new javax.swing.JLabel();
        lblStepConfig_IsolationLevel = new javax.swing.JLabel();
        lblStepConfig_LogLevel = new javax.swing.JLabel();
        lblStepConfig_Delay = new javax.swing.JLabel();
        lblStepConfig_Filter = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtStepConfig_MaxTries = new javax.swing.JTextField();
        txtStepConfig_IsolationLevel = new javax.swing.JTextField();
        cboStepConfig_LogLevel = new javax.swing.JComboBox();
        txtStepConfig_DelayValue = new javax.swing.JTextField();
        cboStepConfig_DelayUnit = new javax.swing.JComboBox();
        txtStepConfig_Filter = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblStepConfig_Param = new javax.swing.JTable();
        btnStepConfig_ParamAdd = new javax.swing.JButton();
        btnStepConfig_ParamRemove = new javax.swing.JButton();
        chkStepConfig_FilterIsSet = new javax.swing.JCheckBox();
        chkStepConfig_DelayIsSet = new javax.swing.JCheckBox();
        chkStepConfig_IsolationLevelIsSet = new javax.swing.JCheckBox();
        chkStepConfig_MaxTriesIsSet = new javax.swing.JCheckBox();
        chkStepConfig_LogLevelIsSet = new javax.swing.JCheckBox();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setPreferredSize(new java.awt.Dimension(397, 342));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.jLabel1.text")); // NOI18N

        cboStepConfig_SubType.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.cboStepConfig_SubType.toolTipText")); // NOI18N

        jLabel2.setLabelFor(cboStepConfig_SubType);
        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.jLabel2.text")); // NOI18N
        jLabel2.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.jLabel2.toolTipText")); // NOI18N

        lblStepConfig_Name.setLabelFor(txtStepConfig_Name);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_Name, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Name.text")); // NOI18N
        lblStepConfig_Name.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Name.toolTipText")); // NOI18N

        txtStepConfig_Name.setText(org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_Name.text")); // NOI18N
        txtStepConfig_Name.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_Name.toolTipText")); // NOI18N

        lblStepConfig_MaxTries.setLabelFor(chkStepConfig_MaxTriesIsSet);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_MaxTries, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_MaxTries.text")); // NOI18N
        lblStepConfig_MaxTries.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_MaxTries.toolTipText")); // NOI18N

        lblStepConfig_IsolationLevel.setLabelFor(chkStepConfig_IsolationLevelIsSet);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_IsolationLevel, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_IsolationLevel.text")); // NOI18N
        lblStepConfig_IsolationLevel.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_IsolationLevel.toolTipText")); // NOI18N

        lblStepConfig_LogLevel.setLabelFor(cboStepConfig_LogLevel);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_LogLevel, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_LogLevel.text")); // NOI18N
        lblStepConfig_LogLevel.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_LogLevel.toolTipText")); // NOI18N

        lblStepConfig_Delay.setLabelFor(chkStepConfig_DelayIsSet);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_Delay, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Delay.text")); // NOI18N
        lblStepConfig_Delay.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Delay.toolTipText")); // NOI18N

        lblStepConfig_Filter.setLabelFor(chkStepConfig_FilterIsSet);
        org.openide.awt.Mnemonics.setLocalizedText(lblStepConfig_Filter, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Filter.text")); // NOI18N
        lblStepConfig_Filter.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.lblStepConfig_Filter.toolTipText")); // NOI18N

        jLabel9.setLabelFor(tblStepConfig_Param);
        org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.jLabel9.text")); // NOI18N
        jLabel9.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.jLabel9.toolTipText")); // NOI18N

        txtStepConfig_MaxTries.setText(org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_MaxTries.text")); // NOI18N
        txtStepConfig_MaxTries.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_MaxTries.toolTipText")); // NOI18N

        txtStepConfig_IsolationLevel.setText(org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_IsolationLevel.text")); // NOI18N
        txtStepConfig_IsolationLevel.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_IsolationLevel.toolTipText")); // NOI18N

        cboStepConfig_LogLevel.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.cboStepConfig_LogLevel.toolTipText")); // NOI18N

        txtStepConfig_DelayValue.setText(org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_DelayValue.text")); // NOI18N
        txtStepConfig_DelayValue.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_DelayValue.toolTipText")); // NOI18N

        cboStepConfig_DelayUnit.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.cboStepConfig_DelayUnit.toolTipText")); // NOI18N

        txtStepConfig_Filter.setText(org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_Filter.text")); // NOI18N
        txtStepConfig_Filter.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.txtStepConfig_Filter.toolTipText")); // NOI18N

        tblStepConfig_Param.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblStepConfig_Param.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.tblStepConfig_Param.toolTipText")); // NOI18N
        jScrollPane2.setViewportView(tblStepConfig_Param);

        org.openide.awt.Mnemonics.setLocalizedText(btnStepConfig_ParamAdd, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.btnStepConfig_ParamAdd.text")); // NOI18N
        btnStepConfig_ParamAdd.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.btnStepConfig_ParamAdd.toolTipText")); // NOI18N
        btnStepConfig_ParamAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStepConfig_ParamAddActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(btnStepConfig_ParamRemove, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.btnStepConfig_ParamRemove.text")); // NOI18N
        btnStepConfig_ParamRemove.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.btnStepConfig_ParamRemove.toolTipText")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(chkStepConfig_FilterIsSet, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_FilterIsSet.text")); // NOI18N
        chkStepConfig_FilterIsSet.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_FilterIsSet.toolTipText")); // NOI18N
        chkStepConfig_FilterIsSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStepConfig_FilterIsSetActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(chkStepConfig_DelayIsSet, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_DelayIsSet.text")); // NOI18N
        chkStepConfig_DelayIsSet.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_DelayIsSet.toolTipText")); // NOI18N
        chkStepConfig_DelayIsSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStepConfig_DelayIsSetActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(chkStepConfig_IsolationLevelIsSet, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_IsolationLevelIsSet.text")); // NOI18N
        chkStepConfig_IsolationLevelIsSet.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_IsolationLevelIsSet.toolTipText")); // NOI18N
        chkStepConfig_IsolationLevelIsSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStepConfig_IsolationLevelIsSetActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(chkStepConfig_MaxTriesIsSet, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_MaxTriesIsSet.text")); // NOI18N
        chkStepConfig_MaxTriesIsSet.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_MaxTriesIsSet.toolTipText")); // NOI18N
        chkStepConfig_MaxTriesIsSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStepConfig_MaxTriesIsSetActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(chkStepConfig_LogLevelIsSet, org.openide.util.NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_LogLevelIsSet.text")); // NOI18N
        chkStepConfig_LogLevelIsSet.setToolTipText(NbBundle.getMessage(BasicStepPanel.class, "BasicStepPanel.chkStepConfig_LogLevelIsSet.toolTipText")); // NOI18N
        chkStepConfig_LogLevelIsSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStepConfig_LogLevelIsSetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(chkStepConfig_LogLevelIsSet)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(chkStepConfig_FilterIsSet)
                                .addComponent(chkStepConfig_IsolationLevelIsSet)
                                .addComponent(chkStepConfig_MaxTriesIsSet)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblStepConfig_LogLevel)
                            .addComponent(lblStepConfig_MaxTries)
                            .addComponent(lblStepConfig_IsolationLevel)
                            .addComponent(lblStepConfig_Filter)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chkStepConfig_DelayIsSet)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStepConfig_Delay))
                    .addComponent(lblStepConfig_Name)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtStepConfig_Filter, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(txtStepConfig_MaxTries, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(txtStepConfig_IsolationLevel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(txtStepConfig_Name, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(cboStepConfig_SubType, javax.swing.GroupLayout.Alignment.LEADING, 0, 251, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtStepConfig_DelayValue, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboStepConfig_DelayUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cboStepConfig_LogLevel, 0, 251, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 210, Short.MAX_VALUE)
                .addComponent(btnStepConfig_ParamRemove)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnStepConfig_ParamAdd)
                .addGap(12, 12, 12))
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel1)
                .addContainerGap(221, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboStepConfig_SubType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStepConfig_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStepConfig_Name))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblStepConfig_LogLevel)
                        .addComponent(cboStepConfig_LogLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(chkStepConfig_LogLevelIsSet))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(chkStepConfig_MaxTriesIsSet)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkStepConfig_IsolationLevelIsSet)
                        .addGap(8, 8, 8)
                        .addComponent(chkStepConfig_DelayIsSet)
                        .addGap(8, 8, 8))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtStepConfig_MaxTries, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStepConfig_MaxTries))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtStepConfig_IsolationLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStepConfig_IsolationLevel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtStepConfig_DelayValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboStepConfig_DelayUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStepConfig_Delay))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStepConfig_Filter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkStepConfig_FilterIsSet)
                    .addComponent(lblStepConfig_Filter))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStepConfig_ParamAdd)
                    .addComponent(btnStepConfig_ParamRemove)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnStepConfig_ParamAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStepConfig_ParamAddActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnStepConfig_ParamAddActionPerformed

    private void chkStepConfig_FilterIsSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStepConfig_FilterIsSetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStepConfig_FilterIsSetActionPerformed

    private void chkStepConfig_DelayIsSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStepConfig_DelayIsSetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStepConfig_DelayIsSetActionPerformed

    private void chkStepConfig_IsolationLevelIsSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStepConfig_IsolationLevelIsSetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStepConfig_IsolationLevelIsSetActionPerformed

    private void chkStepConfig_MaxTriesIsSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStepConfig_MaxTriesIsSetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStepConfig_MaxTriesIsSetActionPerformed

    private void chkStepConfig_LogLevelIsSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStepConfig_LogLevelIsSetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStepConfig_LogLevelIsSetActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnStepConfig_ParamAdd;
    public javax.swing.JButton btnStepConfig_ParamRemove;
    public javax.swing.JComboBox cboStepConfig_DelayUnit;
    public javax.swing.JComboBox cboStepConfig_LogLevel;
    public javax.swing.JComboBox cboStepConfig_SubType;
    public javax.swing.JCheckBox chkStepConfig_DelayIsSet;
    public javax.swing.JCheckBox chkStepConfig_FilterIsSet;
    public javax.swing.JCheckBox chkStepConfig_IsolationLevelIsSet;
    public javax.swing.JCheckBox chkStepConfig_LogLevelIsSet;
    public javax.swing.JCheckBox chkStepConfig_MaxTriesIsSet;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    public javax.swing.JLabel lblStepConfig_Delay;
    public javax.swing.JLabel lblStepConfig_Filter;
    public javax.swing.JLabel lblStepConfig_IsolationLevel;
    public javax.swing.JLabel lblStepConfig_LogLevel;
    public javax.swing.JLabel lblStepConfig_MaxTries;
    public javax.swing.JLabel lblStepConfig_Name;
    public javax.swing.JTable tblStepConfig_Param;
    public javax.swing.JTextField txtStepConfig_DelayValue;
    public javax.swing.JTextField txtStepConfig_Filter;
    public javax.swing.JTextField txtStepConfig_IsolationLevel;
    public javax.swing.JTextField txtStepConfig_MaxTries;
    public javax.swing.JTextField txtStepConfig_Name;
    // End of variables declaration//GEN-END:variables
}

