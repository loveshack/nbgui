/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.confgui.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfComplexType;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.shared.ui.confgui.ConfElement;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;

public class ConfTimeIntervalType extends ConfComplexType {

    private final String CBO_UNIT_MODEL_NAME;
    private final String CBO_UNIT_NAME;
    private final String TXT_VALUE_NAME;

    public ConfTimeIntervalType(Class parent, String namePrefix, String name,boolean isRequired) throws UnexpectedStructureException {
        super(parent, TimeInterval.class, namePrefix, name, isRequired, false);
        TXT_VALUE_NAME = ConfElement.PREFIX_TEXTFIELD_COMPONENT + this.getFullName() + "Value";
        CBO_UNIT_NAME = ConfElement.PREFIX_COMBOBOX_COMPONENT + this.getFullName() + "Unit";
        CBO_UNIT_MODEL_NAME = CBO_UNIT_NAME + ConfElement.SUFFIX_COMPONENT_MODEL;
    }

    @Override
    public void outline(int deep) {
        System.out.println(getIntention(deep) + "TimeInterval: " + getFullName() + "(" + TimeInterval.class.getName() + ")");
        System.out.println(getIntention(deep + 1) + "It consists of a (value<" + TXT_VALUE_NAME + ":int>,unit<" + CBO_UNIT_NAME + ":String>) pair");
    }

    @Override
    public void putConfInGui(WizardDescriptor wd, Preferences pref, Object confObject) throws UnexpectedStructureException {
        TimeInterval timeUnit = (TimeInterval) confObject;
        pref.put(TXT_VALUE_NAME, "" + timeUnit.getValue());
        pref.put(CBO_UNIT_NAME, timeUnit.getUnit());
    }

    @Override
    public Object getConfFromGui(WizardDescriptor wd) throws UnexpectedStructureException {
        try {
            TimeInterval newTimeUnit = new TimeInterval();
            Object value = wd.getProperty(TXT_VALUE_NAME);
            if(value==null){
                value ="-1";
            }
            newTimeUnit.setValue(Integer.parseInt(value.toString()));
            
            value = wd.getProperty(CBO_UNIT_NAME);
            if(value==null){
                value =TimeIntervalHelper.SECONDS;
            }
            newTimeUnit.setUnit(value.toString());
            return newTimeUnit;
        } catch (Exception ex) {
            throw new UnexpectedStructureException(ex.getMessage());
        }
    }

    @Override
    public void storeModels(WizardDescriptor wd) throws UnexpectedStructureException {
        wd.putProperty(CBO_UNIT_MODEL_NAME, new ConfTimeIntervalTypeComboBoxModel());
    }
}
