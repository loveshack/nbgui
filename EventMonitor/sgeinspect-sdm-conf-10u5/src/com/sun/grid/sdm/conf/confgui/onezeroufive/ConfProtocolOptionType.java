/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.confgui.onezeroufive;

import com.sun.grid.grm.config.gef.ProtocolOption;
import com.sun.grid.shared.ui.confgui.ConfComplexType;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import org.openide.util.NbBundle;

class ConfProtocolOptionType extends ConfComplexType {

    ConfProtocolOptionType(Class parent, String namePrefix, String name, boolean isRequired) throws UnexpectedStructureException {
        super(parent, ProtocolOption.class, namePrefix, name, isRequired, false);
    }

    @Override
    protected void outline(int deep) {
        System.out.println(getIntention(deep) + "ProtocolOption: " + getFullName() + "(" + ProtocolOption.class.getName() + ")");
        System.out.println(getIntention(deep + 1) + "It consists of: three log levels");
    }

    @Override
    protected String getSingleStringRepresentation(Object obj) {
        ProtocolOption pO = (ProtocolOption) obj;
        if (pO == null) {
            return NbBundle.getMessage(ConfProtocolOptionType.class, "NOT_SET");
        }
        return "[Script:" + pO.getScriptLogLevel().value() + "]" +
                "[StdOut:" + pO.getStdoutLogLevel().value() + "]" +
                "[StdErr:" + pO.getStderrLogLevel().value() + "]";
    }
}

