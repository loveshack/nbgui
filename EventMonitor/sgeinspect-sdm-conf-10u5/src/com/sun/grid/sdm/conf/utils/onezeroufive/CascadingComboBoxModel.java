/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.utils.onezeroufive;

import java.util.HashMap;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * The CascadingComboBoxModel allows to couple two compoBox models.
 * Changing the selection of this model will change the selection of a second cascaded model
 * It is used in the following way:
 * CascadingComboBoxModel mainModel = new CascadingComboBoxModel();
 * CascadingComboBoxModel cascadedModel = mainModel.getCascadedModel();
 *
 * Element should be added to the models with addCascadedModelElement(Object modelName, Object element)
 *
 * The element will be added to a model that can be chosen by the main model by selecting modelName
 */
public class CascadingComboBoxModel implements ComboBoxModel {

    private DefaultComboBoxModel mainModel = new DefaultComboBoxModel();
    private HashMap<Object, DefaultComboBoxModel> cascadedModels = new HashMap<Object, DefaultComboBoxModel>();
    private volatile CascadingComboBoxModel cascadedModel = null;

    @Override
    public void setSelectedItem(Object anObject) {
        mainModel.setSelectedItem(anObject);
        DefaultComboBoxModel newCascaded = cascadedModels.get(anObject);
        getCascadedModel().mainModel.removeAllElements();
        // can not exchange the references to the models directly but copy // because of attached listeners
        if(newCascaded!=null){
            for(int i=0;i<newCascaded.getSize();i++){
                getCascadedModel().mainModel.addElement(newCascaded.getElementAt(i));
            }
        }
    }

    public Object getSelectedItem() {
        return mainModel.getSelectedItem();
    }

    public void addElement(Object anObject) {
        addCascadedModelElement(anObject, (Object) null);
    }

    public void addCascadedModelElement(Object modelName, Object element) {
        if (!contains(modelName)) {
            mainModel.addElement(modelName);
            cascadedModels.put(modelName, new DefaultComboBoxModel());
        }
        if (element != null) {
            cascadedModels.get(modelName).addElement(element);
        }
    }

    public CascadingComboBoxModel getCascadedModel() {
        if (cascadedModel == null) {
            cascadedModel = new CascadingComboBoxModel();
        }
        return cascadedModel;
    }

    public void addListDataListener(ListDataListener l) {
        mainModel.addListDataListener(l);
    }

    public Object getElementAt(int index) {
        return mainModel.getElementAt(index);
    }

    public int getSize() {
        return mainModel.getSize();
    }

    public void removeListDataListener(ListDataListener l) {
        mainModel.removeListDataListener(l);
    }

    public boolean contains(Object value) {
        return this.cascadedModels.containsKey(value);
    }
}
