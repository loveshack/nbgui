/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.slo.onezeroufive;

import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.config.common.MinResourceSLOConfig;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.SubClassEntry;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import org.openide.util.NbBundle;
//import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;

public class SloConfigHelper extends ConfHelper<SLOConfig> {
    public static final String SLO_DESCRIPTION_FIXED_USAGE = NbBundle.getMessage(SloConfigHelper.class, "FIXED_USAGE_SLO");
    public static final String SLO_DESCRIPTION_MIN_RESOURCE = NbBundle.getMessage(SloConfigHelper.class, "MIN_RESOURCE_SLO");
    public static final String SLO_DESCRIPTION_PERMANENT_REQUEST = NbBundle.getMessage(SloConfigHelper.class, "PERMANENT_REQUEST_SLO");
    public static final String SLO_DESCRIPTION_MAX_PENDING_JOBS = NbBundle.getMessage(SloConfigHelper.class, "MAX_PENDING_JOBS_SLO");

    public static final SubClassEntry SUB_CLASS_ENTRY_FIXED_USAGE = new SubClassEntry(FixedUsageSLOConfig.class, SLO_DESCRIPTION_FIXED_USAGE);
    public static final SubClassEntry SUB_CLASS_ENTRY_MIN_RESOURCE = new SubClassEntry(MinResourceSLOConfig.class,SLO_DESCRIPTION_MIN_RESOURCE);
    public static final SubClassEntry SUB_CLASS_ENTRY_PERMANENT_REQUEST = new SubClassEntry(PermanentRequestSLOConfig.class, SLO_DESCRIPTION_PERMANENT_REQUEST);
    public static final SubClassEntry SUB_CLASS_ENTRY_MAX_PENDING_JOBS = new SubClassEntry(MaxPendingJobsSLOConfig.class, SLO_DESCRIPTION_MAX_PENDING_JOBS);

    public SloConfigHelper(String wizardTitle) throws UnexpectedStructureException {
        super(SloWizardIterator.class, SLOConfig.class, SLOConfig.class.getSimpleName(), getDefaultConfObjects(), wizardTitle);
    }

    private static SLOConfig[] getDefaultConfObjects() throws UnexpectedStructureException {
        FixedUsageSLOConfig fixedUsageSloConfig = new FixedUsageSLOConfig();
        setDefaultSLOConfigValues(fixedUsageSloConfig, "FixedUsageSLO");

        PermanentRequestSLOConfig permanentRequestSLOConfig = new PermanentRequestSLOConfig();
        setDefaultSLOConfigValues(permanentRequestSLOConfig, "PermanentRequestSLO");
        permanentRequestSLOConfig.setQuantity(22);

        MinResourceSLOConfig minResourceSLOConfig = new MinResourceSLOConfig();
        setDefaultSLOConfigValues(minResourceSLOConfig, "MinResourceSLO");
        minResourceSLOConfig.setMin(11);

        MaxPendingJobsSLOConfig maxPendingJobsSLOConfig = new MaxPendingJobsSLOConfig();
        setDefaultSLOConfigValues(maxPendingJobsSLOConfig, "MaxPendingJobsSLO");
        maxPendingJobsSLOConfig.setAverageSlotsPerHost(1);
        maxPendingJobsSLOConfig.setJobFilter("-");
        maxPendingJobsSLOConfig.setMax(1);
        TimeInterval ti = new TimeInterval();
        ti.setValue(10);
        maxPendingJobsSLOConfig.setMaxWaitTimeForJobs(ti);

        return new SLOConfig[]{};//maxPendingJobsSLOConfig, permanentRequestSLOConfig, minResourceSLOConfig,fixedUsageSloConfig};
    }

    private static void setDefaultSLOConfigValues(SLOConfig sloConfig, String name) {
        sloConfig.setName(name);
        //sloConfig.setRequest("-");
        //sloConfig.setResourceFilter("-");
        //sloConfig.setUrgency(1);
        //sloConfig.setUsage(1);
    }

}
