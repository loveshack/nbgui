/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.protocoloption.onezeroufive;

import com.sun.grid.grm.config.gef.LogLevel;
import com.sun.grid.grm.config.gef.ProtocolOption;
import com.sun.grid.shared.ui.confgui.ConfHelper;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;

public class ProtocolOptionConfigHelper extends ConfHelper<ProtocolOption> {
    public ProtocolOptionConfigHelper(String wizardTitle) throws UnexpectedStructureException {
        super(ProtocolOptionWizardIterator.class,ProtocolOption.class,ProtocolOption.class.getSimpleName(),getDefaultProtocolOption(),wizardTitle);
    }

    private static ProtocolOption[] getDefaultProtocolOption(){
        ProtocolOption defaultProtocolOption = new ProtocolOption();
        defaultProtocolOption.setScriptLogLevel(LogLevel.DEBUG);
        defaultProtocolOption.setScriptLogLevel(LogLevel.ERROR);
        defaultProtocolOption.setScriptLogLevel(LogLevel.INFO);
        return new ProtocolOption[]{defaultProtocolOption};
    }
}
