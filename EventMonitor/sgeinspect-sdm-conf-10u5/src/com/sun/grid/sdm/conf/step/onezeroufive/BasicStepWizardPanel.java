/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.step.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.grid.sdm.conf.parameter.onezeroufive.ParameterConfigHelper;
import java.awt.Component;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;


public class BasicStepWizardPanel extends ConfWizardPanel<BasicStepPanel>  {
    private ParameterConfigHelper parameterConfigHelper;
    public BasicStepWizardPanel(){
        super(new BasicStepPanel());
        try {
            parameterConfigHelper = new ParameterConfigHelper(NbBundle.getMessage(BasicStepWizardPanel.class, "STEP_PARAMETER"));
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

   
    @Override
    protected void setupActionListeners() {
        addEditTableActionListeners(parameterConfigHelper, panel.tblStepConfig_Param,panel.btnStepConfig_ParamAdd, panel.btnStepConfig_ParamRemove);
        setupCheckBoxControlledComponents(panel.chkStepConfig_DelayIsSet, new Component[]{
            panel.lblStepConfig_Delay,
            panel.txtStepConfig_DelayValue,
            panel.cboStepConfig_DelayUnit
        });
        setupCheckBoxControlledComponents(panel.chkStepConfig_FilterIsSet, new Component[]{
            panel.lblStepConfig_Filter,
            panel.txtStepConfig_Filter
        });
        setupCheckBoxControlledComponents(panel.chkStepConfig_IsolationLevelIsSet, new Component[]{
            panel.lblStepConfig_IsolationLevel,
            panel.txtStepConfig_IsolationLevel
        });

        setupCheckBoxControlledComponents(panel.chkStepConfig_LogLevelIsSet, new Component[]{
            panel.lblStepConfig_LogLevel,
            panel.cboStepConfig_LogLevel
        });
        setupCheckBoxControlledComponents(panel.chkStepConfig_MaxTriesIsSet, new Component[]{
            panel.lblStepConfig_MaxTries,
            panel.txtStepConfig_MaxTries,
        });
        this.validateNonEmpty(panel.lblStepConfig_Name, panel.txtStepConfig_Name);
        this.validatePositiveInteger(panel.lblStepConfig_MaxTries, panel.txtStepConfig_MaxTries);
        this.validateNonEmpty(panel.lblStepConfig_IsolationLevel, panel.txtStepConfig_IsolationLevel);
        this.validatePositiveInteger(panel.lblStepConfig_Delay, panel.txtStepConfig_DelayValue);
        this.validateNonEmpty(panel.lblStepConfig_Filter, panel.txtStepConfig_Filter);
        
    }


}

