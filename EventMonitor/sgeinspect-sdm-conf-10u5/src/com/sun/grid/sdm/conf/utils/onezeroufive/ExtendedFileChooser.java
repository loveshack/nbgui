/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sdm.conf.utils.onezeroufive;


import java.awt.Component;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.openide.util.NbBundle;


public class ExtendedFileChooser {

    Component component;

    public ExtendedFileChooser(Component component) {
        this.component = component;
    }
    private final JFileChooser fileChooser = new JFileChooser();

    public void addActionListener(final JButton button, final JTextField txt, final JLabel label, final int mode) {
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startFileChooser(txt, label.getText(), mode);
            }
        });
    }

    private void startFileChooser(JTextField txt, String title, int mode) {
        fileChooser.setDialogTitle(NbBundle.getMessage(ExtendedFileChooser.class, "PLEASE_CHOOSE_THE_") + title);
        fileChooser.setFileSelectionMode(mode);
        String old = txt.getText().trim();
        if (!"".equals(old)) {
            fileChooser.setSelectedFile(new File(old));
        }
        int returnVal = fileChooser.showOpenDialog(this.component);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            txt.setText(file.getAbsolutePath());
        } else {
            //cancel
        }
    }
}
