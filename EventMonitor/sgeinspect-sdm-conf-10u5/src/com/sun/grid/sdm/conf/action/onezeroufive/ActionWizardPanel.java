/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.action.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.grid.sdm.conf.parameter.onezeroufive.ParameterConfigHelper;
import com.sun.grid.sdm.conf.protocoloption.onezeroufive.ProtocolOptionConfigHelper;
import com.sun.grid.sdm.conf.step.onezeroufive.StepConfigHelper;
import java.awt.Component;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ActionWizardPanel extends ConfWizardPanel<ActionVisualPanel> {

    private ProtocolOptionConfigHelper onSuccessPOConfHelper;
    private ProtocolOptionConfigHelper onErrorPOConfHelper;
    private ParameterConfigHelper parameterConfigHelper;
    private StepConfigHelper stepConfigHelper;

    public ActionWizardPanel() {
        super(new ActionVisualPanel());
        try {
            this.onSuccessPOConfHelper = new ProtocolOptionConfigHelper(NbBundle.getMessage(ActionWizardPanel.class, "ON_SUCCESS"));
            this.onErrorPOConfHelper = new ProtocolOptionConfigHelper(NbBundle.getMessage(ActionWizardPanel.class, "ON_ERRROR"));
            this.parameterConfigHelper = new ParameterConfigHelper(NbBundle.getMessage(ActionWizardPanel.class, "ACTION_PARAMETER"));
            this.stepConfigHelper= new StepConfigHelper(NbBundle.getMessage(ActionWizardPanel.class, "ACTION_STEP"));
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public void setupActionListeners() {
        this.addStartSubWizardActionListener(onSuccessPOConfHelper, panel.txtActionConfig_Protocol_OnSuccess, panel.btnActionConfig_Protocol_OnSuccess);
        this.addStartSubWizardActionListener(onErrorPOConfHelper, panel.txtActionConfig_Protocol_OnError, panel.btnActionConfig_Protocol_OnError);
        this.addEditTableActionListeners(parameterConfigHelper, panel.tblActionConfig_Param, panel.btnActionConfig_ParamAdd, panel.btnActionConfig_ParamRemove);
        this.addEditTableActionListeners(stepConfigHelper, panel.tblActionConfig_Step, panel.btnActionConfig_StepAdd, panel.btnActionConfig_StepRemove);
         this.setupCheckBoxControlledComponents(panel.chkActionConfig_ExecuteAsIsSet, new Component[]{
                panel.lblActionConfig_ExecuteAs,
                panel.txtActionConfig_ExecuteAs
        });
        this.setupCheckBoxControlledComponents(panel.chkActionConfig_ProtocolIsSet, new Component[]{
                panel.lblActionConfig_Protocol_OnError,
                panel.lblActionConfig_Protocol_OnSuccess,
                panel.txtActionConfig_Protocol_OnError,
                panel.txtActionConfig_Protocol_OnSuccess,
                panel.btnActionConfig_Protocol_OnError,
                panel.btnActionConfig_Protocol_OnSuccess
        });
        this.validateNonEmptyNonWhiteSpace(panel.lblActionConfig_ExecuteAs, panel.txtActionConfig_ExecuteAs);
        this.registerTableSortButtons(panel.tblActionConfig_Step,panel.btnActionConfig_StepUp, panel.btnActionConfig_StepDown);
    }
}
