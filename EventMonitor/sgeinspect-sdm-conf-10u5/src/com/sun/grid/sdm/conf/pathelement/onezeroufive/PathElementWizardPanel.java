/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.pathelement.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.sdm.conf.utils.onezeroufive.ExtendedFileChooser;
import javax.swing.JFileChooser;

/**
 *
 */
public class PathElementWizardPanel extends ConfWizardPanel<PathElementVisualPanel> {

    private ExtendedFileChooser fc;

    public PathElementWizardPanel() {
        super(new PathElementVisualPanel());
    }

    @Override
    public void setupActionListeners() {
        if (fc == null) {
            fc = new ExtendedFileChooser(panel);
            fc.addActionListener(panel.btnPathElementConfig,
                    panel.txtPathElementConfig,
                    panel.lblPathElementConfig,
                    JFileChooser.FILES_AND_DIRECTORIES);
        }
        this.validatePath(panel.lblPathElementConfig, panel.txtPathElementConfig);
    }
}
