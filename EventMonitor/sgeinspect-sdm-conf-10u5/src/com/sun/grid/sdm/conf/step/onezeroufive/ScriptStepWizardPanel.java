/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.conf.step.onezeroufive;

import com.sun.grid.shared.ui.confgui.ConfWizardPanel;
import com.sun.grid.sdm.conf.utils.onezeroufive.ExtendedFileChooser;
import java.awt.Component;
import javax.swing.JFileChooser;
public class ScriptStepWizardPanel extends ConfWizardPanel<ScriptStepPanel>  {
   private ExtendedFileChooser fc;


    public ScriptStepWizardPanel(){
        super(new ScriptStepPanel());
    }
       
    @Override
    public void setupActionListeners() {
        if (fc == null) {
            fc = new ExtendedFileChooser(panel);
            
            fc.addActionListener(panel.btnStepConfig_Script_File,
                    panel.txtStepConfig_Script_File,
                    panel.lblStepConfig_Script_File,
                    JFileChooser.FILES_ONLY);

            fc.addActionListener(panel.btnStepConfig_Undo_File,
                    panel.txtStepConfig_Undo_File,
                    panel.lblStepConfig_Undo_File,
                    JFileChooser.FILES_ONLY);
        }

        this.setupCheckBoxControlledComponents(panel.chkStepConfig_ExecuteAsIsSet, new Component[]{
            panel.lblStepConfig_ExecuteAs,
            panel.txtStepConfig_ExecuteAs
        });
        this.setupCheckBoxControlledComponents(panel.chkStepConfig_Script_TimeoutIsSet, new Component[]{
            panel.lblStepConfig_Script_Timeout,
            panel.txtStepConfig_Script_TimeoutValue,
            panel.cboStepConfig_Script_TimeoutUnit
        });
        this.setupCheckBoxControlledComponents(panel.chkStepConfig_UndoIsSet, new Component[]{
            panel.lblStepConfig_Undo_File,
            panel.txtStepConfig_Undo_File,
            panel.lblStepConfig_Undo_Timeout,
            panel.txtStepConfig_Undo_TimeoutValue,
            panel.cboStepConfig_Undo_TimeoutUnit,
            panel.chkStepConfig_Undo_TimeoutIsSet,
            panel.btnStepConfig_Undo_File
        });
        this.setupCheckBoxControlledComponents(panel.chkStepConfig_Undo_TimeoutIsSet, new Component[]{
            panel.lblStepConfig_Undo_Timeout,
            panel.txtStepConfig_Undo_TimeoutValue,
            panel.cboStepConfig_Undo_TimeoutUnit
        });

        this.validateNonEmptyNonWhiteSpace(panel.lblStepConfig_ExecuteAs,panel.txtStepConfig_ExecuteAs);
        this.validatePath(panel.lblStepConfig_Script_File,panel.txtStepConfig_Script_File);
        this.validatePositiveInteger(panel.lblStepConfig_Script_Timeout,panel.txtStepConfig_Script_TimeoutValue);
        this.validatePath(panel.lblStepConfig_Undo_File,panel.txtStepConfig_Undo_File);
        this.validatePositiveInteger(panel.lblStepConfig_Undo_Timeout,panel.txtStepConfig_Undo_TimeoutValue);





    }
}

