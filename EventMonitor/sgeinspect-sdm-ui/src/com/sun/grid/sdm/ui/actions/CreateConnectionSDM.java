/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.actions;

import com.sun.grid.sdm.api.base.SDMContainer;
import com.sun.grid.sdm.api.jvm.JvmDataSourceCreationException;
import com.sun.grid.sdm.api.jvm.JvmDataSourceFactory;
import com.sun.grid.sdm.api.jvm.JvmDataSourceFactoryLookup;
import com.sun.grid.sdm.api.jvm.JvmDataSourceVersionException;
import com.sun.grid.sdm.ui.actions.MonitorSystemDialog.SystemNode;
import com.sun.grid.sdm.ui.util.PreferencesUtil;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

public class CreateConnectionSDM {

    private static final String BUNDLE = "com.sun.grid.sdm.ui.actions.Bundle";
    private static final Logger log = Logger.getLogger(CreateConnectionSDM.class.getName(), BUNDLE);
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_sdm.png";
    private static MonitorSystemDialog dialog;
    private static final CreateConnectionSDMAction action = new CreateConnectionSDMAction();
    private static final CreateConnectionSDMDSAction dsAction = new CreateConnectionSDMDSAction();

    public static AbstractAction getAction() {
        return action;
    }

    public static SingleDataSourceAction<SDMContainer> getDSAction() {
        return dsAction;
    }

    private static void performAction(ActionEvent event) {
        if (dialog == null) {
            dialog = new MonitorSystemDialog(WindowManager.getDefault().getMainWindow(), true);
        }
        dialog.setVisible(true);
        if (dialog.getReturnStatus() == MonitorSystemDialog.RET_OK) {
            final SystemNode sn = dialog.getSelectedSystem();
            if (sn != null) {
                final JvmDataSourceFactory factory = JvmDataSourceFactoryLookup.getInstance().getFactory(dialog.getSelectedSystemVersion());
                final String csInfo = PreferencesUtil.getCSInfo(sn.getSystemName(), sn.getPreferencesType());
                if (csInfo != null) {
                    if (SDMContainer.sharedInstance().registerSDMSystemForApplication(sn.getSystemName(), csInfo, true)) {
                        try {
                            SDMContainer.sharedInstance().addSDMWrapperDS(factory.createJvmDataSource(sn.getSystemName(), sn.getPreferencesType().name(), "cs_vm", csInfo.split(":")[0], Integer.parseInt(csInfo.split(":")[1])));
                        } catch (JvmDataSourceCreationException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                            SDMContainer.sharedInstance().unregisterSDMSystemForApplication(sn.getSystemName(), csInfo);
                        } catch (JvmDataSourceVersionException ex) {
                            ErrorDisplayer.submitWarning(ex.getLocalizedMessage());
                            SDMContainer.sharedInstance().unregisterSDMSystemForApplication(sn.getSystemName(), csInfo);
                        }
                    } else {
                        // todo show message
                    }
                } else {
                    // todo show warning
                }
            } else {
                ErrorDisplayer.submitWarning(NbBundle.getMessage(CreateConnectionSDM.class, "LBL_No_system_selected"));
            }
        }
    }

    private static class CreateConnectionSDMAction extends AbstractAction {

        private static final long serialVersionUID = -2009040101L;

        private CreateConnectionSDMAction() {
            super();
            putValue(NAME, NbBundle.getMessage(CreateConnectionSDM.class, "CTL_CreateConnectionSDMAction"));
            putValue(SMALL_ICON, ImageUtilities.mergeImages(ImageUtilities.loadImage(ICON_PATH),
                    ImageUtilities.loadImage("com/sun/grid/sge/ui/resources/add_8.png"), 0, 0));  // NOI18N

            // Customize Toolbar dialog needs it
            putValue("iconBase", "");  // NOI18N
        }

        public void actionPerformed(ActionEvent event) {
            performAction(event);
        }
    }

    private static class CreateConnectionSDMDSAction extends SingleDataSourceAction<SDMContainer> {

        private static final long serialVersionUID = -2009040101L;

        private CreateConnectionSDMDSAction() {
            super(SDMContainer.class);
            putValue(NAME, NbBundle.getMessage(CreateConnectionSDM.class, "CTL_CreateConnectionSDMAction"));
            putValue(SMALL_ICON, ImageUtilities.mergeImages(ImageUtilities.loadImage(ICON_PATH),
                    ImageUtilities.loadImage("com/sun/grid/sge/ui/resources/add_8.png"), 0, 0));  // NOI18N
//            putValue("iconBase", ICON_PATH);  // NOI18N

            // Customize Toolbar dialog needs it
            putValue("iconBase", "");  // NOI18N
        }

        public void actionPerformed(SDMContainer ds, ActionEvent event) {
            performAction(event);
        }

        protected boolean isEnabled(SDMContainer ds) {
            return true;
        }
    }
}

