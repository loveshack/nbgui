/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  Copyright 2009 Sun Microsystems, Inc. All rights reserved. Use is
 *  subject to license terms.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sdm.ui.util;

import java.util.Calendar;
import org.openide.util.NbBundle;


public class ComboHelper {
       public static final String ONE_MIN = NbBundle.getMessage(ComboHelper.class,"1_min");
       public static final String FIVE_MIN = NbBundle.getMessage(ComboHelper.class,"5_mins");
       public static final String TEN_MIN = NbBundle.getMessage(ComboHelper.class,"10_mins");
       public static final String THIRTY_MIN = NbBundle.getMessage(ComboHelper.class,"30_mins");
       public static final String ONE_HRS = NbBundle.getMessage(ComboHelper.class,"1_hr");
       public static final String TWO_HRS = NbBundle.getMessage(ComboHelper.class,"2_hrs");
       public static final String SIX_HRS = NbBundle.getMessage(ComboHelper.class,"6_hrs");
       public static final String TWELVE_HRS = NbBundle.getMessage(ComboHelper.class,"12_hrs");
       public static final String ALL = NbBundle.getMessage(ComboHelper.class,"all");
    
       public static Calendar getTimeout(String value) {
        Calendar now = Calendar.getInstance();
        if (value.equals(ONE_MIN)) {
            now.add(Calendar.MINUTE, -1);
        } else if (value.equals(FIVE_MIN)) {
            now.add(Calendar.MINUTE, -5);
        } else if (value.equals(TEN_MIN)) {
            now.add(Calendar.MINUTE, -10);
        } else if (value.equals(THIRTY_MIN)) {
            now.add(Calendar.MINUTE, -30);
        } else if (value.equals(ONE_HRS)) {
            now.add(Calendar.HOUR, -1);
        } else if (value.equals(TWO_HRS)) {
            now.add(Calendar.HOUR, -2);
        } else if (value.equals(SIX_HRS)) {
            now.add(Calendar.HOUR, -6);
        } else if (value.equals(TWELVE_HRS)) {
            now.add(Calendar.HOUR, -12);
        } else if (value.equals(ALL)) {
            now.clear();
        }
        return now;
    }
       
    public static String [] getTimeoutValues() {
       return new String [] {ONE_MIN, FIVE_MIN, TEN_MIN, THIRTY_MIN, ONE_HRS, TWO_HRS, SIX_HRS, TWELVE_HRS, ALL};
    }

}
