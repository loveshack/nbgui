/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.jvm.JvmModelProvider;
import com.sun.grid.sdm.ui.component.ca.CADataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.component.ca.CADataSourceViewProvider;
import com.sun.grid.sdm.ui.component.executor.ExecutorDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.component.executor.ExecutorDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.general.GeneralComponentDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.component.general.GeneralComponentDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.reporter.ReporterDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.component.reporter.ReporterDataSourceViewProvider;
import com.sun.grid.sdm.ui.component.rp.ResourceProviderDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.component.rp.ResourceProviderDataSourceViewProvider;
import com.sun.grid.sdm.ui.jvm.SDMSystemViewProvider;
import com.sun.grid.sdm.ui.jvm.overview.BootstrapOverviewPluginProvider;
import com.sun.grid.sdm.ui.navigator.NavigatorSupport;
import com.sun.grid.sdm.ui.resource.CachedResourceDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.resource.CachedResourceDataSourceViewProvider;
import com.sun.grid.sdm.ui.resource.ResourceDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.resource.ResourceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.ServiceDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.service.cloud.CloudServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.cloud.CloudServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.ge.GEServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.ge.GEServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.general.GeneralServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.general.GeneralServiceDataSourceViewProvider;
import com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceResourceViewProvider;
import com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceViewProvider;
import org.openide.modules.ModuleInstall;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    private static final long serialVersionUID = -20090925L;

    public Installer() {
        super();
    }

    @Override
    public void restored() {

        JvmModelProvider.initialize();
        SDMSystemViewProvider.getSharedInstance().initialize();

        BootstrapOverviewPluginProvider.initialize();

        ServiceDataSourceDescriptorProvider.initialize();

        GEServiceDataSourceViewProvider.getSharedInstance().initialize();
        CloudServiceDataSourceViewProvider.getSharedInstance().initialize();
        SparePoolServiceDataSourceViewProvider.getSharedInstance().initialize();
        GeneralServiceDataSourceViewProvider.getSharedInstance().initialize();

        GEServiceDataSourceResourceViewProvider.getSharedInstance().initialize();
        CloudServiceDataSourceResourceViewProvider.getSharedInstance().initialize();
        SparePoolServiceDataSourceResourceViewProvider.getSharedInstance().initialize();
        GeneralServiceDataSourceResourceViewProvider.getSharedInstance().initialize();

        ResourceDataSourceViewProvider.getSharedInstance().initialize();
        ResourceDataSourceDescriptorProvider.initialize();

        CachedResourceDataSourceViewProvider.getSharedInstance().initialize();
        CachedResourceDataSourceDescriptorProvider.initialize();

        ReporterDataSourceDescriptorProvider.initialize();
        ReporterDataSourceViewProvider.getSharedInstance().initialize();

        ResourceProviderDataSourceDescriptorProvider.initialize();
        ResourceProviderDataSourceViewProvider.getSharedInstance().initialize();

        ExecutorDataSourceDescriptorProvider.initialize();
        ExecutorDataSourceViewProvider.getSharedInstance().initialize();

        CADataSourceDescriptorProvider.initialize();
        CADataSourceViewProvider.getSharedInstance().initialize();

        GeneralComponentDataSourceDescriptorProvider.initialize();
        GeneralComponentDataSourceViewProvider.getSharedInstance().initialize();

        NavigatorSupport.getInstance(JvmDataSource.class).initialize();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void uninstalled() {
        super.uninstalled();
//        ApplicationListener.unregisterViewProvider();

        JvmModelProvider.shutdown();
        SDMSystemViewProvider.getSharedInstance().shutdown();

        BootstrapOverviewPluginProvider.shutdown();

        ServiceDataSourceDescriptorProvider.shutdown();

        GEServiceDataSourceViewProvider.getSharedInstance().shutdown();
        CloudServiceDataSourceViewProvider.getSharedInstance().shutdown();
        SparePoolServiceDataSourceViewProvider.getSharedInstance().shutdown();
        GeneralServiceDataSourceViewProvider.getSharedInstance().shutdown();

        GEServiceDataSourceResourceViewProvider.getSharedInstance().shutdown();
        CloudServiceDataSourceResourceViewProvider.getSharedInstance().shutdown();
        SparePoolServiceDataSourceResourceViewProvider.getSharedInstance().shutdown();
        GeneralServiceDataSourceResourceViewProvider.getSharedInstance().shutdown();

        ResourceDataSourceViewProvider.getSharedInstance().shutdown();
        ResourceDataSourceDescriptorProvider.shutdown();

        CachedResourceDataSourceViewProvider.getSharedInstance().shutdown();
        CachedResourceDataSourceDescriptorProvider.shutdown();

        ReporterDataSourceDescriptorProvider.shutdown();
        ReporterDataSourceViewProvider.getSharedInstance().shutdown();

        ResourceProviderDataSourceDescriptorProvider.shutdown();
        ResourceProviderDataSourceViewProvider.getSharedInstance().shutdown();

        ExecutorDataSourceDescriptorProvider.shutdown();
        ExecutorDataSourceViewProvider.getSharedInstance().shutdown();

        CADataSourceDescriptorProvider.shutdown();
        CADataSourceViewProvider.getSharedInstance().shutdown();

        GeneralComponentDataSourceDescriptorProvider.shutdown();
        GeneralComponentDataSourceViewProvider.getSharedInstance().shutdown();

        NavigatorSupport.getInstance(JvmDataSource.class).shutdown();
    }
}
