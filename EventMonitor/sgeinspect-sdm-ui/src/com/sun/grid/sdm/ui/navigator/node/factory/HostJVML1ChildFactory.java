/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.HostNode;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class HostJVML1ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private static final Map<JvmDataSource, HostJVML1ChildFactory> f = new WeakHashMap<JvmDataSource, HostJVML1ChildFactory>();
    private final DCL dcl = new DCL();
    private int childrenSortType = 0;

    private HostJVML1ChildFactory(JvmDataSource jvm, int sortType) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, JvmDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, JvmDataSource.class);
        }
        this.childrenSortType = sortType;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        Set<String> hosts = new HashSet<String>();
        if (jvm != null) {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                hosts.add(ds.getHost());
            }
            hosts.add(jvm.getHost());
        }
        arg0.addAll(hosts);
        Collections.sort(arg0);
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        return new HostNode(arg0, Children.create(new HostJVML2ChildFactory(jvm, arg0, childrenSortType), true));
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        nodez.add(createNodeForKey(arg0));
        return nodez.toArray(new Node[nodez.size()]);
    }

    public static HostJVML1ChildFactory getFactory(JvmDataSource jvm, int sortType) {
        HostJVML1ChildFactory factory = f.get(jvm);
        if (factory == null) {
            factory = new HostJVML1ChildFactory(jvm, sortType);
            f.put(jvm, factory);
        }
        return factory;
    }

    public void setChildrenSortType(int sortType) {
        if (this.childrenSortType != sortType) {
            this.childrenSortType = sortType;
            this.refresh(true);
        }
    }

    private class DCL implements DataChangeListener<JvmDataSource> {

        public void dataChanged(DataChangeEvent<JvmDataSource> arg0) {
            refresh(true);
        }
    }
}
