/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.component.ca.CADataSource;
import com.sun.grid.sdm.api.component.executor.ExecutorDataSource;
import com.sun.grid.sdm.api.component.general.GeneralComponentDataSource;
import com.sun.grid.sdm.api.component.reporter.ReporterDataSource;
import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.CANode;
import com.sun.grid.sdm.ui.navigator.node.ExecutorNode;
import com.sun.grid.sdm.ui.navigator.node.GeneralComponentNode;
import com.sun.grid.sdm.ui.navigator.node.ReporterNode;
import com.sun.grid.sdm.ui.navigator.node.ResourceProviderNode;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class HostComponentL2ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private final DCL dcl = new DCL();
    private final int sortType;

    public HostComponentL2ChildFactory(JvmDataSource jvm, int sortType) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, ComponentDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, ComponentDataSource.class);
        }
        this.sortType = sortType;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        List<ComponentDataSource> clist = new LinkedList<ComponentDataSource>();
        if (jvm != null) {
            for (ComponentDataSource ds : jvm.getApplication().getRepository().getDataSources(ComponentDataSource.class)) {
                clist.add(ds);
            }
        }

        Collections.sort(clist, NameComparator.INSTANCE);
        for (ComponentDataSource cds : clist) {
            arg0.add(wrap(cds.getComponentName(), cds.getHostname()));
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        ComponentDataSource cds = jvm.getComponentDataSource(unwrapName(arg0));
        if (cds != null) {
            return createComponentNode(cds);
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        ComponentDataSource cds = jvm.getComponentDataSource(unwrapName(arg0));
        if (cds != null) {
            nodez.add(createComponentNode(cds));
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    private static String wrap(String name, String host) {
        return String.format("%s@%s", name, host);
    }

    private static String unwrapName(String id) {
        int pos = id.lastIndexOf('@');
        if (pos == -1) {
            return id;
        } else {
            return id.substring(0, pos);
        }
    }

    private class DCL implements DataChangeListener<ComponentDataSource> {

        public void dataChanged(DataChangeEvent<ComponentDataSource> arg0) {
            refresh(true);
        }
    }

    private Node createComponentNode(DataSource cds) {
        if (cds instanceof GeneralComponentDataSource) {
            return new GeneralComponentNode((GeneralComponentDataSource) cds, Children.LEAF);
        } else if (cds instanceof ExecutorDataSource) {
            return new ExecutorNode((ExecutorDataSource) cds, Children.LEAF);
        } else if (cds instanceof ReporterDataSource) {
            return new ReporterNode((ReporterDataSource) cds, Children.LEAF);
        } else if (cds instanceof ResourceProviderDataSource) {
            return new ResourceProviderNode((ResourceProviderDataSource) cds, Children.LEAF);
        } else if (cds instanceof CADataSource) {
            return new CANode((CADataSource) cds, Children.LEAF);
        } else {
            return AbstractNode.EMPTY;
        }
    }

    private static class NameComparator implements
            Comparator<ComponentDataSource> {

        private static NameComparator INSTANCE = new NameComparator();

        public int compare(ComponentDataSource o1, ComponentDataSource o2) {
            return o1.getComponentName().compareTo(o2.getComponentName());
        }
    }
}
