/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2009
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class PreferencesUtil {

    /** Key for the url containing connection string to CS.
     */
    private static final String CS_INFO_KEY = "csInfo";

    /**
     * Returns all system names which are stored in the preferences of the local host.
     *
     * @param prefs use user of system preferences
     * @return List of hedeby system names.
     */
    public static List<String> getSystemNames(PreferencesType prefs) {
        Preferences node = getBaseNode(prefs);
        List<String> ret = Collections.emptyList();
        String[] nodeNames;
        try {
            nodeNames = node.childrenNames();
            ret = new ArrayList<String>(nodeNames.length);
            for (String nodeName : nodeNames) {
                ret.add(nodeName);
            }
        } catch (BackingStoreException ex) {
            ret = Collections.emptyList();
        }
        return ret;
    }

    private static Preferences getBaseNode(PreferencesType type) {
        Preferences prefs = null;
        switch (type) {
            case USER:
                prefs = FilePreferencesReader.getUserRoot();
                break;
            default:
                prefs = FilePreferencesReader.getSystemRoot();
        }
        return prefs;
    }

    public static String getCSInfo(String systemName, PreferencesType prefs) {
        Preferences baseNode = getBaseNode(prefs).node(systemName);
        return baseNode.get(CS_INFO_KEY, null);
    }

    public enum PreferencesType {

        /**
         *  Use the user preferences
         */
        USER,
        /**
         * Use the system preferences
         */
        SYSTEM,
        /**
         * Look at the system properties to find out wether
         * user or system preferences are used
         */
        SYSTEM_PROPERTIES
    }
}
