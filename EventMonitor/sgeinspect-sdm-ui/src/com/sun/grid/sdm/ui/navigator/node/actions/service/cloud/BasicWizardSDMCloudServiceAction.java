/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.actions.service.cloud;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.CloudAdapterWizardCookie;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;
import java.awt.Image;
import java.io.IOException;
import javax.swing.Action;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.Repository;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.CookieAction;

public final class BasicWizardSDMCloudServiceAction extends CookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/add_cloud_16.png";
    private static final String SMALL_ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_cloud.png";
    private static final String LARGE_ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_cloud.png";
    private static final Image SMALL_IMAGE = ImageUtilities.mergeImages(ImageUtilities.loadImage(SMALL_ICON_PATH), ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/add_8.png"), 0, 0);
    private static final Image LARGE_IMAGE = ImageUtilities.mergeImages(ImageUtilities.loadImage(LARGE_ICON_PATH), ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/add_8.png"), 0, 0);
    private static final BasicWizardSDMCloudServiceAction SINGLETON = new BasicWizardSDMCloudServiceAction();

    protected void performAction(Node[] activatedNodes) {
        JvmDataSource serviceDataSource = activatedNodes[0].getLookup().lookup(JvmDataSource.class);
        if (serviceDataSource != null) {
            // todo NASTY to use string, fix it for next release
            String packagename = serviceDataSource.getVersionName(serviceDataSource.getVersion());
            String actionpattern = "com-sun-grid-sdm-conf-cloudadapter-%s-BasicCloudAdapterWizardAction.instance";

            FileObject sdmLCFolder = Repository.getDefault().getDefaultFileSystem().findResource("CustomSDM/Tools/SDM system configuration");
            FileObject actionobject = sdmLCFolder.getFileObject(String.format(actionpattern, packagename));
            DataObject actiondo = null;
            try {
                actiondo = DataObject.find(actionobject);
            } catch (DataObjectNotFoundException ex) {
                return;
            }

            RelaxedCookieAction action = null;
            try {
                action = (RelaxedCookieAction) actiondo.getCookie(InstanceCookie.class).instanceCreate();
            } catch (IOException ex) {
                return;
            } catch (ClassNotFoundException ex) {
                return;
            }

            if (action == null) {
                return;
            } else {
                action.performAction(activatedNodes);
            }
        }
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        boolean enable = super.enable(activatedNodes);
        if (enable) {
            JvmDataSource serviceDataSource = activatedNodes[0].getLookup().lookup(JvmDataSource.class);
            if (serviceDataSource != null) {
                // todo NASTY to use string, fix it for next release
                String packagename = serviceDataSource.getVersionName(serviceDataSource.getVersion());
                String actionpattern = "com-sun-grid-sdm-conf-cloudadapter-%s-BasicCloudAdapterWizardAction.instance";

                FileObject sdmLCFolder = Repository.getDefault().getDefaultFileSystem().findResource("CustomSDM/Tools/SDM system configuration");

                if (sdmLCFolder == null) {
                    System.out.println("There is no folder with name '" + "CustomSDM/Tools/SDM system configuration" + "' on the default file system!"); // NOI18N
                    return false;
                }

                FileObject actionobject = sdmLCFolder.getFileObject(String.format(actionpattern, packagename));
                if (actionobject == null) {
                    return false;
                }

                DataObject actiondo;
                try {
                    actiondo = DataObject.find(actionobject);
                } catch (DataObjectNotFoundException ex) {
                    return false;
                }

                Action action = null;
                try {
                    action = (Action) actiondo.getCookie(InstanceCookie.class).instanceCreate();
                } catch (IOException ex) {
                    return false;
                } catch (ClassNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }

                if (action == null) {
                    return false;
                }

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return NbBundle.getMessage(BasicWizardSDMCloudServiceAction.class, "CTL_BasicWizardSDMCloudServiceAction");
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{CloudAdapterWizardCookie.class};
    }
    
    @Override
    public boolean isEnabled() {
        boolean isEnabled = super.isEnabled();
        if(isEnabled){
            Lookup lkp = Utilities.actionsGlobalContext();
            CloudAdapterWizardCookie wizCookie = lkp.lookup (CloudAdapterWizardCookie.class);
            if(wizCookie==null){
                isEnabled=false;
            }
        }
        return isEnabled;
    }

    public static final BasicWizardSDMCloudServiceAction getInstance() {
        return SINGLETON;
    }

    @Override
    protected void initialize() {
        super.initialize();
        putValue(NAME, NbBundle.getMessage(BasicWizardSDMCloudServiceAction.class, "CTL_BasicWizardSDMCloudServiceAction"));  // NOI18N
        putValue(SHORT_DESCRIPTION, NbBundle.getMessage(BasicWizardSDMCloudServiceAction.class, "TT_BasicWizardSDMCloudServiceAction")); // NOI18N
        putValue(SMALL_ICON, ImageUtilities.image2Icon(SMALL_IMAGE));
//        putValue(LARGE_ICON_KEY, new ImageIcon(LARGE_IMAGE));
    }

    @Override
    protected String iconResource() {
        return ICON_PATH;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

