/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.component.rp;

import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent.DetailsView;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ResourceProviderDataSourceView extends DataSourceView {

    private ResourceProviderDetailsTablePanel details;
    private ResourceProviderResourcesTablePanel resources;

    public ResourceProviderDataSourceView(ResourceProviderDataSource cds) {
        super(cds, NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPName"), ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_rp.png"), 0x7fffffff, true);
    }

    protected DataViewComponent createComponent() {
        DataViewComponent.MasterView mv = new DataViewComponent.MasterView(NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPDetailsView"), NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPDetailsView"), new JPanel());
        DataViewComponent dvc = new DataViewComponent(mv, new DataViewComponent.MasterViewConfiguration(false));
        dvc.addDetailsView(getResourcesView(), DataViewComponent.TOP_RIGHT);
        dvc.addDetailsView(getDetailsView(), DataViewComponent.TOP_LEFT);
        return dvc;
    }

    @Override
    protected void added() {
        // todo register details view to listen to service state changes
    }

    @Override
    protected void removed() {
        // todo unregister details view service listener
    }

    protected DetailsView getDetailsView() {
        if (details == null) {
            details = new ResourceProviderDetailsTablePanel((ResourceProviderDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPDetailsLBL"), NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPDetails"), 0, details, null);    // NOI18N
    }

    protected DetailsView getResourcesView() {
        if (resources == null) {
            resources = new ResourceProviderResourcesTablePanel((ResourceProviderDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPResourcesLBL"), NbBundle.getMessage(ResourceProviderDataSourceView.class, "CTL_RPResources"), 10, resources, null);    // NOI18N
    }
}
