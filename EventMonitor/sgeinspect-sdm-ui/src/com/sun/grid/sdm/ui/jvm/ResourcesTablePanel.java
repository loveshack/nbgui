/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.jvm;

import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.resource.ResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.SystemColor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ResourcesTablePanel extends javax.swing.JPanel {

    private static final long serialVersionUID = -2009082401L;
    private final Image resources_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_resources_all.png");
    private final Image resource_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_host_resource.png");
    private final Image rp_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_rp.png");
    private final Image service_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_service.png");
    private final Image resource_property_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_host_resource_property.png");
    private final Icon down_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/down_arrow.png"));
    private final Icon up_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/up_arrow.png"));
    private final DCL_SDS dcl_sds = new DCL_SDS();
    private final DCL_RDS dcl_rds = new DCL_RDS();
    private final PCL pcl = new PCL();
    private final DCL_RPDS dcl_rpds = new DCL_RPDS();
    private final DRL drl = new DRL();
    private final Map<String, DefaultMutableTreeNode> rmap = new HashMap<String, DefaultMutableTreeNode>();
    private final DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private final DefaultTreeModel treeMdl = new DefaultTreeModel(root, false);
    private final OutlineDynamic tod = new OutlineDynamic();
    private final JvmDataSource jds;
    private final AtomicBoolean refilling = new AtomicBoolean(false);

    /** Creates new ResourcesTablePanel
     * @param jds 
     */
    public ResourcesTablePanel(JvmDataSource jds) {
        initComponents();
        this.jds = jds;
        JScrollPane scroller = new JScrollPane(tod);
        scroller.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        otlPanel.add(scroller, BorderLayout.CENTER);
        tod.fillModel();
        btnShowDetails.setEnabled(false);
        jds.getApplication().getRepository().addDataChangeListener(dcl_sds, ServiceDataSource.class);
        jds.getApplication().getRepository().addDataChangeListener(dcl_rpds, ResourceProviderDataSource.class);
//        jds.Repository().addDataChangeListener(dcl_rds, ResourceDataSource.class);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        otlPanel = new javax.swing.JPanel();
        btnPanel = new javax.swing.JPanel();
        btnHide = new javax.swing.JToggleButton();
        tglPanel = new javax.swing.JPanel();
        tglExpandResources = new javax.swing.JToggleButton();
        btnShowDetails = new javax.swing.JButton();
        tglExpandServices = new javax.swing.JToggleButton();

        setName("jPanel"); // NOI18N

        otlPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        otlPanel.setLayout(new java.awt.BorderLayout());

        btnHide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/resources/down_arrow.png"))); // NOI18N
        btnHide.setToolTipText(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.btnHide.toolTipText")); // NOI18N
        btnHide.setBorderPainted(false);
        btnHide.setContentAreaFilled(false);
        btnHide.setFocusPainted(false);
        btnHide.setMaximumSize(new java.awt.Dimension(32767, 32767));
        btnHide.setMinimumSize(new java.awt.Dimension(0, 0));
        btnHide.setPreferredSize(new java.awt.Dimension(100, 10));
        btnHide.setVerifyInputWhenFocusTarget(false);
        btnHide.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHideMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHideMouseExited(evt);
            }
        });
        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        tglExpandResources.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/resources/icon_host_resource_property.png"))); // NOI18N
        tglExpandResources.setMnemonic('S');
        tglExpandResources.setToolTipText(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.tglExpandResources.toolTipText")); // NOI18N
        tglExpandResources.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglExpandResourcesActionPerformed(evt);
            }
        });

        btnShowDetails.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/resources/icon_view_host_resource.png"))); // NOI18N
        btnShowDetails.setMnemonic('D');
        btnShowDetails.setToolTipText(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.btnShowDetails.toolTipText")); // NOI18N
        btnShowDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowDetailsActionPerformed(evt);
            }
        });

        tglExpandServices.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/sdm/ui/resources/icon_service.png"))); // NOI18N
        tglExpandServices.setMnemonic('E');
        tglExpandServices.setToolTipText(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.tglExpandServices.toolTipText")); // NOI18N
        tglExpandServices.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tglExpandServicesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tglPanelLayout = new javax.swing.GroupLayout(tglPanel);
        tglPanel.setLayout(tglPanelLayout);
        tglPanelLayout.setHorizontalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(tglExpandServices)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tglExpandResources)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnShowDetails)
                .addContainerGap(412, Short.MAX_VALUE))
        );
        tglPanelLayout.setVerticalGroup(
            tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tglPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(tglPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnShowDetails)
                    .addComponent(tglExpandServices)
                    .addComponent(tglExpandResources))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tglExpandResources.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.tglExpandResources.AccessibleContext.accessibleName")); // NOI18N
        btnShowDetails.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.btnShowDetails.AccessibleContext.accessibleName")); // NOI18N
        tglExpandServices.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(ResourcesTablePanel.class, "ResourcesTablePanel.tglExpandServices.AccessibleContext.accessibleName")); // NOI18N

        javax.swing.GroupLayout btnPanelLayout = new javax.swing.GroupLayout(btnPanel);
        btnPanel.setLayout(btnPanelLayout);
        btnPanelLayout.setHorizontalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnHide, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
            .addComponent(tglPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        btnPanelLayout.setVerticalGroup(
            btnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnPanelLayout.createSequentialGroup()
                .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tglPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(otlPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                    .addComponent(btnPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tglExpandServicesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglExpandServicesActionPerformed
        if (tglExpandServices.isSelected()) {
            expandServices();
        } else {
            collapseServices();
        }
}//GEN-LAST:event_tglExpandServicesActionPerformed

    private void btnShowDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowDetailsActionPerformed
        DefaultMutableTreeNode dmtn = tod.getSelectedNode();
        if (dmtn != null && (dmtn.getUserObject() instanceof ResourceDataSource)) {
            DataSourceWindowManager.sharedInstance().openDataSource(((ResourceDataSource) dmtn.getUserObject()));
        }
}//GEN-LAST:event_btnShowDetailsActionPerformed

    private void tglExpandResourcesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tglExpandResourcesActionPerformed
        if (tglExpandResources.isSelected()) {
            expandResources();
        } else {
            collapseResources();
        }
}//GEN-LAST:event_tglExpandResourcesActionPerformed

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        tglPanel.setVisible(!(tglPanel.isVisible()));
        if (tglPanel.isVisible()) {
            btnHide.setIcon(down_arrow);
        } else {
            btnHide.setIcon(up_arrow);
        }
}//GEN-LAST:event_btnHideActionPerformed

    private void btnHideMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseExited
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseExited

    private void btnHideMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseEntered
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseEntered
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnHide;
    private javax.swing.JPanel btnPanel;
    private javax.swing.JButton btnShowDetails;
    private javax.swing.JPanel otlPanel;
    private javax.swing.JToggleButton tglExpandResources;
    private javax.swing.JToggleButton tglExpandServices;
    private javax.swing.JPanel tglPanel;
    // End of variables declaration//GEN-END:variables

    private class ServiceNode {

        final private ServiceDataSource s;

        public ServiceNode(ServiceDataSource s) {
            this.s = s;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ServiceNode other = (ServiceNode) obj;
            if ((this.s.getServiceName() == null && other.s.getServiceName() != null) || (this.s.getServiceName() != null && !this.s.getServiceName().equals(other.s.getServiceName()))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 89 * hash + (this.s.getServiceName() != null ? this.s.getServiceName().hashCode() : 0);
            return hash;
        }

        @Override
        public String toString() {
            return s.getServiceName();
        }
    }

    private class RPNode {

        final private ResourceProviderDataSource s;

        public RPNode(ResourceProviderDataSource s) {
            this.s = s;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final RPNode other = (RPNode) obj;
            if ((this.s.getComponentName() == null && other.s.getComponentName() != null) || (this.s.getComponentName() != null && !this.s.getComponentName().equals(other.s.getComponentName()))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 89 * hash + (this.s.getComponentName() != null ? this.s.getComponentName().hashCode() : 0);
            return hash;
        }

        @Override
        public String toString() {
            return s.getComponentName();
        }
    }

    private class PropertyNode {

        final private String key;
        final private Object value;

        public PropertyNode(String k, Object v) {
            this.key = k;
            this.value = v;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PropertyNode other = (PropertyNode) obj;
            if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
                return false;
            }
            if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 61 * hash + (this.key != null ? this.key.hashCode() : 0);
            hash = 61 * hash + (this.value != null ? this.value.hashCode() : 0);
            return hash;
        }
    }

    private class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        public String getDisplayName(Object node) {
            Object o = ((DefaultMutableTreeNode) node).getUserObject();
            if ((o instanceof RPNode)) {
                return ((RPNode) o).s.getComponentName();
            } else if ((o instanceof ServiceNode)) {
                return ((ServiceNode) o).s.getServiceName();
            } else {
                return "";
            }
        }

        public boolean isHtmlDisplayName(Object node) {
            return false;
        }

        public Color getBackground(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof PropertyNode) {
                return SystemColor.control;
            } else if (arg0 instanceof ResourceDataSource) {
                return SystemColor.controlHighlight;
            } else {
                return Color.WHITE;
            }
        }

        public Color getForeground(Object node) {
            return Color.BLACK;
        }

        public String getTooltipText(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof ResourceDataSource) {
                return NbBundle.getMessage(ResourcesTablePanel.class, "show_hide");
            } else if (arg0 instanceof PropertyNode) {
                return NbBundle.getMessage(ResourcesTablePanel.class, "resource_property");
            } else if (arg0 instanceof RootNode) {
                return NbBundle.getMessage(ResourcesTablePanel.class, "service_resources");
            } else {
                return NbBundle.getMessage(ResourcesTablePanel.class, "Unknown_Data");
            }
        }

        public Icon getIcon(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof ResourceDataSource) {
                return ImageUtilities.image2Icon(resource_image);
            } else if (arg0 instanceof PropertyNode) {
                return ImageUtilities.image2Icon(resource_property_image);
            } else if (arg0 instanceof RootNode) {
                return ImageUtilities.image2Icon(resources_image);
            } else if (arg0 instanceof RPNode) {
                return ImageUtilities.image2Icon(rp_image);
            } else if (arg0 instanceof ServiceNode) {
                return ImageUtilities.image2Icon(service_image);
            } else {
                return null;
            }
        }
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(final PropertyChangeEvent evt) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    refilling.set(true);
                    DefaultTreeModel dtm = treeMdl;

                    DefaultMutableTreeNode rmtn = rmap.remove(((ResourceDataSource) evt.getSource()).getName());
                    if (rmtn == null) {
                        return;
                    }
                    boolean exp = false;
                    DefaultMutableTreeNode smtn = (DefaultMutableTreeNode) rmtn.getParent();
                    if (rmtn != null) {
                        if (rmtn.getPath() != null) {
                            exp = tod.isExpanded(new TreePath(rmtn.getPath()));
                        }
                        int index = smtn.getIndex(rmtn);
                        smtn.remove(rmtn);
                        dtm.nodesWereRemoved(smtn, new int[]{index}, new TreeNode[]{rmtn});
                    }

                    rmtn = new DefaultMutableTreeNode((ResourceDataSource) evt.getSource());
                    smtn.add(rmtn);
                    rmap.put(((ResourceDataSource) evt.getSource()).getName(), rmtn);
                    int i = smtn.getChildCount();
                    dtm.nodesWereInserted(smtn, new int[]{i - 1});
                    if (tglExpandResources.isSelected()) {
                        expandResources();
                    } else if (tglExpandServices.isSelected()) {
                        expandServices();
                    } else {
                        expandRoot();
                    }
                    if (exp) {
                        tod.expandPath(new TreePath(rmtn.getPath()));
                    }
                    refilling.set(false);
                }
            });
        }
    }

    private class DCL_RDS implements DataChangeListener<ResourceDataSource> {

        public void dataChanged(DataChangeEvent<ResourceDataSource> arg0) {
            tod.fillModel();
        }
    }

    private class DCL_SDS implements DataChangeListener<ServiceDataSource> {

        public void dataChanged(DataChangeEvent<ServiceDataSource> arg0) {
            for (ServiceDataSource s : arg0.getAdded()) {
                s.getRepository().addDataChangeListener(dcl_rds, ResourceDataSource.class);
            }
            for (ServiceDataSource s : arg0.getRemoved()) {
                s.getRepository().removeDataChangeListener(dcl_rds);
            }
            tod.fillModel();
        }
    }

    private class DCL_RPDS implements DataChangeListener<ResourceProviderDataSource> {

        public void dataChanged(final DataChangeEvent<ResourceProviderDataSource> arg0) {
            for (ResourceProviderDataSource s : arg0.getAdded()) {
                s.getRepository().addDataChangeListener(dcl_rds, ResourceDataSource.class);
            }
            for (ResourceProviderDataSource s : arg0.getRemoved()) {
                s.getRepository().removeDataChangeListener(dcl_rds);
            }
            tod.fillModel();
        }
    }

    private final class DRL implements DataRemovedListener<DataSource> {

        public void dataRemoved(DataSource arg0) {
            if (arg0 instanceof ResourceDataSource) {
                arg0.removePropertyChangeListener(pcl);
            }
            tod.fillModel();
        }
    }

    private class OutlineDynamic extends Outline {

        private static final long serialVersionUID = -2009082401L;
        private TableColumn kC;
        private TableColumn vC;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            super(DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), false, NbBundle.getMessage(ResourcesTablePanel.class, "Owner")));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);

            getSelectionModel().addListSelectionListener(
                    new ListSelectionListener() {

                        public void valueChanged(ListSelectionEvent e) {
                            if (refilling.get() != true) {
                                DefaultMutableTreeNode dmtn = getSelectedNode();
                                if (dmtn != null) {
                                    if (dmtn.getUserObject() instanceof ResourceDataSource) {
                                        btnShowDetails.setEnabled(true);
                                    } else {
                                        btnShowDetails.setEnabled(false);
                                    }
                                }
                            }
                        }
                    });

            kC = getColumnModel().getColumn(5);
            vC = getColumnModel().getColumn(6);
        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        private void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    refilling.set(true);
                    for (int x = 0; x < root.getChildCount(); x++) {
                        DefaultMutableTreeNode tn = (DefaultMutableTreeNode) root.getChildAt(x);
                        if ((tn.getUserObject() instanceof ServiceNode) ||
                                (tn.getUserObject() instanceof RPNode)) {
                            for (int z = 0; z < tn.getChildCount(); z++) {
                                DefaultMutableTreeNode cn = (DefaultMutableTreeNode) tn.getChildAt(z);
                                if (cn.getUserObject() instanceof ResourceDataSource) {
                                    ResourceDataSource rn = (ResourceDataSource) cn.getUserObject();
                                    rmap.remove(rn.getName());
                                    rn.removePropertyChangeListener(pcl);
                                }
                            }
                        }
                    }
                    root.removeAllChildren();
                    treeMdl.reload(root);
                    if (jds != null) {
                        for (JvmDataSource ds : jds.getRepository().getDataSources(JvmDataSource.class)) {
                            fillServices(treeMdl, ds.getApplication());
                            fillRPs(treeMdl, ds.getApplication());
                        }
                    }
                    fillServices(treeMdl, jds.getApplication());
                    fillRPs(treeMdl, jds.getApplication());
                    treeMdl.reload(root);

                    if (tglExpandResources.isSelected()) {
                        expandResources();
                    } else if (tglExpandServices.isSelected()) {
                        expandServices();
                    } else {
                        expandRoot();
                    }
                    refilling.set(false);
                }
            });

        }
    }

    private void fillServices(DefaultTreeModel dtm, DataSource jds) {
        for (ServiceDataSource ds : jds.getRepository().getDataSources(ServiceDataSource.class)) {
            ds.notifyWhenRemoved(drl);
            ServiceNode sn = new ServiceNode(ds);
            DefaultMutableTreeNode smtn = new DefaultMutableTreeNode(sn);
            root.add(smtn);
            int i = root.getChildCount();
            dtm.nodesWereInserted(root, new int[]{i - 1});
            for (ResourceDataSource rds : ds.getRepository().getDataSources(ResourceDataSource.class)) {
                rds.notifyWhenRemoved(drl);
                rds.addPropertyChangeListener(pcl);
                DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rds);
                smtn.add(rmtn);
                int n = smtn.getChildCount();
                dtm.nodesWereInserted(smtn, new int[]{n - 1});
                for (Map.Entry<String, Object> property : rds.getProperties().entrySet()) {
                    PropertyNode pn = new PropertyNode(property.getKey(), property.getValue());
                    DefaultMutableTreeNode pmtn = new DefaultMutableTreeNode(pn);
                    rmtn.add(pmtn);
                    dtm.nodesWereInserted(rmtn, new int[]{rmtn.getChildCount() - 1});
                }
                rmap.put(rds.getName(), rmtn);
            }
        }
    }

    private void fillRPs(DefaultTreeModel dtm, DataSource jds) {
        for (ResourceProviderDataSource ds : jds.getRepository().getDataSources(ResourceProviderDataSource.class)) {
            ds.notifyWhenRemoved(drl);
            RPNode sn = new RPNode(ds);
            DefaultMutableTreeNode smtn = new DefaultMutableTreeNode(sn);
            root.add(smtn);
            int i = root.getChildCount();
            dtm.nodesWereInserted(root, new int[]{i - 1});
            for (ResourceDataSource rds : ds.getRepository().getDataSources(ResourceDataSource.class)) {
                rds.notifyWhenRemoved(drl);
                rds.addPropertyChangeListener(pcl);
                DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rds);
                smtn.add(rmtn);
                int n = smtn.getChildCount();
                dtm.nodesWereInserted(smtn, new int[]{n - 1});
                for (Map.Entry<String, Object> property : rds.getProperties().entrySet()) {
                    PropertyNode pn = new PropertyNode(property.getKey(), property.getValue());
                    DefaultMutableTreeNode pmtn = new DefaultMutableTreeNode(pn);
                    rmtn.add(pmtn);
                    dtm.nodesWereInserted(rmtn, new int[]{rmtn.getChildCount() - 1});
                }
                rmap.put(rds.getName(), rmtn);
            }
        }
    }

    private void expandRoot() {
        tod.expandPath(new TreePath(root));
        setupColumns();
    }

    private void expandServices() {
        for (int i = 0; i < root.getChildCount(); i++) {
            tod.expandPath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void collapseServices() {
        for (int i = 0; i < root.getChildCount(); i++) {
            tod.collapsePath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void expandResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            for (int y = 0; y < root.getChildAt(i).getChildCount(); y++) {
                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) root.getChildAt(i).getChildAt(y);
                tod.expandPath(new TreePath(dmtn.getPath()));
            }
        }
        setupColumns();
    }

    private void collapseResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            for (int y = 0; y < root.getChildAt(i).getChildCount(); y++) {
                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) root.getChildAt(i).getChildAt(y);
                tod.collapsePath(new TreePath(dmtn.getPath()));
            }
        }
        setupColumns();
    }

    private void setupColumns() {
        if (tod.getColumnModel() instanceof ETableColumnModel) {
            ((ETableColumnModel) tod.getColumnModel()).setColumnHidden(tod.kC, !tglExpandResources.isSelected());
            ((ETableColumnModel) tod.getColumnModel()).setColumnHidden(tod.vC, !tglExpandResources.isSelected());
        }
    }

    private class NodeRowModel implements RowModel {

        public Class<?> getColumnClass(int column) {
            switch (column) {
                case 3:
                    return Integer.class;
                default:
                    return String.class;
            }
        }

        public int getColumnCount() {
            return 6;
        }

        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Resource");
                case 1:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Type");
                case 2:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Status");
                case 3:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Usage");
                case 4:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Property_Name");
                case 5:
                    return NbBundle.getMessage(ResourcesTablePanel.class, "Property_Value");
                default:
                    assert false;
            }
            return "";
        }

        public Object getValueFor(Object node, int column) {
            Object o = ((DefaultMutableTreeNode) node).getUserObject();
            if (o instanceof ResourceDataSource) {
                switch (column) {
                    case 0:
                        return ((ResourceDataSource) o).getName();
                    case 1:
                        return ((ResourceDataSource) o).getType();
                    case 2:
                        return ((ResourceDataSource) o).getState();
                    case 3:
                        return ((ResourceDataSource) o).getUsageLevel();
                    default:
                        return "";
                }
            } else if ((o instanceof RPNode)) {
                switch (column) {
                    default:
                        return "";
                }
            } else if ((o instanceof ServiceNode)) {
                switch (column) {
                    default:
                        return "";
                }
            } else if (o instanceof RootNode) {
                switch (column) {
                    default:
                        return "";
                }
            } else if (o instanceof PropertyNode) {
                switch (column) {
                    case 4:
                        return ((PropertyNode) o).key;
                    case 5:
                        if (((PropertyNode) o).value != null) {
                            return ((PropertyNode) o).value.toString();
                        } else {
                            return "null";
                        }
                    default:
                        return "";
                }
            }

            return null;
        }

        public boolean isCellEditable(Object node, int column) {
            return false;
        }

        public void setValueFor(Object node, int column, Object value) {
        }
    }
}
