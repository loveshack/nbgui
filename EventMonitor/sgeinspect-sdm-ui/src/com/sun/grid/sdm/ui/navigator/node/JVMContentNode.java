/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.actions.RelaxedCookieAction;

import java.awt.Image;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.Lookups;

public class JVMContentNode extends AbstractNode {

    private final Image icon;
    private final String name;
    private JvmDataSource sds=null;
    //old constructor still in use by groupnode for components
    public JVMContentNode(String name, Image icon, Children c) {
        this(null,name,icon,c, new InstanceContent());
    }

    //here we have a group node for  Services
    public JVMContentNode(JvmDataSource sds, String name, Image icon, Children c,InstanceContent ic) {
        super(c, new AbstractLookup(ic));
        this.name = name;
        this.icon = icon;
        if(sds!=null){
            this.sds = sds;
            ic.add(sds.getApplication());
            ic.add(sds);
            //store the dedicated cookie to signal that we allow adding of Cloud Adapters
            ic.add(CloudAdapterWizardCookie.SINGLETON_INSTANCE);
        }
    }
   
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public Image getIcon(int arg0) {
        return icon;
    }

    @Override
    public Image getOpenedIcon(int arg0) {
        return getIcon(arg0);
    }

    @Override
    public String getHtmlDisplayName() {
        return getDisplayName();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
//        Sheet.Set set = Sheet.createPropertiesSet();
//        Property prop = new ReadOnlyProperty("hostname", host, "hostname", "A name of host holding the components");
//        set.put(prop);
//        sheet.put(set);
        return sheet;
    }

    private class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private final String value;

        private ReadOnlyProperty(String name, String value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    @Override
    public Action[] getActions(boolean popup) {
        //in case that we have a non null sds look for additional actions
        if(sds!=null){
            final String versionname =  sds.getVersionName(sds.getVersion());
            List<Action> actionz = new LinkedList<Action>();
            Lookup lkp = Lookups.forPath("CustomSDM/Context/SDM system configuration/" + versionname + "/CloudAdapterWizardCookie");
            actionz.addAll(lkp.lookupAll(RelaxedCookieAction.class));
            actionz.add(null);
            actionz.addAll(Arrays.asList(super.getActions(popup)));
            return actionz.toArray(new Action[actionz.size()]);
        }else{
            return super.getActions(popup);
        }
    }
}
