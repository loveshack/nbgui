/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.actions;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.event.ActionEvent;
import org.openide.util.NbBundle;

public class OpenJVMAction extends SingleDataSourceAction<JvmDataSource> {

    private final static long serialVersionUID = -2009100101L;

    private static final class Singleton {

        private static final OpenJVMAction INSTANCE = new OpenJVMAction();

        private Singleton() {
        }
    }

    public static OpenJVMAction createInstance() {
        return Singleton.INSTANCE;
    }

    private OpenJVMAction() {
        super(JvmDataSource.class);
        putValue(NAME, NbBundle.getMessage(OpenJVMAction.class, "LBL_Open_JVM"));
    }

    /**
     * temporary fix (=to do nothing)- there is probably a bug in visualvm. needs to be solved
     * in september 09.
     * TODO fix this with visualvm team
     * @param ds
     * @param event 
     */
    protected void actionPerformed(final JvmDataSource ds, ActionEvent event) {
        DataSource.EVENT_QUEUE.post(new Runnable() {

            public void run() {
//                a bug in visualvm? application reports from time-to-time that it is unaivalable (0)
//                if (ds.getApplication().getState() == Stateful.STATE_AVAILABLE) {
                DataSourceWindowManager.sharedInstance().openDataSource(ds);
//                } else {
//                    SwingUtilities.invokeLater(new Runnable() {
//
//                        public void run() {
//
//                            JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(),
//                                    NbBundle.getMessage(OpenJVMAction.class, "MSG_JVM_app_not_active", ds.getName()), // NOI18N
//                                    NbBundle.getMessage(OpenJVMAction.class, "LBL_Warning"), // NOI18N
//                                    JOptionPane.WARNING_MESSAGE);
//                        }
//                    });
//                }
            }
        });
    }

    protected boolean isEnabled(JvmDataSource ds) {
        return ds.isCS();
    }
}

