/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.component.ca.CADataSource;
import com.sun.grid.sdm.api.component.executor.ExecutorDataSource;
import com.sun.grid.sdm.api.component.general.GeneralComponentDataSource;
import com.sun.grid.sdm.api.component.reporter.ReporterDataSource;
import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.CAL1Node;
import com.sun.grid.sdm.ui.navigator.node.ExecutorL1Node;
import com.sun.grid.sdm.ui.navigator.node.GeneralComponentL1Node;
import com.sun.grid.sdm.ui.navigator.node.ReporterL1Node;
import com.sun.grid.sdm.ui.navigator.node.ResourceProviderL1Node;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ComponentHostL1ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private static final Map<JvmDataSource, ComponentHostL1ChildFactory> f = new WeakHashMap<JvmDataSource, ComponentHostL1ChildFactory>();
    private final DCL dcl = new DCL();

    private ComponentHostL1ChildFactory(JvmDataSource jvm) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, ComponentDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, ComponentDataSource.class);
        }
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        Set<String> names = new HashSet<String>();
        if (jvm != null) {
            for (ComponentDataSource ds : jvm.getApplication().getRepository().getDataSources(ComponentDataSource.class)) {
                names.add(ds.getComponentName());
            }
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                for (ComponentDataSource ads : ds.getApplication().getRepository().getDataSources(ComponentDataSource.class)) {
                    names.add(ads.getComponentName());
                }
            }
        }
        arg0.addAll(names);
        Collections.sort(arg0);
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        ComponentDataSource cds = jvm.getComponentDataSource(arg0);
        if (cds != null) {
            return createComponentNode(cds);
        } else {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                cds = ds.getComponentDataSource(arg0);
                if (cds != null) {
                    return createComponentNode(cds);
                }
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        Node n = createNodeForKey(arg0);
        if (n != null) {
            nodez.add(n);
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    private Node createComponentNode(ComponentDataSource cds) {
        // we just need a single representant of ds because of icon - name remains the same
        if (cds instanceof GeneralComponentDataSource) {
            return new GeneralComponentL1Node((GeneralComponentDataSource) cds, Children.create(new ComponentHostL2ChildFactory(jvm, cds.getComponentName()), true));
        } else if (cds instanceof ExecutorDataSource) {
            return new ExecutorL1Node((ExecutorDataSource) cds, Children.create(new ComponentHostL2ChildFactory(jvm, cds.getComponentName()), true));
        } else if (cds instanceof ReporterDataSource) {
            return new ReporterL1Node((ReporterDataSource) cds, Children.create(new ComponentHostL2ChildFactory(jvm, cds.getComponentName()), true));
        } else if (cds instanceof ResourceProviderDataSource) {
            return new ResourceProviderL1Node((ResourceProviderDataSource) cds, Children.create(new ComponentHostL2ChildFactory(jvm, cds.getComponentName()), true));
        } else if (cds instanceof CADataSource) {
            return new CAL1Node((CADataSource) cds, Children.create(new ComponentHostL2ChildFactory(jvm, cds.getComponentName()), true));
        } else {
            return AbstractNode.EMPTY;
        }
    }

    public static ComponentHostL1ChildFactory getFactory(JvmDataSource jvm) {
        ComponentHostL1ChildFactory factory = f.get(jvm);
        if (factory == null) {
            factory = new ComponentHostL1ChildFactory(jvm);
            f.put(jvm, factory);
        }
        return factory;
    }

    private class DCL implements DataChangeListener<ComponentDataSource> {

        public void dataChanged(DataChangeEvent<ComponentDataSource> arg0) {
            refresh(true);
        }
    }
}
