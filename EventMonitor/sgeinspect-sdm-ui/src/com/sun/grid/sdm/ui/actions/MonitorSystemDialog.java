/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.actions;

import com.sun.grid.sdm.api.jvm.JvmDataSourceFactoryLookup;
import com.sun.grid.sdm.ui.util.PreferencesUtil;
import com.sun.grid.sdm.ui.util.PreferencesUtil.PreferencesType;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 */
class MonitorSystemDialog extends javax.swing.JDialog {

    private final static long serialVersionUID = -20090930L;
    /** A return status code - returned if Cancel button has been pressed */
    static final int RET_CANCEL = 0;
    /** A return status code - returned if OK button has been pressed */
    static final int RET_OK = 1;
    private static final Image SYSTEM_IMAGE = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_sdm.png");
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private final OutlineDynamic tod = new OutlineDynamic();
    private SystemNode selectedSystem = null;

    /** Creates new form MonitorSystemDialog */
    MonitorSystemDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle(org.openide.util.NbBundle.getMessage(MonitorSystemDialog.class, "MonitorSystemDialog.title")); // NOI18N
        setIconImage(SYSTEM_IMAGE);
        setLocationRelativeTo(null);
        JScrollPane scroller = new JScrollPane(tod);
        scroller.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        otlPanel.add(scroller, BorderLayout.CENTER);
    }

    /** @return the return status of this dialog - one of RET_OK or RET_CANCEL */
    public int getReturnStatus() {
        return returnStatus;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        otlPanel = new javax.swing.JPanel();
        cmbSDMVersion = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setMnemonic('C');
        okButton.setText(org.openide.util.NbBundle.getMessage(MonitorSystemDialog.class, "MonitorSystemDialog.okButton.text")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setMnemonic('a');
        cancelButton.setText(org.openide.util.NbBundle.getMessage(MonitorSystemDialog.class, "MonitorSystemDialog.cancelButton.text")); // NOI18N
        cancelButton.setPreferredSize(new java.awt.Dimension(81, 26));
        cancelButton.setRequestFocusEnabled(false);
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        otlPanel.setBorder(null);
        otlPanel.setLayout(new java.awt.BorderLayout());

        jLabel1.setText(org.openide.util.NbBundle.getMessage(MonitorSystemDialog.class, "MonitorSystemDialog.jLabel1.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(otlPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(okButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cmbSDMVersion, 0, 243, Short.MAX_VALUE))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, okButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(otlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbSDMVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        DefaultMutableTreeNode dmtn = tod.getSelectedNode();
        if (dmtn != null) {
            selectedSystem = (SystemNode) dmtn.getUserObject();
        } else {
            selectedSystem = null;
        }
        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        tod.fillModel();
        DefaultComboBoxModel cbm = new DefaultComboBoxModel();
        for (String version : JvmDataSourceFactoryLookup.getInstance().getAllRegisteredFactoryVersions()) {
            cbm.addElement(version);
        }
        cmbSDMVersion.setModel(cbm);
    }//GEN-LAST:event_formWindowActivated

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    SystemNode getSelectedSystem() {
        return selectedSystem;
    }

    String getSelectedSystemVersion() {
        return cmbSDMVersion.getSelectedItem().toString();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox cmbSDMVersion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton okButton;
    private javax.swing.JPanel otlPanel;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = RET_CANCEL;

    class SystemNode {

        private final String name;
        private final PreferencesType prefs;

        SystemNode(String name, PreferencesType prefs) {
            this.name = name;
            this.prefs = prefs;
        }

        String getSystemName() {
            return name;
        }

        PreferencesType getPreferencesType() {
            return prefs;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final SystemNode other = (SystemNode) obj;
            if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
                return false;
            }
            if (this.prefs != other.prefs) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 53 * hash + (this.prefs != null ? this.prefs.hashCode() : 0);
            return hash;
        }

        @Override
        public String toString() {
            return name + ":" + prefs.toString();
        }
    }

    private class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        public String getDisplayName(Object node) {
            return ((SystemNode) ((DefaultMutableTreeNode) node).getUserObject()).name;
        }

        public boolean isHtmlDisplayName(Object node) {
            return false;
        }

        public Color getBackground(Object node) {
//            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            return Color.WHITE;
        }

        public Color getForeground(Object node) {
            return Color.BLACK;
        }

        public String getTooltipText(Object node) {
            return NbBundle.getMessage(MonitorSystemDialog.class, "CTL_BootstrapTooltip");
        }

        public Icon getIcon(Object node) {
            return ImageUtilities.image2Icon(SYSTEM_IMAGE);
        }
    }

    private class OutlineDynamic extends Outline {

        private final static long serialVersionUID = -20090930L;
        private TreeModel treeMdl;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            setLayout(new BorderLayout());
            treeMdl = new DefaultTreeModel(root, false);
            OutlineModel mdl = DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), true, NbBundle.getMessage(MonitorSystemDialog.class, "System_Name"));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);
            setModel(mdl);

        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        private class NodeRowModel implements RowModel {

            public Class<?> getColumnClass(int column) {
                switch (column) {
                    default:
                        return String.class;
                }
            }

            public int getColumnCount() {
                return 1;
            }

            public String getColumnName(int column) {
                switch (column) {
                    case 0:
                        return NbBundle.getMessage(MonitorSystemDialog.class, "System_Type");
                    default:
                        assert false;
                }
                return "";
            }

            public Object getValueFor(Object node, int column) {
                Object o = ((DefaultMutableTreeNode) node).getUserObject();
                if (o instanceof SystemNode) {
                    switch (column) {
                        case 0:
                            return ((SystemNode) o).prefs.toString();
                        default:
                            return "";
                    }
                } else if (o instanceof RootNode) {
                    switch (column) {
                        default:
                            return "";
                    }
                }

                return null;
            }

            public boolean isCellEditable(Object node, int column) {
                return false;
            }

            public void setValueFor(Object node, int column, Object value) {
            }
        }

        private void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    DefaultTreeModel dtm = (DefaultTreeModel) treeMdl;
                    root.removeAllChildren();
                    dtm.reload(root);

                    for (String name : PreferencesUtil.getSystemNames(PreferencesType.SYSTEM)) {
                        SystemNode rn = new SystemNode(name, PreferencesType.SYSTEM);
                        DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rn);
                        root.add(rmtn);
                        int i = root.getChildCount();
                        dtm.nodesWereInserted(root, new int[]{i - 1});
                    }
                    for (String name : PreferencesUtil.getSystemNames(PreferencesType.USER)) {
                        SystemNode rn = new SystemNode(name, PreferencesType.USER);
                        DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rn);
                        root.add(rmtn);
                        int i = root.getChildCount();
                        dtm.nodesWereInserted(root, new int[]{i - 1});
                    }
                    expandRoot();
                }
            });

        }
    }

    private void expandRoot() {
        tod.expandPath(new TreePath(root));
    }
}
