/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.component;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.ui.component.ca.CADetailsTablePanel;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.NbBundle;

public class ComponentDetailsPanelSupport<T extends ComponentDataSource> {

    private T cds;
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private OutlineDynamic o;
    private final PCL pcl = new PCL();
    private final DRL drl = new DRL();
    private JScrollPane scroller;

    public ComponentDetailsPanelSupport(T c) {
        this.cds = c;
        this.cds.addPropertyChangeListener(pcl);
        this.cds.notifyWhenRemoved(drl);
        this.o = new OutlineDynamic();
    }

    public synchronized Component getComponent() {
        if (scroller == null) {
            scroller = new JScrollPane(o);
        }
        o.fillModel();
        return scroller;
    }

    private static class PropertyNode {

        final private String key;
        final private Object value;

        public PropertyNode(String k, Object v) {
            this.key = k;
            this.value = v;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            @SuppressWarnings("unchecked")
            final PropertyNode other = (PropertyNode) obj;
            if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
                return false;
            }
            if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 61 * hash + (this.key != null ? this.key.hashCode() : 0);
            hash = 61 * hash + (this.value != null ? this.value.hashCode() : 0);
            return hash;
        }
    }

    private static class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        @SuppressWarnings("unchecked")
        public String getDisplayName(Object node) {
            return ((PropertyNode) ((DefaultMutableTreeNode) node).getUserObject()).key;
        }

        public boolean isHtmlDisplayName(Object arg0) {
            return false;
        }

        public Color getBackground(Object node) {
            return Color.WHITE;
        }

        public Color getForeground(Object node) {
            return Color.BLACK;
        }

        public String getTooltipText(Object arg0) {
            return NbBundle.getMessage(CADetailsTablePanel.class, "CADetailsTablePanel.sd") + " '" + cds.getComponentName() + "'";
        }

        public Icon getIcon(Object node) {
            return null;
        }
    }

    private class NodeRowModel implements RowModel {

        public Class<?> getColumnClass(int column) {
            switch (column) {
                default:
                    return String.class;
            }
        }

        public int getColumnCount() {
            return 1;
        }

        public String getColumnName(int column) {
            switch (column) {
                case 0:
//                        return "Property";
//                    case 1:
                    return NbBundle.getMessage(CADetailsTablePanel.class, "LBL_Value");// TODO I18N
                default:
                    assert false;
            }
            return "";
        }

        @SuppressWarnings("unchecked")
        public Object getValueFor(Object node, int column) {
            Object ob = ((DefaultMutableTreeNode) node).getUserObject();
            if (ob instanceof RootNode) {
                switch (column) {
                    default:
                        return "";
                }
            } else if (ob instanceof PropertyNode) {
                switch (column) {
                    case 0:
//                            return ((PropertyNode) o).key;
//                        case 1:
                        if (((PropertyNode) ob).value != null) {
                            return ((PropertyNode) ob).value.toString();
                        } else {
                            return "null";
                        }
                    default:
                        return "";
                }
            }
            return null;
        }

        public boolean isCellEditable(Object node, int column) {
            return false;
        }

        public void setValueFor(Object node, int column, Object value) {
        }
    }

    private class OutlineDynamic extends Outline {

        private final static long serialVersionUID = -2009100101L;
        private TreeModel treeMdl;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            setLayout(new BorderLayout());
            treeMdl = new DefaultTreeModel(root, false);
            OutlineModel mdl = DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), true, NbBundle.getMessage(CADetailsTablePanel.class, "LBL_Component_Property"));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);
            setModel(mdl);
        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        public void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    DefaultTreeModel dtm = (DefaultTreeModel) treeMdl;
                    root.removeAllChildren();
                    dtm.reload(root);

                    PropertyNode pn;
                    DefaultMutableTreeNode rmtn;
                    // name
                    pn = new PropertyNode(NbBundle.getMessage(CADetailsTablePanel.class, "LBL_Name"), cds.getComponentName());
                    rmtn = new DefaultMutableTreeNode(pn);
                    root.add(rmtn);
                    dtm.nodesWereInserted(root, new int[]{root.getChildCount() - 1});
                    // hostname
                    pn = new PropertyNode(NbBundle.getMessage(CADetailsTablePanel.class, "LBL_Hostname"), cds.getHostname());
                    rmtn = new DefaultMutableTreeNode(pn);
                    root.add(rmtn);
                    dtm.nodesWereInserted(root, new int[]{root.getChildCount() - 1});
                    pn = new PropertyNode(NbBundle.getMessage(CADetailsTablePanel.class, "LBL_Component_State"), cds.getComponentState());
                    rmtn = new DefaultMutableTreeNode(pn);
                    root.add(rmtn);
                    dtm.nodesWereInserted(root, new int[]{root.getChildCount() - 1});
                    // jvm name
                    pn = new PropertyNode(NbBundle.getMessage(CADetailsTablePanel.class, "LBL_JVM"), cds.getJvmName());
                    rmtn = new DefaultMutableTreeNode(pn);
                    root.add(rmtn);
                    dtm.nodesWereInserted(root, new int[]{root.getChildCount() - 1});
                    // exapnd root
                    expandRoot();
                }
            });

        }
    }

    private void expandRoot() {
        o.expandPath(new TreePath(root));
    }

    private class DRL implements DataRemovedListener<T> {

        public void dataRemoved(T arg0) {
            if (arg0.equals(cds)) {
                cds.removePropertyChangeListener(pcl);
            }
        }
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(final PropertyChangeEvent evt) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    o.fillModel();
                }
            });
        }
    }
}
