/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.*;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class JVMHostL2ChildFactory extends ChildFactory<JvmDataSource> {

    private final JvmDataSource jvm;
    private final DCL dcl = new DCL();
    private final String name;

    public JVMHostL2ChildFactory(JvmDataSource jvm, String name) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, JvmDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, JvmDataSource.class);
        }
        this.name = name;
    }

    @Override
    protected boolean createKeys(List<JvmDataSource> arg0) {
        List<JvmDataSource> clist = new LinkedList<JvmDataSource>();
        if (jvm != null) {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                if (name.equals(ds.getName())) {
                    clist.add(ds);
                }
            }
            if (name.equals(jvm.getName())) {
                clist.add(jvm);
            }
        }

        Collections.sort(clist, HostNameComparator.INSTANCE);
        arg0.addAll(clist);
        return true;
    }

    @Override
    protected Node createNodeForKey(JvmDataSource arg0) {
        return createJvmNode(arg0);
    }

    @Override
    protected Node[] createNodesForKey(JvmDataSource arg0) {
        List<Node> nodez = new LinkedList<Node>();
        nodez.add(createNodeForKey(arg0));
        return nodez.toArray(new Node[nodez.size()]);
    }

    private class DCL implements DataChangeListener<JvmDataSource> {

        public void dataChanged(DataChangeEvent<JvmDataSource> arg0) {
            refresh(true);
        }
    }

    private Node createJvmNode(JvmDataSource cds) {
        return new JVML2Node(cds, Children.create(new JVML3ChildFactory(cds), true));
    }

    private static class HostNameComparator implements
            Comparator<JvmDataSource> {

        private static HostNameComparator INSTANCE = new HostNameComparator();

        public int compare(JvmDataSource o1, JvmDataSource o2) {
            return o1.getHost().compareTo(o2.getHost());
        }
    }
}
