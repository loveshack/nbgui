/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.factory.HostComponentL1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.factory.HostJVML1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.RootNode;
import com.sun.grid.sdm.ui.navigator.node.factory.ComponentHostL1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.factory.HostServiceL1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.factory.JVMHostL1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.factory.ServiceHostL1ChildFactory;
import com.sun.grid.sdm.ui.navigator.node.factory.ServiceL1ChildFactory;
import com.sun.grid.shared.ui.navigator.NavigatorActionHandler;
import com.sun.grid.shared.ui.navigator.NavigatorTopComponent;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 * Utility class suporting a navigator view for sdm.
 *
 * @param <T>
 */
public class NavigatorSupport<T extends JvmDataSource> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("com/sun/grid/sdm/ui/navigator/Bundle");
    private final String host_jvm_all = bundle.getString("CTL_host_jvm_all");
    private final String jvm_host_all = bundle.getString("CTL_jvm_host_all");
    private final String host_components = bundle.getString("CTL_host_components");
    private final String host_services = bundle.getString("CTL_host_services");
    private final String services = bundle.getString("CTL_services");
    private final String component_hosts = bundle.getString("CTL_component_hosts");
    private final String service_hosts = bundle.getString("CTL_service_hosts");
    private final MyNavigatorActionHandler nah = new MyNavigatorActionHandler();
    private final ComboBoxModel cbm = new DefaultComboBoxModel(new String[]{
                host_jvm_all,
                jvm_host_all,
                host_components,
                component_hosts,
                host_services,
                service_hosts,
                services});
    private final Map<T, RootNode> hcViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> hsViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> sViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> chViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> shViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> hjViewRoots = new WeakHashMap<T, RootNode>();
    private final Map<T, RootNode> jhViewRoots = new WeakHashMap<T, RootNode>();
    private T last_selected = null;
    private final NavigatorButtonPanel nbp = new NavigatorButtonPanel();
    private final BPL bpl = new BPL();
    private int componentChildrenSortType = 0;
    private final Class<T> type;
    private static final Map<Class<? extends JvmDataSource>, NavigatorSupport<? extends JvmDataSource>> navigatormap = new HashMap<Class<? extends JvmDataSource>, NavigatorSupport<? extends JvmDataSource>>();

    private NavigatorSupport(Class<T> type) {
        this.nbp.addButtonPanelListener(bpl);
        this.componentChildrenSortType = nbp.getSortType();
        this.type = type;
    }

    public static <U extends JvmDataSource> NavigatorSupport<U> getInstance(Class<U> type) {
        @SuppressWarnings("unchecked")
        NavigatorSupport<U> ns = (NavigatorSupport<U>) navigatormap.get(type);
        if (ns == null) {
            ns = new NavigatorSupport<U>(type);
            navigatormap.put(type, ns);
        }
        return ns;
    }

    public void initialize() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                NavigatorTopComponent.findInstance().addNavigatorActionHandler(type, nah);
            }
        });
    }

    public void shutdown() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                NavigatorTopComponent.findInstance().removeNavigatorActionHandler(type, nah);
            }
        });
    }

    private class MyNavigatorActionHandler implements NavigatorActionHandler {

        private RootNode root = null;

        @SuppressWarnings("unchecked")
        public void handleSelectionChanged(Set<DataSource> arg0, Object selectedItem) {
            if (!arg0.isEmpty()) {
                Object obj = arg0.iterator().next();
                if (obj instanceof JvmDataSource) {
                    last_selected = (T) obj;
                    submitChanges(selectedItem);
                } else {
                    last_selected = null;
                }
            } else {
                last_selected = null;
            }
        }

        public void handleComponentOpened() {
        }

        public void handleComponentClosed() {
        }

        public void handleViewChanged(ActionEvent evt, Object selectedItem) {
            submitChanges(selectedItem);
        }

        public Node getRootContext() {
            return root;
        }

        public JPanel getToolbarPanel() {
//            return nbp;
            return null;
        }

        public ComboBoxModel getViewModel() {
            return cbm;
        }

        private void submitChanges(Object selectedItem) {
            if (selectedItem != null && last_selected != null) {
                if (selectedItem.equals(host_components)) {
                    root = hcViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(HostComponentL1ChildFactory.getFactory(last_selected, componentChildrenSortType), true));
                        hcViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(host_services)) {
                    root = hsViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(HostServiceL1ChildFactory.getFactory(last_selected), true));
                        hsViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(component_hosts)) {
                    root = chViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(ComponentHostL1ChildFactory.getFactory(last_selected), true));
                        chViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(service_hosts)) {
                    root = shViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(ServiceHostL1ChildFactory.getFactory(last_selected), true));
                        shViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(services)) {
                    root = sViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(ServiceL1ChildFactory.getFactory(last_selected), true));
                        sViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(host_jvm_all)) {
                    root = hjViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(HostJVML1ChildFactory.getFactory(last_selected, 0), true));
                        hjViewRoots.put(last_selected, root);
                    }
                } else if (selectedItem.equals(jvm_host_all)) {
                    root = jhViewRoots.get(last_selected);
                    if (root == null) {
                        root = new RootNode(last_selected, Children.create(JVMHostL1ChildFactory.getFactory(last_selected, 0), true));
                        jhViewRoots.put(last_selected, root);
                    }
                }
            }
        }
    }

    private class BPL implements
            NavigatorButtonPanel.ButtonPanelListener {

        public void sortTypeChanged(int sortType) {
            componentChildrenSortType = sortType;
            HostComponentL1ChildFactory crcf = HostComponentL1ChildFactory.getFactory(last_selected, componentChildrenSortType);
            if (crcf != null) {
                crcf.setChildrenSortType(componentChildrenSortType);
            }
        }

        public void filterChanged() {
            throw new UnsupportedOperationException(bundle.getString("CTL_not_supported_yet"));
        }
    }
}
