/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.ui.navigator.node.ServiceNode;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class HostServiceL2ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private final DCL dcl = new DCL();

    public HostServiceL2ChildFactory(JvmDataSource jvm) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, ServiceDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, ServiceDataSource.class);
        }
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        List<ServiceDataSource> slist = new LinkedList<ServiceDataSource>();
        if (jvm != null) {
            for (ServiceDataSource ds : jvm.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
                slist.add(ds);
            }
//            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
//                for (ServiceDataSource ads : ds.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
//                    if (host.equals(ads.getHost())) {
//                        slist.add(ads);
//                    }
//                }
//            }
        }
        Collections.sort(slist, NameComparator.INSTANCE);
        for (ServiceDataSource sds : slist) {
            arg0.add(sds.getServiceName());
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        ServiceDataSource sds = jvm.getServiceDataSource(arg0);
        if (sds != null) {
            return new ServiceNode(sds, Children.LEAF);
//        } else {
//            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
//                sds = ds.getServiceDataSource(arg0);
//                if (sds != null) {
//                    return new ServiceNode(sds, Children.LEAF);
//                }
//            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        Node n = createNodeForKey(arg0);
        if (n != null) {
            nodez.add(n);
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    private class DCL implements DataChangeListener<ServiceDataSource> {

        public void dataChanged(DataChangeEvent<ServiceDataSource> arg0) {
            refresh(true);
        }
    }

    private static class NameComparator implements
            Comparator<ServiceDataSource> {

        private static NameComparator INSTANCE = new NameComparator();

        public int compare(ServiceDataSource o1, ServiceDataSource o2) {
            return o1.getServiceName().compareTo(o2.getServiceName());
        }
    }

    private static class StatusComparator implements
            Comparator<ServiceDataSource> {

        private static StatusComparator INSTANCE = new StatusComparator();

        public int compare(ServiceDataSource o1, ServiceDataSource o2) {
            return o1.getServiceState().compareTo(o2.getServiceState());
        }
    }
}
