/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.actions.component;

import com.sun.grid.sdm.ui.navigator.node.actions.*;
import com.sun.grid.sdm.api.component.ComponentDataSource;
import java.io.IOException;
import javax.swing.Action;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.Repository;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class StartupSDMComponentAction extends CookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/startup_component_16.png";
    private static final StartupSDMComponentAction SINGLETON = new StartupSDMComponentAction();

    protected void performAction(Node[] activatedNodes) {
        ComponentDataSource componentDataSource = activatedNodes[0].getLookup().lookup(ComponentDataSource.class);
        if (componentDataSource != null) {
            // todo NASTY to use string, fix it for next release
            String packagename = componentDataSource.getVersionName(componentDataSource.getVersion());
            String actionpattern = "com-sun-grid-sdm-ui-%s-navigator-actions-component-StartupSDMComponentAction.instance";

            FileObject sdmLCFolder = Repository.getDefault().getDefaultFileSystem().findResource("CustomSDM/Tools/SDM system lifecycle");
            FileObject actionobject = sdmLCFolder.getFileObject(String.format(actionpattern, packagename));
            DataObject actiondo = null;
            try {
                actiondo = DataObject.find(actionobject);
            } catch (DataObjectNotFoundException ex) {
                return;
            }

            RelaxedCookieAction action = null;
            try {
                action = (RelaxedCookieAction) actiondo.getCookie(InstanceCookie.class).instanceCreate();
            } catch (IOException ex) {
                return;
            } catch (ClassNotFoundException ex) {
                return;
            }

            if (action == null) {
                return;
            } else {
                action.performAction(activatedNodes);
            }
        }
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        boolean enable = super.enable(activatedNodes);
        if (enable) {
            ComponentDataSource componentDataSource = activatedNodes[0].getLookup().lookup(ComponentDataSource.class);
            if (componentDataSource != null) {
                // todo NASTY to use string, fix it for next release
                String packagename = componentDataSource.getVersionName(componentDataSource.getVersion());
                String actionpattern = "com-sun-grid-sdm-ui-%s-navigator-actions-component-StartupSDMComponentAction.instance";

                FileObject sdmLCFolder = Repository.getDefault().getDefaultFileSystem().findResource("CustomSDM/Tools/SDM system lifecycle");

                if (sdmLCFolder == null) {
                    System.out.println("There is no folder with name '" + "Custom/Tools/SDM system lifecycle" + "' on the default file system!"); // NOI18N
                    return false;
                }

                FileObject actionobject = sdmLCFolder.getFileObject(String.format(actionpattern, packagename));
                if (actionobject == null) {
                    return false;
                }

                DataObject actiondo;
                try {
                    actiondo = DataObject.find(actionobject);
                } catch (DataObjectNotFoundException ex) {
                    return false;
                }

                Action action = null;
                try {
                    action = (Action) actiondo.getCookie(InstanceCookie.class).instanceCreate();
                } catch (IOException ex) {
                    return false;
                } catch (ClassNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }

                if (action == null) {
                    return false;
                }

                return (componentDataSource.getComponentState().equals("STOPPED") ||
                        componentDataSource.getComponentState().equals("UNKNOWN"));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return NbBundle.getMessage(StartupSDMComponentAction.class, "CTL_StartupSDMComponent");
    }

    @Override
    protected String iconResource() {
        return ICON_PATH;
    }

    @Override
    protected void initialize() {
        super.initialize();
        // see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
        putValue("noIconInMenu", Boolean.TRUE);
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{ComponentDataSource.class};
    }

    public static final StartupSDMComponentAction getInstance() {
        return SINGLETON;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

