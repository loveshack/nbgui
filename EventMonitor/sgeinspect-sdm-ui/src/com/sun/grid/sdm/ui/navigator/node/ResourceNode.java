/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node;

import com.sun.grid.sdm.api.resource.ResourceDataSource;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.NbBundle;

public class ResourceNode extends AbstractNode {

    private ResourceDataSource resource;
    private static final Logger log = Logger.getLogger(ResourceNode.class.getName());

    public ResourceNode(Children c, ResourceDataSource r) {
        super(c);
        this.resource = r;
        this.resource.addPropertyChangeListener(new PCL());
    }

    @Override
    public String getName() {
        return this.resource.getName();
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public String getHtmlDisplayName() {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='222222'>");
        sb.append(getDisplayName());
        sb.append("</font>");
        return sb.toString();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Property<String> prop = new ReadOnlyProperty(NbBundle.getMessage(ResourceNode.class, "name"), resource.getName(),
                NbBundle.getMessage(ResourceNode.class, "name"),
                NbBundle.getMessage(ResourceNode.class, "A_resource_name"));
        set.put(prop);
        prop = new ReadOnlyProperty(NbBundle.getMessage(ResourceNode.class, "id"), resource.getId(),
                NbBundle.getMessage(ResourceNode.class, "id"),
                NbBundle.getMessage(ResourceNode.class, "A_resource_id"));
        set.put(prop);
        prop = new ReadOnlyProperty(NbBundle.getMessage(ResourceNode.class, "resource_state"), resource.getState(),
                NbBundle.getMessage(ResourceNode.class, "resource_state"),
                NbBundle.getMessage(ResourceNode.class, "A_resource_state"));
        set.put(prop);
        prop = new ReadOnlyProperty(NbBundle.getMessage(ResourceNode.class, "resource_type"), resource.getType(),
                NbBundle.getMessage(ResourceNode.class, "resource_type"),
                NbBundle.getMessage(ResourceNode.class, "A_resource_type"));
        sheet.put(set);
        for (Map.Entry<String, Object> entry : resource.getProperties().entrySet()) {
            prop = new ReadOnlyProperty(entry.getKey(), entry.getValue(), entry.getKey(), null);
            set.put(prop);
        }
        return sheet;
    }

    private class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private final Object value;

        private ReadOnlyProperty(String name, Object value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return value.toString();
        }
    }

    @Override
    public Action[] getActions(boolean popup) {
        return super.getActions(popup);
    }

    @Override
    public String toString() {
        return String.format("[ ResourceNode, name=%s ]", getName());
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            setSheet(createSheet());
        }
    }
}
