/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.service;

import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.api.service.cloud.CloudServiceDataSource;
import com.sun.grid.sdm.api.service.ge.GEServiceDataSource;
import com.sun.grid.sdm.api.service.general.GeneralServiceDataSource;
import com.sun.grid.sdm.api.service.spare_pool.SparePoolServiceDataSource;
import com.sun.grid.sdm.ui.service.cloud.CloudServiceDataSourceDescriptor;
import com.sun.grid.sdm.ui.service.ge.GEServiceDataSourceDescriptor;
import com.sun.grid.sdm.ui.service.general.GeneralServiceDataSourceDescriptor;
import com.sun.grid.sdm.ui.service.spare_pool.SparePoolServiceDataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;

public class ServiceDataSourceDescriptorProvider extends AbstractModelProvider<DataSourceDescriptor, DataSource> {

    public static final ServiceDataSourceDescriptorProvider INSTANCE = new ServiceDataSourceDescriptorProvider();

    @Override
    public ServiceDataSourceDescriptor createModelFor(DataSource ds) {
        if (ds instanceof GEServiceDataSource) {
            return new GEServiceDataSourceDescriptor((GEServiceDataSource) ds);
        } else if (ds instanceof CloudServiceDataSource) {
            return new CloudServiceDataSourceDescriptor((CloudServiceDataSource) ds);
        } else if (ds instanceof SparePoolServiceDataSource) {
            return new SparePoolServiceDataSourceDescriptor((SparePoolServiceDataSource) ds);
        } else if (ds instanceof GeneralServiceDataSource) {
            return new GeneralServiceDataSourceDescriptor((GeneralServiceDataSource) ds);
        } else if (ds instanceof ServiceDataSource) {
            return new ServiceDataSourceDescriptor((ServiceDataSource) ds);
        } else {
            return null;
        }
    }

    @Override
    public int priority() {
        return 0;
    }

    public static void initialize() {
        DataSourceDescriptorFactory.getDefault().registerProvider(INSTANCE);
    }

    public static void shutdown() {
        DataSourceDescriptorFactory.getDefault().unregisterProvider(INSTANCE);
    }
}

