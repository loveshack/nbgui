/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.actions;

import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class OpenServiceAction extends CookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_service.png";
    private static final OpenServiceAction SINGLETON = new OpenServiceAction();

    private OpenServiceAction() {
    }

    protected void performAction(Node[] activatedNodes) {
        final DataSourceWindowManager manager = DataSourceWindowManager.sharedInstance();
        for (Node n : activatedNodes) {
            ServiceDataSource service = n.getLookup().lookup(ServiceDataSource.class);
            manager.openDataSource(service);
        }
    }

    protected int mode() {
        return CookieAction.MODE_ALL;
    }

    public String getName() {
        return NbBundle.getMessage(OpenServiceAction.class, "CTL_OpenServiceAction");
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{ServiceDataSource.class};
    }    

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected String iconResource() {
        return ICON_PATH;
    }

    @Override
    protected void initialize() {
        super.initialize();
        // see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
        putValue("noIconInMenu", Boolean.TRUE);
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    public static final OpenServiceAction getInstance() {
        return SINGLETON;
    }

}

