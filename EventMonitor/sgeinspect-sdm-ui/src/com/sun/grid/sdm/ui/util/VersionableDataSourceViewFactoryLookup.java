/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.sdm.ui.util;

import com.sun.grid.sdm.api.base.VersionableDataSource;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Item;
import org.openide.util.Lookup.Template;
import org.openide.util.lookup.AbstractLookup;

/**
 */
public class VersionableDataSourceViewFactoryLookup<T extends VersionableDataSource> extends AbstractLookup {

    private final static long serialVersionUID = -209093001L;

    private VersionableDataSourceViewFactoryLookup() {
    }

    public static <U extends VersionableDataSource> VersionableDataSourceViewFactoryLookup<U> createInstance() {
        return new VersionableDataSourceViewFactoryLookup<U>();
    }

    @Override
    protected void beforeLookup(Template<?> tmplt) {
        super.beforeLookup(tmplt);
    }

    public void registerFactory(VersionableDataSourceViewFactory<T> instance) {
        P<T> p = new P<T>(instance);
        addPair(p);
    }

    public void unregisterFactory(VersionableDataSourceViewFactory<T> instance) {
        P<T> p = new P<T>(instance);
        removePair(p);
    }

    @SuppressWarnings({"unchecked", "unchecked"})
    public VersionableDataSourceViewFactory<T> getFactory(String version) {
        final Lookup.Result<VersionableDataSourceViewFactory> result = lookup(new Template<VersionableDataSourceViewFactory>(VersionableDataSourceViewFactory.class));
        @SuppressWarnings("unchecked")
        Collection<? extends Item<VersionableDataSourceViewFactory>> pairs = result.allItems();

        for (Item<VersionableDataSourceViewFactory> pair : pairs) {
            if (version.equals(pair.getInstance().getVersion())) {
                return pair.getInstance();
            }
        }

        return null;
    }

    public Collection<String> getAllFactoryVersions() {
        final Lookup.Result<VersionableDataSourceViewFactory> result = lookup(new Template<VersionableDataSourceViewFactory>(VersionableDataSourceViewFactory.class));
        @SuppressWarnings("unchecked")
        Collection<? extends Item<VersionableDataSourceViewFactory>> pairs = result.allItems();

        Set<String> versions = new HashSet<String>();
        for (Item<VersionableDataSourceViewFactory> pair : pairs) {
            versions.add(pair.getInstance().getVersion());
        }

        return versions;
    }

    private static final class P<V extends VersionableDataSource> extends AbstractLookup.Pair<VersionableDataSourceViewFactory<V>> {

        private static final long serialVersionUID = -2009093001L;
        VersionableDataSourceViewFactory<V> instance;

        public P(VersionableDataSourceViewFactory<V> instance) {
            this.instance = instance;
        }

        @Override
        protected boolean instanceOf(Class<?> clazz) {
            return clazz.isAssignableFrom(instance.getClass());
        }

        @Override
        protected boolean creatorOf(Object obj) {
            return obj == instance;
        }

        @Override
        public VersionableDataSourceViewFactory<V> getInstance() {
            return instance;
        }

        @Override
        @SuppressWarnings("unchecked")
        public Class<? extends VersionableDataSourceViewFactory<V>> getType() {
            return (Class<? extends VersionableDataSourceViewFactory<V>>) instance.getClass();
        }

        @Override
        public String getId() {
            return instance.getClass().getName();
        }

        @Override
        public String getDisplayName() {
            return getId();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final P other = (P) obj;
            if (this.instance != other.instance && (this.instance == null || !this.instance.equals(other.instance))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + (this.instance != null ? this.instance.hashCode() : 0);
            return hash;
        }
    }
}
