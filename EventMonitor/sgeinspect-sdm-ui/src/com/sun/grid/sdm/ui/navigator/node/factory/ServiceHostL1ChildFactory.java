/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.ui.navigator.node.*;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ServiceHostL1ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private static final Map<JvmDataSource, ServiceHostL1ChildFactory> f = new WeakHashMap<JvmDataSource, ServiceHostL1ChildFactory>();
    private final DCL dcl = new DCL();

    private ServiceHostL1ChildFactory(JvmDataSource jvm) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, ServiceDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, ServiceDataSource.class);
        }
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        Set<String> names = new HashSet<String>();
        if (jvm != null) {
            for (ServiceDataSource ds : jvm.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
                names.add(ds.getServiceName());
            }
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                for (ServiceDataSource ads : ds.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
                    names.add(ads.getServiceName());
                }
            }
        }
        arg0.addAll(names);
        Collections.sort(arg0);
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        // we just need a first matching service - to get correct icon ..
        if (jvm != null) {
            ServiceDataSource cds = jvm.getServiceDataSource(arg0);
            if (cds != null) {
                return new ServiceL1Node(cds, Children.create(new ServiceHostL2ChildFactory(jvm, arg0), true));
            } else {
                for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                    cds = ds.getServiceDataSource(arg0);
                    if (cds != null) {
                        return new ServiceL1Node(cds, Children.create(new ServiceHostL2ChildFactory(jvm, arg0), true));
                    }
                }
            }
            return null;
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        Node n = createNodeForKey(arg0);
        if (n != null) {
            nodez.add(n);
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    public static ServiceHostL1ChildFactory getFactory(JvmDataSource jvm) {
        ServiceHostL1ChildFactory factory = f.get(jvm);
        if (factory == null) {
            factory = new ServiceHostL1ChildFactory(jvm);
            f.put(jvm, factory);
        }
        return factory;
    }

    private class DCL implements DataChangeListener<ServiceDataSource> {

        public void dataChanged(DataChangeEvent<ServiceDataSource> arg0) {
            refresh(true);
        }
    }
}
