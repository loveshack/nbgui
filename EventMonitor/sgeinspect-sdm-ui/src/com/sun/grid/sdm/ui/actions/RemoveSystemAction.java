/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.actions;

import com.sun.grid.sdm.api.base.SDMContainer;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.event.ActionEvent;
import org.openide.util.NbBundle;

public class RemoveSystemAction extends SingleDataSourceAction<JvmDataSource> {

    private final static long serialVersionUID = -2009100101L;

    private static final class Singleton {

        private static final RemoveSystemAction INSTANCE = new RemoveSystemAction();

        private Singleton() {
        }
    }

    public static RemoveSystemAction createInstance() {
        return Singleton.INSTANCE;
    }

    private RemoveSystemAction() {
        super(JvmDataSource.class);
        putValue(NAME, NbBundle.getMessage(RemoveSystemAction.class, "LBL_Remove_System"));
    }

    protected void actionPerformed(final JvmDataSource ds, ActionEvent event) {
        if (ds.isCS()) {
            DataSource.EVENT_QUEUE.post(new Runnable() {

                public void run() {
                    SDMContainer.sharedInstance().removeSDMWrapperDS(ds);
                }
            });
        }
    }

    protected boolean isEnabled(JvmDataSource ds) {
        return ds.isCS();
    }
}

