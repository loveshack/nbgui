/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.jvm;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.util.VersionableDataSourceViewProvider;
import com.sun.grid.shared.ui.util.ErrorDisplayer;
import com.sun.tools.visualvm.core.datasupport.Stateful;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.NbBundle;

public class SDMSystemViewProvider extends VersionableDataSourceViewProvider<JvmDataSource> {

    private static final SDMSystemViewProvider instance = new SDMSystemViewProvider();

    public SDMSystemViewProvider() {
        super(JvmDataSource.class);
        registerCVDataSourceViewFactory(new SDMSystemViewFactory());
    }

    public static SDMSystemViewProvider getSharedInstance() {
        return instance;
    }

    @Override
    public DataSourceView createView(JvmDataSource jvm) {
        jvm.addPropertyChangeListener(new JvmStateChangeListener(jvm));
        return super.createView(jvm);
    }

    private static class JvmStateChangeListener implements PropertyChangeListener {

        private final JvmDataSource jvm;

        JvmStateChangeListener(JvmDataSource dataSource) {
            this.jvm = dataSource;
        }

        public void propertyChange(PropertyChangeEvent evt) {
            if (Stateful.PROPERTY_STATE.equals(evt.getPropertyName())) {
                if (Stateful.STATE_UNAVAILABLE == evt.getNewValue()) {
                    ErrorDisplayer.submitWarning(NbBundle.getMessage(SDMSystemViewProvider.class, "MSG_SystemDown", jvm.toString()));
                    jvm.removePropertyChangeListener(this);
                    DataSourceWindowManager.sharedInstance().closeDataSource(jvm);
                    DataSourceWindowManager.sharedInstance().closeDataSource(jvm.getApplication());
                }
            }
        }
    }
}

