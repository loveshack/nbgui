/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.JVMNode;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class HostJVML2ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private final String host;
    private final DCL dcl = new DCL();
    private final int sortType;

    public HostJVML2ChildFactory(JvmDataSource jvm, String host, int sortType) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, JvmDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, JvmDataSource.class);
        }
        this.host = host;
        this.sortType = sortType;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        List<JvmDataSource> clist = new LinkedList<JvmDataSource>();
        if (jvm != null) {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                if (host.equals(ds.getHost())) {
                    clist.add(ds);
                }
            }
            if (host.equals(jvm.getHost())) {
                clist.add(jvm);
            }
        }

//        if (sortType == 1) {
        Collections.sort(clist, NameComparator.INSTANCE);
//        } else if (sortType ==2) {
//            Collections.sort(clist, StatusComparator.INSTANCE);
//        }
        for (JvmDataSource cds : clist) {
            arg0.add(wrap(cds.getName(), cds.getHost()));
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        JvmDataSource cds = jvm.getJvmDataSource(unwrapName(arg0), host);
        if (cds != null) {
            return createJvmNode(cds);
        } else if (jvm.getName().equals(unwrapName(arg0))) {
            return createJvmNode(jvm);
        } else {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                cds = ds.getJvmDataSource(unwrapName(arg0), host);
                if (cds != null) {
                    return createJvmNode(cds);
                }
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        JvmDataSource cds = jvm.getJvmDataSource(unwrapName(arg0), host);
        if (cds != null) {
            nodez.add(createJvmNode(cds));
        } else if (jvm.getName().equals(unwrapName(arg0))) {
            nodez.add(createJvmNode(jvm));
        } else {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                cds = ds.getJvmDataSource(unwrapName(arg0), host);
                if (cds != null) {
                    nodez.add(createJvmNode(cds));
                }
            }
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    private static String wrap(String name, String host) {
        return String.format("%s@%s", name, host);
    }

    private static String unwrapHost(String id) {
        int pos = id.lastIndexOf('@');
        if (pos == -1) {
            return "#unknown#";
        } else {
            return id.substring(pos + 1);
        }
    }

    private static String unwrapName(String id) {
        int pos = id.lastIndexOf('@');
        if (pos == -1) {
            return id;
        } else {
            return id.substring(0, pos);
        }
    }

    private class DCL implements DataChangeListener<JvmDataSource> {

        public void dataChanged(DataChangeEvent<JvmDataSource> arg0) {
            refresh(true);
        }
    }

    private Node createJvmNode(JvmDataSource cds) {
        if (cds instanceof JvmDataSource) {
            return new JVMNode(cds, Children.create(new JVML3ChildFactory(cds), true));
        } else {
            return AbstractNode.EMPTY;
        }
    }

    private static class NameComparator implements
            Comparator<JvmDataSource> {

        private static NameComparator INSTANCE = new NameComparator();

        public int compare(JvmDataSource o1, JvmDataSource o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    private static class StatusComparator implements
            Comparator<ComponentDataSource> {

        private static StatusComparator INSTANCE = new StatusComparator();

        public int compare(ComponentDataSource o1, ComponentDataSource o2) {
            return o1.getComponentState().compareTo(o2.getComponentState());
        }
    }
}
