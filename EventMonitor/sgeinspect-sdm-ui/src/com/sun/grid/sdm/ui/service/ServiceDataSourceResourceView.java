/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.service;

import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent.DetailsView;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ServiceDataSourceResourceView extends DataSourceView {

    private ServiceResourcesTablePanel resources;
    private ServiceCachedResourcesTablePanel cached;

    public ServiceDataSourceResourceView(ServiceDataSource sds) {
        super(sds, NbBundle.getMessage(ServiceDataSourceResourceView.class, "service_resource_view"),
                ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_resources_all.png"), 0x7fffffff, true);
    }

    protected DataViewComponent createComponent() {
        DataViewComponent.MasterView mv = new DataViewComponent.MasterView(
                NbBundle.getMessage(ServiceDataSourceResourceView.class, "service_resource_view"),
                NbBundle.getMessage(ServiceDataSourceResourceView.class, "SDM_service_resource_view"), new JPanel());
        DataViewComponent dvc = new DataViewComponent(mv, new DataViewComponent.MasterViewConfiguration(false));
        dvc.addDetailsView(getServiceResourcesView(), DataViewComponent.TOP_RIGHT);
        dvc.addDetailsView(getServiceCachedResourcesView(), DataViewComponent.BOTTOM_RIGHT);
        return dvc;
    }

    @Override
    protected void added() {
        // todo register details view to listen to service state changes
    }

    @Override
    protected void removed() {
        // todo unregister details view service listener
    }

    protected DetailsView getServiceResourcesView() {
        if (resources == null) {
            resources = new ServiceResourcesTablePanel((ServiceDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ServiceDataSourceResourceView.class, "Resources"),
                NbBundle.getMessage(ServiceDataSourceResourceView.class, "Service_Resources"), 11, resources, null);
    }

    protected DetailsView getServiceCachedResourcesView() {
        if (cached == null) {
            cached = new ServiceCachedResourcesTablePanel((ServiceDataSource) getDataSource());
        }
        return new DataViewComponent.DetailsView(NbBundle.getMessage(ServiceDataSourceResourceView.class, "Cached_Resources"),
                NbBundle.getMessage(ServiceDataSourceResourceView.class, "Service_Cached_Resources"), 11, cached, null);
    }
}
