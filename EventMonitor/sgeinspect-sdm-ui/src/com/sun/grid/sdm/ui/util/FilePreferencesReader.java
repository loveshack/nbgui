/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * FilePreferencesReader for sdm 1.0, 1.0u1, 1.0u2, 1.0u3, 1.0u4, 1.0u5
 */
public class FilePreferencesReader extends AbstractPreferences {

    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    private final static Logger log = Logger.getLogger(FilePreferencesReader.class.getName());
    private static File systemRootDir = null;
    private static RootFilePreferences systemRoot = null;
    private static File userRootDir = null;
    private static RootFilePreferences userRoot = null;

    public static class RootFilePreferences extends FilePreferencesReader {
        // here we use a SaveFileLock to ensure that both the file lock and the open file will be closed correctly (issue 601)

        private File modFile;
        private boolean canWrite;

        public RootFilePreferences(File dir, boolean isUser) {
            super(dir, isUser);
            if (!dir.exists()) {
//                log.log(Level.FINE, "mkdir", dir);
            }
            modFile = new File(dir, ".modFile");
            canWrite = dir.canWrite();
            if (canWrite && !modFile.exists()) {
                try {
                    // create if does not exist.
                    modFile.createNewFile();
                } catch (IOException e) {
                    log.warning(e.toString());
                }
            }
        }

        @Override
        public void flush() throws BackingStoreException {
            if (canWrite) {
                super.flush();
            }
        }
    }
    private final File dir;
    private final File prefsFile;
    private Map<String, String> prefsCache = null;
    private final boolean isUserNode;

    /**
     * Get the root node of the user preferences
     * @return the root node of the user preferences
     */
    public static synchronized Preferences getUserRoot() {
        if (userRoot == null) {
            userRoot = AccessController.doPrivileged(new PrivilegedAction<RootFilePreferences>() {

                public RootFilePreferences run() {
                    if (userRootDir == null) {
                        userRootDir = new File(System.getProperty("user.home"), ".sdm" + File.separatorChar + "bootstrap");
                    }
                    return new RootFilePreferences(userRootDir, true);
                }
            });
        }
        return userRoot;
    }

    public static synchronized Preferences getSystemRoot() {
        if (systemRoot == null) {
            systemRoot = AccessController.doPrivileged(new PrivilegedAction<RootFilePreferences>() {

                public RootFilePreferences run() {
                    if (systemRootDir == null) {
                        systemRootDir = new File("/etc/sdm/bootstrap".replace('/', File.separatorChar));
                    }
                    return new RootFilePreferences(systemRootDir, false);
                }
            });
        }
        return systemRoot;
    }

    protected FilePreferencesReader(File dir, boolean user) {
        super(null, "");
        isUserNode = user;
        this.dir = dir;
        prefsFile = new File(dir, "prefs.properties");
    }

    private FilePreferencesReader(FilePreferencesReader parent, String name) {
        super(parent, name);
        isUserNode = parent.isUserNode;
        dir = new File(parent.dir, dirName(name));
        prefsFile = new File(dir, "prefs.properties");
        AccessController.doPrivileged(new PrivilegedAction<Void>() {

            public Void run() {
                newNode = !dir.exists();
                return null;
            }
        });
        if (newNode) {
            // Add an entry to the change log to ensure that
            // the next sync will create the node
            prefsCache = new TreeMap<String, String>();
        }
    }

    @Override
    public boolean isUserNode() {
        return isUserNode;
    }

    protected void putSpi(String key, String value) {
        // do nothing;
    }

    protected String getSpi(String key) {
        initCache();
        return prefsCache.get(key);
    }

    protected void removeSpi(String key) {
        // do nothing;
    }

    private void initCache() {
        if (prefsCache == null) {
            try {
                loadCache();
            } catch (Exception e) {
                prefsCache = new TreeMap<String, String>();
            }
        }
    }

    private void loadCache() throws BackingStoreException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {

                public Void run() throws BackingStoreException {
                    Properties props = new Properties();
                    long newLastSyncTime = 0;
                    try {
                        newLastSyncTime = prefsFile.lastModified();
                        FileInputStream fis = new FileInputStream(prefsFile);
                        props.load(fis);
                        fis.close();
                    } catch (FileNotFoundException e) {
                        log.log(Level.FINE, "prefs.removed", prefsFile);
                    } catch (Exception e) {
                        throw new BackingStoreException(e);
                    }
                    // Attempt succeeded; update state
                    prefsCache = new TreeMap<String, String>();
                    for (Object key : props.keySet()) {
                        String s = key.toString();
                        prefsCache.put(s, props.getProperty(s));
                    }

                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw (BackingStoreException) e.getException();
        }
    }

    protected String[] keysSpi() {
        initCache();
        return prefsCache.keySet().toArray(new String[prefsCache.size()]);
    }

    protected String[] childrenNamesSpi() {
        return AccessController.doPrivileged(new PrivilegedAction<String[]>() {

            public String[] run() {
                List<String> result = new ArrayList<String>();
                File[] dirContents = dir.listFiles();
                if (dirContents != null) {
                    for (int i = 0; i < dirContents.length; i++) {
                        if (dirContents[i].isDirectory()) {
                            String name = nodeName(dirContents[i].getName());
                            if (log.isLoggable(Level.FINER)) {
                                log.log(Level.FINER, "subdir", new Object[]{dirContents[i], name});
                            }
                            result.add(name);
                        }
                    }
                }
                return result.toArray(EMPTY_STRING_ARRAY);
            }
        });
    }

    protected AbstractPreferences childSpi(String name) {
        return new FilePreferencesReader(this, name);
    }

    @Override
    public void removeNode() throws BackingStoreException {
        // do nothing;
    }

    /**
     * Called with file lock held (in addition to node locks).
     */
    protected void removeNodeSpi() throws BackingStoreException {
        // do nothing;
    }

    @Override
    public synchronized void sync() throws BackingStoreException {
        // do nothing;
    }

    protected void syncSpi() throws BackingStoreException {
        // do nothing;
    }

    @Override
    public void flush() throws BackingStoreException {
        // do nothing;
    }

    protected void flushSpi() throws BackingStoreException {
        // do nothing;
    }

    private static boolean isDirChar(char ch) {
        return ch > 0x1f && ch < 0x7f && ch != '/' && ch != '.' && ch != '_' && ch != '\\' && ch != '#';
    }

    private static int encode(char ch) {
        Charset charSet = Charset.forName("UTF8");
        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(ch);
        cb.position(0);
        ByteBuffer bb = charSet.encode(cb);
        switch (bb.capacity()) {
            case 1:
                return bb.get();
            case 2:
                return bb.get() | (bb.get() << 8);
            default:
                throw new IllegalStateException("encoded char has invalid length");
        }
    }

    private static char decode(int c) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        byte b = (byte) ((0xff00 & c) >> 8);
        if (b != 0) {
            bb.put(b);
        }
        b = (byte) (0xff & c);
        bb.put(b);
        bb.position(0);
        Charset charSet = Charset.forName("UTF8");
        CharBuffer cb = charSet.decode(bb);
        return cb.get();
    }

    private static String dirName(String nodeName) {
        StringBuilder ret = new StringBuilder(nodeName.length());
        for (int i = 0, n = nodeName.length(); i < n; i++) {
            char c = nodeName.charAt(i);
            if (isDirChar(c)) {
                ret.append(c);
            } else {
                ret.append('#');
                ret.append(String.format("%04x", encode(c)));
            }
        }
        return ret.toString();
    }

    private static String nodeName(String dirName) {
        StringBuffer result = new StringBuffer(dirName.length());
        for (int i = 0, n = dirName.length(); i < n; i++) {
            char c = dirName.charAt(i);
            if (c == '#' && i + 4 < n) {
                String tmp = dirName.substring(i + 1, i + 5);
                try {
                    c = decode(Integer.parseInt(tmp, 16));
                    i += 4;
                } catch (NumberFormatException ex) {
                    // ignore
                }
            }
            result.append(c);
        }
        return result.toString();
    }
}
