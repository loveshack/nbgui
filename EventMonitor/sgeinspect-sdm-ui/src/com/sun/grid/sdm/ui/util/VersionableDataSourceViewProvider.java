/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.util;

import com.sun.grid.sdm.api.base.VersionableDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.DataSourceViewProvider;
import com.sun.tools.visualvm.core.ui.DataSourceViewsManager;
import java.util.Collection;
import java.util.logging.Logger;

public abstract class VersionableDataSourceViewProvider<T extends VersionableDataSource> extends DataSourceViewProvider<T> {

    private static final String BUNDLE = "com.sun.grid.sdm.ui.util.Bundle";
    private static final Logger log = Logger.getLogger(VersionableDataSourceViewProvider.class.getName(), BUNDLE);
    private static final long serialVersionUID = -2009100101L;
    private final VersionableDataSourceViewFactoryLookup<T> lookup = VersionableDataSourceViewFactoryLookup.<T>createInstance();
    private final Class<T> dsType;

    public VersionableDataSourceViewProvider(Class<T> type) {
        dsType = type;
    }

    protected DataSourceView createView(T app) {
        VersionableDataSourceViewFactory<T> f;
        if (getRegisteredVersions().contains(app.getVersion())) {
            f = getRegisteredCVDataSourceViewFactory(app.getVersion());
        } else {
            f = getRegisteredCVDataSourceViewFactory(VersionableDataSourceViewFactory.ALL);
        }
        assert f != null;
        return f.createView(app);

    }

    protected boolean supportsViewFor(T app) {
        if (getRegisteredVersions().contains(app.getVersion())) {
            log.info("@@@ got dedicated view @@@");
        } else {
            log.info("@@@ DEFAULT view will be used @@@");
        }
        return true;
    }

    public void initialize() {
        DataSourceViewsManager.sharedInstance().addViewProvider(this, dsType);
    }

    public void shutdown() {
        DataSourceViewsManager.sharedInstance().removeViewProvider(this);
    }

    public void registerCVDataSourceViewFactory(VersionableDataSourceViewFactory<T> factory) {
        lookup.registerFactory(factory);
    }

    public void unregisterCVDataSourceViewFactory(VersionableDataSourceViewFactory<T> factory) {
        lookup.unregisterFactory(factory);
    }

    public Collection<String> getRegisteredVersions() {
        return lookup.getAllFactoryVersions();
    }

    public VersionableDataSourceViewFactory<T> getRegisteredCVDataSourceViewFactory(String version) {
        return lookup.getFactory(version);
    }
}

