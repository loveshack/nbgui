/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.grid.sdm.ui.navigator.node.ServiceL2Node;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ServiceHostL2ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private final String name;
    private final DCL dcl = new DCL();

    public ServiceHostL2ChildFactory(JvmDataSource jvm, String name) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, ServiceDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, ServiceDataSource.class);
        }
        this.name = name;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        List<ServiceDataSource> clist = new LinkedList<ServiceDataSource>();
        if (jvm != null) {
            for (ServiceDataSource ds : jvm.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
                if (name.equals(ds.getServiceName())) {
                    clist.add(ds);
                }
            }
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                for (ServiceDataSource ads : ds.getApplication().getRepository().getDataSources(ServiceDataSource.class)) {
                    if (name.equals(ads.getServiceName())) {
                        clist.add(ads);
                    }
                }
            }
        }

        Collections.sort(clist, HostNameComparator.INSTANCE);
        for (ServiceDataSource cds : clist) {
            arg0.add(wrap(cds.getServiceName(), cds.getHostname()));
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        ServiceDataSource cds = jvm.getServiceDataSource(unwrapName(arg0));
        if (cds != null && jvm.getHost().equals(unwrapHost(arg0))) {
            return createNode(cds);
        } else {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                cds = ds.getServiceDataSource(unwrapName(arg0));
                if (cds != null && ds.getHost().equals(unwrapHost(arg0))) {
                    return createNode(cds);
                }
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        ServiceDataSource cds = jvm.getServiceDataSource(unwrapName(arg0));
        if (cds != null && jvm.getHost().equals(unwrapHost(arg0))) {
            nodez.add(createNode(cds));
        } else {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                cds = ds.getServiceDataSource(unwrapName(arg0));
                if (cds != null && ds.getHost().equals(unwrapHost(arg0))) {
                    nodez.add(createNode(cds));
                }
            }
        }
        return nodez.toArray(new Node[nodez.size()]);
    }

    private static String wrap(String name, String host) {
        return String.format("%s@%s", name, host);
    }

    private static String unwrapHost(String id) {
        int pos = id.lastIndexOf('@');
        if (pos == -1) {
            return "#unknown#";
        } else {
            return id.substring(pos + 1);
        }
    }

    private static String unwrapName(String id) {
        int pos = id.lastIndexOf('@');
        if (pos == -1) {
            return id;
        } else {
            return id.substring(0, pos);
        }
    }

    private class DCL implements DataChangeListener<ServiceDataSource> {

        public void dataChanged(DataChangeEvent<ServiceDataSource> arg0) {
            refresh(true);
        }
    }

    private Node createNode(ServiceDataSource sds) {
        return new ServiceL2Node(sds, Children.LEAF);
    }

    private static class HostNameComparator implements
            Comparator<ServiceDataSource> {

        private static HostNameComparator INSTANCE = new HostNameComparator();

        public int compare(ServiceDataSource o1, ServiceDataSource o2) {
            return o1.getHostname().compareTo(o2.getHostname());
        }
    }
}
