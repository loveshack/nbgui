/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.*;
import java.awt.Image;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.lookup.InstanceContent;

public class JVML3ChildFactory extends ChildFactory<String> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("com/sun/grid/sdm/ui/navigator/node/Bundle");
    private static final String C = bundle.getString("CTL_components");
    private static final String S = bundle.getString("CTL_services");
    private static final Image CI = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_component.png");
    private static final Image SI = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_service.png");
    private final JvmDataSource jds;

    public JVML3ChildFactory(JvmDataSource jds) {
        this.jds = jds;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        arg0.add(C);
        arg0.add(S);
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        if (C.equals(arg0)) {
            return new JVMContentNode(arg0, CI, Children.create(new HostComponentL2ChildFactory(jds, 0), true));
        } else if (S.equals(arg0)) {
            return new JVMContentNode(jds,arg0, SI, Children.create(new HostServiceL2ChildFactory(jds), true),new InstanceContent());
        } else {
            return null;
        }
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        nodez.add(createNodeForKey(arg0));
        return nodez.toArray(new Node[nodez.size()]);
    }

    private static class NameComparator implements
            Comparator<JvmDataSource> {

        private static NameComparator INSTANCE = new NameComparator();

        public int compare(JvmDataSource o1, JvmDataSource o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
