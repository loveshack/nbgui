/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.api.jvm.JvmDataSourceDescriptor;
import com.sun.grid.sdm.api.jvm.JvmDataSourceDescriptorProvider;
import com.sun.grid.sdm.ui.navigator.node.actions.OpenServiceAction;
import java.awt.Image;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class JVML1Node extends AbstractNode {

    private final JvmDataSource sds;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_vm.png";

    public JVML1Node(JvmDataSource sds, Children c) {
        super(c);
        this.sds = sds;
    }

    @Override
    public String getName() {
        return sds.getName();
    }

    @Override
    public String getDisplayName() {
        return sds.getName();
    }

    @Override
    public Image getIcon(int arg0) {
//        JvmDataSourceDescriptor sdsd = JvmDataSourceDescriptorProvider.INSTANCE.createModelFor(sds);
//        if (sdsd != null) {
//            return sdsd.getIcon();
//        } else {
            return ImageUtilities.loadImage(ICON_PATH);
//        }
    }

    @Override
    public Image getOpenedIcon(int arg0) {
        return getIcon(arg0);
    }

    @Override
    public Action[] getActions(boolean popup) {
        List<Action> actionz = new LinkedList<Action>();
        actionz.addAll(Arrays.asList(super.getActions(popup)));
        return actionz.toArray(new Action[actionz.size()]);
    }

    @Override
    public Action getPreferredAction() {
        return OpenServiceAction.getInstance();
    }

    @Override
    public String getHtmlDisplayName() {
        return getDisplayName();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            @SuppressWarnings("unchecked")
            Property idProp = new PropertySupport.Reflection(this, String.class,
                    "getDisplayName", null);

            idProp.setName(NbBundle.getMessage(JVML1Node.class, "jvm_name"));
            set.put(idProp);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, NbBundle.getMessage(JVML1Node.class, "error_property_sheet"), ex);
        }
        sheet.put(set);
        return sheet;
    }
}
