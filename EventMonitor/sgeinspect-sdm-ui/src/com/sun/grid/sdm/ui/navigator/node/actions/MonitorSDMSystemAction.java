/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.actions;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceWindowManager;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

/**
 * action that opens sdm system view. jvm node needs to expose JvmDataSource
 * as cookie.
 */
public final class MonitorSDMSystemAction extends CookieAction {

    private final static long serialVersionUID = -2009100101L;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_sdm.png";
    private static final MonitorSDMSystemAction SINGLETON = new MonitorSDMSystemAction();

    protected void performAction(Node[] activatedNodes) {
        final DataSourceWindowManager manager = DataSourceWindowManager.sharedInstance();
        for (Node n : activatedNodes) {
            JvmDataSource component = n.getLookup().lookup(JvmDataSource.class);
            manager.openDataSource(component);
        }
    }

    protected int mode() {
        return CookieAction.MODE_ALL;
    }

    public String getName() {
        return NbBundle.getMessage(MonitorSDMSystemAction.class, "CTL_MonitorSDMSystemAction");
    }

    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{JvmDataSource.class};
    }

    @Override
    protected String iconResource() {
        return ICON_PATH;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected void initialize() {
        super.initialize();
        // see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
        putValue("noIconInMenu", Boolean.TRUE);
    }

    public static final MonitorSDMSystemAction getInstance() {
        return SINGLETON;
    }
}

