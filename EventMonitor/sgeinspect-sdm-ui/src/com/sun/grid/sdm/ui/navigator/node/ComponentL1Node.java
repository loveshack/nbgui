/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node;

import com.sun.grid.sdm.api.component.ComponentDataSource;
import com.sun.grid.sdm.ui.navigator.node.actions.OpenComponentAction;
import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public abstract class ComponentL1Node<T extends ComponentDataSource> extends AbstractNode {

    protected final T cds;
    private static final String ICON_PATH = "com/sun/grid/sdm/ui/resources/icon_component.png";

    ComponentL1Node(T cds, Children c) {
        super(c);
        this.cds = cds;
    }

    @Override
    public String getName() {
        return cds.getComponentName(); // + "@[" +  cds.getJvmName() + ":" + cds.getHostname() + "]";
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public Image getIcon(int arg0) {
        return ImageUtilities.loadImage(ICON_PATH);
    }

    @Override
    public Image getOpenedIcon(int arg0) {
        return getIcon(arg0);
    }

    @Override
    public Action[] getActions(boolean popup) {
        List<Action> actionz = new LinkedList<Action>();
        actionz.addAll(Arrays.asList(super.getActions(popup)));
        return actionz.toArray(new Action[actionz.size()]);
    }

    @Override
    public Action getPreferredAction() {
        return OpenComponentAction.getInstance();
    }

    @Override
    public String getHtmlDisplayName() {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='000000' size='2'>");
        sb.append(getDisplayName());
        sb.append("</font>");
        return sb.toString();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            @SuppressWarnings("unchecked")
            Property idProp = new PropertySupport.Reflection(this, String.class,
                  "getDisplayName", null);

            idProp.setName(NbBundle.getMessage(ComponentL1Node.class, "component_name"));
            set.put(idProp);
            Property prop = new ReadOnlyProperty(
                  NbBundle.getMessage(ComponentL1Node.class, "jvm_name"), cds.getJvmName(), 
                  NbBundle.getMessage(ComponentL1Node.class, "jvm_name"), 
                  NbBundle.getMessage(ComponentL1Node.class, "A_name_of_jvm_holding_the_components"));
            set.put(prop);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, 
                  NbBundle.getMessage(ComponentL1Node.class, "error_property_sheet"), ex);
        }
        sheet.put(set);
        return sheet;
    }

    private class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private final String value;

        private ReadOnlyProperty(String name, String value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return value;
        }
    }
}
