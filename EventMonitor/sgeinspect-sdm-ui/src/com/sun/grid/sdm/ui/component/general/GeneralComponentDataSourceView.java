/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.component.general;

import com.sun.grid.sdm.api.component.general.GeneralComponentDataSource;
import com.sun.tools.visualvm.core.ui.DataSourceView;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent;
import com.sun.tools.visualvm.core.ui.components.DataViewComponent.DetailsView;
import javax.swing.JPanel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class GeneralComponentDataSourceView extends DataSourceView {

    private GeneralComponentDetailsTablePanel details;

    public GeneralComponentDataSourceView(GeneralComponentDataSource cds) {
        super(cds, NbBundle.getMessage(GeneralComponentDataSourceView.class, "LBL_SDM"),
                ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_component.png"), 0x7fffffff, true);
    }

    protected DataViewComponent createComponent() {
        if (details == null) {
            details = new GeneralComponentDetailsTablePanel((GeneralComponentDataSource) getDataSource());
        }
        DataViewComponent.MasterView mv = new DataViewComponent.MasterView(
                NbBundle.getMessage(GeneralComponentDataSourceView.class, "LBL_Component_Details"),
                NbBundle.getMessage(GeneralComponentDataSourceView.class, "LBL_SDM_Component_Detailed_View"), new JPanel());
        DataViewComponent dvc = new DataViewComponent(mv, new DataViewComponent.MasterViewConfiguration(false));
        dvc.addDetailsView(getComponentDetailsView(), DataViewComponent.TOP_RIGHT);
        return dvc;
    }

    @Override
    protected void added() {
        // todo register details view to listen to service state changes
    }

    @Override
    protected void removed() {
        // todo unregister details view service listener
    }

    private DetailsView getComponentDetailsView() {
        return new DataViewComponent.DetailsView(NbBundle.getMessage(
                GeneralComponentDataSourceView.class, "LBL_Component_Details"),
                NbBundle.getMessage(GeneralComponentDataSourceView.class, "LBL_Component_Details"), 0, details, null);
    }
}
