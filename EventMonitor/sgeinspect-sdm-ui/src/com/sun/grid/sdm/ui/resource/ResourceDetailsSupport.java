/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.resource;

import com.sun.grid.sdm.api.component.rp.ResourceProviderDataSource;
import com.sun.grid.sdm.api.resource.BaseResourceDataSource;
import com.sun.grid.sdm.api.service.ServiceDataSource;
import com.sun.tools.visualvm.core.datasupport.DataRemovedListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ResourceDetailsSupport<T extends BaseResourceDataSource> {

    private T rds;
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode(new RootNode());
    private OutlineDynamic o;
    private final PCL pcl = new PCL();
    private final DRL drl = new DRL();
    private final Image resources_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_resources_all.png");
    private final Image resource_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_host_resource.png");
    private final Image resource_property_image = ImageUtilities.loadImage("com/sun/grid/sdm/ui/resources/icon_host_resource_property.png");
    private boolean expandResources = false;
    private JScrollPane scroller;

    public ResourceDetailsSupport(T c) {
        this.rds = c;
        this.rds.addPropertyChangeListener(pcl);
        this.rds.notifyWhenRemoved(drl);
        this.o = new OutlineDynamic();
    }

    public Component getComponent() {
        if (scroller == null) {
            scroller = new JScrollPane(o);
        }
        return scroller;
    }

    public void setExpandResources(boolean expand) {
        expandResources = expand;
        if (expandResources) {
            expandResources();
        } else {
            collapseResources();
        }
        o.fillModel();
    }

    private static class PropertyNode {

        final private String key;
        final private Object value;

        public PropertyNode(String k, Object v) {
            this.key = k;
            this.value = v;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PropertyNode other = (PropertyNode) obj;
            if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
                return false;
            }
            if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 61 * hash + (this.key != null ? this.key.hashCode() : 0);
            hash = 61 * hash + (this.value != null ? this.value.hashCode() : 0);
            return hash;
        }
    }

    private static class RootNode {

        @Override
        public String toString() {
            return "root";
        }
    }

    private class RenderRT implements
            RenderDataProvider {

        public String getDisplayName(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return ((BaseResourceDataSource) arg0).getName();
            } else {
                return "";
            }
        }

        public boolean isHtmlDisplayName(Object node) {
            return false;
        }

        public Color getBackground(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof PropertyNode) {
                return UIManager.getColor("control");
            } else {
                return Color.WHITE;
            }
        }

        public Color getForeground(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof PropertyNode) {
                return Color.BLACK;
            } else {
                return Color.DARK_GRAY;
            }
        }

        public String getTooltipText(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "show_hide");
            } else if (arg0 instanceof PropertyNode) {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "resource_property");
            } else {
                return NbBundle.getMessage(ResourceDetailsTablePanel.class, "resource_details");
            }
        }

        public Icon getIcon(Object node) {
            Object arg0 = ((DefaultMutableTreeNode) node).getUserObject();
            if (arg0 instanceof BaseResourceDataSource) {
                return ImageUtilities.image2Icon(resource_image);
            } else if (arg0 instanceof PropertyNode) {
                return ImageUtilities.image2Icon(resource_property_image);
            } else if (arg0 instanceof RootNode) {
                return ImageUtilities.image2Icon(resources_image);
            } else {
                return null;
            }
        }
    }

    private class OutlineDynamic extends Outline {

        private final static long serialVersionUID = -2009100101L;
        private TreeModel treeMdl;
        private TableColumn kC;
        private TableColumn vC;

        /** Creates a new instance of Test */
        public OutlineDynamic() {
            setLayout(new BorderLayout());
            treeMdl = new DefaultTreeModel(root, false);
            OutlineModel mdl = DefaultOutlineModel.createOutlineModel(treeMdl,
                    new NodeRowModel(), true, NbBundle.getMessage(ResourceDetailsTablePanel.class, "Name"));
            setRenderDataProvider(new RenderRT());
            setRootVisible(false);
            setModel(mdl);

            kC = getColumnModel().getColumn(8);
            vC = getColumnModel().getColumn(9);
        }

        public DefaultMutableTreeNode getSelectedNode() {
            if (getSelectedRow() >= 0 && getSelectedRow() < getRowCount()) {
                return ((DefaultMutableTreeNode) getValueAt(getSelectedRow(), 0));
            } else {
                return null;
            }
        }

        private class NodeRowModel implements RowModel {

            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 5:
                        return Integer.class;
                    default:
                        return String.class;
                }
            }

            public int getColumnCount() {
                return 9;
            }

            public String getColumnName(int column) {
                switch (column) {
                    case 0:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Owner");
                    case 1:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Type");
                    case 2:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Static");
                    case 3:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Ambiguous");
                    case 4:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Status");
                    case 5:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Usage");
                    case 6:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Annotation");
                    case 7:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Property_Name");
                    case 8:
                        return NbBundle.getMessage(ResourceDetailsTablePanel.class, "Property_Value");
                    default:
                        assert false;
                }
                return "";
            }

            public Object getValueFor(Object node, int column) {
                Object o = ((DefaultMutableTreeNode) node).getUserObject();
                if (o instanceof BaseResourceDataSource) {
                    // the only case- if it's instance of BaseResourceDataSource, then it must be our rds
                    switch (column) {
                        case 0:
                            if (rds.getOwner() != null) {
                                if (rds.getOwner() instanceof ServiceDataSource) {
                                    return ((ServiceDataSource) rds.getOwner()).getServiceName();
                                } else if (rds.getOwner() instanceof ResourceProviderDataSource) {
                                    return ((ResourceProviderDataSource) rds.getOwner()).getComponentName();
                                }
                            }
                            return NbBundle.getMessage(ResourceDetailsTablePanel.class, "unknown");
                        case 1:
                            return rds.getType();
                        case 2:
                            return rds.isStatic();
                        case 3:
                            return rds.isAmbiguous();
                        case 4:
                            return rds.getState();
                        case 5:
                            return rds.getUsageLevel();
                        case 6:
                            return rds.getAnnotation();
                        default:
                            return "";
                    }
                } else if (o instanceof RootNode) {
                    switch (column) {
                        default:
                            return "";
                    }
                } else if (o instanceof PropertyNode) {
                    switch (column) {
                        case 7:
                            return ((PropertyNode) o).key;
                        case 8:
                            if (((PropertyNode) o).value != null) {
                                return ((PropertyNode) o).value.toString();
                            } else {
                                return "null";
                            }
                        default:
                            return "";
                    }
                }

                return null;
            }

            public boolean isCellEditable(Object node, int column) {
                return column == 1;
            }

            public void setValueFor(Object node, int column, Object value) {
//                if (column == 1) {
//                    ((Node) ((DefaultMutableTreeNode) node).getUserObject()).setComment(value.toString());
//                }
            }
        }

        private void fillModel() {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    DefaultTreeModel dtm = (DefaultTreeModel) treeMdl;
                    root.removeAllChildren();
                    dtm.reload(root);

                    DefaultMutableTreeNode rmtn = new DefaultMutableTreeNode(rds);
                    root.add(rmtn);
                    int i = root.getChildCount();
                    dtm.nodesWereInserted(root, new int[]{i - 1});
                    for (Map.Entry<String, Object> property : rds.getProperties().entrySet()) {
                        PropertyNode pn = new PropertyNode(property.getKey(), property.getValue());
                        DefaultMutableTreeNode pmtn = new DefaultMutableTreeNode(pn);
                        rmtn.add(pmtn);
                        dtm.nodesWereInserted(rmtn, new int[]{rmtn.getChildCount() - 1});
                    }
                    if (expandResources) {
                        expandResources();
                    } else {
                        expandRoot();
                    }
                }
            });

        }
    }

    private void expandRoot() {
        o.expandPath(new TreePath(root));
        setupColumns();
    }

    private void expandResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            o.expandPath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void collapseResources() {
        for (int i = 0; i < root.getChildCount(); i++) {
            o.collapsePath(new TreePath(((DefaultMutableTreeNode) root.getChildAt(i)).getPath()));
        }
        setupColumns();
    }

    private void setupColumns() {
        if (o.getColumnModel() instanceof ETableColumnModel) {
            ((ETableColumnModel) o.getColumnModel()).setColumnHidden(o.kC, !expandResources);
            ((ETableColumnModel) o.getColumnModel()).setColumnHidden(o.vC, !expandResources);
        }
    }

    private class DRL implements DataRemovedListener<T> {

        public void dataRemoved(T arg0) {
            if (arg0.equals(rds)) {
                rds.removePropertyChangeListener(pcl);
            }
        }
    }

    private class PCL implements PropertyChangeListener {

        public void propertyChange(final PropertyChangeEvent evt) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    o.fillModel();
                }
            });
        }
    }
}
