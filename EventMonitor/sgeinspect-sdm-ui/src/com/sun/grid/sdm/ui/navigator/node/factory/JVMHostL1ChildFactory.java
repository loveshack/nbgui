/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sdm.ui.navigator.node.factory;

import com.sun.grid.sdm.api.jvm.JvmDataSource;
import com.sun.grid.sdm.ui.navigator.node.JVML1Node;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class JVMHostL1ChildFactory extends ChildFactory<String> {

    private final JvmDataSource jvm;
    private static final Map<JvmDataSource, JVMHostL1ChildFactory> f = new WeakHashMap<JvmDataSource, JVMHostL1ChildFactory>();
    private final DCL dcl = new DCL();

    private JVMHostL1ChildFactory(JvmDataSource jvm) {
        this.jvm = jvm;
        if (jvm != null && jvm.isCS()) {
            DataSourceRepository.sharedInstance().addDataChangeListener(dcl, JvmDataSource.class);
        } else {
            this.jvm.getApplication().getRepository().addDataChangeListener(dcl, JvmDataSource.class);
        }
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        Set<String> names = new HashSet<String>();
        if (jvm != null) {
            for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
                names.add(ds.getName());
            }
            names.add(jvm.getName());
        }
        arg0.addAll(names);
        Collections.sort(arg0);
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        for (JvmDataSource ds : jvm.getRepository().getDataSources(JvmDataSource.class)) {
            if (arg0.equals(ds.getName())) {
                return new JVML1Node(ds, Children.create(new JVMHostL2ChildFactory(jvm, arg0), true));
            }
        }
        if (arg0.equals(jvm.getName())) {
            return new JVML1Node(jvm, Children.create(new JVMHostL2ChildFactory(jvm, arg0), true));
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        nodez.add(createNodeForKey(arg0));
        return nodez.toArray(new Node[nodez.size()]);
    }

    public static JVMHostL1ChildFactory getFactory(JvmDataSource jvm, int sortType) {
        JVMHostL1ChildFactory factory = f.get(jvm);
        if (factory == null) {
            factory = new JVMHostL1ChildFactory(jvm);
            f.put(jvm, factory);
        }
        return factory;
    }

    private class DCL implements DataChangeListener<JvmDataSource> {

        public void dataChanged(DataChangeEvent<JvmDataSource> arg0) {
            refresh(true);
        }
    }

    private static class NameComparator implements Comparator<JvmDataSource> {

        private static NameComparator INSTANCE = new NameComparator();

        public int compare(JvmDataSource o1, JvmDataSource o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
