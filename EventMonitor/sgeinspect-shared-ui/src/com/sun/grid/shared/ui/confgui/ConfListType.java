/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

class ConfListType extends ConfElement {

    Class elementClass;
    ConfElement confElement;

    ConfListType(Class parent, ParameterizedType listType, String namePrefix, String name) throws UnexpectedStructureException {
        super(parent, namePrefix, name, true, false); // you automatically unset a list by emptying it!
        Type[] typeArguments = listType.getActualTypeArguments();
        if (typeArguments.length != 1) {
            new UnexpectedStructureException("Expected Parameterized list for ParameterizedType:" + listType.toString());
        }

        //should be just one!
        elementClass = (Class) typeArguments[0];
        confElement = ConfElementFactory.getConfElement(null, null, elementClass, null, "", name, false /*we define lists can be empty! if not we can ensure that on gui side*/);

    }

    final protected void outline(int deep) {
        System.out.println(getIntention(deep) + "Dialog with List as Table: " + getFullName() + "(" + elementClass.getName() + ")");
        if (confElement.isSeparateDialog()) {
            System.out.println(getIntention(deep + 1) + "In a subwizard:");
            confElement.outline(deep + 2);
        } else {
            System.out.println(getIntention(deep + 1) + "In a popup window:");
            confElement.outline(deep + 2);
        }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    protected void putConfInGui(WizardDescriptor wd, Preferences pref, Object confObject) {
        //we create the model put the list content into it and put it in the wd

        ConfListTypeTableModel model = new ConfListTypeTableModel(confElement);

        if (confObject != null) {
            List list = (List) confObject;
            for (Object confElem : list) {
                try {
                    model.add(confElem);
                } catch (UnexpectedStructureException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
        //System.out.println("Created Model for "+this.getFullName());
        wd.putProperty(PREFIX_TABLE_COMPONENT + this.getFullName() + SUFFIX_COMPONENT_MODEL, model);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Object getConfFromGui(WizardDescriptor wd) throws UnexpectedStructureException {
        //we will look for the model in the wd and create a list element from it!
        ConfListTypeTableModel model = (ConfListTypeTableModel) wd.getProperty(PREFIX_TABLE_COMPONENT + this.getFullName() + SUFFIX_COMPONENT_MODEL);
        List list = new ArrayList();
        if (model != null) {
            for (int i = 0; i < model.getRowCount(); i++) {
                list.add(model.getConfigObjectAt(i));
            }
        }
        return list; // this will not be added but reappears a sec later in setValue Obj: Looking forward :)
    }

    @Override
    @SuppressWarnings({"unchecked"})
    protected void setValueObj(Object parentObj, Object value) throws UnexpectedStructureException {
        if (value == null) {
            return;
        }
        //there is a JAXB problem in case of a list type: no setList! we have get the list and add them seperately!!
        List list = (List) getValueObj(parentObj);
        if(list!=null){
            List elementsToAdd = (List) value;
            for (Object element : elementsToAdd) {
                list.add(element);
            }
        }//TODO sanity elsecase should not happen
    }

    @Override
    protected String getValueStr(Object parentObject) throws UnexpectedStructureException {
        if (parentObject == null) {
            return "{-}";
        }
        List list = (List) getValueObj(parentObject);
        if (list == null) {
            return "{-}";
        }
        return "{" + list.size() + "}";
    }

    @Override
    protected void storeModels(WizardDescriptor wd) throws UnexpectedStructureException {
        //we have to store an empty Table model that might be replaced by actual data!
        ConfListTypeTableModel model = new ConfListTypeTableModel(confElement);

        //System.out.println("Created Model for "+this.getFullName());
        wd.putProperty(PREFIX_TABLE_COMPONENT + this.getFullName() + SUFFIX_COMPONENT_MODEL, model);
    }
}
