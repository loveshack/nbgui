/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in txtliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.openide.util.NbBundle;

/**
 *
 */
class JTextFieldValidator {

    private List<Entry> list = new ArrayList<Entry>();

    void registerPositiveInteger(JLabel label, JTextField txt) {
        list.add(new Entry(label, txt) {
            @Override
            String getError() {
                return testPositiveInteger();
            }
        });
    }

    void registerInteger(JLabel label, JTextField txt) {
        list.add(new Entry(label, txt) {

            @Override
            String getError() {
                return testInteger();
            }
        });
    }

    void registerNonEmpty(JLabel label, JTextField txt) {
        list.add(new Entry(label, txt) {
            @Override
            String getError() {
                return testNonEmpty();
            }
        });
    }

    void registerNonEmptyNonWhiteSpace(JLabel label, JTextField txt) {
        list.add(new Entry(label, txt) {
            @Override
            String getError() {
                return testNonEmptyNonWhiteSpace();
            }
        });
    }

    void registerPath(JLabel label, JTextField txt) {
        list.add(new Entry(label, txt) {
            @Override
            String getError() {
               return testPath();
            }
        });
    }

    void registerNotOnUsedList(JLabel label, JTextField txt, List<String> used) {
        list.add(new Entry(label, txt, used) {
            @Override
            String getError() {
                return testNotOnUsedList();
            }
        });
    }

    Entry validate() {
        for (Entry entry : list) {
            if(!entry.getTextField().isEnabled()){
                continue;
            }
            if (!entry.isValid()) {
                return entry;
            }
        }
        return null;
    }

    abstract class Entry {

        private JLabel label;
        private JTextField txt;
        private List<String> data;

        @SuppressWarnings("unchecked")
        Entry(JLabel label, JTextField txt) {
            this(label, txt, (List<String>) Collections.EMPTY_LIST);
        }

        Entry(JLabel label, JTextField txt, List<String> data) {
            this.label = label;
            this.txt = txt;
            this.data = data;
        }

        abstract String getError();

        JTextField getTextField() {
            return txt;
        }

        JLabel getLabel() {
            return label;
        }


        private boolean isValid(){
            return getError()==null;
        }

        public List<String> getData() {
            return data;
        }

        String testInteger() {
            try {
                Integer.parseInt(txt.getText());
            } catch (NumberFormatException e) {
                return NbBundle.getMessage(JTextFieldValidator.class, "NOT_AN_INTEGER_VALUE!");
            }
            return null;
        }

        String testPositiveInteger() {
            try {
                int number = Integer.parseInt(txt.getText());
                if (number < 0) {
                    return NbBundle.getMessage(JTextFieldValidator.class, "NEGATIVE_NUMBERS_ARE_NOT_ALLOWED!");
                }
            } catch (NumberFormatException e) {
                return NbBundle.getMessage(JTextFieldValidator.class, "NOT_AN_INTEGER_VALUE!");
            }
            return null;
        }

        String testNonEmpty() {
            if (txt.getText() == null || "".equals(txt.getText())) {
                return NbBundle.getMessage(JTextFieldValidator.class, "CANNOT_BE_EMPTY!");
            }
            return null;
        }

        String testNonEmptyNonWhiteSpace() {
            String err = testNonEmpty();
            if(err!=null){
                return err;
            }
            if (txt.getText().matches("^.*[\\s].*")) {
                return NbBundle.getMessage(JTextFieldValidator.class, "CANNOT_CONTAIN_WHITESPACES!");
            }
            return null;
        }

        String testPath() {
            //if(!txt.getText().matches("^(.*?/|.*?\\\\)?([^\\./|^\\.\\\\]+)(?:\\.([^\\\\]*)|)")){

            //"([a-zA-Z]  \\w+)*\\[a-zA-Z0_9]+)?"

            // if(!txt.getText().matches("^(.*?/|.*?\\\\)?([^\\./|^\\.\\\\]+)(?:\\.([^\\\\]*)|)$")){
            //     return "Not a valid Path!";
            // }
            return testNonEmpty();
        }

        String testNotOnUsedList() {
            List<String> used =this.getData();
            for (String bad : used) {
                if (txt.getText().equals(bad)) {
                    return NbBundle.getMessage(JTextFieldValidator.class, "IS_ALREADY_USED!");
                }
            }
            return null;
        }
    }
}
