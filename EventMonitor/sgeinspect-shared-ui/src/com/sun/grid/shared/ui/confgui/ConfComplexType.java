/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.DefaultComboBoxModel;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ConfComplexType extends ConfElement {

    private static SubClassRegistry subClassRegistry = new SubClassRegistry();
    private String subTypeComboBoxName;
    private ConfElement[] confElements;
    private Class complexTypeClass;
    // MAY CONTAIN THE SUPERCLASS if instanciable!! but be carefull should be a new instance to avoid recursion!
    private ConfComplexType[] subTypes;

    ConfComplexType(Object obj) throws UnexpectedStructureException {
        this(null, obj.getClass(), "", obj.getClass().getName(), true/* no parent so default to true*/);
    }

    ConfComplexType(Class parent, Class complexTypeClass, String namePrefix, String name, boolean isRequired) throws UnexpectedStructureException {
        this(parent, complexTypeClass, namePrefix, name, isRequired, true);
    }
    //TODO test the separate Dialog thing!!

    protected ConfComplexType(Class parent, Class complexTypeClass, String namePrefix, String name, boolean isRequired, boolean separateDialog) throws UnexpectedStructureException {
        super(parent, namePrefix, name, isRequired, separateDialog);
        this.complexTypeClass = complexTypeClass;
        String newPrefix = namePrefix + name + "_";
        this.confElements = ConfElementFactory.getConfElements(complexTypeClass, newPrefix);
        this.subTypeComboBoxName = ConfElement.PREFIX_COMBOBOX_COMPONENT + newPrefix + SUFFIX_SUBTYPE;

        //generate ConfComplexTypes for all registered subtypes
        List<SubClassEntry> entryList = subClassRegistry.getSubClassEntriesBySuperClass(complexTypeClass);
        List<ConfComplexType> subTypesList = new ArrayList<ConfComplexType>();
        for (SubClassEntry entry : entryList) {
            ConfComplexType subType = null;
            if (entry.getSubType() == complexTypeClass) {
                subType = this; //otherwise endless look :)
            } else {
                subType = new ConfComplexType(parent, entry.getSubType(), namePrefix, name, isRequired, separateDialog);
            }
            String toString = entry.getToString();
            if (toString != null) {
                subType.setToString(toString);
            }
            subTypesList.add(subType);
        }
        subTypes = subTypesList.toArray(new ConfComplexType[subTypesList.size()]);
    }

    static void setSubClassRegistry(SubClassRegistry subClassRegistry) {
        if (subClassRegistry != null) {
            ConfComplexType.subClassRegistry = subClassRegistry;
        }
    }

    static SubClassRegistry getSubClassRegistry() {
        if (subClassRegistry != null) {
            ConfComplexType.subClassRegistry = subClassRegistry;
        }
        return subClassRegistry;
    }

    ConfComplexType[] getSubTypes() {
        return subTypes;
    }

    public ConfElement[] getConfElements() {
        return confElements;
    }

    Class getComplexTypeClass() {
        return complexTypeClass;
    }

    Object getNewComplexTypeInstance() throws UnexpectedStructureException {
        try {
            return complexTypeClass.newInstance();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            throw new UnexpectedStructureException(ex.getMessage());
        }
    }

    ConfComplexType getSubType(String subTypeClassName) throws UnexpectedStructureException {
        for (ConfComplexType subType : this.subTypes) {
            if (subTypeClassName.equals(subType.getComplexTypeClass().getName())) {
                return subType;
            }
        }
        //if it is none of them try to use the super one!
        return this;
    }

    @Override
    protected void outline(int deep) {
        System.out.println(getIntention(deep) + "Dialog: " + getFullName() + "(" + complexTypeClass.getName() + ")");
        System.out.println(getIntention(deep + 1) + "It consists of:");

        for (ConfElement confElement : confElements) {
            if (!confElement.isSeparateDialog()) {
                confElement.outline(deep + 2);
            }
        }

        for (ConfElement confElement : confElements) {
            if (confElement.isSeparateDialog()) {
                confElement.outline(deep);
            }
        }
        if (this.subTypes.length != 0) {
            System.out.println(getIntention(deep + 1) + "It can appear in following Subtypes:");
            for (ConfComplexType subType : subTypes) {
                subType.outline(deep + 2);
            }
        }
    }

    @Override
    void outLineRequired(int deep) {
        System.out.println(getIntention(deep) + this.isIsRequired() + " = isRequired :: " + this.getFullName());
        for (ConfElement confElement : confElements) {
            confElement.outLineRequired(deep + 1);
        }

        if (this.subTypes.length != 0) {
            System.out.println(getIntention(deep + 1) + "It can appear in following Subtypes:");
            for (ConfComplexType subType : subTypes) {
                subType.outLineRequired(deep + 2);
            }
        }
    }

    @Override
    protected void putConfInGui(WizardDescriptor wd, Preferences pref, Object confObject) throws UnexpectedStructureException {
        if (confObject == null) {
            //System.out.println("ConfObj is null!" + this.getFullName());
            return;
        }
        wd.putProperty(PREFIX_TEXTFIELD_COMPONENT + this.getFullName() + SUFFIX_COMPONENT_MODEL, confObject);

        if (subTypes.length != 0) {
            // look for a possible subtype
            String subTypeClassName = confObject.getClass().getName();
            ConfComplexType subConfType = getSubType(subTypeClassName);
            pref.put(subTypeComboBoxName, subConfType.toString());
            subConfType.putConfInGui(wd, pref, confObject);
        } else {
            for (ConfElement confElement : confElements) {
                if (!confElement.isIsRequired()) {
                    boolean isSet = confElement.isSetValueObj(confObject);
                    String cboIsSetName = PREFIX_CHECKBOX_COMPONENT + confElement.getFullName() + SUFFIX_COMPONENT_IS_SET;
                    pref.put(cboIsSetName, "" + isSet);
                    if (!isSet) {
                        continue;
                    }
                }
                confElement.putConfInGui(wd, pref, confElement.getValueObj(confObject));

            }
            //put a singleString in the corresponding textfield!
            pref.put(PREFIX_TEXTFIELD_COMPONENT + this.getFullName(), this.getSingleStringRepresentation(confObject));
        }
    }

    protected Object getConfFromGui(WizardDescriptor wd) throws UnexpectedStructureException {
        Object lookedUp = wd.getProperty(PREFIX_TEXTFIELD_COMPONENT + this.getFullName() + SUFFIX_COMPONENT_OBJECT);
        if (lookedUp != null) {
            return lookedUp;
        }
        try {
            ConfComplexType selectedConfType = (ConfComplexType) wd.getProperty(subTypeComboBoxName);
            if (selectedConfType == null) {
                selectedConfType = this;
            }
            Object newConfObject = selectedConfType.getComplexTypeClass().newInstance();

            ConfComplexType subType = this.getSubType(newConfObject.getClass().getName());

            for (ConfElement confElement : subType.getConfElements()) {

                //System.out.println("Create: "+confElement.getFullName() +" isRequired:"+confElement.isIsRequired() + " isSet:"+ isSetInGui(wd,confElement));
                if (!confElement.isIsRequired() && !isSetInGui(wd, confElement)) {
                    //System.out.println("Will skip!");
                    continue;
                }
                Object valueObj = confElement.getConfFromGui(wd);
                if(valueObj!=null){
                    confElement.setValueObj(newConfObject, valueObj);
                }
            }
            return newConfObject;
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            throw new UnexpectedStructureException(ex.getMessage());
        }
    }

    @Override
    protected String getValueStr(Object parentObject) throws UnexpectedStructureException {
        if (parentObject == null) {
            return null;
        }
        Object child = this.getValueObj(parentObject);
        if (child != null) {
            StringBuilder sb = new StringBuilder();
            for (ConfElement confElem : confElements) {
                //these are the grand cildren of the parent object!
                String str = confElem.getValueStr(child);
                if (!"".equals(str)) {
                    sb.append(str + " ");
                }
            }
            return sb.toString().trim();
        } else {
            return null;
        }
    }

    @Override
    protected String getSingleStringRepresentation(Object confObject) throws UnexpectedStructureException {
        return "[" + this.getName() + ":" + getSingleStringRepresentation(confObject, null) + "]";
    }

    String getSingleStringRepresentation(Object confObject, String[] filterNames) throws UnexpectedStructureException {

        if (confObject == null) {
            return NbBundle.getMessage(ConfComplexType.class, "NOT_SET");
        }
        List filter = new ArrayList();
        if (filterNames != null) {
            filter = Arrays.asList(filterNames);
        }

        StringBuilder sb = new StringBuilder();
        if (subTypes.length != 0) {
            // look for a possible subtype
            String subTypeClassName = confObject.getClass().getName();
            ConfComplexType subConfType = getSubType(subTypeClassName);
            // Very important pass the filter here, subtypes will be filered!
            sb.append(subConfType.getSingleStringRepresentation(confObject, filterNames));
        } else {
            for (ConfElement confElement : confElements) {
                if (!filter.contains(confElement.toString())) {
                    //filter will not be applied recursively!
                    sb.append(confElement.getSingleStringRepresentation(confElement.getValueObj(confObject)));
                }
            }
        }
        return sb.toString();
    }

    @Override
    protected void storeModels(WizardDescriptor wd) throws UnexpectedStructureException {
        for (ConfElement confElement : confElements) {
            confElement.storeModels(wd);
        }
        if (subTypes.length != 0) {
            for (ConfComplexType subType : subTypes) {
                subType.storeModels(wd);
            }
        }
        storeSubTypeComboBoxModels(wd);
    }

    private void storeSubTypeComboBoxModels(WizardDescriptor wd) throws UnexpectedStructureException {
        if (this.subTypes.length != 0) {
            String modelName = subTypeComboBoxName + ConfElement.SUFFIX_COMPONENT_MODEL;
            DefaultComboBoxModel model = new DefaultComboBoxModel(subTypes);
            wd.putProperty(modelName, model);
        }
    }
}
