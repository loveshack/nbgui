/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

public abstract class ConfElement {

    private final FieldAccessor<Object> fieldAccessor = new FieldAccessor<Object>();
    public static final String SINGLE_INTENT = "  ";
    public static final String SUFFIX_COMPONENT_OBJECT = "Object";
    public static final String SUFFIX_COMPONENT_MODEL = "Model";
    public static final String SUFFIX_COMPONENT_IS_SET = "IsSet";
    public static final String PREFIX_TEXTFIELD_COMPONENT = "txt";
    public static final String PREFIX_BUTTON_COMPONENT = "btn";
    public static final String PREFIX_COMBOBOX_COMPONENT = "cbo";
    public static final String PREFIX_TABLE_COMPONENT = "tbl";
    public static final String PREFIX_CHECKBOX_COMPONENT = "chk";
    public static final String SUFFIX_SUBTYPE = "SubType";
    private final String name;
    private String toString;
    private final boolean separateDialog;
    private final Class parentClass;
    private final String namePrefix;
    private final boolean isBoolean;
    private final boolean isRequired;

    ConfElement(Class parentClass, String namePrefix, String name, boolean isRequired, boolean separateDialog) {
        this(parentClass, namePrefix, name, isRequired, separateDialog, false/*default is:noBoolean*/);
    }

    ConfElement(Class parentClass, String namePrefix, String name, boolean isRequired, boolean separateDialog, boolean isBoolean) {
        this.name = name;
        this.parentClass = parentClass;
        this.separateDialog = separateDialog;
        this.namePrefix = namePrefix;
        this.isBoolean = isBoolean;
        setToString(name);

        this.isRequired = isRequired;
    }

    protected boolean isIsRequired() {
        return isRequired;
    }

    protected String getNamePrefix() {
        return namePrefix;
    }

    @Override
    public String toString() {
        return toString;
    }

    protected void setToString(String toString) {
        this.toString = toString;
    }

    protected Class getParentClass() {
        return parentClass;
    }

    protected String getFullName() {
        return namePrefix + name;
    }

    protected String getName() {
        return name;
    }

    protected void storeModels(WizardDescriptor wd) throws UnexpectedStructureException {
        //dont store a model by default
    }

    protected boolean isSeparateDialog() {
        return separateDialog;
    }

    protected boolean isSetInGui(WizardDescriptor wd, ConfElement confElement) {
        String key = PREFIX_CHECKBOX_COMPONENT + confElement.getFullName() + SUFFIX_COMPONENT_IS_SET;
        String valStr = (String) wd.getProperties().get(key);
        if (valStr == null) {
            return false;
        } else {
            return Boolean.parseBoolean(valStr);
        }
    }

    abstract protected void putConfInGui(WizardDescriptor wd, Preferences pref, Object confObject) throws UnexpectedStructureException;

    abstract protected Object getConfFromGui(WizardDescriptor wd) throws UnexpectedStructureException;

    protected void setValueObj(Object parentObj, Object value) throws UnexpectedStructureException {
        fieldAccessor.invokeSetX(getParentClass(), getName(), parentObj, value);
    }

    protected String getValueStr(Object parentObj) throws UnexpectedStructureException {
        if (parentObj == null) {
            return null;
        }
        Object value = getValueObj(parentObj);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    final protected Object getValueObj(Object parentObj) throws UnexpectedStructureException {
        if (isBoolean) {
            return fieldAccessor.invokeIsX(getParentClass(), getName(), parentObj);
        } else {
            return fieldAccessor.invokeGetX(getParentClass(), getName(), parentObj);
        }
    }

    final protected boolean isSetValueObj(Object parentObj) throws UnexpectedStructureException {
        return fieldAccessor.invokeIsXSet(getParentClass(), getName(), parentObj);
    }

    protected String getSingleStringRepresentation(Object confObject) throws UnexpectedStructureException {
        if (confObject == null) {
            return "";
        } else {
            return "[" + this.getName() + ":" + confObject.toString() + "]";
        }
    }

    protected abstract void outline(int deep);

    void outLineRequired(int deep) {
        System.out.println(getIntention(deep) + this.isRequired + " = isRequired :: " + this.getFullName());

    }

    protected static String getIntention(int deep) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < deep; i++) {
            sb.append(SINGLE_INTENT);
        }
        return sb.toString();
    }

    private class FieldAccessor<T> {

        public static final String METHOD_PREFIX_SET_X = "set";
        public static final String METHOD_PREFIX_IS_X = "is";
        public static final String METHOD_PREFIX_GET_X = "get";
        public static final String METHOD_PREFIX_IS_SET_X = "isSet";

        T invokeGetX(Class parent, String name, Object parentObj) throws UnexpectedStructureException {

            try {
                String getMethodName = METHOD_PREFIX_GET_X + name;
                Method getMethod = parent.getMethod(getMethodName);
                // in case of a primitive returntype we will have problems because of the autounboxing of null ==> test it
                if (getMethod.getReturnType().isPrimitive()) {
                    if (!invokeIsXSet(parent, name, parentObj)) {
                        return null;
                    }
                }
                return (T) getMethod.invoke(parentObj);

            } catch (Exception ex) {
                //System.out.println("ERROR in invokeGetX: parent: " + parent + " name: " + name + " parentObj: " + parentObj);
                ex.printStackTrace();
                throw new UnexpectedStructureException(ex.getMessage());
            }
        }

        T invokeIsX(Class parent, String name, Object parentObj) throws UnexpectedStructureException {
            try {
                String getMethodName = METHOD_PREFIX_IS_X + name;
                Method getMethod = parent.getMethod(getMethodName);
                return (T) getMethod.invoke(parentObj);
            } catch (Exception ex) {
                //System.out.println("ERROR in invokeGetX: parent: "+parent+" name: "+name+" parentObj: "+parentObj);
                Exceptions.printStackTrace(ex);
                throw new UnexpectedStructureException(ex.getMessage());
            }
        }

        void invokeSetX(Class parent, String name, Object parentObj, T value) throws UnexpectedStructureException {
            if (value == null) {
                //System.out.println("invokeSetX:Do nothing: value is null!! Parent:"+parent+" Name:"+ name +" ParentObj: "+parentObj+" Value:"+value);
                return;
            }
            Class valueClass = value.getClass();
            if (valueClass == Integer.class) {
                valueClass = int.class;
            }
            if (valueClass == Boolean.class) {
                valueClass = boolean.class;
            }
            try {
                String setMethodName = METHOD_PREFIX_SET_X + name;
                @SuppressWarnings("unchecked")
                Method setMethod = parent.getMethod(setMethodName, valueClass);
                setMethod.invoke(parentObj, value);
            } catch (Exception ex) {
                //System.out.println("ERROR in invokeSetX: parent: "+parent+" name"+name+" parentObj"+parentObj +" value:"+ value);
                Exceptions.printStackTrace(ex);
                throw new UnexpectedStructureException(ex.getMessage());
            }
        }

        boolean invokeIsXSet(Class parent, String name, Object parentObj) throws UnexpectedStructureException {
            try {
                //lets ensure first that this method exists

                String isXSetMethodName = METHOD_PREFIX_IS_SET_X + name;
                for (Method isXSetMethod : parent.getMethods()) {
                    if (isXSetMethod.getName().equals(isXSetMethodName)) {
                        return (Boolean) isXSetMethod.invoke(parentObj);
                    }
                }//there was no such method
            } catch (InvocationTargetException ex) {
                Exceptions.printStackTrace(ex.getTargetException());
                throw new UnexpectedStructureException(ex.getMessage());
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
                throw new UnexpectedStructureException(ex.getMessage());
            }
            //we cant check lets hope for the best and assume that it is!
            return true;
        }
    }
}
