/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;

final class ConfBooleanType extends ConfElement {

    ConfBooleanType(Class parent, String namePrefix, String name,boolean isRequired) {
        super(parent, namePrefix, name,isRequired, false,true);
    }

    final protected void outline(int deep) {
        System.out.println(getIntention(deep) + "Boolean: " + getFullName());
    }

    @Override
    final protected void putConfInGui(WizardDescriptor wd, Preferences pref, Object confObject) throws UnexpectedStructureException {
        String value = confObject.toString();
        String name = ConfElement.PREFIX_CHECKBOX_COMPONENT + getFullName();
        pref.put(name, value);
    }


    @Override
    final protected Object getConfFromGui(WizardDescriptor wd) throws UnexpectedStructureException {
        String name = ConfElement.PREFIX_CHECKBOX_COMPONENT + getFullName();
        Object value =wd.getProperty(name);
        if(value==null){
            //System.out.println("WARNING: Boolenan Field " +name +" was empty");
            return null;
        }
        return new Boolean(Boolean.parseBoolean(value.toString()));
    }
}

