/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.widgets;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

public class InplaceComboBoxEditor implements InplaceEditor {

    private final JComboBox combo = new JComboBox();
    private PropertyEditor editor = null;

    public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
        editor = propertyEditor;
        reset();
    }

    public JComponent getComponent() {
        return combo;
    }

    public void clear() {
        editor = null;
        model = null;
    }

    public Object getValue() {
        return combo.getSelectedItem();
    }

    public void setValue(Object object) {
        combo.setSelectedItem(object);
    }

    public boolean supportsTextEntry() {
        return true;
    }

    public void reset() {

        String[] values = editor.getTags();
        combo.setEditable(true);
        combo.setModel(new javax.swing.DefaultComboBoxModel(values));
        combo.setSelectedItem(editor.getAsText());

    }

    public KeyStroke[] getKeyStrokes() {
        return new KeyStroke[0];
    }

    public PropertyEditor getPropertyEditor() {
        return editor;
    }

    public PropertyModel getPropertyModel() {
        return model;
    }
    private PropertyModel model;

    public void setPropertyModel(PropertyModel propertyModel) {
        this.model = propertyModel;
    }

    public boolean isKnownComponent(Component component) {
        return component == combo || combo.isAncestorOf(component);
    }

    public void addActionListener(ActionListener actionListener) {
        //do nothing - not needed for this component
        }

    public void removeActionListener(ActionListener actionListener) {
        //do nothing - not needed for this component
        }
}
