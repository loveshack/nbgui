/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class SubClassRegistry {

    private HashMap<Class, List<SubClassEntry>> entryBySuperClass = new HashMap<Class, List<SubClassEntry>>();
    private HashMap<Class, SubClassEntry> entryBySubClass = new HashMap<Class, SubClassEntry>();

    public void registerSubClass(Class superClass, Class subClass, String subClassToString) {
        registerSubClass(superClass, new SubClassEntry(subClass, subClassToString));
    }

    public void registerSubClass(Class superClass,SubClassEntry entry) {
        if(!entryBySubClass.containsKey(entry.getSubType())){
            entryBySubClass.put(entry.getSubType(), entry);
        }
        getSubClassEntriesBySuperClass(superClass).add(entry);
    }
    
    SubClassEntry getSubClassEntryBySubClass(Class subClass) {
        return entryBySubClass.get(subClass);
    }

    List<SubClassEntry> getSubClassEntriesBySuperClass(Class superClass) {
        List<SubClassEntry> list = entryBySuperClass.get(superClass);
        if (list == null) {
            list = new ArrayList<SubClassEntry>();
            entryBySuperClass.put(superClass, list);
        }
        return list;
    }
}
