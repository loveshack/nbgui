/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ConfElementFactory {

    public static final String METHOD_PREFIX_GET = "get";
    public static final String METHOD_PREFIX_SET = "set";
    public static final String METHOD_PREFIX_IS = "is";
    public static final String METHOD_PREFIX_IS_SET = "isSet";
    public static final String METHOD_GET_CLASS = "getClass";
    private static List <ConfElementFactoryExtension> extensions =new ArrayList<ConfElementFactoryExtension>();

    private ConfElementFactory() {
    }

    public static List<ConfElementFactoryExtension> getFactoryExtensionsList() {
        return extensions;
    }



    static String getSingleStringRepresentation(Object obj) throws UnexpectedStructureException {
        if (obj == null) {
            return NbBundle.getMessage(ConfElementFactory.class, "<NOT_SET>");
        }
        ConfElement elem = getConfElement(null, null, obj.getClass(), null, "", obj.getClass().getSimpleName(),true/*no parent==> default to true*/);
        return elem.getSingleStringRepresentation(obj);
    }

    static ConfElement[] getConfElements(Class parentClass, String namePrefix) throws UnexpectedStructureException {
        List<ConfElement> list = new ArrayList<ConfElement>();
        Method[] methods = parentClass.getMethods();
        Field[] allFields = getAllFields(parentClass);
        // look for public Modifiers
        for (Method method : methods) {
            
            if (!Modifier.isPublic(method.getModifiers())) {
                continue;
            }
            String name = null;
            if (method.getName().equals(METHOD_GET_CLASS)) {
                continue;
            }else if (method.getName().startsWith(METHOD_PREFIX_GET)) {
                name = method.getName().substring(METHOD_PREFIX_GET.length());
            } else if (method.getName().startsWith(METHOD_PREFIX_IS) && !method.getName().startsWith(METHOD_PREFIX_IS_SET)) {
                name = method.getName().substring(METHOD_PREFIX_IS.length());
            } else {
                continue;
            }
            boolean isRequired = isRequired(allFields,name);

            Class returnClass = method.getReturnType();
            Type genericReturnType = method.getGenericReturnType();
            list.add(getConfElement(parentClass, method, returnClass, genericReturnType, namePrefix, name,isRequired));
        }
        return list.toArray(new ConfElement[list.size()]);
    }

    static ConfElement getConfElement(Class parentClass,Method method, Class elementClass, Type genericElementType, String namePrefix, String name,boolean isRequired) throws UnexpectedStructureException {
        // rule: default overrides isRequired=false!
        if(defaults(parentClass,method)){
            isRequired =true;
        }

        for(ConfElementFactoryExtension extension:extensions){
            ConfElement extendedElement = extension.getConfElement(parentClass, method, elementClass, genericElementType, namePrefix, name, isRequired);
            if(extendedElement!=null){
                return extendedElement;
            }
        }

        if (elementClass == String.class) {
            return new ConfStringType(parentClass, namePrefix, name,isRequired);
        } else if (elementClass == int.class) {
            return new ConfIntegerType(parentClass, namePrefix, name,isRequired);
        }
        if (elementClass == boolean.class) {
            return new ConfBooleanType(parentClass, namePrefix, name,isRequired);
        } else if (elementClass == List.class) {
            if (!(genericElementType instanceof ParameterizedType)) {
                throw new UnexpectedStructureException("Expected Parameterized list for type: " + genericElementType);
            }
            return new ConfListType(parentClass, (ParameterizedType) genericElementType, namePrefix, name);
        } else if (elementClass.isEnum()) {
            return new ConfEnumType(parentClass, elementClass, namePrefix, name,isRequired);
        } else{
            return new ConfComplexType(parentClass, elementClass, namePrefix, name,isRequired);
        }
    }

   private static boolean isRequired(Field[] fields, String name) {
        String fieldName = name.substring(0, 1).toLowerCase() + name.substring(1);
        for (Field field : fields) {
            if (fieldName.equals(field.getName())) {
                XmlAttribute attr = field.getAnnotation(XmlAttribute.class);
                if (attr!=null) {
                    //System.out.println(field.getName() +"Attr:  "+attr);
                    return attr.required();
                }
                XmlElement elem  = field.getAnnotation(XmlElement.class);
                if (elem!=null) {
                    //System.out.println(field.getName() +"Elem:  "+elem);
                    return elem.required();
                }
                return false;
            }
        }
        //System.out.println("Could not find name! " +name);
        return false; // not defined => so default to false!
    }

    private static Field[] getAllFields(Class myClass) {

        List<Field> fieldList = new ArrayList<Field>();
        getAllFields(myClass, fieldList);
        return fieldList.toArray(new Field[fieldList.size()]);
    }

    private static void getAllFields(Class myClass, List<Field> fieldList) {
        Field[] fields = myClass.getDeclaredFields();
        for (Field f : fields) {
            fieldList.add(f);
        }
        if (Object.class != myClass.getSuperclass()) {
            getAllFields(myClass.getSuperclass(), fieldList);
        }
    }

    private static boolean defaults(Class parentClass,Method method ) throws UnexpectedStructureException{
         if(parentClass!=null && method !=null){
            try {
                if(Modifier.isAbstract(parentClass.getModifiers())){
                    List<SubClassEntry> list = ConfComplexType.getSubClassRegistry().getSubClassEntriesBySuperClass(parentClass);
                    if(list.isEmpty()){
                        throw new UnexpectedStructureException("No Subclass registered for "+ parentClass.getName());
                    }
                    //use the first subtype to instanciate the Type!
                    parentClass = list.get(0).getSubType();
                }
                Object parentObj = parentClass.newInstance();
                
                Object value = method.invoke(parentObj);
                if(value!=null){
                    //System.out.println(parentClass.getName()+":"+method.getName()+" defaults to "+ value.toString());
                    return true;
                }
            } catch (IllegalArgumentException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InvocationTargetException ex) {
                //Exceptions.printStackTrace(ex);
            } catch (InstantiationException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IllegalAccessException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        return false;
    }
}
