/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.navigator;

import com.sun.grid.shared.ui.options.AppearanceOptionsPanelController;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.explorer.ExplorerSelectionListener;
import com.sun.tools.visualvm.core.explorer.ExplorerSupport;
import java.awt.BorderLayout;
import java.awt.Image;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays something.
 */
public final class NavigatorTopComponent extends TopComponent implements ExplorerManager.Provider {

    private static NavigatorTopComponent instance;
    /** path to the icon used by the component and its open action */
    private final Icon up_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/shared/ui/navigator/resources/up_arrow.png"));
    private final Icon down_arrow = ImageUtilities.image2Icon(ImageUtilities.loadImage("com/sun/grid/shared/ui/navigator/resources/down_arrow.png"));
    private final Image ICON = ImageUtilities.loadImage("com/sun/grid/shared/ui/navigator/resources/icon_navigator.png");
    private static final String PREFERRED_ID = "NavigatorTopComponent";
    private final ExplorerManager em = new ExplorerManager();
    private final ExplorerSupport es = ExplorerSupport.sharedInstance();
    private final JPanel clearTBPanel = new JPanel();
    private JPanel currTBPanel;
    private static final Map<Class<? extends DataSource>, List<NavigatorActionHandler>> nahMap = new HashMap<Class<? extends DataSource>, List<NavigatorActionHandler>>();
    private final ExplorerSelectionListener esListener = new ESL();
    private final ComboBoxModel cbm = new DefaultComboBoxModel(new String[]{});
    private Class<? extends DataSource> rootDSClazz = null;
    private Preferences pref = NbPreferences.forModule(AppearanceOptionsPanelController.class);

    private NavigatorTopComponent() {
        initComponents();
        this.currTBPanel = clearTBPanel;
        setFilterPnlState();
        this.btnHide.setIcon(down_arrow);
        this.em.setRootContext(AbstractNode.EMPTY);
        setName(NbBundle.getMessage(NavigatorTopComponent.class, "CTL_NavigatorTopComponent"));
        setToolTipText(NbBundle.getMessage(NavigatorTopComponent.class, "HINT_NavigatorTopComponent"));
        associateLookup(ExplorerUtils.createLookup(em, getActionMap()));
        
        setIcon(ICON);
        putClientProperty("netbeans.winsys.tc.keep_preferred_size_when_slided_in", Boolean.TRUE);
    }

    private void setFilterPnlState() {
        boolean collapsed = pref.getBoolean(AppearanceOptionsPanelController.PROP_COLLAPSE,
              AppearanceOptionsPanelController.DEFAULT_COLLAPSED);
        currTBPanel.setVisible(!collapsed);
        if (collapsed) {
            btnHide.setIcon(up_arrow);
        } else {
            btnHide.setIcon(down_arrow);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        view = new javax.swing.JComboBox();
        viewPane = new BeanTreeView();
        tbPanel = new javax.swing.JPanel();
        btnHide = new javax.swing.JToggleButton();

        setLayout(new java.awt.BorderLayout());

        view.setLightWeightPopupEnabled(false);
        view.setMinimumSize(new java.awt.Dimension(0, 0));
        view.setPreferredSize(new java.awt.Dimension(100, 24));
        view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewActionPerformed(evt);
            }
        });
        add(view, java.awt.BorderLayout.NORTH);

        ((BeanTreeView)viewPane).setRootVisible(false);
        add(viewPane, java.awt.BorderLayout.CENTER);

        tbPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("inactiveCaptionBorder")));
        tbPanel.setLayout(new java.awt.BorderLayout());

        btnHide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/sun/grid/shared/ui/navigator/resources/down_arrow.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btnHide, org.openide.util.NbBundle.getMessage(NavigatorTopComponent.class, "NavigatorTopComponent.btnHide.text")); // NOI18N
        btnHide.setBorderPainted(false);
        btnHide.setContentAreaFilled(false);
        btnHide.setFocusPainted(false);
        btnHide.setIconTextGap(0);
        btnHide.setMargin(new java.awt.Insets(0, 14, 0, 14));
        btnHide.setMaximumSize(new java.awt.Dimension(32767, 32767));
        btnHide.setMinimumSize(new java.awt.Dimension(0, 0));
        btnHide.setPreferredSize(new java.awt.Dimension(100, 8));
        btnHide.setVerifyInputWhenFocusTarget(false);
        btnHide.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHideMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHideMouseExited(evt);
            }
        });
        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });
        tbPanel.add(btnHide, java.awt.BorderLayout.PAGE_START);

        add(tbPanel, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewActionPerformed
        if (rootDSClazz != null) {

            List<NavigatorActionHandler> nahList = nahMap.get(rootDSClazz);
            if (nahList != null && !nahList.isEmpty()) {
                for (NavigatorActionHandler nah : nahList) {
                    nah.handleViewChanged(evt, getViewSelectedItem());
                    setRootContext(rootDSClazz, nah.getRootContext());
                    setToolBarPanel(nah.getToolbarPanel());
                }
            }
        }
    }//GEN-LAST:event_viewActionPerformed

    private void btnHideMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseEntered
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseEntered

    private void btnHideMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHideMouseExited
        btnHide.setContentAreaFilled(!(btnHide.isContentAreaFilled()));
}//GEN-LAST:event_btnHideMouseExited

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        currTBPanel.setVisible(!(currTBPanel.isVisible()));
        if (currTBPanel.isVisible()) {
            btnHide.setIcon(down_arrow);
        } else {
            btnHide.setIcon(up_arrow);
        }
}//GEN-LAST:event_btnHideActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnHide;
    private javax.swing.JPanel tbPanel;
    private javax.swing.JComboBox view;
    private javax.swing.JScrollPane viewPane;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized NavigatorTopComponent getDefault() {
        if (instance == null) {
            instance = new NavigatorTopComponent();
        }
        return instance;
    }

    /**
     * easiest way to forbid setting name.
     *
     * @param name - ignored
     */
    @Override
    public void setName(String name) {
        super.setName(NbBundle.getMessage(NavigatorTopComponent.class, "CTL_NavigatorTopComponent"));
    }

    /**
     * Obtain the NavigatorTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized NavigatorTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(NavigatorTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof NavigatorTopComponent) {
            return (NavigatorTopComponent) win;
        }
        Logger.getLogger(NavigatorTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID +
                "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentClosed() {
        es.removeSelectionListener(esListener);
        for (Map.Entry<Class<? extends DataSource>, List<NavigatorActionHandler>> entry : nahMap.entrySet()) {
            List<NavigatorActionHandler> list = entry.getValue();
            if (list != null) {
                for (NavigatorActionHandler nah : list) {
                    nah.handleComponentClosed();
                }
            }
        }
    }

    @Override
    public void componentOpened() {
        es.addSelectionListener(esListener);
        for (Map.Entry<Class<? extends DataSource>, List<NavigatorActionHandler>> entry : nahMap.entrySet()) {
            List<NavigatorActionHandler> list = entry.getValue();
            if (list != null) {
                for (NavigatorActionHandler nah : list) {
                    nah.handleComponentOpened();
                }
            }
        }
    }

    /** replaces this in object stream */
    @Override
    public Object writeReplace() {
        return new ResolvableHelper();
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    public ExplorerManager getExplorerManager() {
        return em;
    }

    final static class ResolvableHelper implements Serializable {

        private static final long serialVersionUID = 1L;

        public Object readResolve() {
            return NavigatorTopComponent.getDefault();
        }
    }

    private class ESL implements ExplorerSelectionListener {

        public void selectionChanged(Set<DataSource> arg0) {
            boolean callDefault = true;
            for (DataSource ds : arg0) {
                List<NavigatorActionHandler> nahList = nahMap.get(ds.getClass());
                if (nahList != null && !nahList.isEmpty()) {
                    for (NavigatorActionHandler nah : nahList) {
                        setViewModel(nah.getViewModel());
                        nah.handleSelectionChanged(arg0, getViewSelectedItem());
                        setRootContext(ds.getClass(), nah.getRootContext());
                        setToolBarPanel(nah.getToolbarPanel());
                    }
                    ((BeanTreeView) viewPane).setRootVisible(true);
                    callDefault = false;
                }
            }
            if (callDefault) {
                setViewModel(cbm);
                em.setRootContext(AbstractNode.EMPTY);
                ((BeanTreeView) viewPane).setRootVisible(false);
                clearToolBarPanel();
            }
        }
    }

    private void setToolBarPanel(JPanel panel) {
        if (panel == null) {
            clearToolBarPanel();
        } else if (currTBPanel != panel) {
            this.tbPanel.remove(currTBPanel);
            currTBPanel = panel;
            this.tbPanel.add(currTBPanel, BorderLayout.PAGE_END);
            setFilterPnlState();
        }
    }

    private void clearToolBarPanel() {
        setToolBarPanel(clearTBPanel);
    }

    private void setRootContext(Class<? extends DataSource> clazz, Node n) {
        if (n == null) {
            em.setRootContext(AbstractNode.EMPTY);
            rootDSClazz = null;
        } else if (!em.getRootContext().equals(n)) {
            try {
                em.setRootContext(n);
                em.setSelectedNodes(new Node[]{em.getRootContext()});
                rootDSClazz = clazz;
            } catch (PropertyVetoException ex) {
                // ignore, node will not be selected, nothing serious happens
            }
        }
    }

    private void setViewModel(ComboBoxModel m) {
        if (!view.getModel().equals(m)) {
            view.setModel(m);
        }
    }

    private Object getViewSelectedItem() {
        return view.getSelectedItem();
    }

    public void addNavigatorActionHandler(Class<? extends DataSource> clazz, NavigatorActionHandler nah) {
        if (nah != null) {
            List<NavigatorActionHandler> nahList = nahMap.get(clazz);
            if (nahList == null) {
                nahList = new LinkedList<NavigatorActionHandler>();
            }
            nahList.add(nah);
            nahMap.put(clazz, nahList);
        }
    }

    public void removeNavigatorActionHandler(Class<? extends DataSource> clazz, NavigatorActionHandler nah) {
        if (nah != null) {
            List<NavigatorActionHandler> nahList = nahMap.get(clazz);
            if (nahList != null) {
                nahList.remove(nah);
                if (nahList.isEmpty()) {
                    nahMap.remove(clazz);
                }
            }
        }
    }
}
