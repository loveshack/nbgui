/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

abstract public class ConfWizardIterator implements WizardDescriptor.Iterator<WizardDescriptor> {

    private String alternativeForPanelCounter;
    private ConfIterator iter;
    private boolean configureMode;
    
    protected ConfWizardIterator(String... subTypeNames){
        if(subTypeNames==null ||subTypeNames.length==0){
            this.iter =new ConfIterator();
        }else{
            this.iter = new SubTypeConfIterator(subTypeNames);
        }
    }

    public boolean isConfigureMode() {
        return configureMode;
    }

    public void setConfigureMode(boolean configureMode) {
        this.configureMode = configureMode;
    }



    boolean handlesSubTypes(){
        return iter instanceof SubTypeConfIterator;
    }

    WizardDescriptor.Panel<WizardDescriptor> getPanelBySubTypeName(String subTypeName){
        if(iter instanceof SubTypeConfIterator){
            return this.getPanels().get(((SubTypeConfIterator)iter).getPanelIndexForSubTypeName(subTypeName));
        }else{
            return null;
        }
    }


    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     * @return
     */
    abstract protected List<WizardDescriptor.Panel<WizardDescriptor>> getPanels();

    public WizardDescriptor.Panel<WizardDescriptor> current() {
        return getPanels().get(iter.getIndex());
    }

    String getAlternativeForPanelCounter() {
        return alternativeForPanelCounter;
    }

    void setAlternativeForPanelCounter(String alternativeForPanelCounter) {
        this.alternativeForPanelCounter = alternativeForPanelCounter;
    }

    public String name() {
        if (alternativeForPanelCounter == null) {
            return iter.getIndex() + 1 + ". from " + getPanels().size();
        } else {
            return alternativeForPanelCounter;
        }
    }
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0

    public final void addChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public final void removeChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    protected final void fireChangeEvent() {
        Iterator<ChangeListener> it;
        synchronized (listeners) {
            it = new HashSet<ChangeListener>(listeners).iterator();
        }
        ChangeEvent ev = new ChangeEvent(this);
        while (it.hasNext()) {
            it.next().stateChanged(ev);
        }
    }

  

//DELEGATORs

    public void previousPanel() {
        iter.previousPanel();
    }

    public void nextPanel() {
        iter.nextPanel();
    }

    public boolean hasPrevious() {
        return iter.hasPrevious();
    }

    public boolean hasNext() {
        return iter.hasNext();
    }

    protected int getCurrentPanelIndex() {
        return iter.getIndex();
    }

    final protected String selectedType() {
        return ((ConfWizardPanel) getPanels().get(0)).selectedType();
    }

    final protected boolean isSinglePagedDisplay() {
        return getPanels().size() == 1;
    }
   
    private class ConfIterator {
        protected int index;
        protected boolean hasNext() {
            return index < getPanels().size() - 1;
        }

        protected  boolean hasPrevious() {
            return index > 0;
        }

        protected  void nextPanel() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            index++;
            fireChangeEvent();
        }

        protected void previousPanel() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            index--;
            fireChangeEvent();
        }
        protected int getIndex(){
            return index;
        }
    }

    private class SubTypeConfIterator extends ConfIterator {
        private List<String> subTypeNames;

        protected  SubTypeConfIterator(String[] subTypeNames) {
               this.subTypeNames = Arrays.asList(subTypeNames);
        }
        private  int getPanelIndexForSubTypeName(String subTypeName){
            return subTypeNames.indexOf(subTypeName)+1;
        }

        @Override
        protected  boolean hasNext() {
            return (index == 0);
        }

        @Override
        protected  boolean hasPrevious() {
            return index > 0;
        }

        @Override
        protected  void nextPanel() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            if (index == 0) {
                index = getPanelIndexForSubTypeName(selectedType().toString());
            }
            fireChangeEvent();
        }

        @Override
        protected  void previousPanel() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            if (index > 0) {
                index = 0;
            }
            fireChangeEvent();
        }
    }
}
