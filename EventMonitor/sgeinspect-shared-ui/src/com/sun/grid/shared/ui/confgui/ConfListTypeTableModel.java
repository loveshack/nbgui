/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.openide.util.Exceptions;

class ConfListTypeTableModel<ListElement> extends AbstractTableModel {

    static final String NAME_EXTRA_COLUMN = "Extra";//TODO I18N
    static final String NAME_TYPE = "Type";//TODO I18N
    private final static long serialVersionUID = -2009191601L;
    private boolean extraColumns = false;
    private ArrayList<TableEntry> list = new ArrayList<TableEntry>();
    private ConfElement confElement;
    private final String[] columnNames;
    private final String[] mainTypeColumnNames;

    ConfListTypeTableModel(ConfElement confElement) {
        this.confElement = confElement;

        //create header array
        ArrayList<String> columnNameList = new ArrayList<String>();
        if (confElement instanceof ConfComplexType) {
            for (ConfElement element : ((ConfComplexType) confElement).getConfElements()) {
                columnNameList.add(element.getName());
            }
        } else {
            columnNameList.add(confElement.getName());
        }

        //store the main types columns to filter the subtype later
        mainTypeColumnNames = columnNameList.toArray(new String[columnNameList.size()]);

        if (confElement instanceof ConfComplexType) {
            extraColumns = ((ConfComplexType) confElement).getSubTypes().length != 0;
            if (extraColumns) {
                //add the type name to the front
                columnNameList.add(0, NAME_TYPE);
                //add an extra column to explain the settings of the subtype!
                columnNameList.add(NAME_EXTRA_COLUMN);
            }
        }
        this.columnNames = unCamelCase(columnNameList.toArray(new String[columnNameList.size()]));

    }
    private String[] unCamelCase(String[] columnNames){
        String[] newColumnNames =new String[columnNames.length];
        for(int i=0; i<columnNames.length;i++){
            newColumnNames[i]=unCamelCase(columnNames[i]);
        }
        return newColumnNames;
    }
    private String unCamelCase (String columnName){
        StringBuilder sb =new StringBuilder();
        char lastChar = 0;
        for(int i=0;i<columnName.length();i++){
            char currentChar = columnName.charAt(i);
            if(lastChar !=0  && (currentChar>='A' && currentChar<='Z')  ){
                sb.append(" ");
            }
            lastChar = currentChar;
            sb.append(currentChar);
        }
        return sb.toString();

    }


    void swap(int rowA, int rowB){
        list.add(rowB, list.remove(rowA));
        this.fireTableDataChanged();
    }

    void setColumnName(int column, String name) {
        this.columnNames[column] = name;
    }

     boolean isExtraColumn() {
        return extraColumns;
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return this.columnNames.length;
    }

    public Object getValueAt(int row, int column) {
        try {
            return list.get(row).get(column);
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;//should not happen
    }

    ListElement getConfigObjectAt(int row) {
        return list.get(row).getObj();
    }

    void add(ListElement entry) throws UnexpectedStructureException {
        list.add(new TableEntry(entry));
        fireTableStructureChanged();
    }

    void replace(ListElement entry, int row) throws UnexpectedStructureException {
        list.remove(row);
        list.add(row, new TableEntry(entry));
        fireTableStructureChanged();
    }

    ListElement remove(int index) {
        TableEntry entry = list.remove(index);
        fireTableStructureChanged();
        return entry.getObj();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    private class TableEntry {

        private ListElement obj;
        private ConfElement complexSubType;

        private TableEntry(ListElement obj) throws UnexpectedStructureException {
            this.obj = obj;
            //define the complexSubtype if
            if (confElement instanceof ConfComplexType) {
                this.complexSubType = ((ConfComplexType) confElement).getSubType(obj.getClass().getName());
            }
            if (complexSubType == null) {
                this.complexSubType = confElement;
            }
        }

        private String get(int column) throws UnexpectedStructureException {
            if (extraColumns && column == 0) {
                //look at the first column when it is a type
                return complexSubType.toString();
            } else if (extraColumns && column == (columnNames.length - 1)) {
                //this will only be called if subtype is a Complex type
                return ((ConfComplexType) complexSubType).getSingleStringRepresentation(obj, mainTypeColumnNames);
            } else {
                // normal case: look at a regualr column
                Object value = null;
                if (complexSubType instanceof ConfComplexType) {
                    int offSet = extraColumns ? 1 : 0;
                    value = ((ConfComplexType) confElement).getConfElements()[column - offSet].getValueStr(obj);
                } else {
                    value = obj;
                }
                if (value == null) {
                    value = "";
                }
                return value.toString();
            }
        }

        private ListElement getObj() {
            return obj;
        }
    }
}
