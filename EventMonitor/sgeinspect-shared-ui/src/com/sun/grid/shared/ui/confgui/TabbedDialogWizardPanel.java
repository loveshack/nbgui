/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *   The Contents of this file are made available subject to the terms of
 *   the Sun Industry Standards Source License Version 1.2
 * 
 *   Sun Microsystems Inc., March, 2001
 * 
 * 
 *   Sun Industry Standards Source License Version 1.2
 *   =================================================
 *   The contents of this file are subject to the Sun Industry Standards
 *   Source License Version 1.2 (the "License"); You may not use this file
 *   except in compliance with the License. You may obtain a copy of the
 *   License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *   Software provided under this License is provided on an "AS IS" basis,
 *   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *   See the License for the specific provisions governing your rights and
 *   obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2009 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

/**
 *
 */
class TabbedDialogWizardPanel extends ConfWizardPanel<TabbedDialogVisualPanel> {

    private ConfWizardPanel[] subPanels;

    TabbedDialogWizardPanel(ConfWizardPanel[] subPanels) {
        super(new TabbedDialogVisualPanel());
        this.subPanels = subPanels;
//        Dimension max =new Dimension(0,0);
        for(ConfWizardPanel subPanel: subPanels){
//            Dimension dim =subPanel.panel.getPreferredSize();
//            if(max.getHeight()<dim.getHeight()){
//                max.setSize(max.getWidth() ,dim.getHeight());
//            }
//            if(max.getWidth()<dim.getWidth()){
//                max.setSize(dim.getWidth(),max.getHeight());
//            }
            if(subPanel!=null){
                this.panel.tabPane.add(subPanel.panel.getName(), subPanel.panel);
            }
        }
//        this.panel.setPreferredSize(new Dimension(0,0));
//        max.setSize(max.getWidth()+20,max.getHeight()+20);
//        this.panel.tabPane.setPreferredSize(max);
    }


    @Override
    protected void addEditTableActionListeners(ConfHelper confHelper, JTable table, JButton btnAdd, JButton btnRemove) {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.addEditTableActionListeners(confHelper, table, btnAdd, btnRemove);
        }
    }

    @Override
    protected void addStartSubWizardActionListener(ConfHelper confHelper, JTextField txt, JButton button) {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.addStartSubWizardActionListener(confHelper, txt, button);
        }
    }



    @Override
    public void readSettings(WizardDescriptor wd) {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.readSettings(wd);
        }
    }



    @Override
    protected void setupActionListeners() {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.setupActionListeners();
        }

    }

    @Override
    protected void setupCheckBoxControlledComponents(JCheckBox chkbox, Component[] group) {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.setupCheckBoxControlledComponents(chkbox, group);
        }
    }

   

    @Override
    protected void setupModels() {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.setupModels();
        }

    }

    @Override
    public void storeSettings(WizardDescriptor wd) {
        for (ConfWizardPanel subPanel : subPanels) {
            subPanel.storeSettings(wd);
        }
    }

    @Override
    public void validate() throws WizardValidationException {
       for (ConfWizardPanel subPanel : subPanels) {
            subPanel.validate(this.panel.tabPane);
        }
    }


    @Override
    public boolean isFinishPanel() {
        return true;
    }

    @Override
    public boolean isValid() {
        for (ConfWizardPanel subPanel : subPanels) {
            if(!subPanel.isValid()){
                return false;
            }
        }
        return true;
    }
}
