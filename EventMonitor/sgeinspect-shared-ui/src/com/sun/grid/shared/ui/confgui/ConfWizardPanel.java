
/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbPreferences;

public abstract class ConfWizardPanel<Panel extends JPanel> implements WizardDescriptor.Panel<WizardDescriptor>, WizardDescriptor.ValidatingPanel<WizardDescriptor>, WizardDescriptor.FinishablePanel<WizardDescriptor> {

    protected WizardDescriptor wd;
    protected Preferences pref;
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    private boolean initialized = false;
    private boolean isValid = true;
    private HashMap<String, Object> complexConfObjects = new HashMap<String, Object>();
    private HashMap<String, Component> declaredComponents = new HashMap<String, Component>();
    private HashMap<Component, String> declaredComponentNames = new HashMap<Component, String>();
    private HashMap<JCheckBox, Component[]> checkBoxControlled = new HashMap<JCheckBox, Component[]>();
    private HashMap<Component, Object> disabledComponentContent = new HashMap<Component, Object>();
    private JComboBox cboSubType;
    private JTextFieldValidator txtValidator = new JTextFieldValidator();
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    protected Panel panel;

    public Panel getPanel() {
        return panel;
    }

    protected ConfWizardPanel(Panel panel) {
        this.panel = panel;
        Class panelClass = panel.getClass();
        Field panelFields[] = panelClass.getDeclaredFields();
        for (Field field : panelFields) {
            if (Modifier.isPublic(field.getModifiers())) {
                Object obj = null;
                try {
                    obj = field.get(panel);
                } catch (IllegalArgumentException ex) {
                    Exceptions.printStackTrace(ex);
                } catch (IllegalAccessException ex) {
                    Exceptions.printStackTrace(ex);
                }
                //JTextField
                if (obj instanceof Component) {
                    String cmpName = field.getName();
                    Component cmp = (Component) obj;
                    declaredComponents.put(cmpName, cmp);
                    declaredComponentNames.put(cmp, cmpName);
                    if (cmpName.startsWith(ConfComplexType.PREFIX_COMBOBOX_COMPONENT) && cmpName.endsWith(ConfComplexType.SUFFIX_SUBTYPE)) {
                        cboSubType = (JComboBox) cmp;
                    }

                }
            }
        }
    }

    public void validate() throws WizardValidationException {
        validate(null);
    }
    // this one can be called from a tabbed dialog instead of  validate() and allows to switch the tab!

    void validate(JTabbedPane tabbed) throws WizardValidationException {

        JTextFieldValidator.Entry entry = this.txtValidator.validate();
        if (entry != null) {
            wd.setValue(WizardDescriptor.CANCEL_OPTION);
            JTextField txt = entry.getTextField();

            //if Tabbed switch the tab!
            if (tabbed != null) {
                tabbed.setSelectedComponent(panel);
            }
            txt.setSelectionStart(0);
            txt.setSelectionEnd(txt.getText().length());
            txt.requestFocus();
            throw new WizardValidationException(null, entry.getLabel().getText() + ": \"" + txt.getText() + "\" - " + entry.getError(), null);//TODO TEST null
        }
    }

    public boolean isValid() {
        return isValid;
    }

    final protected boolean isComponentInVisualPanel(Component c) {
        return this.declaredComponentNames.containsKey(c);
    }

    final protected void validatePath(JLabel label, JTextField txt) {
        txtValidator.registerPath(label, txt);
    }

    final protected void validatePositiveInteger(JLabel label, JTextField txt) {
        txtValidator.registerPositiveInteger(label, txt);
    }

    final protected void validateNonEmptyNonWhiteSpace(JLabel label, JTextField txt) {
        txtValidator.registerNonEmptyNonWhiteSpace(label, txt);
    }

    final protected void validateNonEmpty(JLabel label, JTextField txt) {
        txtValidator.registerNonEmpty(label, txt);
    }

    final protected void validateInteger(JLabel label, JTextField txt) {
        txtValidator.registerInteger(label, txt);
    }

    final protected void validateNotOnUsedList(JLabel label, JTextField txt, List<String> used) {
        txtValidator.registerNotOnUsedList(label, txt, used);
    }

    protected void registerTableSortButtons(final JTable table, JButton btnUp, JButton btnDown) {
        btnUp.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int cur = table.getSelectedRow();
                if (cur > 0) {
                    swapTableEntries(table, cur, cur - 1);
                }
            }
        });
        btnDown.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int cur = table.getSelectedRow();
                if (cur >= 0 && cur < table.getRowCount() - 1) {
                    swapTableEntries(table, cur, cur + 1);
                }
            }
        });
    }

    private void swapTableEntries(JTable table, int a, int b) {
        TableModel m = table.getModel();
        if (m instanceof ConfListTypeTableModel) {
            ConfListTypeTableModel model = (ConfListTypeTableModel) m;
            model.swap(a, b);
            table.getSelectionModel().setSelectionInterval(b, b);
        }
        this.fireChangeEvent();
    }

    JComboBox getCboSubType() {
        return cboSubType;
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Panel getComponent() {
        return panel;
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isFinishPanel() {
        return false;
    }

    public final void addChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public final void removeChangeListener(ChangeListener l) {
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    protected final void fireChangeEvent() {
        Iterator<ChangeListener> it;
        synchronized (listeners) {
            it = new HashSet<ChangeListener>(listeners).iterator();
        }
        ChangeEvent ev = new ChangeEvent(this);
        while (it.hasNext()) {
            it.next().stateChanged(ev);
        }
    }

    // You can use a settings object to keep track of state. Normally the
    // settings object will be the WizardDescriptor, so you can use
    // WizardDescriptor.getProperty & putProperty to store information entered
    // by the user.
    public void readSettings(WizardDescriptor wd) {
        //System.out.println("readSettings");

        this.panel = getComponent();
        this.wd = wd;

        this.pref = NbPreferences.forModule((Class) wd.getProperty(ConfHelper.PREFERENCES_CLASS_KEY));
//        try {
//            System.out.println("*************************** Will put that into gui: * ******************************");
//            for (String key : pref.keys()) {
//               System.out.println("Key:" + key + " Value:" + pref.get(key, "<empty>"));
//            }
//            System.out.println("************************************************************************************");
//        } catch (BackingStoreException ex) {
//            Exceptions.printStackTrace(ex);
//        }

        if (!initialized) {
            setupBasicModels();
            setupModels();
            setupBasicActionListeners();
            setupActionListeners();
            initialized = true;
        }


        for (String name : declaredComponents.keySet()) {
            Component obj = declaredComponents.get(name);

            //JTextField
            if (obj instanceof JTextField) {
                JTextField txt = (JTextField) obj;
                declaredComponents.put(name, txt);
                txt.setText(pref.get(name, ""));
                //System.out.println("Loaded " + txt.getText() + " from " + name);
            }

            //JCheckBox
            if (obj instanceof JCheckBox) {
                JCheckBox chk = (JCheckBox) obj;
                chk.setSelected("true".equals(pref.get(name, "false")));
                //System.out.println("Loaded " + chk.isSelected() + " from " + name);
                checkBoxBasedEnabing(chk);
            }

            //JComboBox
            if (obj instanceof JComboBox) {
                JComboBox cbo = (JComboBox) obj;
                cbo.setSelectedItem(pref.get(name, ""));
                int found = -1;
                String selected = pref.get(name, "");
                for (int i = 0; i < cbo.getItemCount(); i++) {
                    if (selected.equals(cbo.getItemAt(i).toString())) {
                        found = i;
                        break;
                    }
                }
                if (found != -1) {
                    cbo.setSelectedIndex(found);
                }
                //System.out.println("Loaded " + selected + " from " + name);
            }
        }

    }

    public void storeSettings(WizardDescriptor wd) {
        for (String name : declaredComponents.keySet()) {
            Component obj = declaredComponents.get(name);


            //JTextField
            if (obj instanceof JTextField) {
                JTextField txt = (JTextField) obj;
                storeSettings(name, txt.getText());
                //System.out.println("Stored " + txt.getText() + " under " + name);
            }
            //JComboBox
            if (obj instanceof JComboBox) {
                JComboBox cbo = (JComboBox) obj;
                //do not store the string but the object!!
                storeSettings(name, cbo.getSelectedItem());
                //System.out.println("Stored " + cbo.getSelectedItem() + " under " + name);
            }

            //JCheckBox
            if (obj instanceof JCheckBox) {
                JCheckBox chk = (JCheckBox) obj;
                storeSettings(name, "" + chk.isSelected());
                //System.out.println("Stored " + chk.isSelected() + " under " + name);

            }
            //JTable
            if (obj instanceof JTable) {
                JTable tbl = (JTable) obj;
                wd.putProperty(name, tbl.getModel());
                //System.out.println("Stored Model of " + name + " under " + name);
                // do not store in prefs!
            }

            //store the ComplexConfObjects!
            for (String key : this.complexConfObjects.keySet()) {
                wd.putProperty(key, this.complexConfObjects.get(key));
            }
        }

    }

    protected void setupCheckBoxControlledComponents(JCheckBox chkbox, Component[] group) {
        this.checkBoxControlled.put(chkbox, group);
    }

    private void checkBoxBasedEnabing(JCheckBox chkBox) {
        if (chkBox.isSelected() && !chkBox.isEnabled()) {
            chkBox.setSelected(false);
            return;
        }
        Component[] group = checkBoxControlled.get(chkBox);
        List<JCheckBox> chkCallAgain = new ArrayList<JCheckBox>();
        if (group != null) {
            for (Component c : group) {
                c.setEnabled(chkBox.isSelected());
                if (c instanceof JCheckBox) {
                    // we will have to disable the groups of these comboboxes again!
                    if (!((JCheckBox) c).isSelected() && c.isEnabled()) {
                        chkCallAgain.add((JCheckBox) c);
                    }
                }
                if (c instanceof JTextField) {
                    if (c.isEnabled()) {
                        String txt = (String) disabledComponentContent.get(c);
                        //place value from cache only if the txt field is empty and there is something in the cache
                        if (txt != null && !"".equals(txt) && "".equals(((JTextField)c).getText())) {
                            ((JTextField) c).setText(txt);
                        }
                    } else {
                        String txt = ((JTextField) c).getText();
                        if (txt != null) {
                            disabledComponentContent.put(c, txt);
                        }
                        ((JTextField) c).setText("");
                    }
                }
                if (c instanceof JTable) {
                    if (c.isEnabled()) {
                        TableModel model = (TableModel) disabledComponentContent.get(c);
                        if (model != null) {
                            ((JTable) c).setModel(model);
                        }
                    } else {
                        TableModel model = ((JTable) c).getModel();
                        if (model != null) {
                            disabledComponentContent.put(c, model);
                        }
                        ((JTable) c).setModel(new DefaultTableModel());
                    }
                }
            }
        }
        for (JCheckBox chk : chkCallAgain) {
            this.checkBoxBasedEnabing(chk);
        }
    }

    private void storeSettings(String key, Object value) {
        if (value != null) {
            wd.putProperty(key, value);
            pref.put(key, value.toString());
        }
    }

    protected void addStartSubWizardActionListener(final ConfHelper confHelper, final JTextField txt, JButton button) {
        this.addStartSubWizardActionListener(confHelper, txt, button, true);
    }

    protected void addStartSubWizardActionListener(final ConfHelper confHelper, final JTextField txt, JButton button, final boolean deletePrefs) {
        button.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startSubWizard(confHelper, txt, deletePrefs);
            }
        });
    }

    protected void addEditTableActionListeners(final ConfHelper confHelper, final JTable table, JButton btnAdd, JButton btnRemove) {
        addEditTableActionListeners(confHelper, table, btnAdd, btnRemove, true);
    }

    protected void addEditTableActionListeners(final ConfHelper confHelper, final JTable table, JButton btnAdd, JButton btnRemove, final boolean deletePrefs) {

        btnAdd.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addNewTableEntry(confHelper, table, deletePrefs);
            }
        });

        btnRemove.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeSelectedTableEntry(table);
            }
        });

        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editTableEntry(confHelper, table, e.getPoint(),deletePrefs);
                }
            }
        });
    }

    private void startSubWizard(ConfHelper confHelper, JTextField txt, boolean deletePrefs) {
        Object oldConfig = getComplexConfObject(txt);
        @SuppressWarnings("unchecked")
        Object newConfig = confHelper.startWizardDialog(oldConfig, deletePrefs);
        if (newConfig != null) {
            setComplexConfObject(declaredComponentNames.get(txt), newConfig);
            fireChangeEvent();
        }
    }

    @SuppressWarnings({"unchecked"})
    private void addNewTableEntry(ConfHelper confHelper, JTable table, boolean deletePrefs) {
        Object sloConfig = confHelper.startWizardDialog(null, deletePrefs);
        if (sloConfig != null) {
            try {
                ((ConfListTypeTableModel) table.getModel()).add(sloConfig);
            } catch (UnexpectedStructureException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void editTableEntry(ConfHelper confHelper, JTable table, Point p,boolean deletePrefs) {
        int row = table.rowAtPoint(p);

        if (row != -1) {
            ConfListTypeTableModel model = (ConfListTypeTableModel) table.getModel();
            Object sloConfig = confHelper.startWizardDialog(model.getConfigObjectAt(row),deletePrefs);
            if (sloConfig != null) {
                try {
                    ((ConfListTypeTableModel) table.getModel()).replace(sloConfig, row);
                } catch (UnexpectedStructureException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }

    private void removeSelectedTableEntry(JTable table) {
        int row = table.getSelectedRow();
        if (row != -1) {
            ConfListTypeTableModel model = (ConfListTypeTableModel) table.getModel();
            model.remove(row);
        }
    }

    private Object getComplexConfObject(Component component) {
        String confTextfieldName = this.declaredComponentNames.get(component);
        return this.complexConfObjects.get(confTextfieldName + ConfElement.SUFFIX_COMPONENT_OBJECT);
    }

    private void setComplexConfObject(String confTextfieldName, Object conf) {
        JTextField correspondigTextField = (JTextField) this.declaredComponents.get(confTextfieldName);
        this.complexConfObjects.put(confTextfieldName + ConfElement.SUFFIX_COMPONENT_OBJECT, conf);

        // try to update the corresponding TextField
        try {
            String singleStr = ConfElementFactory.getSingleStringRepresentation(conf);
            correspondigTextField.setText(singleStr);
            this.fireChangeEvent();
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    String selectedType() {
        if (cboSubType != null) {
            Object item = cboSubType.getSelectedItem();
            if (item != null) {
                return item.toString();
            }
        }
        return "";
    }

    protected void setupModels() {
    }

    protected void setupActionListeners() {
    }

    private final void setupBasicModels() {
        for (String name : declaredComponents.keySet()) {
            Component obj = declaredComponents.get(name);
            //JComboBox
            if (obj instanceof JComboBox) {
                JComboBox cbo = (JComboBox) obj;
                ComboBoxModel model = (ComboBoxModel) wd.getProperty(name + ConfElement.SUFFIX_COMPONENT_MODEL);
                if (model != null) {
                    cbo.setModel(model);
                }
            }

            //JTable
            if (obj instanceof JTable) {
                JTable tbl = (JTable) obj;
                //System.out.println("Looking for model of " + obj + " ...");
                TableModel model = (TableModel) wd.getProperty(name + ConfElement.SUFFIX_COMPONENT_MODEL);
                if (model != null) {
                    tbl.setModel(model);
                    //System.out.println(".. found model for " + obj);
                }
            }

        }



        // look complexObjects and remove them but store them internaly if they have a representative txtField
        for (String key : wd.getProperties().keySet()) {
            if (key.endsWith(ConfElement.SUFFIX_COMPONENT_MODEL)) {
                Object model = wd.getProperty(key);
                if (model != null) {
                    //check if a representant exists!
                    String txtFieldName = key.substring(0, key.length() - ConfElement.SUFFIX_COMPONENT_MODEL.length());
                    if (this.declaredComponents.containsKey(txtFieldName)) {
                        //bingo it is a complex one!
                        //store it in the lookup!
                        this.complexConfObjects.put(txtFieldName + ConfElement.SUFFIX_COMPONENT_OBJECT, model);
                    }
                }
            }
        }
    }

    private void setupBasicActionListeners() {
        for (String name : declaredComponents.keySet()) {
            Component obj = declaredComponents.get(name);

            //JComboBox
            if (obj instanceof JComboBox) {
                JComboBox cbo = (JComboBox) obj;
                cbo.addActionListener(new java.awt.event.ActionListener() {

                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        fireChangeEvent();
                    }
                });
            }

            //JTable
            if (obj instanceof JTable) {
                JTable tbl = (JTable) obj;
                /*
                tbl.addaddActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                fireChangeEvent();
                }
                });
                 */
            }
            if (obj instanceof JCheckBox) {
                final JCheckBox chk = (JCheckBox) obj;
                if (name.endsWith(ConfElement.SUFFIX_COMPONENT_IS_SET)) {
                    chk.addActionListener(new java.awt.event.ActionListener() {

                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                            checkBoxBasedEnabing(chk);
                            fireChangeEvent();
                        }
                    });
                }
            }

        }
    }
}
