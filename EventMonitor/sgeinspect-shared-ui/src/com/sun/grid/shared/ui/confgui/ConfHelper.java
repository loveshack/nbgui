/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.ui.confgui;

import java.awt.Dialog;
import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import org.openide.DialogDisplayer;

abstract public class ConfHelper<ConfObject> {

    public static final String PREFERENCES_CLASS_KEY = "global_preferences";
    public static final String CONFIG_HELPER_KEY = "config_helper_object";
    private ConfElement confType;
    private ConfObject[] defaultConfObjects;
    private Class<? extends ConfWizardIterator> iteratorClass;
    private String wizardTitle;

    protected ConfHelper(Class<? extends ConfWizardIterator> iteratorClass, Class<ConfObject> confClass, String confClassName, ConfObject[] defaultConfs, String wizardTitle) throws UnexpectedStructureException {
        this.iteratorClass = iteratorClass;
        this.defaultConfObjects = defaultConfs;
        this.wizardTitle = wizardTitle;
        this.confType = ConfElementFactory.getConfElement(null, null, confClass, null, "", confClassName, true/* no parent: default to true*/);
        //printConfigurationOutline(); 

    }

    protected static void setSubClassRegistry(SubClassRegistry registry) {
        ConfComplexType.setSubClassRegistry(registry);
    }

    protected void putDefaultsInGui(WizardDescriptor wd, Preferences pref) throws UnexpectedStructureException {
        if (defaultConfObjects != null) {
            for (ConfObject defaultConf : defaultConfObjects) {
                putConfInGui(wd, pref, defaultConf);
            }
        }
    }

    protected void putConfInGui(WizardDescriptor wd, Preferences pref, ConfObject userConfObject) throws UnexpectedStructureException {
        confType.putConfInGui(wd, pref, userConfObject);
    }

    protected ConfObject getConfFromGui(WizardDescriptor wizardDescriptor) throws UnexpectedStructureException {
        @SuppressWarnings("unchecked")
        ConfObject newConfObject = (ConfObject) confType.getConfFromGui(wizardDescriptor);
        return newConfObject;
    }

    protected void storeModels(WizardDescriptor wd,ConfObject config,boolean  isConfigMode) {
    }

    public ConfObject startWizardDialog(ConfObject oldConfig) {
        return startWizardDialog(oldConfig, true, oldConfig != null);
    }

    public ConfObject startWizardDialog(ConfObject oldConfig, boolean deletePrefs) {
        return startWizardDialog(oldConfig, deletePrefs, oldConfig != null);
    }

    public ConfObject startWizardDialog(ConfObject oldConfig, boolean deletePrefs, boolean isConfigMode) {
        try {
            ConfWizardIterator iterator = this.getIterator(oldConfig, isConfigMode);
            WizardDescriptor wizardDescriptor = getWizardDescriptor(iterator);
            Preferences pref = NbPreferences.forModule(iteratorClass);
            //The problem: the pref is shared over dialogs of this module!!
            Map<String, String> copy = null;
            if (deletePrefs) {
                copy = storeAndDeletePrefs(pref);
            }

            //Store the default cbo and table models in the wizard descriptor
            confType.storeModels(wizardDescriptor);
            storeModels(wizardDescriptor, oldConfig,isConfigMode);
            putDefaultsInGui(wizardDescriptor, pref);
            
            if (oldConfig != null) {
                putConfInGui(wizardDescriptor, pref, oldConfig);
            }
            ConfObject newConfig = showDialog(wizardDescriptor);

            if (deletePrefs) {
                restorePrefs(pref, copy);
            }
            return newConfig;
        } catch (InstantiationException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IllegalAccessException ex) {
            Exceptions.printStackTrace(ex);
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;//something went wrong

    }

    private ConfObject showDialog(WizardDescriptor wizardDescriptor) throws UnexpectedStructureException {
        Dialog dialog = DialogDisplayer.getDefault().createDialog(wizardDescriptor);
        dialog.setVisible(true);
        dialog.toFront();
        boolean cancelled = wizardDescriptor.getValue() != WizardDescriptor.FINISH_OPTION;

        if (!cancelled) {
            return getConfFromGui(wizardDescriptor);

        } else {
            return null;
        }

    }

    private WizardDescriptor getWizardDescriptor(ConfWizardIterator iterator) {
        WizardDescriptor wizardDescriptor = new WizardDescriptor(iterator);
        wizardDescriptor.setTitle(this.wizardTitle);
        wizardDescriptor.putProperty(PREFERENCES_CLASS_KEY, iteratorClass);
        wizardDescriptor.setTitleFormat(new MessageFormat("{0} ({1})"));
        if (iterator.isSinglePagedDisplay()) {
            wizardDescriptor.setOptions(new Object[]{WizardDescriptor.OK_OPTION, WizardDescriptor.CANCEL_OPTION});
        }

        return wizardDescriptor;
    }

    private ConfWizardIterator getIterator(ConfObject oldConfig, boolean isConfigMode) throws InstantiationException, IllegalAccessException {

        ConfWizardIterator iterator = this.iteratorClass.newInstance();
        iterator.setConfigureMode(isConfigMode);
        if (isConfigMode && !iterator.isSinglePagedDisplay()) {
            String subTypeName = null;
            if (oldConfig != null) {
                SubClassEntry entry = ConfComplexType.getSubClassRegistry().getSubClassEntryBySubClass(oldConfig.getClass());
                if (entry != null) {
                    subTypeName = entry.getToString();
                }
            }
            iterator = new TabbedDialogIterator(iterator, subTypeName);
        }
        iterator.setAlternativeForPanelCounter(wizardTitle);

        return iterator;


    }

    private Map<String, String> storeAndDeletePrefs(Preferences pref) {
        Map<String, String> copy = new HashMap<String, String>();
        try {
            //System.out.println("Will delete now the following pref: "+pref +" it was attached to  "+ iterator.getClass());
            for (String key : pref.keys()) {
                copy.put(key, pref.get(key, ""));
            }
            pref.clear();

        } catch (BackingStoreException ex) {
            Exceptions.printStackTrace(ex);
        }
        return copy;
    }

    private void restorePrefs(Preferences pref, Map<String, String> copy) {
        try {
            //System.out.println("Will delete now the following pref: "+pref +" it was attached to  "+ iterator.getClass());
            pref.clear();
            for (String key : copy.keySet()) {
                pref.put(key, copy.get(key));
            }

        } catch (BackingStoreException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private void printConfigurationOutline() {
        System.out.println("Structure:");
        confType.outline(1);
        System.out.println("Required Annotations:");
        confType.outLineRequired(0);
    }
}
