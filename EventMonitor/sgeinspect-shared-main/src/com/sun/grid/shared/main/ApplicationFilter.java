/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.main;

import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasupport.DataChangeEvent;
import com.sun.tools.visualvm.core.datasupport.DataChangeListener;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.RequestProcessor;

/**
 *
 * @author mb199851
 */
public class ApplicationFilter {

    private final Set<Class<? extends DataSource>> registeredAppz = new HashSet<Class<? extends DataSource>>();
    private final DataChangeListener<DataSource> l = new L();

    private static final class Singleton {

        private static final ApplicationFilter INSTANCE = new ApplicationFilter();
    }

    public static ApplicationFilter getInstance() {
        return Singleton.INSTANCE;
    }

    private synchronized void filterDataSources(boolean visible) {
        Set<DataSource> dataSources = DataSource.ROOT.getRepository().getDataSources();
        for (DataSource dataSource : dataSources) {
            if (!(registeredAppz.contains(dataSource.getClass()))) {
                dataSource.setVisible(visible);
            } else {
                dataSource.setVisible(!visible);
            }
        }
    }

    public synchronized void initialize() {
        filterDataSources(false);
        DataSource.ROOT.getRepository().addDataChangeListener(l, DataSource.class);
    }

    public synchronized void shutdown() {
        DataSource.ROOT.getRepository().removeDataChangeListener(l);
        filterDataSources(true);
    }

    public synchronized void registerApplication(Class<? extends DataSource> clazz) {
        registeredAppz.add(clazz);
        filterDataSources(false);
    }

    public synchronized void unregisterApplication(Class<? extends DataSource> clazz) {
        registeredAppz.remove(clazz);
        filterDataSources(false);
    }

    private class L implements DataChangeListener<DataSource> {

        public void dataChanged(DataChangeEvent<DataSource> event) {
            RequestProcessor.getDefault().post(new DataChanged(event));
        }

        private class DataChanged implements Runnable {

            private final DataChangeEvent<DataSource> event;

            public DataChanged(DataChangeEvent<DataSource> event) {
                this.event = event;
            }

            public void run() {
                for (DataSource ds : event.getAdded()) {
                    if (!(registeredAppz.contains(ds.getClass()))) {
                        ds.setVisible(false);
                    } else {
                        ds.setVisible(true);
                    }
                }
            }
        }
    }
}
