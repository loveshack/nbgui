/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.main.util;

/**
 * Utility class to force to use the dedicated classloader for execution of
 * a code block within the current thread.
 */
public abstract class ClassLoaderAnchor {

    public static <T> T executeInSDMVersionedClassLoader(Class<?> clazz, CommandBlock<T> block) {
        Thread current = Thread.currentThread();
        ClassLoader orig = current.getContextClassLoader();
        current.setContextClassLoader(clazz.getClassLoader());
        try {
            return block.execute();
        } finally {
            current.setContextClassLoader(orig);
        }
    }

    public static <T, X extends Throwable> T executeInSDMVersionedClassLoader(Class<?> clazz, CommandBlockWithException<T, X> block) throws X {
        Thread current = Thread.currentThread();
        ClassLoader orig = current.getContextClassLoader();
        current.setContextClassLoader(clazz.getClassLoader());
        try {
            return block.execute();
        } finally {
            current.setContextClassLoader(orig);
        }
    }

    public static <T, X extends Throwable, Y extends Throwable> T executeInSDMVersionedClassLoader(Class<?> clazz, CommandBlockWithExceptions<T, X, Y> block) throws X, Y {
        Thread current = Thread.currentThread();
        ClassLoader orig = current.getContextClassLoader();
        current.setContextClassLoader(clazz.getClassLoader());
        try {
            return block.execute();
        } finally {
            current.setContextClassLoader(orig);
        }
    }

    public static interface CommandBlock<U> {

        public U execute();
    }

    public static interface CommandBlockWithException<U, V extends Throwable> {

        public U execute() throws V;
    }

    public static interface CommandBlockWithExceptions<U, V extends Throwable, X extends Throwable> {

        public U execute() throws V, X;
    }
}
