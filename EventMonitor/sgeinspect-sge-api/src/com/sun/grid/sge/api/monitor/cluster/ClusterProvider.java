/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.DataSourceRepository;
import com.sun.tools.visualvm.core.datasource.Storage;
import com.sun.tools.visualvm.core.datasupport.Utils;
import java.awt.BorderLayout;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;
import org.openide.util.RequestProcessor;

public class ClusterProvider {

    protected static final Logger LOGGER = Logger.getLogger(ClusterProvider.class.getName());
    protected boolean initializingClusters = true;
    protected Semaphore initializingClustersSemaphore = new Semaphore(1);
    private static final ClusterProvider INSTANCE = new ClusterProvider();

    private ClusterProvider() {
        try {
            initializingClustersSemaphore.acquire();
        } catch (InterruptedException ex) {
            LOGGER.throwing(ClusterProvider.class.getName(), "<init>", ex);    // NOI18N
        }
    }

    public static ClusterProvider getInstance() {
        return INSTANCE;
    }

    public Cluster getClusterByName(String clusterName) {
        waitForInitialization();

        Set<Cluster> knownClusters = DataSourceRepository.sharedInstance().getDataSources(Cluster.class);
        for (Cluster knownCluster : knownClusters) {
            if (knownCluster.getClusterName().equals(clusterName)) {
                return knownCluster;
            }
        }

        return null;
    }

    protected void waitForInitialization() {
        if (initializingClusters) {
            try {
                initializingClustersSemaphore.acquire();
            } catch (InterruptedException ex) {
                LOGGER.throwing(ClusterProvider.class.getName(), "waitForInitialization", ex); // NOI18N
            }
        }
    }

    protected static void notifyUnresolvedClusters(final Set<File> unresolvedClustersF, final Set<String> unresolvedClustersS) {
        RequestProcessor.getDefault().post(new Runnable() {

            public void run() {
                // TODO unify message dialog window handling
                JPanel messagePanel = new JPanel(new BorderLayout(5, 5));
                messagePanel.add(new JLabel(NbBundle.getMessage(ClusterProvider.class, "MSG_Unresolved_Clusters")), BorderLayout.NORTH); // NOI18N
                JList list = new JList(unresolvedClustersS.toArray());
                list.setVisibleRowCount(4);
                messagePanel.add(new JScrollPane(list), BorderLayout.CENTER);
                NotifyDescriptor dd = new NotifyDescriptor(
                        messagePanel, NbBundle.getMessage(ClusterProvider.class, "Title_Unresolved_Clusters"), // NOI18N
                        NotifyDescriptor.YES_NO_OPTION, NotifyDescriptor.ERROR_MESSAGE,
                        null, NotifyDescriptor.YES_OPTION);
                if (DialogDisplayer.getDefault().notify(dd) == NotifyDescriptor.NO_OPTION) {
                    for (File file : unresolvedClustersF) {
                        Utils.delete(file, true);
                    }
                }

                unresolvedClustersF.clear();
                unresolvedClustersS.clear();
            }
        }, 1000);
    }

    protected void initCluster() {
        Cluster cluster = Cluster.CURRENT_CLUSTER;
        if (cluster != null) {
            DataSource.ROOT.getRepository().addDataSource(cluster);
        }
    }

    public void createCluster(final ClusterConfiguration config, final String version) {
        DataSource.EVENT_QUEUE.post(new Runnable() {

            public void run() {
                waitForInitialization();

                final String clusterName = config.getClusterName();
                LOGGER.log(Level.FINE, NbBundle.getMessage(ClusterProvider.class, "clusterName"), clusterName);
                if (clusterName != null) {
                    final Cluster knownCluster = getClusterByName(clusterName);
                    if (knownCluster != null) {
                        LOGGER.log(Level.FINE, NbBundle.getMessage(ClusterProvider.class, "knownCluster"), knownCluster.getClusterName());
                    } else {
                        File customPropertiesStorage = Utils.getUniqueFile(ClusterSupport.getStorageDirectory(), clusterName, Storage.DEFAULT_PROPERTIES_EXT);
                        Storage storage = new Storage(customPropertiesStorage.getParentFile(), customPropertiesStorage.getName());

                        LOGGER.log(Level.FINE, "storage dir={0} file={1}", new Object[]{customPropertiesStorage.getParentFile(), customPropertiesStorage.getName()});

                        config.store(storage);

                        Cluster newCluster = null;

                        try {
                            // getFactory, create cluster
                            ClusterFactory factory = ClusterFactoryProvider.getRegisteredClusterFactory(version);
                            newCluster = factory.createClusterDataSource(config, storage);
                        } catch (Exception e) {
                            e.printStackTrace();
                            LOGGER.log(Level.SEVERE, NbBundle.getMessage(ClusterProvider.class, "Error_creating_cluster"), e);
                        }

                        if (newCluster != null) {
                            // TODO: if the connection fails due to wrong version
                            // selection change it!
                            newCluster.connect();
                            ClusterContainer.sharedInstance().getRepository().addDataSource(newCluster);
                        }

                    }
                }
            }
        });

    }

    // get the stored clusters
    protected void initStoredClusters() {
        if (ClusterSupport.storageDirectoryExists()) {
            File[] files = ClusterSupport.getStorageDirectory().listFiles(new FilenameFilter() {

                public boolean accept(File dir, String name) {
                    return name.endsWith(Storage.DEFAULT_PROPERTIES_EXT);
                }
            });

            Set<File> unresolvedClustersF = new HashSet<File>();
            Set<String> unresolvedClustersS = new HashSet<String>();

            Set<Cluster> clusters = new HashSet<Cluster>();
            for (File file : files) {
                LOGGER.log(Level.FINE, "reading persistent cluster: dir={0} file={1}", new Object[]{file.getParentFile(), file.getName()});
                Storage storage = new Storage(file.getParentFile(), file.getName());
                // todo get the version to and from stored config !
                String clusterVersion = storage.getCustomProperty(ClusterConfiguration.PROPERTY_CLUSTERVERSION);
                String clusterName = storage.getCustomProperty(ClusterConfiguration.PROPERTY_CLUSTERNAME);
                Cluster persistedCluster = null;
                try {

                    if (clusterVersion == null) {
                        clusterVersion = "62u3";
                    }

                    ClusterConfigurationFactory ccf = ClusterConfigurationFactoryProvider.getRegisteredClusterFactory(clusterVersion);
                    if (ccf == null) {
                        com.sun.grid.shared.ui.util.ErrorDisplayer.submitWarning("Can not find cluster configuration factory for version: " + clusterVersion); // todo I18N!!!
                    }

                    ClusterConfiguration config = ccf.createClusterConfiguration();
                    config.load(storage);

                    ClusterFactory cf = ClusterFactoryProvider.getRegisteredClusterFactory(clusterVersion);
                    if (cf == null) {
                        com.sun.grid.shared.ui.util.ErrorDisplayer.submitWarning("Can not find cluster factory for version: " + clusterVersion); // todo I18N!!!
                    }
                    persistedCluster = cf.createClusterDataSource(config, storage);

                    // TODO connect to the previous cluster if possible ????
                    // persistedCluster.connect();
                } catch (Exception e) {
                    LOGGER.throwing(ClusterProvider.class.getName(), "initPersistedClusters", e);    // NOI18N
                    unresolvedClustersF.add(file);
                    unresolvedClustersS.add(clusterName);
                }

                if (persistedCluster != null) {
                    clusters.add(persistedCluster);
                }
            }

            if (!unresolvedClustersF.isEmpty()) {
                notifyUnresolvedClusters(unresolvedClustersF, unresolvedClustersS);
            }
            ClusterContainer.sharedInstance().getRepository().addDataSources(clusters);
        }

        DataSource.EVENT_QUEUE.post(new Runnable() {

            public void run() {
                initializingClustersSemaphore.release();
                initializingClusters = false;
            }
        });
    }

    public void initialize() {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable() {

            public void run() {
                RequestProcessor.getDefault().post(new Runnable() {

                    public void run() {
                        initCluster();
                        initStoredClusters();
                    }
                });
            }
        });
    }

    public void shutdown() {
        LOGGER.log(Level.FINER, NbBundle.getMessage(ClusterProvider.class, "shutdown"));
        for (DataSource ds : ClusterContainer.sharedInstance().getRepository().getDataSources()) {
            Cluster cluster = (Cluster) ds;
            LOGGER.log(Level.FINER, NbBundle.getMessage(ClusterProvider.class, "disconnect_cluster"), cluster.getClusterName());
            cluster.disconnect();
        }
    }
}
