/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.api.monitor.util;

import java.lang.reflect.Method;

public class ExceptionHelper {
     private ExceptionHelper() {}

    /**
     * Searches the {@link Throwable} cause tree for a certain type.
     * @param <T> A {@link Throwable} subtype
     * @param type The type of the {@link Throwable} we are interested in
     * @param ex The root of the search
     * @return The first instance of the type in the cause hierachy tree
     *
     * @see Throwable#getCause()
     */
    public static <T extends Throwable> T findCauseByType(Class<T> type, Throwable ex) {
        if (ex.getClass() == type) {
            return (T)ex; // gotcha!
        }

        if (ex == null) {
            return null;
        }

        T tmp = null;
        /*
         * Tries to extract values provided by the 'getCause()' and 'getRootCause()'
         * methods.
         */
        for (Method method : ex.getClass().getMethods()) {
            if (method.getName().equals("getCause") || method.getName().equals("getRootCause")) {
                try {
                    // try to browse down in the tree...
                    tmp = findCauseByType(type, (Throwable)method.invoke(ex, (Object[])null));

                    if (tmp != null) {
                        return tmp; // Return the found instance!
                    }
                } catch (Exception ex1) {
                    // Could not execute the method for some reason. Omit this method!
                }
            }
        }

        return tmp;
    }
}
