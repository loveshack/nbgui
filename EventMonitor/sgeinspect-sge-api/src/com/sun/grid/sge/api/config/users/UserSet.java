/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.api.config.users;

import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.List;

public abstract class UserSet extends DataSource implements Comparable<UserSet>{
    private String name;

    public UserSet() {        
    }

    public UserSet(String name) {
        setName(name);
    }

    /**
     *   Set the name attribute.
     *
     *   @param aName  the new value for the  name attribute
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *  Get the value of the name attribute.
     *  @return the value of the name attribute
     */
    public String getName() {
        return name;
    }



    /**
     *  Determine if any name attribute is set
     *  @return <code>true</code> if the name attribute is set
     */
    public boolean isSetName() {
        return name != null;
    }


    // Attribute type ------------------------------------------------


    /**
     *   Set the type attribute.
     *
     *   @param aType  the new value for the  type attribute
     */
    public abstract void setType(int aType);

    /**
     *  Get the value of the type attribute.
     *  @return the value of the type attribute
     */
    public abstract int getType();



    /**
     *  Determine if any type attribute is set
     *  @return <code>true</code> if the type attribute is set
     */
    public abstract boolean isSetType();


    // Attribute fshare ------------------------------------------------


    /**
     *   Set the fshare attribute.
     *
     *   @param aFshare  the new value for the  fshare attribute
     */
    public abstract void setFshare(int aFshare);

    /**
     *  Get the value of the fshare attribute.
     *  @return the value of the fshare attribute
     */
    public abstract int getFshare();



    /**
     *  Determine if any fshare attribute is set
     *  @return <code>true</code> if the fshare attribute is set
     */
    public abstract boolean isSetFshare();


    // Attribute oticket ------------------------------------------------


    /**
     *   Set the oticket attribute.
     *
     *   @param aOticket  the new value for the  oticket attribute
     */
    public abstract void setOticket(int aOticket);

    /**
     *  Get the value of the oticket attribute.
     *  @return the value of the oticket attribute
     */
    public abstract int getOticket();



    /**
     *  Determine if any oticket attribute is set
     *  @return <code>true</code> if the oticket attribute is set
     */
    public abstract boolean isSetOticket();


    // Attribute jobCnt ------------------------------------------------


    /**
     *   Set the jobCnt attribute.
     *
     *   @param aJobCnt  the new value for the  jobCnt attribute
     */
    public abstract void setJobCnt(int aJobCnt);

    /**
     *  Get the value of the jobCnt attribute.
     *  @return the value of the jobCnt attribute
     */
    public abstract int getJobCnt();



    /**
     *  Determine if any jobCnt attribute is set
     *  @return <code>true</code> if the jobCnt attribute is set
     */
    public abstract boolean isSetJobCnt();


    // Attribute entries ------------------------------------------------


    /**
     *   Get a unmodifiable list of all java.lang.String attributes.
     *
     *   @return Unmodifiable list with all java.lang.String attributes
     */
    public abstract List<String>  getEntriesList();

    /**
     *  Get the number of java.lang.String attributes.
     *
     *  @return The number of java.lang.String attributes.
     */
    public abstract int getEntriesCount();

    /**
     *  Get a java.lang.String attribute at an index.
     *  @param index  the index of the java.lang.String attribute which should be
     *                removed
     *  @return the java.lang.String attribute
     */
    public abstract String getEntries(int index);


    /**
     *   Add a java.lang.String attribute.
     *   @param aentries  the new java.lang.String attribute
     */
    public abstract void addEntries(String aentries);

    /**
     *   Set a java.lang.String attribute.
     *   @param index   index of the java.lang.String attribute
     *   @param aentries  the new java.lang.String attribute
     */
    public abstract void setEntries(int index, String aentries);

    /**
     *  Remove all java.lang.String attributes.
     */
    public abstract void removeAllEntries();

    /**
     *  Remote a java.lang.String attribute at an index.
     *  @param index  the index of the java.lang.String attribute which should be
     *                removed
     *  @return the removed java.lang.String attribute or <code>null</code> if
     *          no attribute at this index is stored
     */
    public abstract String removeEntries(int index);

    /**
     *  Remote a specific java.lang.String attribute.
     *  @param aentries  the java.lang.String attribute which should be
     *                         removed
     *  @return <code>true</code> if the java.lang.String attribute has been removed
     */
    public abstract boolean removeEntries(String aentries);



    /**
     *  Determine if any entries attribute is set
     *  @return <code>true</code> if the entries attribute is set
     */
    public abstract boolean isSetEntries();



    /*
     * Dump the object
     * @return <code>String</code> the dump string
     */
     public abstract String dump();
}
