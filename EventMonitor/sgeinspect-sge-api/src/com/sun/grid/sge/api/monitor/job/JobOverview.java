/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.job;

import com.sun.tools.visualvm.core.model.Model;
import java.util.Date;

public abstract class JobOverview extends Model {

    /**
     *  Get the id of the job
     *  @return the id of the job
     */
    public abstract int getId();

    /**
     *  Get the task id of the job
     *  @return the id of the job
     */
    public abstract String getTaskId();

    /**
     *  Get the project of the job
     *  @return the project of the job
     */
    public abstract String getProject();

    /**
     *  Get the department of the job
     *  @return the department of the job
     */
    public abstract String getDepartment();

    /**
     *  Determine if the job is running
     *  @return <code>true</code> of the job is running
     */
    public abstract boolean isRunning();

    /**
     *  Get the priority of the job
     *  @return the priority of the job
     */
    public abstract double getPriority();

    /**
     *  Get the name of the job
     *  @return the name of the job
     */
    public abstract String getName();

    /**
     *  Get the owner of the job
     *  @return user of the of the job
     */
    public abstract String getUser();

    /**
     *  Get the state of the job
     *  @return state of the job
     */
    public abstract String getState();

    /**
     *  Get the queue of the job
     *  @return the queue
     */
    public abstract String getQueue();

    /**
     *  Get the queue instance name of the job
     *  @return the queue instance name
     */
    public abstract String getQinstanceName();

    /**
     *  Get the submit time of the job
     *  @return the submit time
     */
    public abstract Date getSubmitTime();

    /**
     *  Get the start time of the job
     *  @return the start time
     */
    public abstract Date getStartTime();

    /**
     *  Get the number of slots for the job
     *  @return the number of slots for the job
     */
    public abstract int getSlots();

//    /**
//     *  Get the type  of the job
//     *  @return the type if the job
//     */
//    public abstract String getJobType();
//
//    /**
//     *  Get the end time of the job
//     *  @return the end time
//     */
//    public abstract Date getEndTime();
//
//    /**
//     *  Get the exit code of the job
//     *  @return the exit code of the job
//     */
//    public abstract int getExitCode();
//
//    /**
//     *  Get the finished status of the job
//     *  @return the finsihed status of the job
//     */
//    public abstract String getFinishedStatus();
//
//    /**
//     *  Get the AR of the job
//     *  @return the AR
//     */
//    public abstract int getAR();
}
