/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.grid.sge.api.monitor.job.Job;
import com.sun.tools.visualvm.core.model.Model;
import java.util.List;

public abstract class ClusterOverview extends Model {

    public abstract String getClusterName();

    public abstract String getVersion();

    public abstract String getSgeRoot();

    public abstract String getSgeCell();

    public abstract String getSgeAdmin();

    public abstract int getQmasterPort();

    public abstract int getExecdPort();

    public abstract String getJmxMasterHost();

    public abstract int getJmxPort();

    public abstract int getRunningJobsCount();

    public abstract int getZombieJobsCount();

    public abstract int getPendingJobsCount();
    
    public abstract int getAvailability();
    
    public abstract int getOverload();
    
    public abstract int getSlotsUtilization();
    
    public abstract List<Job> getZombieJobs();
    
    public abstract List<Job> getPendingJobs();
    
    public abstract List<Job> getRunningJobs();
}
