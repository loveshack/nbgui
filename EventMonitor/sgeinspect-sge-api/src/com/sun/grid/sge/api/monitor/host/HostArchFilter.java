/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.host;

public enum HostArchFilter implements HostFilter {

    AIX51(true), DARWIN_X86(true), DARWIN_PPC(true), HP_11(true), HP11_64(true), LX24_AMD64(true), LX26_AMD64(true), LX24_IA64(true),
    LX24_SPARC(true), LX24_SPARC64(true), LX24_X86(true), LX26_X86(true), SOL_SPARC(true), SOL_SPARC64(true), SOL_X86(true), SOL_AMD64(true),
    WIN32_X86(true), IRIX65(true), UNKNOWN(true);

    private boolean selected;
    static boolean active = false;

    HostArchFilter(boolean selected) {
        setSelected(selected);
    }

    public synchronized boolean isSelected() {
        return selected;
    }

    public synchronized void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        String value = super.toString();
        return value.toLowerCase().replace("_", "-");
    }

    public static boolean isActive() {
        return active;
    }

    public static void setActive(boolean b) {
        active = b;
    }

    static public HostArchFilter hostArchFilterFromSgeArch(String arch) {
        String[] archs = {"aix51",
                          "darwin-ppc",
                          "darwin-x86",
                          "hp11",
                          "hp11-64",
                          "lx24-amd64",
                          "lx26-amd64",
                          "lx24-ia64",
                          "lx24-sparc",
                          "lx24-sparc64",
                          "lx24-x86",
                          "lx26-x86",
                          "sol-amd64",
                          "sol-sparc64",
                          "sol-x86",
                          "win32-x86",
                          "irix65" };
        HostArchFilter[] hostArchFilters = { HostArchFilter.AIX51,
                                             HostArchFilter.DARWIN_PPC,
                                             HostArchFilter.DARWIN_X86,
                                             HostArchFilter.HP_11,
                                             HostArchFilter.HP11_64,
                                             HostArchFilter.LX24_AMD64,
                                             HostArchFilter.LX26_AMD64,
                                             HostArchFilter.LX24_IA64,
                                             HostArchFilter.LX24_SPARC,
                                             HostArchFilter.LX24_SPARC64,
                                             HostArchFilter.LX24_X86,
                                             HostArchFilter.LX26_X86,
                                             HostArchFilter.SOL_AMD64,
                                             HostArchFilter.SOL_SPARC64,
                                             HostArchFilter.SOL_X86,
                                             HostArchFilter.WIN32_X86,
                                             HostArchFilter.IRIX65 };

       for (int i=0; i<archs.length; i++) {
           if (arch.equals(archs[i])) {
                return hostArchFilters[i];
           }
       }
       return UNKNOWN;
    }
   
}
