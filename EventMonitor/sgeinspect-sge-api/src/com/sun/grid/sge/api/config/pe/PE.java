/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.api.config.pe;

import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.List;


public abstract class PE extends DataSource implements Comparable<PE> {

    private String peName;

    public PE(String name) {
        this.peName = name;
        setVisible(false);
    }

    public final String getPEName() {
        return peName;
    }

    public String getDisplayName() {
        return getPEName();   // NOI18N
    }

    /**
     *   Set the name attribute.
     *
     *   @param aName  the new value for the  name attribute
     */
    public abstract void setName(java.lang.String aName);

    /**
     *  Get the value of the name attribute.
     *  @return the value of the name attribute
     */
    public abstract java.lang.String getName();

    // Attribute slots ------------------------------------------------
    /**
     *   Set the slots attribute.
     *
     *   @param aSlots  the new value for the  slots attribute
     */
    public abstract void setSlots(Integer aSlots);

    /**
     *  Get the value of the slots attribute.
     *  @return the value of the slots attribute
     */
    public abstract int getSlots();

    // Attribute userList ------------------------------------------------
    /**
     *   Get a unmodifiable list of all com.sun.grid.jgdi.configuration.UserSet attributes.
     *
     *   @return Unmodifiable list with all com.sun.grid.jgdi.configuration.UserSet attributes
     */
    public abstract List<UserSet> getUserList();

    public abstract void setUserList(List<UserSet> list);

    /**
     *   Add a com.sun.grid.jgdi.configuration.UserSet attribute.
     *   @param auser  the new com.sun.grid.jgdi.configuration.UserSet attribute
     */
    public abstract void addUser(UserSet auser);

    /**
     *  Remove all com.sun.grid.jgdi.configuration.UserSet attributes.
     */
    public abstract void removeAllUser();

    /**
     *  Remote a specific com.sun.grid.jgdi.configuration.UserSet attribute.
     *  @param auser  the com.sun.grid.jgdi.configuration.UserSet attribute which should be
     *                         removed
     *  @return <code>true</code> if the com.sun.grid.jgdi.configuration.UserSet attribute has been removed
     */
    public abstract boolean removeUser(UserSet auser);

    // Attribute xuserList ------------------------------------------------
    /**
     *   Get a unmodifiable list of all com.sun.grid.jgdi.configuration.UserSet attributes.
     *
     *   @return Unmodifiable list with all com.sun.grid.jgdi.configuration.UserSet attributes
     */
    public abstract List<UserSet> getXuserList();
    public abstract void setXuserList(List<UserSet> list);

    /**
     *   Add a com.sun.grid.jgdi.configuration.UserSet attribute.
     *   @param axuser  the new com.sun.grid.jgdi.configuration.UserSet attribute
     */
    public abstract void addXuser(UserSet axuser);

    /**
     *  Remove all com.sun.grid.jgdi.configuration.UserSet attributes.
     */
    public abstract void removeAllXuser();

    /**
     *  Remote a specific com.sun.grid.jgdi.configuration.UserSet attribute.
     *  @param axuser  the com.sun.grid.jgdi.configuration.UserSet attribute which should be
     *                         removed
     *  @return <code>true</code> if the com.sun.grid.jgdi.configuration.UserSet attribute has been removed
     */
    public abstract boolean removeXuser(UserSet axuser);

    // Attribute startProcArgs ------------------------------------------------
    /**
     *   Set the startProcArgs attribute.
     *
     *   @param aStartProcArgs  the new value for the  startProcArgs attribute
     */
    public abstract void setStartProcArgs(java.lang.String aStartProcArgs) throws PEException;

    /**
     *  Get the value of the startProcArgs attribute.
     *  @return the value of the startProcArgs attribute
     */
    public abstract java.lang.String getStartProcArgs();

    // Attribute stopProcArgs ------------------------------------------------
    /**
     *   Set the stopProcArgs attribute.
     *
     *   @param aStopProcArgs  the new value for the  stopProcArgs attribute
     */
    public abstract void setStopProcArgs(java.lang.String aStopProcArgs) throws PEException;

    /**
     *  Get the value of the stopProcArgs attribute.
     *  @return the value of the stopProcArgs attribute
     */
    public abstract java.lang.String getStopProcArgs();

    // Attribute allocationRule ------------------------------------------------
    /**
     *   Set the allocationRule attribute.
     *
     *   @param aAllocationRule  the new value for the  allocationRule attribute
     */
    public abstract void setAllocationRule(java.lang.String aAllocationRule);

    /**
     *  Get the value of the allocationRule attribute.
     *  @return the value of the allocationRule attribute
     */
    public abstract java.lang.String getAllocationRule();

    // Attribute controlSlaves ------------------------------------------------
    /**
     *   Set the controlSlaves attribute.
     *
     *   @param aControlSlaves  the new value for the  controlSlaves attribute
     */
    public abstract void setControlSlaves(Boolean aControlSlaves);

    /**
     *  Get the value of the controlSlaves attribute.
     *  @return the value of the controlSlaves attribute
     */
    public abstract Boolean getControlSlaves();

    // Attribute jobIsFirstTask ------------------------------------------------
    /**
     *   Set the jobIsFirstTask attribute.
     *
     *   @param aJobIsFirstTask  the new value for the  jobIsFirstTask attribute
     */
    public abstract void setJobIsFirstTask(Boolean aJobIsFirstTask);

    /**
     *  Get the value of the jobIsFirstTask attribute.
     *  @return the value of the jobIsFirstTask attribute
     */
    public abstract Boolean getJobIsFirstTask();

    // Attribute urgencySlots ------------------------------------------------
    /**
     *   Set the urgencySlots attribute.
     *
     *   @param aUrgencySlots  the new value for the  urgencySlots attribute
     */
    public abstract void setUrgencySlots(java.lang.String aUrgencySlots);

    /**
     *  Get the value of the urgencySlots attribute.
     *  @return the value of the urgencySlots attribute
     */
    public abstract java.lang.String getUrgencySlots();

    // Attribute accountingSummary ------------------------------------------------
    /**
     *   Set the accountingSummary attribute.
     *
     *   @param aAccountingSummary  the new value for the  accountingSummary attribute
     */
    public abstract void setAccountingSummary(Boolean aAccountingSummary);

    /**
     *  Get the value of the accountingSummary attribute.
     *  @return the value of the accountingSummary attribute
     */
    public abstract Boolean getAccountingSummary();

    public int compareTo(PE o) {
        return getName().compareTo(o.getName());
    }

     public void firePropertyChange(String property, boolean oldValue, boolean newValue) {
        this.getChangeSupport().firePropertyChange(property, oldValue, newValue);
    }

    public void firePropertyChange(String property, Object oldValue, Object newValue) {
        this.getChangeSupport().firePropertyChange(property, oldValue, newValue);
    }

}
