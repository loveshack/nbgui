/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.grid.sge.api.config.pe.PE;
import com.sun.grid.sge.api.config.pe.PEException;
import com.sun.grid.sge.api.config.users.UserSet;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.shared.api.version.VersionableDataSource;
import com.sun.tools.visualvm.core.datasource.Storage;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public abstract class Cluster extends VersionableDataSource {

    public static final String PROPERTY_CLUSTER_CONNECTED = "clusterConnected"; // NOI18N
    public static final String PROPERTY_CLUSTER_CONFIG = "config"; // NOI18N
    /**
     * Instance representing individual cluster.
     */
    public static final Cluster CURRENT_CLUSTER = null;
    /**
     * PID of the qmaster for this cluster is unknown at beginning.
     */
    protected ClusterConfiguration config;
    private List<ChangeListener> listeners = Collections.synchronizedList(new LinkedList<ChangeListener>());
    private Storage givenStorage;

    /**
     * Creates new instance of cluster.
     *
     * @param config configuration of cluster holding the connection details
     * @param givenStorage instance of storage to store the cluster configuration details
     */
    public Cluster(ClusterConfiguration config, Storage givenStorage) {
        this.config = config;
        this.givenStorage = givenStorage;
    }

    /**
     *
     * @return - the List<Queue> configured on this Cluster
     */
    abstract public List<Queue> getQueueList();

    /**
     *
     * @return - the List<Host> configured on this Cluster
     */
    abstract public List<Host> getHostList();

    /**
     *
     * @return - the List<PE> configured on this Cluster
     */
    abstract public List<PE> getPEList();

    abstract public boolean isPENameExists(String name);

    abstract public void createPE(Object pe) throws PEException;
    abstract public void deletePE(final String name) throws PEException;
    abstract public Object getUserSet(String name);

    /**
     *
     * @return - the List<UserSet> configured on this Cluster
     */
    abstract public List<UserSet> getUserList();

    abstract public void connect();

    abstract public void disconnect();

    abstract public boolean isConnected();

    /**
     * Returns unique identificator of the cluster.
     *
     * @return unique identificator of the acluster.
     */
    public final String getClusterName() {
        return config.getClusterName();
    }

    public String getDisplayName() {
        return config.getDisplayName();   // NOI18N
    }

    @Override
    protected Storage createStorage() {
        return givenStorage;
    }

    @Override
    public boolean supportsUserRemove() {
        return true;
    }

    @Override
    protected void remove() {
        disconnect();
        super.remove();
    }

    @Override
    public String toString() {
        return getDisplayName();
    }

    public ClusterConfiguration getConfiguration() {
        return config;
    }

    public void setConfiguration(ClusterConfiguration config) {
        ClusterConfiguration oldConfig = this.config;
        this.config = config;
        firePropertyChange(PROPERTY_CLUSTER_CONFIG, oldConfig, config);
    }

    protected void fireChangeEvent() {
        synchronized (listeners) { // see Collections.sychronizedList()
            for (ChangeListener l : listeners) {
                l.stateChanged(new ChangeEvent(this));
            }
        }
    }

    public void addChangeListener(ChangeListener cl) {
        listeners.add(cl);
    }

    public void removeChangeListener(ChangeListener cl) {
        listeners.remove(cl);
    }

    public void firePropertyChange(String property, boolean oldValue, boolean newValue) {
        this.getChangeSupport().firePropertyChange(property, oldValue, newValue);
    }

    public void firePropertyChange(String property, Object oldValue, Object newValue) {
        this.getChangeSupport().firePropertyChange(property, oldValue, newValue);
    }
}
