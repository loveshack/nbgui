/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.sge.api.monitor.host;

import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.EnumSet;
import java.util.List;

public abstract class Host extends DataSource implements Comparable<Host> {

    private final String hostName;
    private final EnumSet<HostTypeFilter> types = EnumSet.noneOf(HostTypeFilter.class);
    private HostArchFilter arch;
    private String displayName;

    /**
     * Creates new instance of Host defined by a name and type.
     *
     * @param host Host belonging to the cluster.
     * @param type of the host (submit, execd, master).
     */
    public Host(String name, EnumSet<HostTypeFilter> types, String arch) {
        this.hostName = name;
        this.displayName = name;
        this.types.addAll(types);
        this.arch = HostArchFilter.hostArchFilterFromSgeArch(arch);
        setVisible(false);
    }

    /**
     * Creates new instance of Host defined by a name and type.
     *
     * @param host Host belonging to the cluster.
     * @param type of the host (submit, execd, master).
     */
    public Host(String name, EnumSet<HostTypeFilter> types, HostArchFilter arch) {
        this.hostName = name;
        this.displayName = name;
        this.types.addAll(types);
        this.arch = arch;
        setVisible(false);
    }

    /**
     * Returns the host name
     *
     * @return host name.
     */
    public final String getHostName() {
        return hostName;
    }

    //If either of the fitler criteria are satisfied the host is excluded.
    //Use case: Host is submit host and has architecture sol-sparc
    //If user deselects submit host in filter, this host will be excluded, 
    //the same if submit host are included, but sol_sparc arch is excluded. 
    public boolean isShown() {
        boolean show = true;
        if (HostArchFilter.isActive()) {
            show = arch.isSelected();
        }
        
        if (HostTypeFilter.isActive()) {
            for (HostTypeFilter f : types) {
                if(!f.isSelected()){
                    show = false;
                }
               
            }
        } 
        return show;
    }

    public int compareTo(Host o) {
        return getHostName().compareTo(o.getHostName());
    }

    /**
     * Returns the type of this host (submit, execd, master).
     *
     * @return Returns the type of this host (submit, execd, master).
     */
    public String getType() {
        StringBuffer buffer = new StringBuffer();

        for (HostTypeFilter type : types) {
            buffer.append(type.toString());
        }
        return buffer.toString();
    }

    public HostArchFilter getArch() {
        return arch;
    }

    public String getDisplayName() {
        // displayName is either hostName or the saved displayName from checkRemove
        if (getOwner() != null) {
           displayName = getHostName() + " (" + getOwner() + ")";   // NOI18N
        }
        return displayName;
    }

    @Override
    public boolean checkRemove(DataSource removeRoot) {
        displayName = getHostName() + " (" + getOwner() + ")";   // NOI18N
        return super.checkRemove(removeRoot);
    }

    /**
     *  Get the list of queues which are available on the host
     *
     *  <p><b>Note:</b> The queue list is only set if the
     *  {@link QHostOptions#includeQueue} flag is set.</p>
     *
     *  @return list of queues (instances of {@link QueueInfo})
     *  @see com.sun.grid.jgdi.JGDI#execQHost
     */
    public abstract List<Queue> getQueueList();
}
