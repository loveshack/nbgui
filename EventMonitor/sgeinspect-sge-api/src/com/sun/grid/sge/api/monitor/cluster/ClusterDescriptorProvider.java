/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.grid.sge.api.monitor.host.Host;
import com.sun.grid.sge.api.monitor.host.HostDescriptor;
import com.sun.grid.sge.api.monitor.queue.Queue;
import com.sun.grid.sge.api.monitor.queue.QueueDescriptor;
import com.sun.grid.sge.api.monitor.queue.QueueInstance;
import com.sun.grid.sge.api.monitor.queue.QueueInstanceDescriptor;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;

public class ClusterDescriptorProvider extends AbstractModelProvider<DataSourceDescriptor, DataSource> {

    @Override
    public DataSourceDescriptor createModelFor(DataSource ds) {
        if (ds instanceof Cluster) {
            return new ClusterDescriptor((Cluster) ds);
        } else if (ds instanceof Host) {
            return new HostDescriptor((Host) ds);
        } else if (ds instanceof Queue) {
            return new QueueDescriptor((Queue) ds);
        } else if (ds instanceof QueueInstance) {
            return new QueueInstanceDescriptor((QueueInstance) ds);
        }
        return null;
    }
}
