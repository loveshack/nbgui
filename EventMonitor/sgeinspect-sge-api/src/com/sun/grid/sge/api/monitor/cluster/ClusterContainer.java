/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptorFactory;
import com.sun.tools.visualvm.core.model.AbstractModelProvider;
import java.awt.Image;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

/**
 * Toplevel node 'SGE Clusters' in the Applications window.
 *
 */
public class ClusterContainer extends DataSource {

    private static ClusterContainer sharedInstance;

    //retunrs sigleton instance of ClusterContainer
    public static synchronized ClusterContainer sharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new ClusterContainer();
        }
        return sharedInstance;
    }

    private ClusterContainer() {
        DataSourceDescriptorFactory.getDefault().registerProvider(new ClustersContainerDescriptorProvider());
    }

    private static class ClustersContainerDescriptorProvider extends AbstractModelProvider<DataSourceDescriptor, DataSource> {

        @Override
        public DataSourceDescriptor createModelFor(DataSource ds) {
            if (ClusterContainer.sharedInstance().equals(ds)) {
                return new ClustersContainerDescriptor();
            }
            return null;
        }

        private static class ClustersContainerDescriptor extends DataSourceDescriptor<ClusterContainer> {

            private static final Image CLUSTER_NODE_ICON = ImageUtilities.loadImage(
                    "com/sun/grid/sge/ui/resources/cluster_container.png", true); // NOI18N

            ClustersContainerDescriptor() {
                super(ClusterContainer.sharedInstance(),
                        NbBundle.getMessage(ClusterContainer.class, "LBL_SGE_Clusters"),
                        null,
                        CLUSTER_NODE_ICON,
                        10,
                        EXPAND_ON_EACH_NEW_CHILD);
            }
        }
    }
}
