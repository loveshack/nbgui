/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.queue;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.tools.visualvm.core.datasource.DataSource;

public abstract class QueueInstance extends DataSource implements Comparable<QueueInstance> {

    private final String queueInstanceName;
    private final String type;
    private final Cluster cluster;

    public QueueInstance(Cluster cluster, String name, String type) {
        this.cluster = cluster;
        this.queueInstanceName = name;
        this.type = type;
    }

    public final String getQueueName() {
        return queueInstanceName;
    }

    public final Cluster getCluster() {
        return cluster;
    }

    public String getType() {
        return type;
    }

    //used for the display name of navigator nodes
    public String getNameWithType() {
        return getQueueName() + " (" + getType()+ ")";    // NOI18N
    }


    /**
     * Displays the name in the detailed view tab
     */
    public String getDisplayName() {
        String displayName = getQueueName();
        try {
            displayName = getQueueName() + " (" + cluster.getDisplayName() + ")";    // NOI18N
        } catch (NullPointerException npe) {
            // ignore
        }
        return displayName;
    }

    public int compareTo(QueueInstance o) {
        return getQueueName().compareTo(o.getQueueName());
    }
}
