/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.config.pe;

public final class PEWizardSettings {
    //Properties

    public static final String PROP_NAME = "pe_name";
    public static final String PROP_LOCATION = "pe_location";
    public static final String PROP_USER_LIST = "userList";
    public static final String PROP_XUSER_LIST = "xuserList";
    public static final String PROP_SLOTS = "slots";
    public static final String PROP_URGENCY = "urgency";
    public static final String PROP_ALLOCATION = "allocation";
    public static final String PROP_START_CMD = "startCommand";
    public static final String PROP_START_ARGS = "startArg";
    public static final String PROP_STOP_CMD = "stopCommand";
    public static final String PROP_STOP_ARGS = "stopArg";
    public static final String PROP_SLAVES = "slaves";
    public static final String PROP_IS_TASK = "first_task";
    public static final String PROP_ACCOUNTING = "accounting";
    public static final String PROP_START_AREA = "startArea";
    public static final String PROP_STOP_AREA = "stopArea";

    public static final String SLOTS_DEFAULT = "999";
    //Allocation rules
    public static final String PE_SLOTS = "$pe_slots";
    public static final String FILL_UP = "$fill_up";
    public static final String ROUND_ROBIN = "$round_robin";
    public static final String ONE = "1";
    public static final String ALLOCATION_DEFAULT = "$pe_slots";
    public static final String ALOCATION_ELECTIVE = "<number of processors per host>";
    //Urgency Slots
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String AVG = "avg";
    public static final String URGENCY_DEFAULT = "min";
    public static final String URGENCY_ELECTIVE = "<enter integer value>";
    //Command Arguments
    public static final String HOST_FILE = "$pe_hostfile";
    public static final String HOST = "$host";
    public static final String JOB_OWNER = "$job_owner";
    public static final String JOB_ID = "$job_id";
    public static final String JOB_NAME = "$job_name";
    public static final String PE = "$pe";
    public static final String ARG_PE_SLOTS = "$pe_slots";
    public static final String PROCESSORS = "$processors";
    public static final String QUEUE = "$queue";
    public static final String ARGS_DEFAULT = "<none>";
    public static final String START_CMD_DEFAULT = "NONE";
    public static final String STOP_CMD_DEFAULT = "NONE";
    public static final String CMD_NONE = "NONE";

    public static String[] getAllocationPresets() {
        return new String[]{ALOCATION_ELECTIVE, PE_SLOTS, FILL_UP, ROUND_ROBIN, ONE};
    }

    public static String[] getUrgencyPresets() {
        return new String[]{URGENCY_ELECTIVE, MIN, MAX, AVG};
    }

    public static String[] getArgumentsPresets() {
        return new String[]{ARGS_DEFAULT, HOST, JOB_OWNER, JOB_ID, JOB_NAME, PE, ARG_PE_SLOTS,
                  PROCESSORS, QUEUE};
    }
}

