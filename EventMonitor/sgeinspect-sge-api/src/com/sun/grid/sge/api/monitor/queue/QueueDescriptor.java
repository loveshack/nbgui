/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.queue;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class QueueDescriptor extends DataSourceDescriptor<Queue> implements PropertyChangeListener {

    private static final Image ICON = ImageUtilities.loadImage(
            "com/sun/grid/sge/ui/resources/queue.png", true); // NOI18N

    public QueueDescriptor(Queue queue) {
        super(queue,
                queue.getDisplayName(),
                null,
                ICON,
                EXPAND_NEVER,
                POSITION_AT_THE_END);
        queue.getOwner().addPropertyChangeListener(WeakListeners.propertyChange(this, queue.getOwner()));
    }

    public boolean supportsRename() {
        return true;
    }

    @Override
    public Image getIcon() {
        return ICON;
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (getDataSource() != null) {
            String displayName = getDataSource().getDisplayName();
            if (evt.getPropertyName().equals(Cluster.PROPERTY_CLUSTER_CONNECTED)) {
                Cluster cluster = null;
                try {
                    cluster = (Cluster) getDataSource().getOwner();
                } catch (NullPointerException npe) {
                    //ignore
                }
                if (cluster != null && cluster.isConnected()) {
                    //setIcon(ICON);
                    setName(displayName);
                } else {
                    //setIcon(DISCONNECTED_ICON);
                    setName(NbBundle.getMessage(QueueDescriptor.class, "LBL_DISCONNECTED_TITLE", displayName));
                }
            }

            // setName() writes the datasources properties file which has the
            // side effect that a removed data source rewrites the properties file back
            // since the overwritten remove() method calls disconnect() which calls
            // firePropertyChange(Cluster.PROPERTY_CLUSTER_CONNECTED) to change the descriptors
            // icon that is also used for any open cluster monitor panel
            if (evt.getPropertyName().equals(Cluster.PROPERTY_CLUSTER_CONFIG)) {
                setName(displayName);
            }
        }
    }
}
