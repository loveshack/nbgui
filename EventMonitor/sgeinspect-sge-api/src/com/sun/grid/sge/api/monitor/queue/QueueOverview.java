/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.queue;

import com.sun.tools.visualvm.core.model.Model;

// a data model wrapper for com.sun.grid.jgdi.monitoring.ClusterQueueSummary
public abstract class QueueOverview extends Model {

    /**
     *  Get the name of the cluster queue
     */
    public abstract String getName();

    /**
     *  Get the load of the cluster queue. If the <code>isLoadSet</code> method
     *  returns <code>false</code>, the result of this method is undefined.
     *
     *  @return an average of the normalized load average  of  all queue hosts.  In
     *          order to reflect each hosts different significance the number of
     *          configured slots is used as a weighting  factor  when  determining
     *          cluster queue load. Please note that only hosts with a np_load_value are
     *          considered for this value.
     */
    public abstract double getLoad();

    /**
     *  Has the load value been set
     *
     *  @return  <code>true of the load value is set</code>
     */
    public abstract boolean isLoadSet();

    /**
     *  Get the number of currently reserved slots
     *
     *  @return number of currently reserved slots
     */
    public abstract int getReservedSlots();

    /**
     *  Get the number of currently used slots
     *
     *  @return number of currently used slots
     */
    public abstract int getUsedSlots();

    /**
     *  Get the total number of slots of this cluster queue
     *
     *  @return the total number of slots
     */
    public abstract int getTotalSlots();

    /**
     *   Get the number of currently available slots.
     *
     *   @return the number of currently available slots
     */
    public abstract int getAvailableSlots();

    /**
     *  <p>Get the number of temporary disabled queue instances.</p>
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return number of temporary disabled queue instances
     *  @see BasicQueueOptions#showAdditionalAttributes
     */
    public abstract int getTempDisabled();

    /**
     *  <p>Get the number of manually disabled queue instances.</p>
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return number of manually disabled queue instances
     *  @see BasicQueueOptions#showAdditionalAttributes
     */
    public abstract int getManualIntervention();

    /**
     *  Get the number of manually suspended queue instances.
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return number of manually suspended queue instances
     */
    public abstract int getSuspendManual();

    /**
     *  Get the suspend threshold of the cluster queue
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return the suspend threshold of the cluster queue
     */
    public abstract int getSuspendThreshold();

    /**
     *  Get the number of queue instances which has been suspended on
     *  a subordinate queue.
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return The number of queue instances which has been suspended on
     *          a subordinate queue.
     */
    public abstract int getSuspendOnSubordinate();

    /**
     *  Get the number of queue instances which has been suspended by a
     *  calendar.
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *  @return number of queue instances which has been suspended by a
     *          calendar.
     */
    public abstract int getSuspendByCalendar();

    /**
     *   Get the number of queue instances which are in an unknown state.
     *
     *  <p><b>Note:</b> This value is only set if the <code>showAdditionalAttributes</code>
     *  is set in the options for the {@link com.sun.grid.jgdi.JGDIBase#getClusterQueueSummary
     *  cluster queue summary algorithm}.</p>
     *
     *   @return number of queue instances which are in an unknown state.
     */
    public abstract int getUnknown();

    /**
     *  Get the number of queues instances which has an load alarm.
     *
     *  @return number of queues instances which has an load alarm.
     */
    public abstract int getLoadAlarm();

    /**
     *  Get the number of queue instances which has been manually disabled
     *
     *  @return number of queue instances which has been manually disabled
     */
    public abstract int getDisabledManual();

    /**
     *  Get the number of queue instances which has been by a calendar
     *
     *  @return number of queue instances which has been by a calendar
     */
    public abstract int getDisabledByCalendar();

    /**
     *  Get the number of queue instances which are in the ambiguous state
     *
     *  @return number of queue instances which are in the ambiguous state
     */
    public abstract int getAmbiguous();

    /**
     *  Get the number of queue instances which are orqhaned.
     *
     *  @return number of queue instances which are orqhaned
     */
    public abstract int getOrphaned();

    /**
     *  Get the number of queue instances which are in error state
     *
     *  @return number of queue instances which are in error state
     */
    public abstract int getError();  
}
