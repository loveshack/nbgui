/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.queue;

import com.sun.tools.visualvm.core.datasource.DataSource;
import java.util.List;

public abstract class Queue extends DataSource implements Comparable<Queue> {

    private String queueName;
    private String displayName;

    public Queue(String name) {
        this.queueName = name;
        this.displayName = name;
        setVisible(false);
    }

    public final String getQueueName() {
        return queueName;
    }

    public String getDisplayName() {
        // displayName is either queueName or the saved displayName from checkRemove
        if (getOwner() != null) {
            displayName = getQueueName() + " (" +getOwner() + ")";   // NOI18N
        }
        return displayName;
    }

    public int compareTo(Queue o) {
        return getQueueName().compareTo(o.getQueueName());
    }

   @Override
    public boolean checkRemove(DataSource removeRoot) {
        displayName = getQueueName() + " (" + getOwner() + ")";   // NOI18N
        return super.checkRemove(removeRoot);
    }

     /**
     * Get the list of Queue Instances configured on this Cluster Queue
     * @return Queue Instances on this Cluster Queue
     */
    public abstract List<QueueInstance> getQueueInstances();
}
