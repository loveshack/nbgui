/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.host;

public enum HostTypeFilter implements HostFilter {

    EXECD(true),
    SUBMIT(true),
    ADMIN(true),
    MASTER(true);

//    DEFAULT;
    static boolean active = false;
    private boolean selected;

    HostTypeFilter(boolean selected) {
        setSelected(selected);
    }

    public synchronized boolean isSelected() {
        return selected;
    }

    public synchronized void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String toString() {
        return super.toString().toLowerCase();
    }

    public static boolean isActive() {
        return active;
    }

    public static void setActive(boolean b) {
        active = b;
    }
}
