/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.api.monitor.cluster;

import com.sun.tools.visualvm.core.datasource.Storage;
import com.sun.tools.visualvm.core.datasource.descriptor.DataSourceDescriptor;

public abstract class ClusterConfiguration {

    public static final String SNAPSHOT_VERSION = "snapshot_version";  // NOI18N
    public static final String SNAPSHOT_VERSION_DIVIDER = ".";
    public static final String CURRENT_SNAPSHOT_VERSION_MAJOR = "1";   // NOI18N
    public static final String CURRENT_SNAPSHOT_VERSION_MINOR = "0";   // NOI18N
    public static final String CURRENT_SNAPSHOT_VERSION = CURRENT_SNAPSHOT_VERSION_MAJOR + SNAPSHOT_VERSION_DIVIDER + CURRENT_SNAPSHOT_VERSION_MINOR;
    public static final String PROPERTY_CLUSTERNAME = "prop_clustername"; // NOI18N
    public static final String PROPERTY_CLUSTERVERSION = "prop_clusterversion"; // NOI18N
    public static final String PROPERTY_DISPLAYNAME = "prop_displayname"; // NOI18N
    public static final String PROPERTY_JMX_HOST = "prop_jmx_host"; // NOI18N
    public static final String PROPERTY_JMX_PORT = "prop_jmx_port"; // NOI18N
    public static final String PROPERTY_USERNAME = "prop_username"; // NOI18N
    public static final String PROPERTY_IS_SSL = "prop_is_ssl";
    // public static final String PROPERTY_USERPASSWORD = "prop_userpassword"; // NOI18N
    public static final String PROPERTY_CA_PATH = "prop_ca_path"; // NOI18N
    public static final String PROPERTY_KEYSTORE_PATH = "prop_keystore_path"; // NOI18N
    public static final String PROPERTY_KEYSTORE_PASSWORD = "prop_keystore_password"; // NOI18N
    private String clusterName;
    private String clusterVersion;
    private String displayName;

    public String getClusterName() {
        return clusterName;
    }

    public String getClusterVersion() {
        return clusterVersion;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public void setClusterVersion(String clusterVersion) {
        this.clusterVersion = clusterVersion;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public abstract String getUserName();

    public abstract void setUserName(String userName);

    public abstract String getUserPassword();

    public abstract void setUserPassword(String userPassword);

    public abstract String getKeystorePassword();

    public abstract void setKeystorePassword(String keystorePassword);

    public abstract String getKeystorePath();

    public abstract void setKeystorePath(String keystorePath);

    public abstract String getCaPath();

    public abstract void setCaPath(String caPath);

    public abstract String getJmxHost();

    public abstract void setJmxHost(String jmxHost);

    public abstract int getJmxPort();

    public abstract void setJmxPort(String jmxPort);

    public abstract void setJmxPort(int jmxPort);

    public abstract boolean getSSL();

    public abstract void setSSL(boolean isSSL);

    public void store(Storage storage) {
        final String caPath = getCaPath() != null ? getCaPath() : "";
        final String keystorePath = getKeystorePath() != null ? getKeystorePath() : "";

        String[] propertyValues = new String[]{
            CURRENT_SNAPSHOT_VERSION,
            getClusterName(),
            getClusterVersion(),
            getDisplayName(),
            getJmxHost(),
            Integer.toString(getJmxPort()),
            getUserName(),
            String.valueOf(getSSL()),
            // Utils.encodePassword(userPassword),
            caPath,
            keystorePath,
            // Utils.encodePassword(keystorePassword),
            getClusterName()
        };

        final String[] propertyKeys = new String[]{
            SNAPSHOT_VERSION,
            PROPERTY_CLUSTERNAME,
            PROPERTY_CLUSTERVERSION,
            PROPERTY_DISPLAYNAME,
            PROPERTY_JMX_HOST,
            PROPERTY_JMX_PORT,
            PROPERTY_USERNAME,
            PROPERTY_IS_SSL,
            // PROPERTY_USERPASSWORD,
            PROPERTY_CA_PATH,
            PROPERTY_KEYSTORE_PATH,
            // PROPERTY_KEYSTORE_PASSWORD,
            DataSourceDescriptor.PROPERTY_NAME
        };
        storage.setCustomProperties(propertyKeys, propertyValues);

    }

    public ClusterConfiguration load(Storage storage) {

        setJmxHost(storage.getCustomProperty(PROPERTY_JMX_HOST));
        setJmxPort(storage.getCustomProperty(PROPERTY_JMX_PORT));
        setClusterName(storage.getCustomProperty(PROPERTY_CLUSTERNAME));
        setClusterVersion(storage.getCustomProperty(PROPERTY_CLUSTERVERSION));
        setDisplayName(storage.getCustomProperty(PROPERTY_DISPLAYNAME));
        setUserName(storage.getCustomProperty(PROPERTY_USERNAME));
        setUserPassword(null);
        setSSL(Boolean.parseBoolean(storage.getCustomProperty(PROPERTY_IS_SSL)));

//        String encryptedUserPassword = storage.getCustomProperty(PROPERTY_USERPASSWORD);
//        if (encryptedUserPassword != null) {
//            setUserPassword(Utils.decodePassword(encryptedUserPassword));
//        } else {
//            setUserPassword(null);
//        }
        if (getSSL()) {
            setCaPath(storage.getCustomProperty(PROPERTY_CA_PATH));
            setKeystorePath(storage.getCustomProperty(PROPERTY_KEYSTORE_PATH));
            setKeystorePassword(null);
//            String encryptedKeystorePassword = storage.getCustomProperty(PROPERTY_KEYSTORE_PASSWORD);
//            if (encryptedKeystorePassword != null) {
//                setKeystorePassword(Utils.decodePassword(encryptedKeystorePassword));
//            } else {
//                setKeystorePassword(null);
//            }
        } else {
            setCaPath(null);
            setKeystorePath(null);
            setKeystorePassword(null);
        }
        return this;
    }
}
