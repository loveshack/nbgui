/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.shared.api.layer;

import com.sun.grid.shared.api.version.VersionableDataSource;
import com.sun.tools.visualvm.core.datasource.DataSource;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JSeparator;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.Repository;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.actions.CookieAction;

/**
 * Class to construct file system structure from custom layer configuration.
 * Used to handle the same action implemented by different module versions.
 *
 * Usage:
 * The root of the custom configuration is defined by {@link CustomLayerBuilder#CUSTOM_MENU_ROOT}.
 * Every folder and file object has to have a unique name in the scope of the parent
 * folder. Every file object has to define a key attribute. This will be the groupping
 * id which is used to group the coherent action implementations. Every file object
 * has to define a version attribute. This identifies the version the action supports.
 * 
 * e.g.:
 * layer.xml definition in version 62u3
 * ...
 * &ltfolder name="Custom"&gt
 *  &ltfolder name="Menu"&gt
 *   &ltfolder name="View"&gt
 *    &ltfile name="com-sun-grid-sge-ui-sixtwouthree-monitor-view-node-actions-ClusterOpenAction.instance"&gt
 *     &ltattr name="instanceCreate" methodvalue="com.sun.grid.sge.ui.sixtwouthree.monitor.view.node.actions.ClusterOpenAction.createInstance"/&gt
 *     &ltattr name="key" stringvalue="ClusterOpenAction"/&gt
 *     &ltattr name="version" methodvalue="com.sun.grid.sge.sixtwouthree.Version.getVersions"/&gt
 *     &ltattr name="position" intvalue="1"/&gt
 *    &lt/file&gt
 *   &lt/folder&gt
 *  &lt/folder&gt
 * &lt/folder&gt
 * ...
 *
 * layer.xml definition in version 62u5
 * ...
 * &ltfolder name="Custom"&gt
 *  &ltfolder name="Menu"&gt
 *   &ltfolder name="View"&gt
 *    &ltfile name="com-sun-grid-sge-ui-sixtwoufive-monitor-view-node-actions-ClusterOpenAction.instance"&gt
 *     &ltattr name="instanceCreate" methodvalue="com.sun.grid.sge.ui.sixtwoufive.monitor.view.node.actions.ClusterOpenAction.createInstance"/&gt
 *     &ltattr name="key" stringvalue="ClusterOpenAction"/&gt
 *     &ltattr name="version" methodvalue="com.sun.grid.sge.sixtwoufive.Version.getVersions"/&gt
 *     &ltattr name="position" intvalue="1"/&gt
 *    &lt/file&gt
 *   &lt/folder&gt
 *  &lt/folder&gt
 * &lt/folder&gt
 * ...
 *
 * After the {@link CustomLayerBuilder} run the result will be as follows:
 * &ltfolder name="Menu"&gt
 *   &ltfolder name="View"&gt
 *    &ltfile name="ClusterOpenAction.instance"&gt
 *    &ltattr name="instanceCreate" value="com.sun.grid.shared.api.layer.CustomLayerBuilder@DynamicDataSourceAction"/&gt
 *    &ltattr name="position" intvalue="1"/&gt
 *   &lt/file&gt
 *  &lt/folder&gt
 * &lt/folder&gt
 *
 * The merged action class inherits the latest implementation's attributes.
 * 
 * The action implementations which supposed to be handeled by this facility has 
 * to implement {@link OpenSingleDataSourceAction} and {@link OpenCookieAction}
 * instead {@link SingleDataSourceAction} and {@link CookieAction} respectedly.
 *
 * Shortcuts have to have the shortcut key definied in the {@link KEY_ATTR} attribute.
 *
 * @see CustomLayerBuilder#CUSTOM_MENU_ROOT
 * @see CustomLayerBuilder#KEY_ATTR
 * @see CustomLayerBuilder#VERSION_ATTR
 * @see AbstractAction
 */
public final class CustomLayerBuilder {

    /*The root supFolder of the custom configuration*/
    private static final String CUSTOM_MENU_ROOT = "Custom"; // NOI18N
    /*Key attribute used to group related items*/
    private static final String KEY_ATTR = "key"; // NOI18N
    /*Version attribute used to identify the supported version(s) of the item*/
    private static final String VERSION_ATTR = "version"; // NOI18N

    /*Singleton instance*/
    private static final CustomLayerBuilder INSTANCE = new CustomLayerBuilder();
    private static final Logger logger = Logger.getLogger(CustomLayerBuilder.class.getName(), "com.sun.grid.shared.api.Bundle"); // NOI18N

    /*Stores the FileObjects created during the build process*/
    private List<FileObject> createdObjects;

    /**
     * Private constructor
     */
    private CustomLayerBuilder() {
        createdObjects = new ArrayList<FileObject>();
    }

    /**
     * Returns the singleton instance of the class
     * @return The instance
     */
    public static CustomLayerBuilder getInstance() {
        return INSTANCE;
    }

    /**
     * Adds the custom configuration defined under {@link CUSTOM_MENU_ROOT} root supFolder
     * to the default file system.
     */
    @SuppressWarnings("unchecked")
    public void build() {
        FileSystem fileSystem = Repository.getDefault().getDefaultFileSystem();
        Map<String, List<FileObject>> folderFileMap = new HashMap<String, List<FileObject>>();
        FileObject customFolder = fileSystem.findResource(CUSTOM_MENU_ROOT);

        if (customFolder == null) {
            logger.log(Level.WARNING, "There is no folder with name '" + CUSTOM_MENU_ROOT + "' on the default file system!"); // NOI18N
            return;
        }
        
        try {
            // Group files/folders under CUSTOM_MENU_ROOT by the value of KEY_ATTR attribute
            groupFileObjects(folderFileMap, customFolder, KEY_ATTR);
        } catch (LayerFormatExcpetion ex) {
            logger.log(Level.SEVERE, "Building failure!", new LayerFormatExcpetion(customFolder, "Failed to read layer file!", ex));
            return;
        }

        Map<String, Object> instanceMap = null;
        Object instance = null;
        FileObjectVersionComparator comparator = new FileObjectVersionComparator();
        for (String key : folderFileMap.keySet()) {
            List<FileObject> dataObjects = folderFileMap.get(key);

            Collections.sort(dataObjects, comparator);

            FileObject refFileObject = null;
            try {
                /**
                 * Check whether the group contains only 'instance' or 'shadow'
                 * file objects. If it's mixed, the final file object copy will
                 * have 'instance' extension and every 'shadow' object will be
                 * resolved by searching for 'instance' reference defined in it's
                 * 'originalFile' attribute.
                 * If it's pure 'shadow' group, the final file object copy will
                 * have 'shadow' extension.
                 */
                boolean hasInstanceExt = false;
                for (FileObject fileObject : dataObjects) {
                    if (fileObject.hasExt("instance")) { // NOI18N
                        hasInstanceExt = true;
                    }

                    refFileObject = fileObject;
                }

                // Create map only if it's not a 'shadow' group
                if (hasInstanceExt) {
                    instanceMap = createVersionActionMap(dataObjects);
                } else {
                    instanceMap = null;
                }

                // Create the final file object instance
                if (instanceMap != null && !instanceMap.isEmpty()) {
                    for (Object value : instanceMap.values()) {
                        if (value instanceof OpenCookieAction) {
                            instance = new DynamicContextAwareAction(((Map) instanceMap));
                        } else if (value instanceof JSeparator) {
                            instance = new JSeparator();
                        } else {
                            instance = new DynamicDataSourceAction<VersionableDataSource>((Map) instanceMap, VersionableDataSource.class);
                        }

                        // Check only the first value
                        break;
                    }
                }

                // Create the file object
                createFileObject(instance, refFileObject, hasInstanceExt ? "instance" : "shadow"); // NOI18N

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Building failure!", new LayerFormatExcpetion(customFolder, "Failed to build layer file for '" + key + "' item(s)!", ex));
            } 
        } // for

        if (logger.isLoggable(Level.FINEST)) {
            printFileObjectTree(fileSystem.getRoot());
        }
    }

    public void destroy() {
        // Sort the created file objects so the data objects come first and
        // the folders last.
        Collections.sort(createdObjects, new FileObjectTypeComparator());

        for (Iterator<FileObject> it = createdObjects.iterator(); it.hasNext();) {
            FileObject fileObject = it.next();
            if (fileObject != null && !fileObject.isLocked()) {
                try {
                    // Delete only data or those file objects which does not contain
                    // children. By this fix we aviod to delete those folders which
                    // contain elements are not created by this builder.
                    // e.g.: elements have been moved to 'Custom' toolbar folder
                    // by the toolbar customizer.
                    if (fileObject.getChildren().length == 0) {
                        fileObject.delete();
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "Destroying failure!", new LayerFormatExcpetion(fileObject, "Failed to delete file object!", ex));
                }
            }
        }
    }

    /**
     * For debugging purpose. Prints the tree of {@link FileObject}s
     * @param root The root
     */
    private static void printFileObjectTree(FileObject root) {
        for (FileObject child : root.getChildren()) {
            if (child.isFolder()) {
                System.out.println("Folder: " + child); // NOI18N
                printFileObjectTree(child);
            } else {
                System.out.println(" Data: " + child); // NOI18N
            }
        }
    }

    /**
     * Loads the version atribute's value(s) from the give file object. They has to
     * be separated by wildcard characters.
     *
     * @see VERSION_ATTR
     * @param fileObject The file object whose version value we are interested in
     * @return Array of found version strings or null if there is none or can not be parsed.
     */
    private static String[] loadVersions(FileObject fileObject) {
        if (fileObject == null) {
            return null;
        }

        Object versionsObj = fileObject.getAttribute(VERSION_ATTR);


        if (versionsObj == null) {
//            // This is won't work as during the merge of different layer files
//            // the attributes with same name collides and gets overriden
//            if (fileObject.getName().equals(CUSTOM_MENU_ROOT)) {
//                return null;
//            } else {
//                return loadVersions(fileObject.getParent());
//            }
            return null;
        }

        String versionsStr = "";

        if (versionsObj instanceof String) {
            versionsStr = (String) versionsObj;
        } else if (versionsObj instanceof String[]) {
            return (String[]) versionsObj;
        } else {
            versionsStr = versionsObj.toString();
        }

        String[] result = versionsStr.trim().split(" ");

        return result;
    }

    /**
     * Constructs a version-instance instance map from the given {@link FileObject}s
     * @param fileObjects List of file objects which are versioned and contains
     * reference to instance classes.
     * @return The constructed map.
     * 
     * @throws FileStateInvalidException
     * @throws DataObjectNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static Map<String, Object> createVersionActionMap(List<FileObject> fileObjects)
            throws FileStateInvalidException, IOException, LayerFormatExcpetion {
        Object instance = null;
        InstanceCookie cookie = null;
        Map<String, Object> actionMap = new HashMap<String, Object>(fileObjects.size());

        for (FileObject fileObject : fileObjects) {
            String[] versions = loadVersions(fileObject);

            logger.log(Level.FINEST, "Supported versions for '" + fileObject + "': " + Arrays.deepToString(versions)); // NOI18N

            if (versions == null) {
                LayerFormatExcpetion exception = new LayerFormatExcpetion(fileObject, "File object does not have '" + VERSION_ATTR + "' attribute!");
                logger.throwing(CustomLayerBuilder.class.getName(), "createVersionActionMap", exception);
                throw exception;  // NOI18N
            }

            // Resolve 'shadow' objects by searching for 'instance' reference
            if (fileObject.getExt().equals("shadow") && fileObject.getAttribute("originalFile") != null) { // NOI18N
                String originalFile = fileObject.getAttribute("originalFile").toString(); // NOI18N
                fileObject = fileObject.getFileSystem().findResource(originalFile);
            }

            try {
                 // Try to instantiate the object
                cookie = DataObject.find(fileObject).getCookie(InstanceCookie.class);
                instance = cookie.instanceCreate();
            } catch (Exception ex) {
                LayerFormatExcpetion exception = new LayerFormatExcpetion(fileObject, "Instantiation failure! Could instantiate the file object!", ex);
                logger.throwing(CustomLayerBuilder.class.getName(), "createVersionActionMap", exception);
                throw exception;
            }

            if (instance instanceof Action) {
                Action action = (Action) instance;
                if (action.getValue(Action.NAME) == null) {
                    action.putValue(Action.NAME, fileObject.getAttribute(KEY_ATTR));
                }
            }

            for (String version : versions) {
                actionMap.put(version, instance);
            }
        }

        return actionMap;
    }

    /**
     * Creates a {@link FileObject} instance on the default file system. It will be
     * a copy of the give custom object
     * @param instance The instance instance the new file object refers to. Can be null for instance
     * in case of shadow object which points to an original file.
     * @param customFileObject The reference custom file object whose attributes are used
     * for the new one.
     * @param extension The extension of the new file object (e.g.: instance or shadow)
     * @throws java.io.IOException
     */
    private void createFileObject(Object instance, FileObject customFileObject, String extension)
            throws LayerFormatExcpetion, FileStateInvalidException, IOException {
        // Construct the path. Use the custom one and cut the CUSTOM_MENU_ROOT prefix!
        String path = customFileObject.getParent().getPath();
        int index = path.indexOf(CUSTOM_MENU_ROOT);
        if (index > -1) {
            path = path.substring(index + CUSTOM_MENU_ROOT.length());
        }

        FileObject targetFolderFileObject = customFileObject.getFileSystem().findResource(path);
        // create the target supFolder if it does not exist (e.g.: /Menu)
        if (targetFolderFileObject == null) {
            logger.log(Level.FINE, "Create subfolders: " + path); // NOI18N

            // Can not use FileUtil.createFolder(). Need to know what folders have
            // been created.
            targetFolderFileObject = createFolderHierarchy(customFileObject.getFileSystem(), path);
        }
        // TODO throws FSException in case of mistype of the instance name

        String copyFileObjectName = customFileObject.getAttribute(KEY_ATTR).toString();

        logger.log(Level.FINEST, "Create '" + path + "/" + copyFileObjectName + "' file object with instance: " + instance); // NOI18N

        // Test whether the object is already there
        FileObject oldFileObject;
        if ((oldFileObject = targetFolderFileObject.getFileObject(copyFileObjectName + "." + extension)) != null) {
            // ... if it is delete it
            logger.log(Level.FINEST, "It has been already created! Remove it!"); // NOI18N
            oldFileObject.delete();
        }

        FileObject newFileObject = targetFolderFileObject.createData(copyFileObjectName, extension);

        // Store this new object for cleanup
        createdObjects.add(newFileObject);

        // Copy the attributes
        String attribute;
        Enumeration<String> enumer = customFileObject.getAttributes();
        while (enumer.hasMoreElements()) {
            attribute = enumer.nextElement();

            if (!attribute.equals(KEY_ATTR) || !attribute.equals(VERSION_ATTR)) {
                newFileObject.setAttribute(attribute, customFileObject.getAttribute(attribute));
            }
        }

        if (instance != null) {
            newFileObject.setAttribute("instanceCreate", instance); // NOI18N
        }

        // Trigger refresh
        newFileObject.refresh();
        targetFolderFileObject.refresh();
    }

    /**
     * Creates non-existent directories on the given path
     * 
     * @param fileSystem The file system where the directories have to be created
     * @param path The path where non-existent directories need to be created
     * @return The last directory on the path
     *
     * @throws IOException
     */
    private FileObject createFolderHierarchy(FileSystem fileSystem, String path) throws LayerFormatExcpetion {
        if (path == null || fileSystem == null) {
            throw new NullPointerException("Null argument!");
        }

        // Split the path around slash characters
        String[] folders = path.split("/");

        FileObject root = fileSystem.getRoot();
        FileObject tmp;
        for (String subFolder : folders) {
            // Search for a subfoder in the root folder
            tmp = fileSystem.findResource(root.getPath() + "/" + subFolder);

            // If exists enter, otherwise create it!
            if (tmp != null) {
                root = tmp;
            } else {
                try {
                    root = root.createFolder(subFolder);

                    // This fixes the bug which causes that the empty Menu and
                    // Toolbar folders remains after plugin deactivation/uninstallation.
                    createdObjects.add(root);
                } catch (IOException ex) {
                    LayerFormatExcpetion exception = new LayerFormatExcpetion(root, "Can not create subfolder: " + subFolder);
                    logger.throwing(CustomLayerBuilder.class.getName(), "createFolderHierarchy", exception);
                    throw exception;
                }
            }
        }

        // Return the last folder
        return root;
    }

    /**
     * Groups the file objects under root supFolder based on the given attribute value.
     * @param result The not null map to which stores the result group id - file object list mappings
     * @param root The root supFolder where the groupping starts
     * @param groupIdPropName The name of the attribute wich stores the group id
     */
    public static void groupFileObjects(Map<String, List<FileObject>> result, FileObject root, String groupIdPropName) throws LayerFormatExcpetion {
        if (result == null) {
            throw new NullPointerException("Result map is null!"); // NOI18N
        }

        String key;
        String groupId;
        for (FileObject child : root.getChildren()) {
            if (child.isFolder()) {
                groupFileObjects(result, child, groupIdPropName);
                continue;
            }

            if (child.getAttribute(groupIdPropName) == null) {
                LayerFormatExcpetion exception = new LayerFormatExcpetion(child, "File object does not have '" + groupIdPropName + "' attribute!");  // NOI18N
                logger.throwing(CustomLayerBuilder.class.getName(), "groupFileObjects", exception);
                throw exception;
            } else {
                groupId = child.getAttribute(groupIdPropName).toString();
            }

            /*Construct the key: <parent path>/groupId*/
            key = child.getParent().getPath() + "/" + groupId; // NOI18N

            if (!result.containsKey(key)) {
                result.put(key, new ArrayList<FileObject>());
            }

            result.get(key).add(child);
        }
    }

    /**
     * Wrapper instance class which holds a group of actions with same functionality
     * and target identified by version they are implemented for.
     * Suits to DataSourceAction actions.
     *
     * @param <T> The type of the {@link DataSource} this class is bound to.
     *
     * @see {@link VersionableDataSource}
     * @see {@link SingleDataSourceAction}
     */
    private static class DynamicDataSourceAction<T extends VersionableDataSource> extends SingleDataSourceAction<T> {

        private final Map<String, Action> actionMap;
        private final PropertyListener listener = new PropertyListener();

        /**
         * Constructor
         * @param instanceMap The version-instance instance map
         * @param clazz The class of the {@link DataSource} this class bounds to.
         */
        public DynamicDataSourceAction(Map<String, Action> actionMap, Class<T> clazz) {
            super(clazz);

            this.actionMap = actionMap;

            init();
        }

        /**
         * Initializes the instance properties based on the latest instance version
         */
        private void init() {
            // Look for the latest version
            String latestKey = ""; // NOI18N
            for (String key : actionMap.keySet()) {
                if (latestKey.compareToIgnoreCase(key) < 0) {
                    latestKey = key;
                }
            }

            Action action = actionMap.get(latestKey);

            if (action == null) {
                return;
            }

            if (action instanceof AbstractAction) {
                AbstractAction abstractAction = (AbstractAction) action;

                for (Object key : abstractAction.getKeys()) {
                    putValue(key.toString(), abstractAction.getValue(key.toString()));
                }
            } else { // TODO is it necessary?
                putValue(NAME, action.getValue(NAME));
                putValue(SHORT_DESCRIPTION, action.getValue(SHORT_DESCRIPTION));
                putValue(LONG_DESCRIPTION, action.getValue(LONG_DESCRIPTION));
                putValue(SMALL_ICON, action.getValue(SMALL_ICON));
                putValue(LARGE_ICON_KEY, action.getValue(LARGE_ICON_KEY));
                putValue(MNEMONIC_KEY, action.getValue(MNEMONIC_KEY));
                putValue(SELECTED_KEY, action.getValue(SELECTED_KEY));
                putValue("iconBase", action.getValue("iconBase"));
                putValue("noIconInMenu", action.getValue("noIconInMenu"));
            }
        }

        @Override
        protected void actionPerformed(final T dataSource, final ActionEvent actionEvent) {
            if (actionMap.containsKey(dataSource.getVersion())) {

                EventQueue.invokeLater(new Runnable() {

                    public void run() {
                        Action action = actionMap.get(dataSource.getVersion());

                        action.actionPerformed(actionEvent);
                    }
                });
            } else {
                throw new TypeNotPresentException(dataSource.getVersion(), null);
            }
        }

        @Override
        protected boolean isEnabled(final T dataSource) {
            dataSource.removePropertyChangeListener(listener);
            dataSource.addPropertyChangeListener(listener);

            if (!actionMap.containsKey(dataSource.getVersion())) {
                return false;
            } else {
                Action action = actionMap.get(dataSource.getVersion());

                /** TODO eliminate the need of OpenSingleDataSourceAction class
                 *  Problem: in case of SingleDataSourceAction the call of
                 *  isEnabled() method causes synchronization problems and randomly
                 *  returns true or false.
                 */
                if (action instanceof OpenSingleDataSourceAction) {
                    OpenSingleDataSourceAction<T> osdsAction = (OpenSingleDataSourceAction<T>) action;
                    return osdsAction.isEnabled(dataSource);
                } else {
                    return action.isEnabled();
                }
            }
        }

        @Override
        public String toString() {
            return "" + getValue(NAME) + actionMap; // NOI18N
        }

        private class PropertyListener implements PropertyChangeListener {

            public void propertyChange(PropertyChangeEvent evt) {
                DynamicDataSourceAction.this.setEnabled(DynamicDataSourceAction.this.isEnabled((T) evt.getSource()));
            }
        }
    }

    /**
     * Wrapper instance class which holds a group of actions with same functionality
     * and target identified by version they are implemented for.
     * Suits to ContextAwareAction actions as CookieAction.
     *
     * @see {@link ContextAwareAction}
     */
    private static class DynamicContextAwareAction extends AbstractAction implements ContextAwareAction, LookupListener {

        private final Map<String, Action> actionMap;
        private Lookup context;
        private Lookup.Result<DataSource> dataSources;
        private Lookup.Result<Node> nodes;

        /**
         * Constructor
         * @param instanceMap The version-instance instance map
         */
        public DynamicContextAwareAction(Map<String, Action> actionMap) {
            this(Utilities.actionsGlobalContext(), actionMap);
        }
        
        /**
         * Constructor
         * @param context The Lookup context of the instance
         * @param instanceMap The version-instance instance map
         */
        public DynamicContextAwareAction(Lookup context, Map<String, Action> actionMap) {
            this.context = context;
            this.actionMap = actionMap;

            init();
        }

        /**
         * Initializes the instance properties based on the latest instance version
         */
        private void init() {
            String latestKey = ""; // NOI18N
            for (String key : actionMap.keySet()) {
                if (latestKey.compareToIgnoreCase(key) < 0) {
                    latestKey = key;
                }
            }

            Action action = actionMap.get(latestKey);
            Class type = null;
            /** TODO eliminate the need of OpenCookieAction class
             *  Problem: how to acquire the type(s) this ContextAwareAction should
             *  bound to from the given actions?
             */
            if (action instanceof OpenCookieAction) {
                type = ((OpenCookieAction) action).cookieClasses()[0].getSuperclass();
            } else {
                type = DataSource.class;
            }

            // Lookup the data sources we bound to
            dataSources = context.lookup(new Lookup.Template(type));
            dataSources.addLookupListener(this);

            // Lookup the nodes we bound to
            nodes = context.lookup(new Lookup.Template(Node.class));

            putValue(NAME, action.getValue(NAME));
            putValue(SHORT_DESCRIPTION, action.getValue(SHORT_DESCRIPTION));
            putValue(LONG_DESCRIPTION, action.getValue(LONG_DESCRIPTION));
            putValue(SMALL_ICON, action.getValue(SMALL_ICON));
            putValue(LARGE_ICON_KEY, action.getValue(LARGE_ICON_KEY));
            putValue(MNEMONIC_KEY, action.getValue(MNEMONIC_KEY));
            putValue(SELECTED_KEY, action.getValue(SELECTED_KEY));
            putValue("iconBase", action.getValue("iconBase"));
            putValue("noIconInMenu", action.getValue("noIconInMenu"));
        }

        public void actionPerformed(ActionEvent e) {
            EventQueue.invokeLater(new Runnable() {

                public void run() {
                    VersionableDataSource owner = null;
                    for (DataSource dataSource : dataSources.allInstances()) {
                        owner = getVersionableOwner(dataSource);
                        break;
                    }

                    if (owner != null && actionMap.containsKey(owner.getVersion())) {
                        Action action = actionMap.get(owner.getVersion());
                        action.actionPerformed(new ActionEvent(nodes.allInstances().toArray(), 0, "DynamicCookieAction")); // NOI18N
                    }
                }
            });
        }

        public Action createContextAwareInstance(Lookup actionContext) {
            return new DynamicContextAwareAction(actionContext, actionMap);
        }

        public void resultChanged(LookupEvent ev) {
            firePropertyChange("enabled", null, null); // NOI18N
        }

        @Override
        public boolean isEnabled() {
            if (dataSources.allInstances().size() == 0) {
                return false;
            }

            VersionableDataSource owner = null;
            for (DataSource dataSource : dataSources.allInstances()) {
                owner = getVersionableOwner(dataSource);

                if (owner != null) {
                    break;
                }
            }

            return actionMap.containsKey(owner.getVersion());
        }

        /**
         * Browses for the first DataSource object which implements the {@link  VersionableDataSource}
         * interface in the hierarchy tree.
         * @param dataSource The root {@link  DataSource} object whose {@link  VersionableDataSource} parent
         * we are looking for
         * @return The parent {@link  VersionableDataSource} of the given object
         */
        private static VersionableDataSource getVersionableOwner(DataSource dataSource) {
            if (dataSource instanceof VersionableDataSource) {
                return (VersionableDataSource) dataSource;
            } else {
                return getVersionableOwner(dataSource.getOwner()); // TODO getMaster ?
            }
        }

        @Override
        public String toString() {
            return "" + getValue(NAME) + actionMap; // NOI18N
        }
        }

    /**
     * Comparator class to sort FileObjects based on their version attribute.
     *
     * @see {@link CustomLayerBuilder#loadVersions(org.openide.filesystems.FileObject) 
     */
    private static class FileObjectVersionComparator implements Comparator<FileObject> {

        public int compare(FileObject o1, FileObject o2) {
            String[] o1Versions = loadVersions(o1);
            String[] o2Versions = loadVersions(o2);

            if (o1Versions == null || o1Versions.length == 0) {
                return (o2Versions == null || o2Versions.length == 0) ? 0 : -1;
            } else if (o2Versions == null || o2Versions.length == 0) {
                return 1;
            }

            return o1Versions[0].compareTo(o2Versions[0]);
        }
    }

    /**
     * Comparator class to sort FileObjects based on their type.
     */
    private static class FileObjectTypeComparator implements Comparator<FileObject> {

        public int compare(FileObject o1, FileObject o2) {
            if ((o1.isFolder() && o2.isFolder()) || (o1.isData() && o2.isData())) {
                return 0;
            } else if (o1.isFolder() && o2.isData()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public static class LayerFormatExcpetion extends Exception {
        private FileObject source;

        public LayerFormatExcpetion() {
            this(null);
        }

        public LayerFormatExcpetion(String message) {
            this(null, message);
        }

        public LayerFormatExcpetion(FileObject source, String message) {
            super(message);

            this.source = source;
        }

        public LayerFormatExcpetion(FileObject source, String message, Throwable cause) {
            super(message, cause);

            this.source = source;
        }

        @Override
        public String toString() {
            return super.toString() + (source != null ? "\n source: " + source : "");
        }
    }
}
