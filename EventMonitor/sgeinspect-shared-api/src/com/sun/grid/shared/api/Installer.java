/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.shared.api;

import com.sun.grid.shared.api.layer.CustomLayerBuilder;
import org.openide.modules.ModuleInstall;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    @Override
    public void restored() {
        // Build custom gui elements
        CustomLayerBuilder.getInstance().build();
    }

    @Override
    public boolean closing() {
        CustomLayerBuilder.getInstance().destroy();

        return super.closing();
    }
}
