/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.grid.shared.api.version;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public abstract class AbstractVersionable implements Versionable {
    private final String[] versions;

    protected AbstractVersionable(String[] versions) {
        this.versions = versions;
    }

    @Override
    public final List<String> getVersions() {
        return Collections.unmodifiableList(Arrays.asList(versions));
    }

    @Override
    public String getVersion() {
        String result = "";

        for (String version : versions) {
            version += " " + version;
        }

        return result.trim();
    }
}
