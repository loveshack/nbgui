/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.shared.api.lookup;

import com.sun.grid.shared.api.version.Versionable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup.Template;
import org.openide.util.lookup.AbstractLookup;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Lookup support for {@link LookupFactory} subclasses
 */
public class Lookup<T extends Versionable> extends AbstractLookup {
    
    private final Class<T> type;
    private static final Logger logger = Logger.getLogger(Lookup.class.getName(), "com.sun.grid.shared.api.Bundle"); // NOI18N

    /**
     * Constructor
     * @param type Class type of the {@link LookupFactory} lookup
     */
    public Lookup(Class<T> type) {
        this.type = type;
    }

    /**
     * Constructor
     * @param content The default conent
     * @param type Class type of the {@link LookupFactory} lookup
     */
    public Lookup(Content content, Class<T> type) {
        super(content);
        this.type = type;
    }

    @Override
    protected void beforeLookup(Template<?> tmplt) {
        super.beforeLookup(tmplt);
    }

    /**
     * Registers a service
     * @param instance The service instance to register
     */
    public void registerService(T instance) {
        for (String version : instance.getVersions()) {
            Service s = new Service(new ServiceVersion(version), instance.getClass());
            addPair(new P(s, instance, instance.getClass()));

            logger.log(Level.FINE, "Register service '" + s.getDisplayName() + "' with instance '" + instance + "'."); // NOI18N
        }
    }

    /**
     * Unregisters a service
     * @param instance The service instance to unregister
     */
    public void unregisterService(T instance) {
        for (String version : instance.getVersions()) {
            Service s = new Service(new ServiceVersion(version), instance.getClass());
            removePair(new P(s, instance, instance.getClass()));

            logger.log(Level.FINE, "Unregister service '" + s.getDisplayName() + "' with instance '" + instance + "'."); // NOI18N
        }
    }

    /**
     * Returns a registered service instance.
     * @param version The version of the registered service we are interested in
     * @return The found service instance if it has been registered, null otherwise
     */
    public T getService(String version) {
        Result<T> result = lookup(new Template<T>(this.type));
        Collection<P<T>> pairs = (Collection<P<T>>) result.allItems();

        for (P pair : pairs) {
            if (version.equals(pair.service.version.getVersion())) {
                logger.log(Level.FINER, "Return service '" + pair.getDisplayName() + "' with instance '" + pair.getInstance() + "'."); // NOI18N
                return (T)pair.getInstance();
            }
        }
        
        return null;
    }

    /**
     * Returns all registered service versions
     * @return The registered service versions
     */
    public Collection<String> getAllServiceVersions() {
        Result<T> result = lookup(new Template<T>(type));
        Collection<P<T>> pairs = (Collection<P<T>>) result.allItems();

        Set<String> versions = new HashSet<String>();
        for (P pair : pairs) {
            versions.add(pair.service.getVersion().getVersion());
        }

        return versions;
    }

    /**
     * Class to wrap Service - instance pairs
     * @param <T>
     */
    private static final class P<T> extends AbstractLookup.Pair<T> {
        private Service service;
        private T instance;
        private Class<T> type;

        public P(Service service, T instance, Class<T> type) {
            this.service = service;
            this.instance = instance;
            this.type = type;
        }

        @Override
        protected boolean instanceOf(Class<?> clazz) {
            return clazz.isAssignableFrom(instance.getClass());
        }

        @Override
        protected boolean creatorOf(Object obj) {
            return obj == instance;
        }

        @Override
        public T getInstance() {
            return instance;
        }

        @Override
        public Class<? extends T> getType() {
            return type;
        }

        @Override
        public String getId() {
            return service.getId();
        }

        @Override
        public String getDisplayName() {
            return service.getDisplayName();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final P other = (P) obj;
            if (this.service != other.service && (this.service == null || !this.service.equals(other.service))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + (this.service != null ? this.service.hashCode() : 0);
            return hash;
        }

        
    }

    /**
     * Contains service version information
     */
    private static final class ServiceVersion {
        private String version;

        public ServiceVersion(String version) {
            this.version = version;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return version;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ServiceVersion other = (ServiceVersion) obj;
            if ((this.version == null) ? (other.version != null) : !this.version.equals(other.version)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + (this.version != null ? this.version.hashCode() : 0);
            return hash;
        }
    }

    /**
     * Holds informations from a service
     */
    private static class Service {
        private ServiceVersion version = null;
        private Class clazz;
        private String id;

        public Service(ServiceVersion version, Class clazz) {
            this.version = version;
            this.clazz = clazz;
            this.id = clazz + "@" + version;
        }

        public Class getClazz() {
            return clazz;
        }

        public ServiceVersion getVersion() {
            return version;
        }

        public String getId() {
            return id;
        }

        public String getDisplayName() {
            return getId();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Service other = (Service) obj;
            if (!this.getId().equals(other.getId())) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
            return hash;
        }
    }
}
