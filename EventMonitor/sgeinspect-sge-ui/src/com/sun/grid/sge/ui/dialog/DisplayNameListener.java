/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.dialog;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 */
public class DisplayNameListener implements DocumentListener, ItemListener {

    private JTextField txtDisplayName;
    private JTextField txtUserName;
    private JTextField txtJmxHost;
    private JTextField txtJmxPort;
    private JCheckBox chkDisplayName;
    private JLabel lblDisplayName;
    private boolean internalChange;

    public DisplayNameListener(JCheckBox chkDisplayName, JLabel lblDisplayName, JTextField txtDisplayName, JTextField txtUsername, JTextField txtJmxHost, JTextField txtJmxPort) {
        this.chkDisplayName = chkDisplayName;
        this.lblDisplayName = lblDisplayName;
        this.txtDisplayName = txtDisplayName;
        this.txtUserName = txtUsername;
        this.txtJmxHost = txtJmxHost;
        this.txtJmxPort = txtJmxPort;
    }

    public void insertUpdate(DocumentEvent e) {
        update();
    }

    public void removeUpdate(DocumentEvent e) {
        update();
    }

    public void changedUpdate(DocumentEvent e) {
        update();
    }

    private void update() {
        if (internalChange) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                String userName = txtUserName.getText().trim();
                String jmxHost = txtJmxHost.getText().trim();
                String jmxPort = txtJmxPort.getText().trim();

                if (!chkDisplayName.isSelected()) {
                    internalChange = true;
                    txtDisplayName.setText(
                            (userName.isEmpty() ? "" : userName + "@") +
                            (jmxHost.isEmpty() ? "" : jmxHost + ":") +
                            (jmxPort.isEmpty() ? "" : jmxPort)); // NOI18N
                    internalChange = false;
                }

                lblDisplayName.setEnabled(chkDisplayName.isSelected());
                txtDisplayName.setEnabled(chkDisplayName.isSelected());
            }
        });
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            txtDisplayName.setEnabled(false);
            lblDisplayName.setEnabled(false);
        } else {
            txtDisplayName.setEnabled(true);
            lblDisplayName.setEnabled(true);
        }
    }
}

