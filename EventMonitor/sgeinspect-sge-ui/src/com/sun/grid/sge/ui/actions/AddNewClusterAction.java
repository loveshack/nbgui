/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.actions;

import com.sun.grid.sge.api.monitor.cluster.ClusterAction;
import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;
import com.sun.grid.sge.api.monitor.cluster.ClusterContainer;
import com.sun.grid.sge.api.monitor.cluster.ClusterProvider;
import com.sun.grid.sge.ui.dialog.BasicConnectConfig;
import com.sun.grid.sge.ui.dialog.ConnectConfigHelper;
import com.sun.grid.sge.ui.dialog.DefaultClusterConfiguration;
import com.sun.grid.sge.ui.dialog.KeystoreConnectConfig;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.grid.sge.ui.resources.ResourceHelper;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

class AddNewClusterAction extends ClusterAction {

    private static final Logger logger = Logger.getLogger(AddNewClusterAction.class.getName());
    private static final CreateAbstractNewClusterAction action = new CreateAbstractNewClusterAction();
    private static final CreateSingleNewClusterAction dsAction = new CreateSingleNewClusterAction();

    public static AbstractAction getAction() {
        return action;
    }

    public static SingleDataSourceAction<ClusterContainer> getDSAction() {
        return dsAction;
    }

    private static void performAction(ActionEvent actionEvent) {

        try {
            // register all subtypes of configurations that might be used!
            ConnectConfigHelper.setupSubClassRegistry();
            // start the wizard
            ConnectConfigHelper confHelper = new ConnectConfigHelper("New Connection");

            BasicConnectConfig oldConf = new KeystoreConnectConfig();
            oldConf.wrapClusterConfig(new DefaultClusterConfiguration());

            // startWizardDialog(oldConf, deletePrefs, tabMode)
            BasicConnectConfig newConf = confHelper.startWizardDialog(oldConf, true, false);
            if (newConf != null) {
                if (newConf instanceof KeystoreConnectConfig) {
                    // System.out.println("KeyStoreConnectConfig JMX Host: " + ((KeystoreConnectConfig) newConf).getJmxHost());
                }

                ClusterConfiguration clusterConfig = newConf.unwrapClusterConfig();

                boolean debug = false;  
                // TODO: remove for debugging only
                if (debug) {
                    logger.log(Level.FINEST, "Selected Cluster Version: {0}", newConf.getClusterVersion());
                    System.out.println("Selected Class: " + newConf.getClass().getSimpleName());

                    System.out.println("ClusterConfig: " + clusterConfig);
                    System.out.println("ClusterConfig clusterName: " + clusterConfig.getClusterName());
                    System.out.println("ClusterConfig userName: " + clusterConfig.getUserName());
                    System.out.println("ClusterConfig displayName: " + clusterConfig.getDisplayName());
                    System.out.println("ClusterConfig caPath: " + clusterConfig.getCaPath());
                    System.out.println("ClusterConfig keystorePath: " + clusterConfig.getKeystorePath());
                    System.out.println("ClusterConfig clusterVersion: " + clusterConfig.getClusterVersion());
                    System.out.println("ClusterConfig jmxHost: " + clusterConfig.getJmxHost());
                    System.out.println("ClusterConfig jmxPort: " + clusterConfig.getJmxPort());
                    System.out.println("ClusterConfig isSSL: " + clusterConfig.getSSL());
                }

                // create cluster connection
                ClusterProvider.getInstance().createCluster(clusterConfig, clusterConfig.getClusterVersion());
            }

        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
    }


    private static class CreateAbstractNewClusterAction extends AbstractAction {

        private static final long serialVersionUID = -2009040101L;

        private CreateAbstractNewClusterAction() {
            super();
            putValue(NAME, NbBundle.getMessage(CreateAbstractNewClusterAction.class, "CTL_CreateClusterConnection"));
            putValue(SMALL_ICON, new ImageIcon(ResourceHelper.createBadgedIcon(ICON_PATH)));
            //Does not function
//            putValue(Action.LARGE_ICON_KEY, new ImageIcon(ImageUtilities.loadImage(LARGE_ICON_PATH)));
//            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);

            // Customize Toolbar dialog needs it
            putValue("iconBase", ""); // NOI18N
        }

        public void actionPerformed(ActionEvent event) {
            performAction(event);
        }
    }

    private static class CreateSingleNewClusterAction extends SingleDataSourceAction<ClusterContainer> {

        private static final long serialVersionUID = -2009040101L;

        private CreateSingleNewClusterAction() {
            super(ClusterContainer.class);
            putValue(NAME, NbBundle.getMessage(CreateSingleNewClusterAction.class, "CTL_CreateClusterConnection"));
            putValue(SMALL_ICON, new ImageIcon(ResourceHelper.createBadgedIcon(ICON_PATH)));

            // Customize Toolbar dialog needs it
            putValue("iconBase", ""); // NOI18N
        }

        public void actionPerformed(ClusterContainer ds, ActionEvent event) {
            performAction(event);
        }

        protected boolean isEnabled(ClusterContainer ds) {
            return true;
        }
    }
}
