/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.filter;

import com.sun.grid.sge.ui.filter.CustomRowFilter.StringComparisonType;
import com.sun.grid.sge.ui.tables.PagingTableModel;
import com.sun.grid.shared.ui.options.AppearanceOptionsPanelController;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowFilter.ComparisonType;
import javax.swing.table.TableRowSorter;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class FilterPanel extends javax.swing.JPanel {

    private FilterCriterionPanel lastCriterion;
    private LinkedList<FilterCriterionPanel> criterions;
    private PagingTableModel model;
    private JTable table;
    private final AddFilterListener afListener = new AddFilterListener();
    private Preferences pref = NbPreferences.forModule(AppearanceOptionsPanelController.class);


    /** Creates new form FilterPanel */
    public FilterPanel(JTable table) {
        initComponents();
        this.table = table;
        this.model = (PagingTableModel) table.getModel();
        criterions = new LinkedList<FilterCriterionPanel>();
        FilterCriterionPanel fp = new FilterCriterionPanel(afListener);
        fp.btnMinus.setEnabled(false);
        criterions.add(fp);
        pnlFilters.add(fp);
        lastCriterion = fp;
        populateCombos(lastCriterion);
        pref.addPreferenceChangeListener(new PrefListener());
    }

    private class AddFilterListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            final FilterCriterionPanel fPanel = (FilterCriterionPanel) ((Component) e.getSource()).getParent();
            if (e.getSource() == fPanel.btnPlus) {
                addNewCriterion();
            } else if (e.getSource() == fPanel.btnMinus) {
                removeCriterion(fPanel);
            } else if (e.getSource() == fPanel.cmbFilterSubject) {
                setOperations(fPanel);
                setDatePicker(fPanel);
            }
        }
    }

    private class PrefListener implements PreferenceChangeListener {

        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt.getKey().equals(AppearanceOptionsPanelController.PROP_DATE) ||
                  evt.getKey().equals(AppearanceOptionsPanelController.PROP_TIME)) {
                changeDateFormat();
            }
        }
    }

    private void changeDateFormat() {
        DateFormat df = DateFormat.getDateTimeInstance(pref.getInt(AppearanceOptionsPanelController.PROP_DATE,
                      AppearanceOptionsPanelController.DATE_DEFAULT), pref.getInt(AppearanceOptionsPanelController.PROP_TIME,
                      AppearanceOptionsPanelController.TIME_DEFAULT));

        for (FilterCriterionPanel fc : criterions) {
            fc.picker.setFormats(df);
        }
    }

    private void populateCombos(FilterCriterionPanel criterion) {
        criterion.cmbFilterSubject.setModel(new DefaultComboBoxModel(getColumNames()));
        setOperations(criterion);
    }

    private void setDatePicker(FilterCriterionPanel criterion) {
        final int columnIndex = criterion.cmbFilterSubject.getSelectedIndex();
        if (model.getColumnClass(columnIndex).equals(Calendar.class)) {
            criterion.txtFilterValue.setVisible(false);
            criterion.picker.setVisible(true);
        } else {
            criterion.txtFilterValue.setVisible(true);
            criterion.picker.setVisible(false);
        }
    }

    private void setOperations(FilterCriterionPanel criterion) {
        final int columnIndex = criterion.cmbFilterSubject.getSelectedIndex();
        if (model.getColumnClass(columnIndex).equals(String.class)) {
            criterion.cmbCperation.setModel(new DefaultComboBoxModel(CustomRowFilter.StringComparisonType.values()));
        } else {
            criterion.cmbCperation.setModel(new DefaultComboBoxModel(RowFilter.ComparisonType.values()));
        }
    }

    private void addNewCriterion() {
        lastCriterion.btnPlus.setEnabled(false);
        FilterCriterionPanel fp = new FilterCriterionPanel(afListener);
        criterions.add(fp);
        pnlFilters.add(fp);
        lastCriterion = fp;
        populateCombos(lastCriterion);

    }

    private void removeCriterion(FilterCriterionPanel c) {
        pnlFilters.remove(c);
        criterions.remove(c);
        lastCriterion = criterions.getLast();
        lastCriterion.btnPlus.setEnabled(true);
        if (criterions.size() == 1) {
            criterions.getFirst().btnMinus.setEnabled(false);
            criterions.getFirst().btnPlus.setEnabled(true);
        }
    }

    private String[] getColumNames() {
        String names[] = new String[model.getColumnCount()];
        for (int i = 0; i < model.getColumnCount(); i++) {
            names[i] = model.getColumnName(i);
        }
        return names;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroup = new javax.swing.ButtonGroup();
        pnlFilters = new javax.swing.JPanel();
        pnlButton = new javax.swing.JPanel();
        btnApply = new javax.swing.JButton();
        btnRemoveFilter = new javax.swing.JButton();
        pnlAndOr = new javax.swing.JPanel();
        rbAny = new javax.swing.JRadioButton();
        rbAll = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 1));

        pnlFilters.setBackground(new java.awt.Color(255, 255, 255));
        pnlFilters.setLayout(new javax.swing.BoxLayout(pnlFilters, javax.swing.BoxLayout.Y_AXIS));

        pnlButton.setBackground(new java.awt.Color(255, 255, 255));
        pnlButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 1, 5, 1));

        btnApply.setText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.btnApply.text")); // NOI18N
        btnApply.setToolTipText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.btnApply.toolTipText")); // NOI18N
        btnApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApplyActionPerformed(evt);
            }
        });

        btnRemoveFilter.setToolTipText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.btnRemoveFilter.toolTipText")); // NOI18N
        btnRemoveFilter.setLabel(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.btnRemoveFilter.label")); // NOI18N
        btnRemoveFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveFilterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addComponent(btnApply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRemoveFilter)
                .addContainerGap(380, Short.MAX_VALUE))
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnApply)
                .addComponent(btnRemoveFilter))
        );

        btnRemoveFilter.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.btnRemoveFilter.AccessibleContext.accessibleDescription")); // NOI18N

        pnlAndOr.setBackground(new java.awt.Color(255, 255, 255));

        rbAny.setBackground(new java.awt.Color(255, 255, 255));
        btnGroup.add(rbAny);
        rbAny.setText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAny.text")); // NOI18N
        rbAny.setToolTipText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAny.toolTipText")); // NOI18N

        rbAll.setBackground(new java.awt.Color(255, 255, 255));
        btnGroup.add(rbAll);
        rbAll.setSelected(true);
        rbAll.setText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAll.text")); // NOI18N
        rbAll.setToolTipText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAll.toolTipText")); // NOI18N

        javax.swing.GroupLayout pnlAndOrLayout = new javax.swing.GroupLayout(pnlAndOr);
        pnlAndOr.setLayout(pnlAndOrLayout);
        pnlAndOrLayout.setHorizontalGroup(
            pnlAndOrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAndOrLayout.createSequentialGroup()
                .addComponent(rbAny)
                .addGap(18, 18, 18)
                .addComponent(rbAll)
                .addContainerGap(246, Short.MAX_VALUE))
        );
        pnlAndOrLayout.setVerticalGroup(
            pnlAndOrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAndOrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(rbAny)
                .addComponent(rbAll))
        );

        rbAny.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAny.AccessibleContext.accessibleDescription")); // NOI18N
        rbAll.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.rbAll.AccessibleContext.accessibleDescription")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlAndOr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlFilters, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE)
                    .addComponent(pnlButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(pnlAndOr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlFilters, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void btnApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApplyActionPerformed
// TODO add your handling code here:
    List<RowFilter<Object, Object>> filters = new ArrayList<RowFilter<Object, Object>>();

    for (FilterCriterionPanel cPanel : criterions) {
        final int columnIndex = cPanel.cmbFilterSubject.getSelectedIndex();
        final String operation = cPanel.cmbCperation.getSelectedItem().toString();
        final String value = cPanel.txtFilterValue.getText();
        String colName = model.getColumnName(columnIndex);

        if (model.getColumnClass(columnIndex).equals(Number.class)) {
            Double filterValue = null;
            try {
                filterValue = Double.parseDouble(value);
            } catch (NumberFormatException nfe) {
                showWarningDialog(NbBundle.getMessage(FilterPanel.class, "Warning_Number_Format", colName), cPanel);
                return;
            }
            filters.add(RowFilter.numberFilter(ComparisonType.valueOf(operation),
                  filterValue, columnIndex));
        } else if (model.getColumnClass(columnIndex).equals(Calendar.class)) {
            Date date = (Date) cPanel.picker.getEditor().getValue();

            filters.add(CustomRowFilter.dateFilter(ComparisonType.valueOf(operation),
                  date, columnIndex));
        } else if (model.getColumnClass(columnIndex).equals(String.class)) {
            filters.add(CustomRowFilter.stringFilter(StringComparisonType.valueOf(operation),
                  value, columnIndex));
        }

    }
    TableRowSorter sorter = (TableRowSorter) table.getRowSorter();
    RowFilter<Object, Object> combinedFilter;
    if (rbAll.isSelected()) {
        combinedFilter = RowFilter.andFilter(filters);
    } else {
        combinedFilter = RowFilter.orFilter(filters);
    }

    sorter.setRowFilter(combinedFilter);

}//GEN-LAST:event_btnApplyActionPerformed

    private void showWarningDialog(String msg, FilterCriterionPanel cPanel) {
        // TODO use unified message dialog handling from shared modules
        JOptionPane.showMessageDialog(this, msg, NbBundle.getMessage(FilterPanel.class,
              "Warning_Dialog_Title"), JOptionPane.WARNING_MESSAGE);
        cPanel.txtFilterValue.setText("");

    }

private void btnRemoveFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveFilterActionPerformed

    for (int i = criterions.size() - 1; i > 0; i--) {
        removeCriterion(criterions.get(i));
    }
    lastCriterion.txtFilterValue.setText(NbBundle.getMessage(FilterPanel.class, "FilterCriterionPanel.txtFilterValue.text"));
    lastCriterion.picker.getEditor().setText(NbBundle.getMessage(FilterPanel.class, "FilterCriterionPanel.txtFilterValue.text"));
    TableRowSorter sorter = (TableRowSorter) table.getRowSorter();
    sorter.setRowFilter(null);
}//GEN-LAST:event_btnRemoveFilterActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnApply;
    private javax.swing.ButtonGroup btnGroup;
    private javax.swing.JButton btnRemoveFilter;
    private javax.swing.JPanel pnlAndOr;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlFilters;
    private javax.swing.JRadioButton rbAll;
    private javax.swing.JRadioButton rbAny;
    // End of variables declaration//GEN-END:variables
}
