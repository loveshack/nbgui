/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.dialog;

import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;

/**
 *
 */
public class BasicConnectConfig {

    private ClusterConfiguration conf;
    private String version = "";

    public BasicConnectConfig() {
        this(true);
    }

    public BasicConnectConfig(boolean isSSL) {
        this.conf = new DefaultClusterConfiguration();
        this.conf.setSSL(isSSL);
    }

    public ClusterConfiguration unwrapClusterConfig() {
        String cName = getUserName() + "@" + getJmxHost() + ":" + getJmxPort();
        setClusterName(cName);
        return conf;
    }

    public void wrapClusterConfig(ClusterConfiguration conf) {
        this.conf = conf;
        this.version = conf.getClusterVersion();
    }

    public void setUserPassword(String userPassword) {
        conf.setUserPassword(userPassword);
    }

    public void setUserName(String userName) {
        conf.setUserName(userName);
    }

   
    public void setKeystorePath(String keystorePath) {
        conf.setKeystorePath(keystorePath);
    }

    public void setKeystorePassword(String keystorePassword) {
        conf.setKeystorePassword(keystorePassword);
    }

    public void setJmxPort(int jmxPort) {
        conf.setJmxPort(jmxPort);
    }

    public void setJmxPort(String jmxPort) {
        conf.setJmxPort(jmxPort);
    }

    public void setJmxHost(String jmxHost) {
        conf.setJmxHost(jmxHost);
    }

    public void setDisplayName(String displayName) {
        conf.setDisplayName(displayName);
    }

    public void setClusterVersion(String clusterVersion) {
        conf.setClusterVersion(clusterVersion);
    }

    public void setClusterName(String clusterName) {
        String cName = getUserName() + "@" + getJmxHost() + ":" + getJmxPort();
        conf.setClusterName(cName);

    }

    public void setCaPath(String caPath) {
        conf.setCaPath(caPath);
    }

    public String getUserPassword() {
        return conf.getUserPassword();
    }

    public String getUserName() {
        return conf.getUserName();
    }

    public boolean isSSL() {
        return conf.getSSL();
    }

    public String getKeystorePath() {
        return conf.getKeystorePath();
    }

    public String getKeystorePassword() {
        return conf.getKeystorePassword();
    }

    public int getJmxPort() {
        return conf.getJmxPort();
    }

    public String getJmxHost() {
        return conf.getJmxHost();
    }

    public String getDisplayName() {
        if (conf.getDisplayName() != null) {
            return conf.getDisplayName();
        } else {
            return conf.getUserName() + "@" + conf.getJmxHost() + ":" + conf.getJmxPort();
        }
    }

    public String getClusterVersion() {
        return conf.getClusterVersion();
    }

    public String getClusterName() {
        return getUserName() + "@" + getJmxHost() + ":" + getJmxPort();
    }

    public String getCaPath() {
        return conf.getCaPath();
    }

    public void setVersion(String version) {
        this.version = version;
        setClusterVersion(version);
    }

    public String getVersion() {
        return version;
    }
}
