/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.openide.util.NbBundle;

public class ChartSupport {

    private static final long MAXIMUM_AGE_TIME = 300000; // 5 minutes
    private static final Font DEFAULT_FONT = new Font("SansSerif", Font.BOLD, 24);
    private static final Color DEFAULT_CHART_BACKGROUND = Color.WHITE;
    private static final Color DEAFULT_PANEL_BACKGROUND = Color.WHITE;
    private static final NumberFormat DEFAULT_RANGE_FORMAT = NumberFormat.getNumberInstance();

    public static abstract class Chart {

        String chartTitle;
        String[] seriesLabel;
        String rangeLabel;
        NumberFormat rangeFormat;
        TimeSeries[] series;
        TimeSeriesCollection dataset;
        TickUnits standardUnits;
        DateAxis domain;
        XYItemRenderer renderer;
        XYPlot plot;
        NumberAxis range;
        JFreeChart chart;

        public Chart() {
            this("Chart Title", new String[]{"Series One"}, "Range");
        }

        public Chart(String chartTitle, String[] seriesLabel, String rangeLabel) {
            this(chartTitle, seriesLabel, rangeLabel, DEFAULT_RANGE_FORMAT);
        }

        public Chart(String chartTitle, String[] seriesLabel, String rangeLabel, NumberFormat rangeFormat) {
            this.chartTitle = chartTitle;
            this.seriesLabel = seriesLabel;
            this.rangeLabel = rangeLabel;
            this.rangeFormat = rangeFormat;
            series = createTimeSeries();
            dataset = createDataSet(series);
            standardUnits = createTickUnits();
            renderer = createXYItemRenderer();
            domain = createDomain();
            range = createRange();
            plot = createXYPlot(dataset, domain, range, renderer);
            chart = createChart(plot);
        }

        protected TimeSeries[] createTimeSeries() {
            TimeSeries[] series = new TimeSeries[seriesLabel.length];
            for (int i = 0; i < series.length; i++) {
                series[i] = new TimeSeries(seriesLabel[i], Millisecond.class);
                series[i].setMaximumItemAge(MAXIMUM_AGE_TIME);
            }
            return series;
        }

        protected TimeSeriesCollection createDataSet(TimeSeries[] series) {
            TimeSeriesCollection dataset = new TimeSeriesCollection();
            for (int i = 0; i < series.length; i++) {
                dataset.addSeries(series[i]);
            }
            return dataset;
        }

        protected NumberAxis createRange() {
            NumberAxis rng = new NumberAxis(rangeLabel);

            if (!NbBundle.getMessage(ChartSupport.class, "LBL_Load_Medium").equals(rangeLabel)) {
                rng.setStandardTickUnits(rng.createIntegerTickUnits());
            }
            rng.setAutoRange(true);
            rng.setNumberFormatOverride(rangeFormat);
            return rng;
        }

        public void setMaximumItemAge(long age) {
            for (int i = 0; i < series.length; i++) {
                series[i].setMaximumItemAge(age);
            }
        }

        protected DateAxis createDomain() {
            DateAxis domain = new DateAxis(NbBundle.getMessage(ChartSupport.class, "LBL_Time"));
            domain.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
            domain.setStandardTickUnits(standardUnits);
            domain.setAutoRange(true);
            domain.setLowerMargin(0.0);
            domain.setUpperMargin(0.0);
            domain.setTickLabelsVisible(true);
            return domain;
        }

        protected XYItemRenderer createXYItemRenderer() {
            XYItemRenderer renderer = new XYLineAndShapeRenderer(true, false);
            renderer.setBaseStroke(new BasicStroke(3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
            return renderer;
        }

        protected XYPlot createXYPlot(TimeSeriesCollection dataset, DateAxis domain, NumberAxis range,
              XYItemRenderer renderer) {
            XYPlot plot = new XYPlot(dataset, domain, range, renderer);
            plot.setBackgroundPaint(Color.white);
            plot.setDomainGridlinePaint(Color.lightGray);
            plot.setRangeGridlinePaint(Color.lightGray);
            plot.setDomainCrosshairVisible(true);
            plot.setRangeCrosshairVisible(true);
            plot.setDomainCrosshairLockedOnData(true);
            plot.setRangeCrosshairLockedOnData(true);
            plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
            return plot;
        }

        protected JFreeChart createChart(XYPlot plot) {
            JFreeChart chart = new JFreeChart(null, DEFAULT_FONT, plot, true);
            chart.setBackgroundPaint(DEFAULT_CHART_BACKGROUND);
            return chart;
        }

        private TickUnits createTickUnits() {
            TickUnits standardUnits = new TickUnits();
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.SECOND, 1, new SimpleDateFormat("HH:mm:ss")));
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.SECOND, 5, new SimpleDateFormat("HH:mm:ss")));
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.SECOND, 10, new SimpleDateFormat("HH:mm:ss")));
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.SECOND, 15, new SimpleDateFormat("HH:mm:ss")));
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.SECOND, 30, new SimpleDateFormat("HH:mm:ss")));
            standardUnits.add(
                  new DateTickUnit(DateTickUnit.MINUTE, 1, new SimpleDateFormat("HH:mm:ss")));
            return standardUnits;
        }

        public ChartPanel getPanel() {

            ChartPanel chartPanel = new ChartPanel(chart);
//            chartPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
            chartPanel.setBackground(DEAFULT_PANEL_BACKGROUND);
            chartPanel.setMouseZoomable(true, false);
            chartPanel.setDisplayToolTips(true);
            return chartPanel;
        }

        public void addNewValue(double value[], long time) {
            for (int i = 0; i < series.length; i++) {
                series[i].add(new Millisecond(new Date(time)), value[i]);
            }
        }

        public void addNewValue(int value[], long time) {
            for (int i = 0; i < series.length; i++) {
                series[i].add(new Millisecond(new Date(time)), value[i]);
            }
        }
    }

    public static class LoadChart extends Chart {

        public LoadChart() {
            super(NbBundle.getMessage(ChartSupport.class, "LBL_Load_Medium"),
                  new String[]{NbBundle.getMessage(ChartSupport.class, "LBL_Load_Medium")},
                  NbBundle.getMessage(ChartSupport.class, "LBL_Load_Medium"));
        }

        public LoadPanelSupport getChartPanel() {
            return new LoadPanelSupport(getPanel(), NbBundle.getMessage(ChartSupport.class, "LBL_Load_Medium"));
        }
    }

    public static class MemoryChart extends Chart {

        public MemoryChart() {
            super(NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Usage"),
                  new String[]{NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Used"),
                      NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Total")
                  },
                  NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Units"));
        }

        public MemoryPanelSupport getChartPanel() {
            return new MemoryPanelSupport(getPanel(), NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Used"),
                  NbBundle.getMessage(ChartSupport.class, "LBL_Memory_Total"));

        }
    }

    public static class SwapChart extends Chart {

        public SwapChart() {
            super(NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Usage"),
                  new String[]{NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Used"),
                      NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Total")
                  },
                  NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Units"));
        }

        public MemoryPanelSupport getChartPanel() {
            return new MemoryPanelSupport(getPanel(), NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Used"),
                  NbBundle.getMessage(ChartSupport.class, "LBL_Swap_Total"));
        }
    }

    public static class SlotChart extends Chart {

        public SlotChart() {
            super(NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Usage"),
                  new String[]{NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Used"),
                      NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Total")
                  },
                  NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Usage"), NumberFormat.getIntegerInstance());
        }

        public SlotPanelSupport getChartPanel() {
            return new SlotPanelSupport(getPanel(), NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Used"),
                  NbBundle.getMessage(ChartSupport.class, "LBL_Slot_Total"));
        }
    }

    public static class RunningJobsChart extends Chart {

        public RunningJobsChart() {
            super(NbBundle.getMessage(ChartSupport.class, "LBL_Running_Jobs"),
                  new String[]{NbBundle.getMessage(ChartSupport.class, "LBL_Running_Jobs")},
                  NbBundle.getMessage(ChartSupport.class, "LBL_Running_Jobs"), NumberFormat.getIntegerInstance());
        }

        public JobPanelSupport getChartPanel() {
           return new JobPanelSupport(getPanel(), NbBundle.getMessage(ChartSupport.class, "LBL_Running_Jobs"));
        }
    }
}
