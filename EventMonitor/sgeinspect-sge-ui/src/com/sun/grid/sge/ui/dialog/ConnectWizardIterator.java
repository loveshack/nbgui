/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.dialog;

import com.sun.grid.shared.ui.confgui.ConfWizardIterator;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;

public final class ConnectWizardIterator extends ConfWizardIterator {

    public ConnectWizardIterator(){
        super(ConnectConfigHelper.CONNECT_DESCRIPTION_KEY_STORE,ConnectConfigHelper.CONNECT_DESCRIPTION_UNIX);
    }

    private List<WizardDescriptor.Panel<WizardDescriptor>> panels;

    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     */
    public List<WizardDescriptor.Panel<WizardDescriptor>> getPanels() {
        if (panels == null) {
            panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();

            BasicConnectWizardPanel basicConnectWizardPanel = new BasicConnectWizardPanel();
            // basicConnectWizardPanel.getPanel().cboConnectConfig_SubType.setEnabled(!isConfigureMode());
            basicConnectWizardPanel.getPanel().cboConnectConfig_Version.setEnabled(!isConfigureMode());
            panels.add(basicConnectWizardPanel);
            panels.add(new KeystoreConnectWizardPanel());
            panels.add(new UnixConnectWizardPanel());
            
            setClientProperty((JComponent)panels.get(0).getComponent(), 0, 
                    new String[]{panels.get(0).getComponent().getName(), "..."});
            setClientProperty((JComponent)panels.get(1).getComponent(), 1,
                    new String[]{panels.get(0).getComponent().getName(), panels.get(1).getComponent().getName()});
            setClientProperty((JComponent)panels.get(2).getComponent(), 1,
                    new String[]{panels.get(0).getComponent().getName(), panels.get(2).getComponent().getName()});
        }

        return panels;
    }

    private void setClientProperty(JComponent component, int contentSelectedIndex, String[] contentData) {
        component.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, new Integer(contentSelectedIndex));
        // Sets steps names for a panel
        component.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, contentData);
        // Turn on subtitle creation on each step
        component.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, Boolean.TRUE);
        // Show steps on the left side with the image on the background
        component.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, Boolean.TRUE);
        // Turn on numbering of all steps
        component.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, Boolean.TRUE);
    }
}
