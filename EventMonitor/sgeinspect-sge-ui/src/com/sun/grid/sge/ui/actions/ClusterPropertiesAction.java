/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.actions;

import com.sun.grid.sge.api.monitor.cluster.Cluster;
import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;
import com.sun.grid.sge.ui.dialog.BasicConnectConfig;
import com.sun.grid.sge.ui.dialog.ConnectConfigHelper;
import com.sun.grid.sge.ui.dialog.KeystoreConnectConfig;
import com.sun.grid.sge.ui.dialog.UnixConnectConfig;
import com.sun.grid.shared.ui.confgui.UnexpectedStructureException;
import com.sun.tools.visualvm.core.ui.actions.SingleDataSourceAction;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.ImageUtilities;

class ClusterPropertiesAction extends SingleDataSourceAction<Cluster> {

    private static final String ICON_PATH = "com/sun/grid/sge/ui/resources/addCluster.png";  // NOI18N
    private static final Image ICON = ImageUtilities.loadImage(ICON_PATH);
    private final PropertyChangeListener propertyListener = new ClusterPropertyListener();
    private static ClusterPropertiesAction INSTANCE;

    @Override
    protected void actionPerformed(final Cluster cluster, ActionEvent actionEvent) {
        try {
            // register all subtypes of configurations that might be used!
            ConnectConfigHelper.setupSubClassRegistry();
            ConnectConfigHelper confHelper = new ConnectConfigHelper(NbBundle.getMessage(ClusterPropertiesAction.class, "LBL_Edit_Connection"));
            BasicConnectConfig oldConf = null;
            boolean isSSL = cluster.getConfiguration().getSSL();
            if (isSSL) {
                oldConf = new KeystoreConnectConfig();
            } else {
                oldConf = new UnixConnectConfig();
            }

            oldConf.wrapClusterConfig(cluster.getConfiguration());
            // start wizard in tab mode
            BasicConnectConfig newConf = confHelper.startWizardDialog(oldConf, true, true);
            if (newConf != null) {
                ClusterConfiguration clusterConfig = newConf.unwrapClusterConfig();
                cluster.setConfiguration(clusterConfig);
                cluster.getConfiguration().store(cluster.getStorage());
                cluster.connect();
            }
        } catch (UnexpectedStructureException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    protected boolean isEnabled(Cluster cluster) {
        cluster.addPropertyChangeListener(propertyListener);
        return !cluster.isConnected();
    }

    private class ClusterPropertyListener implements PropertyChangeListener {

        public void propertyChange(PropertyChangeEvent evt) {
            setEnabled(!((Cluster) evt.getSource()).isConnected());
        }
    }

    public static synchronized ClusterPropertiesAction createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClusterPropertiesAction();
        }
        return INSTANCE;
    }

    private ClusterPropertiesAction() {
        super(Cluster.class);
        putValue(NAME, NbBundle.getMessage(ClusterPropertiesAction.class, "LBL_Edit_Cluster"));  // NOI18N
        putValue(SHORT_DESCRIPTION, NbBundle.getMessage(ClusterPropertiesAction.class, "ToolTip_Edit_Cluster")); // NOI18N
    }
}
