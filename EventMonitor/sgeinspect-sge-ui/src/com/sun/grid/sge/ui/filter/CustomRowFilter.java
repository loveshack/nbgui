/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.filter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Pattern;
import javax.swing.RowFilter;
import javax.swing.RowFilter.ComparisonType;
import javax.swing.RowFilter.Entry;
import org.openide.util.Exceptions;

public class CustomRowFilter {

    public enum StringComparisonType {

        /**
         * Indicates that entries with a value starting with the supplied
         * value should be included.
         */
        STARTS_WITH,
        /**
         * Indicates that entries with a value equal to the supplied
         * value should be included.
         */
        EQUAL,
        /**
         * Indicates that entries with a value not equal to the supplied
         * value should be included.
         */
        NOT_EQUAL,
        /**
         * Indicates that entries that match the supplied regular expression
         * value should be included.
         */
        MATCHES
    }

    public static <M, I> RowFilter<M, I> stringFilter(StringComparisonType type,
          String string, int... indices) {
        return (RowFilter<M, I>) new StringFilter(type, string, indices);
    }

    public static <M, I> RowFilter<M, I> dateFilter(ComparisonType type,
          Date date, int... indices) {
        return (RowFilter<M, I>) new DateFilter(type, date.getTime(), indices);
    }

    /**
     * Throws an IllegalArgumentException if any of the values in
     * columns are < 0.
     */
    private static void checkIndices(int[] columns) {
        for (int i = columns.length - 1; i >= 0; i--) {
            if (columns[i] < 0) {
                throw new IllegalArgumentException("Index must be >= 0");
            }
        }
    }

    private static abstract class GeneralFilter extends RowFilter<Object, Object> {

        private int[] columns;

        GeneralFilter(int[] columns) {
            checkIndices(columns);
            this.columns = columns;
        }

        public boolean include(Entry<? extends Object, ? extends Object> value) {
            int count = value.getValueCount();
            if (columns.length > 0) {
                for (int i = columns.length - 1; i >= 0; i--) {
                    int index = columns[i];
                    if (index < count) {
                        if (include(value, index)) {
                            return true;
                        }
                    }
                }
            } else {
                while (--count >= 0) {
                    if (include(value, count)) {
                        return true;
                    }
                }
            }
            return false;
        }

        protected abstract boolean include(
              Entry<? extends Object, ? extends Object> value, int index);
    }

    private static class DateFilter extends GeneralFilter {

        private long date;
        private ComparisonType type;

        DateFilter(ComparisonType type, long date, int[] columns) {
            super(columns);
            if (type == null) {
                throw new IllegalArgumentException("type must be non-null");
            }
            this.type = type;
            this.date = date;
        }

        protected boolean include(
              Entry<? extends Object, ? extends Object> value, int index) {
            Object v = value.getValue(index);

            final DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                  DateFormat.MEDIUM);

            if (v instanceof String) {
                long vDate = 0;
                try {
                    vDate = df.parse(v.toString()).getTime();
                } catch (ParseException ex) {
                    Exceptions.printStackTrace(ex);
                }
                switch (type) {
                    case BEFORE:
                        return (vDate < date);
                    case AFTER:
                        return (vDate > date);
                    case EQUAL:
                        return (vDate == date);
                    case NOT_EQUAL:
                        return (vDate != date);
                    default:
                        break;
                }
            }
            return false;
        }
    }

    private static class StringFilter extends GeneralFilter {

        private String string;
        private StringComparisonType type;

        StringFilter(StringComparisonType type, String string, int[] columns) {
            super(columns);
            if (type == null) {
                throw new IllegalArgumentException("type must be non-null");
            }
            this.type = type;
            this.string = string;
        }

        protected boolean include(
              Entry<? extends Object, ? extends Object> value, int index) {
            String v = value.getStringValue(index);

            switch (type) {
                case STARTS_WITH:
                    return (v.startsWith(string));
                case EQUAL:
                    return (v.equals(string));
                case NOT_EQUAL:
                    return (!(v.equals(string)));
                case MATCHES:
                    return (Pattern.matches(string, v));
                default:
                    break;
            }

            return false;
        }
    }
}
