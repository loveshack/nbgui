/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.charts;

import com.sun.grid.sge.api.monitor.util.NumberFormater;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import org.jfree.chart.ChartPanel;

public class LoadPanelSupport extends javax.swing.JPanel {

    private ChartPanel chPanel;
    private final DecimalFormat form = new DecimalFormat();

    /** Creates new form LoadPanelSupport */
    public LoadPanelSupport(ChartPanel chPanel, String label) {
        this.chPanel = chPanel;
        form.setMaximumFractionDigits(2);
        form.setMinimumFractionDigits(2);
        form.setRoundingMode(RoundingMode.HALF_UP);
        initComponents();
        lblLoad.setText(label + ":");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chrtLablePanel = new javax.swing.JPanel();
        lblLoad = new javax.swing.JLabel();
        txtLoad = new javax.swing.JTextField();
        chrtPanel = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        chrtLablePanel.setBackground(new java.awt.Color(255, 255, 255));
        chrtLablePanel.setMinimumSize(new java.awt.Dimension(0, 0));

        lblLoad.setLabelFor(txtLoad);
        lblLoad.setText(org.openide.util.NbBundle.getMessage(LoadPanelSupport.class, "LoadPanelSupport.lblLoad.text")); // NOI18N

        txtLoad.setText(org.openide.util.NbBundle.getMessage(LoadPanelSupport.class, "LoadPanelSupport.txtLoad.text")); // NOI18N
        txtLoad.setBorder(null);
        txtLoad.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtLoad.setEnabled(false);

        javax.swing.GroupLayout chrtLablePanelLayout = new javax.swing.GroupLayout(chrtLablePanel);
        chrtLablePanel.setLayout(chrtLablePanelLayout);
        chrtLablePanelLayout.setHorizontalGroup(
            chrtLablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chrtLablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLoad)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(434, Short.MAX_VALUE))
        );
        chrtLablePanelLayout.setVerticalGroup(
            chrtLablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chrtLablePanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(chrtLablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLoad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        chrtPanel.setBackground(new java.awt.Color(255, 255, 255));
        chrtPanel.setLayout(new java.awt.BorderLayout());
        chrtPanel.add(chPanel, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(chrtPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                    .addComponent(chrtLablePanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(chrtLablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chrtPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    
    
    public void setLoad(double load) {
        double text = load;
        txtLoad.setText(NumberFormater.format(form, text));
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel chrtLablePanel;
    private javax.swing.JPanel chrtPanel;
    private javax.swing.JLabel lblLoad;
    private javax.swing.JTextField txtLoad;
    // End of variables declaration//GEN-END:variables

}
