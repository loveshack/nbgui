/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.dialog;

import com.sun.grid.sge.api.monitor.cluster.ClusterFactoryProvider;
import java.util.Arrays;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import org.openide.util.NbBundle;

public final class BasicConnectPanel extends JPanel {

    /** Creates new form SloVisualPanel1 */
    public BasicConnectPanel() {
        initComponents();
        setSgeVersions(ClusterFactoryProvider.getRegisteredVersions().toArray());
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(BasicConnectPanel.class, "BasicConnectPanel.ChooseVersionAndConnectMode");
    }

    public void setSgeVersions(Object[] versions) {
        Arrays.sort(versions);
        cboConnectConfig_Version.setModel(new DefaultComboBoxModel(versions));
        // choose as default the latest version
        cboConnectConfig_Version.setSelectedIndex(cboConnectConfig_Version.getItemCount()-1);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cboConnectConfig_SubType = new javax.swing.JComboBox();
        lblConnectConfig = new javax.swing.JLabel();
        cboConnectConfig_Version = new javax.swing.JComboBox();
        lblConnectConfig_Version = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(500, 342));

        cboConnectConfig_SubType.setToolTipText(org.openide.util.NbBundle.getMessage(BasicConnectPanel.class, "BasicConnectPanel.cboConnectConfig_SubType.toolTipText")); // NOI18N

        lblConnectConfig.setLabelFor(cboConnectConfig_SubType);
        org.openide.awt.Mnemonics.setLocalizedText(lblConnectConfig, org.openide.util.NbBundle.getMessage(BasicConnectPanel.class, "BasicConnectPanel.lblConnectConfig.text")); // NOI18N

        cboConnectConfig_Version.setModel(new DefaultComboBoxModel(ClusterFactoryProvider.getRegisteredVersions().toArray()));
        cboConnectConfig_Version.setToolTipText(org.openide.util.NbBundle.getMessage(BasicConnectPanel.class, "BasicConnectPanel.cboConnectConfig_Version.toolTipText")); // NOI18N

        lblConnectConfig_Version.setLabelFor(cboConnectConfig_Version);
        org.openide.awt.Mnemonics.setLocalizedText(lblConnectConfig_Version, org.openide.util.NbBundle.getMessage(BasicConnectPanel.class, "BasicConnectPanel.lblConnectConfig_Version.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblConnectConfig_Version)
                    .addComponent(lblConnectConfig, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cboConnectConfig_SubType, 0, 399, Short.MAX_VALUE)
                    .addComponent(cboConnectConfig_Version, 0, 399, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboConnectConfig_Version, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblConnectConfig_Version))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboConnectConfig_SubType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblConnectConfig))
                .addContainerGap(288, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox cboConnectConfig_SubType;
    public javax.swing.JComboBox cboConnectConfig_Version;
    private javax.swing.JLabel lblConnectConfig;
    private javax.swing.JLabel lblConnectConfig_Version;
    // End of variables declaration//GEN-END:variables
}

