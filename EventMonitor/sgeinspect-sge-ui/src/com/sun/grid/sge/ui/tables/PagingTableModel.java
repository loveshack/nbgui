/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.tables;

import com.sun.grid.shared.ui.options.AppearanceOptionsPanelController;
import com.sun.tools.visualvm.core.datasource.DataSource;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.table.AbstractTableModel;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public abstract class PagingTableModel<D extends DataSource> extends AbstractTableModel {

    private int pageSize = 10;
    private int pageOffset = 0;
    private boolean all = false;
    private List<D> data = Collections.<D>emptyList();
    private DateFormat formatter;
    private Preferences pref = NbPreferences.forModule(AppearanceOptionsPanelController.class);

    public PagingTableModel(List<D> data) {
        pref.addPreferenceChangeListener(new PrefListener());
        formatter = DateFormat.getDateTimeInstance(pref.getInt(AppearanceOptionsPanelController.PROP_DATE,
              AppearanceOptionsPanelController.DATE_DEFAULT), pref.getInt(AppearanceOptionsPanelController.PROP_TIME,
              AppearanceOptionsPanelController.TIME_DEFAULT));
        runUpdate(data);
    }
    
    public DateFormat getDateFormat() {
        return formatter;
    }

    private class PrefListener implements PreferenceChangeListener {

        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt.getKey().equals(AppearanceOptionsPanelController.PROP_DATE) ||
                  evt.getKey().equals(AppearanceOptionsPanelController.PROP_TIME)) {
                formatter = DateFormat.getDateTimeInstance(pref.getInt(AppearanceOptionsPanelController.PROP_DATE,
                      AppearanceOptionsPanelController.DATE_DEFAULT), pref.getInt(AppearanceOptionsPanelController.PROP_TIME,
                      AppearanceOptionsPanelController.TIME_DEFAULT));
            }
        }
    }

    public int getRowCount() {
        return Math.min(pageSize, data.size() - (pageOffset * pageSize));
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        int realRow = rowIndex + (pageOffset * pageSize);
        if (realRow < data.size() && realRow >= 0) {
            D source = data.get(realRow);
            return getValueAt(colIndex, source);
        } else {
            return " ";
        }
    }

    //returns the current page number
    public int getPageOffset() {
        return pageOffset;
    }

    //returns total number of pages
    public int getPageCount() {
        return (int) Math.ceil((double) data.size() / pageSize);
    }

    public int getRealRowCount() {
        return data.size();
    }

    public void setOffset(int offset) {
        pageOffset = offset;
        fireTableDataChanged();
    }

    public void setNumberOfRows(String value) {
        if (value.equals(NbBundle.getMessage(PagingTableModel.class, "LBL_All"))) {
            all = true;
            setPageSize(data.size());
        } else {
            try {
                all = false;
                setPageSize(Integer.parseInt(value));
            } catch (NumberFormatException nfe) {
                all = false;
                setPageSize(data.size());
            }
        }
        fireTableDataChanged();
    }

    public int getPageSize() {
        return pageSize;
    }

    private void setPageSize(int size) {
        if (size == pageSize) {
            return;
        }

        int oldPageSize = pageSize;
        pageSize = size;
        if (pageSize != 0) {
            pageOffset = (oldPageSize * pageOffset) / pageSize;
        }
        fireTableDataChanged();
    }

    public void nextPage() {
        if (pageOffset < getPageCount() - 1) {
            pageOffset++;
        }
        fireTableDataChanged();
    }

    public void previousPage() {
        if (pageOffset > 0) {
            pageOffset--;
        }
        fireTableDataChanged();
    }

    public void runUpdate(final List<D> newData) {

        final List<D> copy = new ArrayList<D>(newData);

        int oldPageCount = getPageCount();
        if (oldPageCount == 0) {
            oldPageCount = (int) Math.ceil((double) copy.size() / pageSize);
        }
        this.data = copy;
        if (all) {
            setNumberOfRows(NbBundle.getMessage(PagingTableModel.class, "All"));
        }
        if (oldPageCount > getPageCount()) {
            pageOffset = Math.max(0, pageOffset - (oldPageCount - getPageCount()));
        } else if (oldPageCount < getPageCount()) {
            pageOffset = pageOffset + (getPageCount() - oldPageCount - 1);
        }
        fireTableDataChanged();
    }

    public abstract Object getValueAt(int colIndex, D source);
}
