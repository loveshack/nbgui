/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.sge.ui.dialog;

import com.sun.grid.sge.api.monitor.cluster.ClusterConfiguration;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

/**
 *
 */
public class DefaultClusterConfiguration extends ClusterConfiguration {

    private static final Logger logger = Logger.getLogger(DefaultClusterConfiguration.class.getName());
    private String userName = "<user>";
    private String userPassword = "";
    private String keystorePassword = "";
    private String keystorePath = ""; // NOI18N
    private String caPath = ""; // NOI18N
    private String jmxHost = "<dummyhost>"; // NOI18N
    private String jmxPort = "0";
    private boolean isSSL = true;

    public DefaultClusterConfiguration() {
        initFromPrefs();
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserPassword() {
        return userPassword;
    }

    @Override
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String getKeystorePassword() {
        return keystorePassword;
    }

    @Override
    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    @Override
    public String getKeystorePath() {
        return keystorePath;
    }

    @Override
    public void setKeystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
    }

    @Override
    public String getCaPath() {
        return caPath;
    }

    @Override
    public void setCaPath(String caPath) {
        this.caPath = caPath;
    }

    @Override
    public String getJmxHost() {
        return jmxHost;
    }

    @Override
    public void setJmxHost(String jmxHost) {
        this.jmxHost = jmxHost;
    }

    @Override
    public int getJmxPort() {
        return Integer.valueOf(jmxPort);
    }

    @Override
    public void setJmxPort(String jmxPort) {
        this.jmxPort = jmxPort;
    }

    @Override
    public void setJmxPort(int jmxPort) {
        this.jmxPort = Integer.toString(jmxPort);
    }

    @Override
    public boolean getSSL() {
        return isSSL;
    }

    @Override
    public void setSSL(boolean isSSL) {
        this.isSSL = isSSL;
    }

    ////////////////////////////////////////////////////////////////////////////
    // TODO: do cleanup here
    ////////////////////////////////////////////////////////////////////////////
    private static Properties propertiesFromFile(String fname)
            throws IOException {
        Properties p = new Properties();
        if (fname == null) {
            return p;
        }
        FileInputStream fin = new FileInputStream(fname);
        p.load(fin);
        fin.close();
        return p;
    }

    private static String getMasterHostFromFile(String actQmasterFile) {
        String masterHost = null;
        FileReader fr = null;
        try {
            fr = new FileReader(actQmasterFile);
            BufferedReader reader = new BufferedReader(fr);
            masterHost = reader.readLine();
        } catch (IOException ex) {
            // ignore
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    // ignore
                }
            }
        }
        return masterHost;
    }

    private void initFromPrefs() {

        String fs = System.getProperty("file.separator", "/");
        String sge_root = System.getenv("SGE_ROOT"); // NOI18N
        String sge_cell = System.getenv("SGE_CELL"); // NOI18N
        if (sge_cell == null) {
            sge_cell = "default"; // NOI18N
        }


        String sge_qmaster_port = System.getenv("SGE_QMASTER_PORT"); // NOI18N
        if (System.getenv("HOST") != null) {
            jmxHost = System.getenv("HOST"); // NOI18N
        }
        userName = System.getProperty("user.name", "<Enter user>");
        if (sge_root != null) {
            String sgeCommonDir = sge_root + fs + sge_cell + fs + "common" + fs; // NOI18N
            String masterHost = getMasterHostFromFile(sgeCommonDir + "act_qmaster"); // NOI18N
            if (masterHost != null) {
                jmxHost = masterHost;
            }
            try {
                jmxPort = propertiesFromFile(sgeCommonDir + "jmx" + fs + "management.properties").getProperty("com.sun.grid.jgdi.management.jmxremote.port"); //NOI18N
            } catch (IOException ex) {
                logger.warning(ex.getLocalizedMessage());
                // Exceptions.printStackTrace(ex);
            }
            caPath = sgeCommonDir + "sgeCA"; // NOI18N
            if (sge_qmaster_port == null) {
                keystorePath = fs + "var" + fs + "sgeCA" + fs + "sge_qmaster" + fs + sge_cell + fs + "userkeys" + fs + userName + fs + "keystore"; // NOI18N
            } else {
                keystorePath = fs + "var" + fs + "sgeCA" + fs + "port" + sge_qmaster_port + fs + sge_cell + fs + "userkeys" + fs + userName + fs + "keystore"; // NOI18N
            }
        } else {
            keystorePath = fs + "var" + fs + "sgeCA" + fs + "<portNNNN>" + fs + "<sge_cell>" + fs + "userkeys" + fs + userName + fs + "keystore"; // NOI18N
            caPath = fs + "<sge_root>" + fs + "<sge_cell>" + fs + "common" + fs + "sgeCA"; // NOI18N
        }
    }
}
