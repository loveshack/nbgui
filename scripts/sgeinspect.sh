#!/bin/sh
#
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# allow user to easily use other GUI versions
if [ "$SGEINSPECT_ROOT" != "" ]; then
   basedir=$SGEINSPECT_ROOT
elif [ -d $SGE_ROOT/sgeinspect ]; then
   basedir="$SGE_ROOT/sgeinspect"
else
   if [ -f $SGE_ROOT/$SGE_CELL/common/sgeinspect_root ]; then
      basedir=`cat $SGE_ROOT/$SGE_CELL/common/sgeinspect_root`
   else
      echo "Sorry - don't know where SGE Inspect is installed"
      exit 1
   fi
fi

# start GUI and pass all supported CLI options
# MYARGS="-J-Xms64m -J-Xmx256m -J-Dorg.netbeans.ProxyClassLoader.level=1000 -J-Djava.util.logging.config.file=$basedir/etc/logging.properties"
MYARGS="-J-Xms64m -J-Xmx256m -J-Dorg.netbeans.ProxyClassLoader.level=1000"

if [ "$JAVA_HOME" != "" ]; then
   MYARGS="$MYARGS --jdkhome $JAVA_HOME"
fi

$basedir/bin/sgeinspect $MYARGS $@
